
#ifndef __REGS_STM32_H__
#define __REGS_STM32_H__

/* GPIO cell. Use base+offset: the offset map is common to all devices */
#define REGOFF_GPIO_MODER	(0x000 / 4)
#define REGOFF_GPIO_OTYPER	(0x004 / 4)
#define REGOFF_GPIO_OSPEEDR	(0x008 / 4)
#define REGOFF_GPIO_PUPDR	(0x00c / 4)
#define REGOFF_GPIO_IDR		(0x010 / 4)
#define REGOFF_GPIO_ODR		(0x014 / 4)
#define REGOFF_GPIO_BSRR	(0x018 / 4)
#define REGOFF_GPIO_LCKR	(0x01c / 4)
#define REGOFF_GPIO_AFRL	(0x020 / 4)
#define REGOFF_GPIO_AFRH	(0x024 / 4)
#define REGOFF_GPIO_BRR		(0x028 / 4)

/*
 * UART registers. Again, base + offset.  The offsets are same in all
 * devices I consider (including LPUART), but F205, which has a
 * different map. Register bits are shared for the important ones, but
 * every device has some special bit added.
 */
#if !CONFIG_CPU_IS_F205
#define REGOFF_UART_CR1		(0x000 / 4)
#define REGOFF_UART_CR2		(0x004 / 4)
#define REGOFF_UART_CR3		(0x008 / 4)
#define REGOFF_UART_BRR		(0x00c / 4)
#define REGOFF_UART_GTPR	(0x010 / 4)
#define REGOFF_UART_RTOR	(0x014 / 4)
#define REGOFF_UART_RRQR	(0x018 / 4)
#define REGOFF_UART_ISR		(0x01c / 4)
#define REGOFF_UART_ICR		(0x020 / 4)
#define REGOFF_UART_RDR		(0x024 / 4)
#define REGOFF_UART_TDR		(0x028 / 4)
#else /* important bits are in the same place, name and address differs */
#define REGOFF_UART_ISR		(0x000 / 4) /* "SR" */
#define REGOFF_UART_ICR		-1 /* does not exist! */
#define REGOFF_UART_RDR		(0x004 / 4) /* "DR" - when reading */
#define REGOFF_UART_TDR		(0x004 / 4) /* "DR" - when writing */
#define REGOFF_UART_BRR		(0x008 / 4)
#define REGOFF_UART_CR1		(0x00c / 4)
#define REGOFF_UART_CR2		(0x010 / 4)
#define REGOFF_UART_CR3		(0x014 / 4)
#define REGOFF_UART_GTPR	(0x018 / 4)
#endif

/* Some bits are common to all STM32 families; name them */
#define BIT_UART_CR1_UE		0x0001 /* uart enable -- F205 lacks it!!! */
#define BIT_UART_CR1_TE		0x0008 /* transmit enable */
#define BIT_UART_CR1_RE		0x0004 /* receive enable */
#define BIT_UART_ISR_RXNE	0x0020 /* RX not empty */
#define BIT_UART_ISR_TXE	0x0080 /* TX register empty */
#define BIT_UART_ISR_TC		0x0040 /* transmission complete */
#define BIT_UART_ISR_ORE	0x0008 /* overrun error */

/* Support for extra serial ports */
struct uart {
	uint32_t baseaddr;
	uint32_t baudrate;
	uint32_t enable_reg;
	uint32_t enable_bit;
	int pin_tx, af_tx;
	int pin_rx, af_rx;
	int clock_mult, clock_div; /* lpuart1 e.g. needs 256*clock */
};
/* Everyone elects a console (default: usart2), uart = usart here */
extern const struct uart dev_uart2, *console;

/* Various devices have various uarts, applications can define them */
extern const struct uart dev_uart1, dev_uart3, dev_uart4;
extern const struct uart dev_uart5, dev_uart6, dev_lpuart1;


/*
 * Reset and clock control are all different among families, so
 * its registers are not defined here
 */


/*
 * Timers are like the UARTs : all of them are similar (but not lptimers).
 * Name common registers and bits, as we consider all of them equal
 */
#define REGOFF_TIM_CR1		(0x000 / 4)
#define REGOFF_TIM_CR2		(0x004 / 4)
#define REGOFF_TIM_SMCR		(0x008 / 4)
#define REGOFF_TIM_DIER		(0x00c / 4)
#define REGOFF_TIM_SR		(0x010 / 4)
#define REGOFF_TIM_EGR		(0x014 / 4)
#define REGOFF_TIM_CCMR1	(0x018 / 4)
#define REGOFF_TIM_CCMR2	(0x01c / 4)
#define REGOFF_TIM_CCER		(0x020 / 4)
#define REGOFF_TIM_CNT		(0x024 / 4)
#define REGOFF_TIM_PSC		(0x028 / 4)
#define REGOFF_TIM_ARR		(0x02c / 4)

/* Only bits we use are defined, because not all timers have all bits */
#define BIT_TIM_CR1_CEN		0x0001
#define BIT_TIM_EGR_UG		0x0001
#define BIT_TIM_SMCR_ECE	0x4000


/* NVIC -- the "1" registers only in cores with > 32 irq sources */
#define REG_NVIC_ISER0		(0xe000e100 / 4)
#define REG_NVIC_ISER1		(0xe000e104 / 4)
#define REG_NVIC_ICER0		(0xe000e180 / 4)
#define REG_NVIC_ICER1		(0xe000e184 / 4)
#define REG_NVIC_ISPR0		(0xe000e200 / 4)
#define REG_NVIC_ISPR1		(0xe000e204 / 4)
#define REG_NVIC_ICPR0		(0xe000e280 / 4)
#define REG_NVIC_ICPR1		(0xe000e284 / 4)
#define REG_NVIC_IABR0		(0xe000e300 / 4)
#define REG_NVIC_IABR1		(0xe000e304 / 4)
/* and we ignore the priority stuff */

#define __NVIC_INTERNAL_INTERRUPTS	16

#endif /* __REGS_STM32_H__ */
