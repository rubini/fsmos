#ifndef __REGS_LPC1_NEW_H__
#define __REGS_LPC1_NEW_H__
/*
 * 11xx and 13xx feature two generations. Older processors have
 * a 12 bit wide GPIO port, and their own USB slave logic cell.
 * Newer ones have a 32 bit wide GPIO port and a different USB slave.
 *
 * Then, uart0 vs. uart, ssp0 vs. ssp
 */
#include <regs-lpc1-common.h>

#define REGBASE_SPI0		(0x40040000 / 4)
#define REGBASE_SPI1		(0x40058000 / 4)

#define REG_FLASHTIM		(0x4003c010 / 4)

#define USBRAM_ADDR 0x20004000

/* the oversampling register is missing in old lpc1[13] devices */
#define REG_U0OSR		(0x4000802c / 4) /* oversampling */
#define REGOFF_UARTOSR		(0x02c / 4)

/* usb in new devices is different from older ones like 1343 and 1125 */
#define REG_DEVCMDSTAT		(0x40080000 / 4)
#define REG_DEVCMDSTAT_DEVADDR_MASK	     0x7f
#define REG_DEVCMDSTAT_DEVEN		(1 <<  7)
#define REG_DEVCMDSTAT_SETUP		(1 <<  8)
#define REG_DEVCMDSTAT_PLLON		(1 <<  9)
#define REG_DEVCMDSTAT_LPMSUP		(1 << 11)
#define REG_DEVCMDSTAT_INTNAK_CO	(1 << 14)
#define REG_DEVCMDSTAT_INTNAK_CI	(1 << 15)
#define REG_DEVCMDSTAT_DCON		(1 << 16)
#define REG_DEVCMDSTAT_DSUS		(1 << 17)
#define REG_DEVCMDSTAT_VBUS		(1 << 28)

#define REG_INFO		(0x40080004 / 4)
#define REG_INFO_FRAMENR_MASK		0x07ff
#define REG_INFO_ERRCODE_MASK		0x7800 /* 0 == noerror */
#define REG_INFO_ERRCODE_SHIFT		11

#define REG_EPLISTSTART		(0x40080008 / 4) /* 256-aligned */
#define REG_DATABUFSTART	(0x4008000c / 4) /* 4M-aligned */
#define REG_DATABUFSTART_MASK	 0xffc00000
#define REG_LPM			(0x40080010 / 4)
#define REG_EPSKIP		(0x40080014 / 4)
#define REG_EPINUSE		(0x40080018 / 4) /* for double-buffer */
#define REG_EPBUFCFG		(0x4008001c / 4) /* single/double buffer */
#define REG_INTSTAT		(0x40080020 / 4)
#define REG_INTEN		(0x40080024 / 4)
#define REG_INTSETSTAT		(0x40080028 / 4)
#define REG_INTROUTING		(0x4008002c / 4)
/* nothing at 0x30 */
#define REG_EPTOGGLE		(0x40080034 / 4)

/* bits for the eplist data words */
#define EPLIST_A		(1 << 31) /* active */
#define EPLIST_D		(1 << 30) /* disabled  */
#define EPLIST_S		(1 << 29) /* stall */
#define EPLIST_TR		(1 << 28) /* toggle reset */
#define EPLIST_TV		(1 << 27) /* toggle value; same as RF */
#define EPLIST_RF		(1 << 27) /* same as TV (rate-fb for int) */
#define EPLIST_T		(1 << 26) /* type 1 = isochronous */
#define EPLIST_NBYTES_MASK	0x03ff0000
#define EPLIST_NBYTES_SHIFT	16
#define EPLIST_OFFSET_MASK	0x0000ffff
#define EPLIST_OFFSET_SHIFT	0

/* Interrupts for new-version LPC11 and LPC13 */
enum lpc_irq {
	LPC_IRQ_PININT0		= 0,
	LPC_IRQ_PININT1		= 1,
	LPC_IRQ_PININT2		= 2,
	LPC_IRQ_PININT3		= 3,
	LPC_IRQ_PININT4		= 4,
	LPC_IRQ_PININT5		= 5,
	LPC_IRQ_PININT6		= 6,
	LPC_IRQ_PININT7		= 7,
	LPC_IRQ_GINT0		= 8,
	LPC_IRQ_GINT1		= 9,
	/* missing items */
	LPC_IRQ_SSP1		= 14,
	LPC_IRQ_I2C0		= 15,
	LPC_IRQ_CT16B0		= 16,
	LPC_IRQ_CT16B1		= 17,
	LPC_IRQ_CT32B0		= 18,
	LPC_IRQ_CT32B1		= 19,
	LPC_IRQ_SSP0		= 20,
	LPC_IRQ_USART0		= 21,
	LPC_IRQ_USB_IRQ		= 22,
	LPC_IRQ_USB_FIQ		= 23,
	LPC_IRQ_ADC		= 24,
	LPC_IRQ_WWDT		= 25,
	LPC_IRQ_BOD		= 26,
	LPC_IRQ_FLASH		= 27,
	/* missing items */
	LPC_IRQ_USB_WAKEUP	= 30,
	LPC_IRQ_USB_IOH		= 31,
	__NR_IRQS,
};

#endif /* __REGS_LPC1_NEW_H__ */
