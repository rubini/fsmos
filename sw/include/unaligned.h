#ifndef __UNALIGNED_H__
#define __UNALIGNED_H__

#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#error "Only little-endian is supported by now; please fix unaligned r/w"
#endif

/* I use w/l/ll convention like readl/writel */
static inline uint16_t unaligned_readw_le(void *ptr)
{
	uint8_t *p = ptr;
	return p[0] | ((uint16_t)p[1] << 8);
}
static inline uint16_t unaligned_readw_be(void *ptr)
{
	uint8_t *p = ptr;
	return p[1] | ((uint16_t)p[0] << 8);
}
static inline uint16_t unaligned_readw(void *ptr)
{
	if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
		return unaligned_readw_le(ptr);
	else
		return unaligned_readw_be(ptr);
}

static inline void unaligned_writew_le(void *ptr, uint16_t value)
{
	uint8_t *p = ptr;
	p[0] = value; p[1] = value >> 8;
}
static inline void unaligned_writew_be(void *ptr, uint16_t value)
{
	uint8_t *p = ptr;
	p[1] = value; p[0] = value >> 8;
}
static inline void unaligned_writew(void *ptr, uint16_t value)
{
	if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
		unaligned_writew_le(ptr, value);
	else
		unaligned_writew_be(ptr, value);
}

#endif /* __UNALIGNED_H__ */
