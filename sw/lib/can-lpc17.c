#include <io.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <can.h>

static int can_base[] = {REG_CAN1BASE, REG_CAN2BASE};

static void can_w(int devn, int reg, int val)
{
	if (devn > ARRAY_SIZE(can_base))
		return;

	if (reg < 0x4000)
		reg = can_base[devn] + reg;
	regs[reg] = val;
}

static uint32_t can_r(int devn, int reg)
{
	if (devn > ARRAY_SIZE(can_base))
		return -1;

	if (reg < 0x4000)
		reg = can_base[devn] + reg;
	return regs[reg];
}

struct can_dev *can_create(struct can_dev *dev)
{
	uint32_t reg;

	if (dev->devn != 0) {
		/* Not supported yet */
		return NULL;
	}
	gpio_dir_af(GPIO_NR(0, 0), GPIO_DIR_IN, 0, GPIO_AF(1));
	gpio_dir_af(GPIO_NR(0, 1), GPIO_DIR_OUT, 0, GPIO_AF(1));
	regs[REG_PCONP] |= REG_PCONP_PCCAN1;
	udelay(10);
	can_w(dev->devn, CANx_MOD, CANx_MOD_RM); /* reset, for configuration */

	/*
	 * Now, timing.
	 *
	 * Let's use 8 clocks per bit (250kb): 2MHz.
	 * Pclk is 1/4 cpu clock by default.
	 * Then, 1 is syn (fixed)c, try 4 for tseg1 and 3 for tseg2
	 */
	reg = ((CPU_FREQ / 4 / 250000 / 8) - 1) << CANx_BTR_BRP_SHIFT;
	reg |= 1 << CANx_BTR_SJW_SHIFT; /* not a bit timing */
	reg |= 3 << CANx_BTR_TSEG1_SHIFT;
	reg |= 2 << CANx_BTR_TSEG2_SHIFT;
	can_w(dev->devn, CANx_BTR, reg);

	can_w(dev->devn, CANx_MOD, 0); /* operational */
	/* And place acceptance filter in bypass */
	regs[REG_AFMR] = 0x02;

	return dev;
}

void can_destroy(struct can_dev *dev)
{
	regs[REG_PCONP] &= ~REG_PCONP_PCCAN1;
}

int can_send(struct can_dev *dev, uint32_t can_id, int len, void *data)
{
	return -EPERM;
}

int can_recv(struct can_dev *dev, uint32_t *can_id, int len, void *data)
{
	unsigned long sr, rfs, rid, rda, rdb;
	static unsigned long j;
	int dlc;
	uint32_t data32[2];

	if (!j)
		j = jiffies;

	sr = can_r(dev->devn, CANx_SR);
	rfs = can_r(dev->devn, CANx_RFS);
	rid = can_r(dev->devn, CANx_RID);
	rda = can_r(dev->devn, CANx_RDA);
	rdb = can_r(dev->devn, CANx_RDB);

	if (0 && time_after(jiffies, j)) {
		printf("SR: %08lx, RFS %08lx, RID %08lx, data %08lx %08lx\n",
		       sr, rfs, rid, rda, rdb);
		j += HZ;
	}
	if ((sr & CANx_SR_DOS)) {
		/* An overrun occurred. Report it, and clear it */
		can_w(dev->devn, CANx_CMR, CANx_CMR_CDO);
		return -ENOSPC;
	}
	if ((sr & CANx_SR_RBS) == 0)
		return -EAGAIN;

	/* Aha! */
	can_w(dev->devn, CANx_CMR, CANx_CMR_RRB); /* release receive buffer */
	dlc = (rfs & CANx_RFS_DLC_MASK) >> CANx_RFS_DLC_SHIFT;
	*can_id = (uint32_t)rid;
	/* data is little-endian, in two 32-bit values */
	data32[0] = rda;
	data32[1] = rdb;
	if (dlc > 8)
		dlc = 8;
	memcpy(data, data32, dlc);
	return dlc;
}
