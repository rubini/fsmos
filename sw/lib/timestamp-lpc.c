#include <stdint.h>
#include <timestamp-lpc.h>

/* Trivial: if somebody wants it in the symbol table, here it is */
void timestamp_get(struct timestamp *t)
{
	__timestamp_get(t);
}

void timestamp_complete(struct timestamp *t, uint32_t frac)
{
	__timestamp_complete(t, frac);
}

/* For the conversions, we depend on clocking information */
#define PRESCALER_MULT (CPU_FREQ / HZ) /* same as in timer setup */
#define NANOS_PER_SECOND_SCALED ((1LL * 1000 * 1000 * 1000) << 16)
#define NANOS_PER_CLOCK_SCALED (NANOS_PER_SECOND_SCALED / CPU_FREQ)

uint64_t timestamp_to_64(struct timestamp *t)
{
	return (uint64_t)t->counts * PRESCALER_MULT + t->fraction;
}

uint64_t timestamp_to_ns(struct timestamp *t)
{
	uint64_t ret = timestamp_to_64(t);

	return (ret * NANOS_PER_CLOCK_SCALED) >> 16;
}

uint64_t timestamp_get_64(void)
{
	struct timestamp t;
	timestamp_get(&t);
	return timestamp_to_64(&t);
}

uint64_t timestamp_get_ns(void)
{
	struct timestamp t;
	timestamp_get(&t);
	return timestamp_to_ns(&t);
}


