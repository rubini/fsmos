#include <io.h>
#include <time.h>
#include <i2c.h>
#include <mcp3428.h>
#include <command.h>
#include <sched-simple.h>
#include <panic.h>
#include <revision.h>

struct panic_ledinfo panic_ledinfo = {
	.gpio_clock = GPIO_NR(0, 7),
	.gpio_data = GPIO_NR(1, 28),
};

struct i2c_cfg i2c_cfg = {
	.gpio_sda = GPIO_NR(0,5),
	.gpio_scl = GPIO_NR(0,4),
	.flags = I2C_FLAG_HW_OPENDRAIN,
};

struct i2c_dev i2c_dev = {
	.cfg = &i2c_cfg,
}, *d;


struct mcp3428_dev test_3428 = {
	.i2c = &i2c_dev,
	.addr7 = 0x68,
	.ch = {
		{ .chflags = MCP3428_GAIN_1 | MCP3428_16B, },
		{ .chflags = MCP3428_GAIN_1 | MCP3428_16B, },
		{ .chflags = MCP3428_GAIN_1 | MCP3428_14B, },
		{ .chflags = MCP3428_GAIN_1 | MCP3428_12B, },
	},
};

struct task __task t_adc = {
	.version = SCHED_VERSION,
	.name = "adc",
	.init = mcp3428_init,
	.job = mcp3428_fsm,
	.arg = &test_3428,
	.period = HZ/10,
};

/* Model the shell as a task */
static struct command test_commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	{}
};

static void *job_shell(void *arg)
{
	static char str[80];

	if (polls(str, sizeof(str))) {
		command_parse(str, test_commands);
		puts(command_reply);
		str[0] = '\0';
	}
	return arg;
}

struct task __task task_shell = {
	.version = SCHED_VERSION,
	.name = "shell",
	.job = job_shell,
	.release = HZ,
	.period = jiffies_to_ms_const(10),
};

/* And every 5 seconds print all channels */
void *printthem(void *arg)
{
	struct mcp3428_dev *dev = arg;
	struct mcp3428_channel *ch;
	int i;

	for (i = 0; i < 4; i++) {
		ch = dev->ch + i;
		printf("ch %p %i(%02x): %4li jiffies ago: %04x = %9i uV\n", ch,
		       i, ch->chflags, jiffies - ch->tstamp, ch->raw & 0xffff,
		       (int)ch->chvalue);
	}
	printf("\n");
	return arg;
}

struct task __task task_dump = {
	.version = SCHED_VERSION,
	.name = "dump",
	.job = printthem,
	.arg = &test_3428,
	.release = HZ * 5,
	.period = HZ * 5,
};


void main(void)
{
	printf("%s, commit %s\n", __FILE__, __gitc__);
	d = i2c_create(&i2c_dev);
	if (!d)
		panic(16, "Can't create i2c\n");

	sched_simple();
}

