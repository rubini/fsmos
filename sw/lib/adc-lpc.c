#include <io.h>
#include <time.h>
#include <errno.h>
#include <adc.h>

/* Unfortunately, 112x is slightly different */
#ifdef CONFIG_LPC112x
#  define CONFIG_IS_LPC112x 1
#else
#  define CONFIG_IS_LPC112x 0
#endif


#define ADC_DEFAULT_TIMEOUT HZ

struct ch_gpio {
	int afnum, gpio;
};

static const struct ch_gpio adc_gpio_list_generic[ADC_MAX_CHANNELS] = {
	[0] = {2, GPIO_NR(0, 11)}, /* pin 32 */
	#ifdef CONFIG_LPC1 /* "new" gpio logic cell */
	[1] = {2, GPIO_NR(0, 12)}, /* pin 33 */
	[2] = {2, GPIO_NR(0, 13)}, /* pin 34 */
	[3] = {2, GPIO_NR(0, 14)}, /* pin 35 */
	[4] = {2, GPIO_NR(0, 15)}, /* pin 39 */
	[5] = {1, GPIO_NR(0, 16)}, /* pin 40 */
	[6] = {1, GPIO_NR(0, 22)}, /* pin 30 */
	[7] = {1, GPIO_NR(0, 23)}, /* pin 42 */
	#elif defined(CONFIG_LPC1_OLD) /* old gpio mask, same pins */
	[1] = {2, GPIO_NR(1,  0)}, /* pin 33 */
	[2] = {2, GPIO_NR(1,  1)}, /* pin 34 */
	[3] = {2, GPIO_NR(1,  2)}, /* pin 35 */
	[4] = {2, GPIO_NR(1,  3)}, /* pin 39 */
	[5] = {1, GPIO_NR(1,  4)}, /* pin 40 */
	[6] = {1, GPIO_NR(1, 10)}, /* pin 30 */
	[7] = {1, GPIO_NR(1, 11)}, /* pin 42 */
	#else
	#error "Unsupported CPU"
	#endif
};

static const struct ch_gpio adc_gpio_list_112x[ADC_MAX_CHANNELS] = {
	[1] = {1, GPIO_NR(1, 11)}, /* pin 42 */
	[2] = {1, GPIO_NR(1,  4)}, /* pin 39 */
	[3] = {2, GPIO_NR(1,  3)}, /* pin 38 */
	[4] = {2, GPIO_NR(1,  2)}, /* pin 35 */
	[5] = {2, GPIO_NR(1,  1)}, /* pin 32 */
	[6] = {2, GPIO_NR(1,  0)}, /* pin 31 */
	[7] = {2, GPIO_NR(0, 11)}, /* pin 30 */
	[8] = {1, GPIO_NR(1, 10)}, /* pin 29 */
};

struct adc_dev *adc_create(struct adc_dev *d)
{
	const struct ch_gpio *ch;
	int i;

	if (!d->nbits)
		d->nbits = 12;
	if (d->nbits) {
		if (d->nbits != 12 && d->nbits != 10)
			return NULL;
	}
	/* turn on */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_ADC;
	regs[REG_PDRUNCFG] &= ~REG_PDRUNCFG_ADC;

	/* configure gpio pins */
	if (CONFIG_IS_LPC112x)
		ch = adc_gpio_list_112x;
	else
		ch = adc_gpio_list_generic;

	for (i = 0; i < ADC_MAX_CHANNELS; i++, ch++) {
		if (d->chmask & (1 << i)) {
			gpio_dir_af(ch->gpio, GPIO_DIR_IN, 0,
				    ch->afnum | GPIO_AF_ANALOG);
		}
		d->chvalue[i] = 0;
		d->chflags[i] = 0;
	}

	if (!CONFIG_IS_LPC112x) {
		/* save the clock divisor into driverdata, to run at 12MHz */
		d->driverdata = CONFIG_PLL_CPU << REG_ADCCR_CLKDIV_SHIFT;
		regs[REG_ADCCR] = d->driverdata; /* prepare clock right now */
	} else {
		unsigned long to = jiffies + HZ/10;
		/* Force autocalibration */
		regs[REG_ADCCR] = REG_ADCCR_CALMODE | 0x1; /* clkdiv */
		while (time_before(jiffies, to))
			if ((regs[REG_ADCCR] & REG_ADCCR_CALMODE) == 0)
				break;
		if (time_after_eq(jiffies, to))
			return NULL;
	}
	return d;
}

void adc_destroy(struct adc_dev *d)
{
	const struct ch_gpio *ch;
	int i;

	if (CONFIG_IS_LPC112x)
		ch = adc_gpio_list_112x;
	else
		ch = adc_gpio_list_generic;

	/* Reset gpio pins to default */
	for (i = 0; i < ADC_MAX_CHANNELS; i++, ch++) {
		if (d->chmask & (1 << i)) {
			gpio_dir_af(ch->gpio, GPIO_DIR_IN, 0,
				    GPIO_AF_GPIO | GPIO_AF_PULLUP);
		}
	}
	/* Turn off */
	regs[REG_PDRUNCFG] |= REG_PDRUNCFG_ADC;
	regs[REG_AHBCLKCTRL] &= ~REG_AHBCLKCTRL_ADC;
}

/* generic implementation, for most LPC microcontroller models */
static int __adc_get_channel_to(struct adc_dev *dev, int ch, unsigned long to)
{
	unsigned long j = jiffies + to;
	uint32_t datareg;

	regs[REG_ADCCR] = dev->driverdata
		| ((1 << ch) << REG_ADCCR_SELMASK_SHIFT)
		| REG_ADCCR_START_NOW;
	do {
		datareg = regs[REG_ADCGDR];
		if (datareg & REG_ADCDR_DONE)
			break;
	} while (time_before(jiffies, j));

	if (!(datareg & REG_ADCDR_DONE))
		return -EAGAIN;

	dev->chflags[ch] |= ADC_FLAG_DONE;
	if (datareg & REG_ADCDR_OVERRUN)
		dev->chflags[ch] |= ADC_FLAG_OVERRUN;
	dev->chvalue[ch] = datareg & REG_ADCDR_DATA;
	return dev->chvalue[ch];
}

/* 112x is different, and this is not optimized: I make SEQ_A with 1ch */
static int __adc_get_channel_to_112x(struct adc_dev *dev, int ch,
				     unsigned long to)
{
	unsigned long j = jiffies + to;
	uint32_t datareg;

	regs[REG_ADCCR] = 1; /* clkdiv */
	regs[REG_ADCSEQA] = (1 << ch) | REG_ADCSEQA_START | REG_ADCSEQA_ENA;
	do {
		datareg = regs[REG_ADCDATx(ch)];
		if (datareg & REG_ADCDAT_DONE)
			break;
	} while (time_before(jiffies, j));

	if (!(datareg & REG_ADCDR_DONE))
		return -EAGAIN;

	dev->chflags[ch] |= ADC_FLAG_DONE;
	if (datareg & REG_ADCDR_OVERRUN)
		dev->chflags[ch] |= ADC_FLAG_OVERRUN;
	dev->chvalue[ch] = datareg & REG_ADCDR_DATA;
	return dev->chvalue[ch];
}

int adc_get_channel_to(struct adc_dev *dev, int ch, unsigned long to)
{
	if (CONFIG_IS_LPC112x)
		return __adc_get_channel_to_112x(dev, ch, to);
	else
		return __adc_get_channel_to(dev, ch, to);
}


/* based on the above one, with 1 minute timeout, to avoid code duplication */
int adc_get_channel(struct adc_dev *dev, int ch)
{
	return adc_get_channel_to(dev, ch, ADC_DEFAULT_TIMEOUT);
}

/* the two below, on the contrary, duplicate code */
int adc_acquire_to(struct adc_dev *dev, int chmask, unsigned long to)
{
	int i, bit, res, err = 0;

	for (i = 0, bit = 1; i < ADC_MAX_CHANNELS; i++, bit <<= 1) {
		if (!(chmask & bit))
			continue;
		res = adc_get_channel_to(dev, i, to);
		if (!err && res < 0) {
			/* preserve the last error, if no error return 0 */
			err = res;
		}
	}
	return err;
}

int adc_acquire(struct adc_dev *dev, int chmask, unsigned long flags)
{
	return adc_acquire_to(dev, chmask, ADC_DEFAULT_TIMEOUT);
}
