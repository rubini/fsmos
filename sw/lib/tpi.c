/*
 *  Atmel "Tiny Programmer Interface" for attiny
 */
#include <errno.h>
#include <time.h>
#include <string.h>
#include <io.h>
#include <tpi.h>

#define REG_NVMCMD		0x33
#define NVMCMD_CHIPERASE		0x10
#define NVMCMD_SECTERASE		0x14
#define NVMCMD_WORDWRITE		0x1d
#define REG_NVMCSR		0x32
#define REG_NVMCSR_BUSY			0x80

/* The data lines may be shared or not. Manage the various cases */
static void tpi_output_mode(struct tpi_hw *hw)
{
	int idle = !(hw->data_out.flags & TPI_FLAG_INVERT);

	gpio_dir(hw->data_out.gpio, GPIO_DIR_OUT, idle);
}

static void tpi_input_mode(struct tpi_hw *hw)
{
	int recessive = !(hw->data_out.flags & TPI_FLAG_RECESSIVE_IS_0);

	gpio_dir(hw->data_in.gpio, GPIO_DIR_IN, 1);
	/* if data out != data in, put out in recessive level */
	if (hw->data_in.gpio != hw->data_out.gpio)
		gpio_set(hw->data_out.gpio, recessive);
}

static void tpi_do_reset(struct tpi_hw *hw, int on)
{
	if (on) {
		gpio_dir_af(hw->reset.gpio, GPIO_DIR_OUT,
		    !!(hw->reset.flags & TPI_FLAG_INVERT), GPIO_AF_GPIO);
		return;
	}
	/* off: if pullup turn as input */
	if (hw->reset.flags & TPI_FLAG_PULLUP)
		gpio_dir_af(hw->reset.gpio, GPIO_DIR_IN, 1, GPIO_AF_GPIO);
	else
		gpio_dir_af(hw->reset.gpio, GPIO_DIR_OUT,
			    !(hw->reset.flags & TPI_FLAG_INVERT), GPIO_AF_GPIO);
}

/* bit operations -- caller must set mode in advance, clk assumed high */
static void tpi_output_bit(struct tpi_hw *hw, int bit)
{
	gpio_set(hw->clock.gpio, hw->clock.flags & TPI_FLAG_INVERT);
	gpio_set(hw->data_out.gpio,
		 (bit & 0x01) ^ (hw->clock.flags & TPI_FLAG_INVERT));
	udelay(2);
	gpio_set(hw->clock.gpio, !(hw->clock.flags & TPI_FLAG_INVERT));
	udelay(2);
}
static int tpi_input_bit(struct tpi_hw *hw)
{
	int ret;

	gpio_set(hw->clock.gpio, hw->clock.flags & TPI_FLAG_INVERT);
	udelay(2);
	ret = gpio_get(hw->data_in.gpio)
		^ (hw->data_in.flags & TPI_FLAG_INVERT);
	gpio_set(hw->clock.gpio, !(hw->clock.flags & TPI_FLAG_INVERT));
	udelay(2);
	return ret;
}

/* byte operations, setting mode internally */
static void tpi_output_byte(struct tpi_hw *hw, int byte)
{
	int i, parity = 1; /* we have an idle bit at the beginning... */

	tpi_output_mode(hw);

	/* LSB first, with idle (1), start bit (0), parity, two stop bits */
	byte = ((byte & 0xff) << 2) | 0x01 | (3 << 11);
	for (i = 0; i < 13; i++) {
		tpi_output_bit(hw, byte);
		parity ^= (byte & 0x01);
		byte >>= 1;
		if (i == 9)
			byte |= parity;
	}
}

static int tpi_input_byte(struct tpi_hw *hw)
{
	int ret = 0, i;

	tpi_input_mode(hw);

	/* wait for start bit: default guard time is 128 bits */
	for (i = 150; i; i--) {
		if (tpi_input_bit(hw) == 0)
			break;
	}
	if (i == 0)
		return -ETIMEDOUT;
	for (i = 0; i < 8; i++) {
		ret |= tpi_input_bit(hw) << i;

	}
	/* ignore parity and two stop bits */
	tpi_input_bit(hw);
	tpi_input_bit(hw);
	tpi_input_bit(hw);
	return ret;
}

/* higher level */
static void tpi_write_pr(struct tpi_hw *hw, int address)
{
	tpi_output_byte(hw, TPI_PROTO_SSTPR(0));
	tpi_output_byte(hw, address & 0xff);
	tpi_output_byte(hw, TPI_PROTO_SSTPR(1));
	tpi_output_byte(hw, address >> 8);
}

static int tpi_read_data(struct tpi_hw *hw, struct tpi_data *data)
{
	int i, j;

	tpi_write_pr(hw, data->address);
	for (i = 0; i < data->len; i++) {
		tpi_output_byte(hw, TPI_PROTO_SLDI);
		j = tpi_input_byte(hw);
		if (j < 0)
			return i ?: j;
		data->data[i] = j;
	}
	return data->len;
}

static int tpi_wait_nvmbusy(struct tpi_hw *hw, int loops)
{
	int csr, timeout;
	for (timeout = loops; timeout; timeout--) {
		tpi_output_byte(hw, TPI_PROTO_SIN(REG_NVMCSR));
		csr = tpi_input_byte(hw);
		if (csr < 0)
			return csr;
		if ((csr & REG_NVMCSR_BUSY) == 0)
			break;
	}
	return timeout ?: -ETIMEDOUT;
}

static int tpi_write_flash(struct tpi_hw *hw, struct tpi_data *data)
{
	int i, err, ini = data->address;

	if ((data->address & 1) || (data->len & 1))
		return -EINVAL;
	for (i = 0; i < data->len; i += 2) {
		tpi_output_byte(hw, TPI_PROTO_SOUT(REG_NVMCMD));
		tpi_output_byte(hw, NVMCMD_WORDWRITE);
		tpi_write_pr(hw, ini + i);
		tpi_output_byte(hw, TPI_PROTO_SSTI);
		tpi_output_byte(hw, data->data[i]);
		tpi_output_byte(hw, TPI_PROTO_SSTI);
		tpi_output_byte(hw, data->data[i+1]);

		/* wait for busy to be clean (usually it takes 22-24 loops) */
		err = tpi_wait_nvmbusy(hw, 100);
		if (err < 0)
			return err;
	}
	return data->len;
}

static int tpi_write_lock(struct tpi_hw *hw, struct tpi_data *data)
{
	int err;

	tpi_output_byte(hw, TPI_PROTO_SOUT(REG_NVMCMD));
	tpi_output_byte(hw, NVMCMD_WORDWRITE);
	tpi_write_pr(hw, TPI_ADDR_LOCK);
	tpi_output_byte(hw, TPI_PROTO_SSTI);
	tpi_output_byte(hw, data->data[0]);
	tpi_output_byte(hw, TPI_PROTO_SSTI);
	tpi_output_byte(hw, 0xff); /* dummy byte */

	/* wait for busy to be clean (usually it takes 10 loops) */
	err = tpi_wait_nvmbusy(hw, 40);
	if (err < 0)
		return err;
	return 0;
}

static int tpi_write_data(struct tpi_hw *hw, struct tpi_data *data)
{
	int i;

	if (data->address >= TPI_ADDR_FLASH)
		return tpi_write_flash(hw, data);
	if (data->address == TPI_ADDR_LOCK)
		return tpi_write_lock(hw, data);

	tpi_write_pr(hw, data->address);
	for (i = 0; i < data->len; i++) {
		tpi_output_byte(hw, TPI_PROTO_SSTI);
		tpi_output_byte(hw, data->data[i]);
	}
	return data->len;
}


/*
 * commands follow
 */
static int tpi_init(struct tpi_hw *hw, struct tpi_data *data)
{
	static const uint8_t key[]= {
		0xff, 0x88, 0xd8, 0xcd, 0x45, 0xab, 0x89, 0x12};
	int i;

	gpio_dir_af(hw->reset.gpio, GPIO_DIR_OUT,
		    !(hw->reset.flags & TPI_FLAG_INVERT), GPIO_AF_GPIO);
	gpio_dir_af(hw->clock.gpio, GPIO_DIR_OUT,
		    !(hw->clock.flags & TPI_FLAG_INVERT), GPIO_AF_GPIO);
	/* Ensure the alternate function is toggled to gpio mode */
	gpio_dir_af(hw->data_out.gpio, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(hw->data_in.gpio, GPIO_DIR_IN, 0, GPIO_AF_GPIO);
	/* Then deal with the special cases of shared/recessive etc */
	tpi_input_mode(hw);
	tpi_output_mode(hw);

	/* Now force reset low and make the 16 pulses (or more) */
	tpi_do_reset(hw, 1);
	udelay(5);

	for (i = 0; i < 20; i++)
		tpi_output_bit(hw, 1);

	/* Read the "identification code" that must be 80 */
	udelay(10);
	tpi_output_byte(hw, TPI_PROTO_SLDCS(0xf));
	if (tpi_input_byte(hw) != 0x80)
		return -ENODEV;

	/* shorten the direction change idle pulses */
	tpi_output_byte(hw, TPI_PROTO_SSTCS(0x2));
	tpi_output_byte(hw, 0x07);

	/* Read ID again so I check on the scope that it worked */
	udelay(10);
	tpi_output_byte(hw, TPI_PROTO_SLDCS(0xf));
	if (tpi_input_byte(hw) != 0x80)
		return -ENODEV;

	/* Enable access to NVRAM, and poll the bit until it is set */
	tpi_output_byte(hw, TPI_PROTO_SKEY);
	for (i = 0; i < ARRAY_SIZE(key); i++)
		tpi_output_byte(hw, key[i]);
	for (i = 0; i < 10; i++) {
		tpi_output_byte(hw, TPI_PROTO_SLDCS(0x0));
		if ((tpi_input_byte(hw) & 0x02) == 0x02)
			break;
	}
	if (i == 10)
		return -ETIMEDOUT;


	/* And finally read the device ID (addr 3f c0) */
	data->address = TPI_ADDR_DEVID;
	data->len = 3;
	return tpi_read_data(hw, data);
}

static int tpi_fini(struct tpi_hw *hw, struct tpi_data *data)
{
	int i;

	/* Clear NMV enable, put lines in input mode and raise reset */
	tpi_output_byte(hw, TPI_PROTO_SSTCS(0x0));
	tpi_output_byte(hw, 0);

	gpio_dir_af(hw->clock.gpio, GPIO_DIR_IN, 0,
		    GPIO_AF_GPIO | GPIO_AF_PULLUP);
	gpio_dir_af(hw->data_in.gpio, GPIO_DIR_IN, 0,
		    GPIO_AF_GPIO | GPIO_AF_PULLUP);
	gpio_dir_af(hw->data_out.gpio, GPIO_DIR_IN, 0,
		    GPIO_AF_GPIO | GPIO_AF_PULLUP);

	tpi_do_reset(hw, 0);
	/* This is not enough, it seem: pulse reset again */
	for (i = 1; i >= 0; i--) {
		udelay(500);
		tpi_do_reset(hw, i);
	}
	return 0;
}

static int tpi_erase(struct tpi_hw *hw, struct tpi_data *data)
{
	int csr, timeout;

	tpi_output_byte(hw, TPI_PROTO_SOUT(REG_NVMCMD));
	tpi_output_byte(hw, NVMCMD_CHIPERASE);
	tpi_write_pr(hw, TPI_ADDR_FLASH + 1); /* "high byte" */
	tpi_output_byte(hw, TPI_PROTO_SSTI);
	tpi_output_byte(hw, 0xff);

	/* wait for busy to be clean (usually it takes 70 loops) */
	for (timeout = 500; timeout; timeout--) {
		tpi_output_byte(hw, TPI_PROTO_SIN(REG_NVMCSR));
		csr = tpi_input_byte(hw);
		if (csr < 0)
			return csr;
		if ((csr & REG_NVMCSR_BUSY) == 0)
			break;
	}
	return timeout ? 0 : -ETIMEDOUT;
}

static int (*tpi_commands[])(struct tpi_hw *hw, struct tpi_data *data) = {
	[TPI_COMMAND_INIT] = tpi_init,
	[TPI_COMMAND_FINI] = tpi_fini,
	[TPI_COMMAND_LOAD] = tpi_read_data,
	[TPI_COMMAND_STORE] = tpi_write_data,
	[TPI_COMMAND_CHIPERASE] = tpi_erase,
};

int tpi_action(struct tpi_hw *hw, enum tpi_command cmd, struct tpi_data *data)
{
	if (cmd >= ARRAY_SIZE(tpi_commands))
		return -ENOENT;
	return tpi_commands[cmd](hw, data);
}

static struct tpi_data __tpi_data;

static int __tpi_init(struct tpi_hw *hw)
{
	int i, err;

	for (i = 0; i < 3; i++) {
		err = tpi_init(hw, &__tpi_data);
		if (err >= 0)
			break;
	}
	if (i == 3)
		return err;
	return 0;
}

static int __tpi_fini(struct tpi_hw *hw)
{
	return tpi_fini(hw, &__tpi_data);
}

int tpi_check_binary(struct tpi_hw *hw, int flags, const void *_code,
		     int codesize)
{
	int i, j, err = 0, ret;
	const uint8_t *code = _code;

	if (!(flags & TPI_NO_INIT))
		__tpi_init(hw);

	for (i = 0; i < codesize; i+=16) {
		__tpi_data.len = 16;
		if (codesize - i < 16)
			__tpi_data.len = codesize - i;
		__tpi_data.address = TPI_ADDR_FLASH + i;
		memcpy(__tpi_data.data, code + i, __tpi_data.len);
		ret = tpi_read_data(hw, &__tpi_data);
		if (ret != __tpi_data.len) {
			err = -ETIMEDOUT;
			break;
		}
		for (j = 0; j < __tpi_data.len; j++)
			if (__tpi_data.data[j] !=  code[i + j])
				err++;
	}

	if (!(flags & TPI_NO_FINI))
		__tpi_fini(hw);

	return err;
}

int tpi_write_binary(struct tpi_hw *hw, int flags, const void *_code,
		     int codesize)
{
	int i, err = 0, ret;
	const uint8_t *code = _code;

	if (!(flags & TPI_NO_INIT))
		__tpi_init(hw);

	tpi_erase(hw, &__tpi_data);
	for (i = 0; i < codesize; i+=16) {
		__tpi_data.len = 16;
		if (codesize - i < 16)
			__tpi_data.len = codesize - i;
		__tpi_data.address = TPI_ADDR_FLASH + i;
		memcpy(__tpi_data.data, code + i, __tpi_data.len);
		ret = tpi_write_data(hw, &__tpi_data);
		//printf("%s: len %i ret %i\n", __func__, __tpi_data.len, ret);
		if (ret != __tpi_data.len) {
			err = -ETIMEDOUT;
			break;
		}
	}

	if (!(flags & TPI_NO_FINI))
		__tpi_fini(hw);

	return err;

}
