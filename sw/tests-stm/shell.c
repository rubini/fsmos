#include <io.h>
#include <time.h>
#include <string.h>
#include <command.h>
#include <errno.h>

static int command_go(char **reply, int argc, char **argv)
{
	int addr;
	char c;
	void (*f)(void);

	if (sscanf(argv[1], "%x%c", &addr, &c) != 1)
		return -EINVAL;
	f = (void *)addr;
	printf("going to %p\n", f);
	f();
	return 0;
}

static struct command rwgo_commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	{"go",    command_go,      2, 2},
	{}
};

void main(void)
{
	static char str[80];
	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);
	while (1) {

		if (polls(str, sizeof(str))) {
			if (str[strlen(str)-1] == '\n')
				str[strlen(str)-1] = '\0';
			if (str[strlen(str)-1] == '\r')
				str[strlen(str)-1] = '\0';
			command_parse(str, rwgo_commands);
			puts(command_reply);
			str[0] = '\0';
		}
	}
}
