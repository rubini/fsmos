#include <init.h>
#include <time.h>
#include <board.h>

uint32_t __attribute__((weak)) board_get_clksel(void)
{
	return REG_SYSPLLCLKSEL_OSC; /* use oscillator, by default */
}

#if XTAL_FREQ != 12 * 1000 * 1000
#error "This code only works with 12MHz xtals"
#endif

/*
 * Unfortunately (well...) LPC1347 does not feature REG_*CLKUEN, which
 * exists in both LPC1 and LPC1_OLD (besides this 13xx series).
 */
#ifdef CONFIG_LPC1347
#define CONFIG_HAS_CLKUEN 0
#else
#define CONFIG_HAS_CLKUEN 1
#endif


/* Program CPU PLL and USB PLL */
static int pll_init(void)
{
	int m, msel, p, psel;
	int fcco_min = 156 * 1000 * 1000;
	int fcco_max = 320 * 1000 * 1000;
	int fout;

	/* First, write flash wait states */
	if (CONFIG_CPU_IS_LPC17) {
		; /* This is still in boot.S: please fix */
	} else {
		int lowbits = (CPU_FREQ - 1) / (20 * 1000 * 1000);
		if (lowbits > 2) lowbits = 2;
		regs[REG_FLASHTIM] = (regs[REG_FLASHTIM] & ~3) | lowbits;
	}
	if (CONFIG_PLL_CPU == 1)
		return 0;

	fout = CPU_FREQ; /* xtal * PLL_CPU */
	m = CONFIG_PLL_CPU;
	msel = m - 1;

	/* find value for P */
	for (p = 2, psel = 0; p < 16; p *= 2, psel++) {
		if (fout * p < fcco_min)
			continue;
		if (fout * p > fcco_max)
			continue; /* can't happen, I hope */
		break;
	}

	/* power up oscillator */
	regs[REG_PDRUNCFG] &= ~REG_PDRUNCFG_SYSOSC;

	/* setting the system oscillator: board-dependent (xtal o clkin) */
	regs[REG_SYSOSCCTRL] = BOARD_SYSOSCCTRL;

	 /* a little wait */
	{ volatile int i; for (i=0; i<300; i++);}

	regs[REG_SYSPLLCLKSEL] = board_get_clksel();
	if (CONFIG_HAS_CLKUEN) {
		/* 1347 does *not* have REG_SYSPLLCLKUEN, but 1343 does! */
		regs[REG_SYSPLLCLKUEN] = 0;
		regs[REG_SYSPLLCLKUEN] = 1;
		while((regs[REG_SYSPLLCLKUEN] & 1) == 0)
			;
	}
	regs[REG_SYSPLLCTRL] = (psel << 5) | msel;

	/* power up pll, which is down by default */
	regs[REG_PDRUNCFG] &= ~REG_PDRUNCFG_SYSPLL;

	while (regs[REG_SYSPLLSTAT] == 0) /* wait for the pll lock */
		;

	regs[REG_MAINCLKSEL] = 3 /* SYSPLLOUT */;
	if (CONFIG_HAS_CLKUEN) {
		/* 1347 does *not* have REG_MAINCLKUEN, but 1343 does! */
		regs[REG_MAINCLKUEN] = 0;
		regs[REG_MAINCLKUEN] = 1;
		while((regs[REG_MAINCLKUEN] & 1) == 0)
			;
	}
	if (!CONFIG_HAS_USB)
		return 0;
	if (board_get_clksel() == REG_SYSPLLCLKSEL_IRC) {
		/* no external oscillator: no usb */
		return 0;
	}

	/*
	 * USB PLL, similar to above
	 */
	regs[REG_SYSAHBCLKCTRL] |=
		REG_AHBCLKCTRL_USBREG | REG_AHBCLKCTRL_USBRAM;


	/* power down pll and pads before changing divisors */
	regs[REG_PDRUNCFG] |=
		(REG_PDRUNCFG_USBPLL | REG_PDRUNCFG_USBPAD);

	regs[REG_USBPLLCLKSEL] = 1; /* SYSOSC */
	if (CONFIG_HAS_CLKUEN) {
		regs[REG_USBPLLCLKUEN] = 0;
		regs[REG_USBPLLCLKUEN] = 1;
		while((regs[REG_USBPLLCLKUEN] & 1) == 0)
			;
	}
	psel = 1; msel = 3; /* 2p = 4:    12 -> 192 -> 48 */
	regs[REG_USBPLLCTRL] = (psel << 5) | msel;

	/* power up pll and pads */
	regs[REG_PDRUNCFG] &=
		~(REG_PDRUNCFG_USBPLL | REG_PDRUNCFG_USBPAD);

	while (regs[REG_USBPLLSTAT] == 0) /* wait for the pll lock */
		;

	regs[REG_USBCLKDIV] = 1;
	regs[REG_USBCLKSEL] = 0; /* PLL */
	udelay(100 * 1000);
	regs[REG_USBCLKUEN] = 0;
	regs[REG_USBCLKUEN] = 1;
	udelay(100 * 1000);
	return 0;
}


core_initcall(pll_init);

unsigned char pll_hook;
