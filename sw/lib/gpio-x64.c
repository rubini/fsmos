#include <io.h>

void gpio_init(void)
{}

int gpio_dir_af(int gpio, int output, int value, int afnum)
{
	if (CONFIG_GPIO_IS_VERBOSE)
		printf("GPIO: %s(%i, %i, %i, %i)\n", __func__,
		       gpio, output, value, afnum);
	return 0;
}

void gpio_dir(int gpio, int output, int value)
{
	if (CONFIG_GPIO_IS_VERBOSE)
		printf("GPIO: %s(%i, %i, %i)\n", __func__,
		       gpio, output, value);
}

int gpio_get(int gpio)
{
	if (CONFIG_GPIO_IS_VERBOSE)
		printf("GPIO: %s(%i)\n", __func__, gpio);
	return 0;
}

uint32_t __gpio_get(int gpio)
{
	return gpio_get(gpio);
}

void gpio_set(int gpio, int value)
{
	if (CONFIG_GPIO_IS_VERBOSE)
		printf("GPIO: %s(%i, %i)\n", __func__, gpio, value);
}

void __gpio_set(int gpio, uint32_t value)
{
	gpio_set(gpio, value);
}
