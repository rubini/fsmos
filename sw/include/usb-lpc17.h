#ifndef __USB_17xx_H__
#define __USB_17xx_H__

#ifndef __USB_INT_H__
#  error "Please include <usb-int.h> not the hw-specific header"
#endif

struct usb_hw_device {
	unsigned long cfg_timeout;
	int tout_pending;
};

#endif /* __USB_17xx_H__ */
