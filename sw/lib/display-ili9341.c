/*
 * This is used for example in arietta-xterm by acme
 */
#include <io.h>
#include <errno.h>
#include <string.h>
#include <spi.h>
#include <time.h>
#include <net.h>
#include <display-ili9341.h>

#define SCREEN_WIDTH  240
#define SCREEN_HEIGHT 320

/* Some display commands */
#define CMD_DISPOFF 0x28
#define CMD_RAMWR 0x2c
#define CMD_CAS   0x2a
#define CMD_PAS   0x2b

#define DELAY 0
#define ONE_BYTE_SEQUENCE 1
#define TWO_BYTES_SEQUENCE 2
#define THREE_BYTES_SEQUENCE 3
#define FOUR_BYTES_SEQUENCE 4
#define MULTIPLE_BYTES_SEQUENCE 5
#define TERMINATOR 0xff

/* Initializers for init sequence table */
#define short_seq(n, a...)			\
	{					\
		.type = n,			\
			.data = {		\
			.bytes = {a},		\
		},				\
	}

#define long_seq(ptr)				\
	{					\
		.type = ARRAY_SIZE(ptr),	\
			.data = {		\
			.sequence = ptr,	\
		},				\
	}

#define delay(d)				\
	{					\
		.type = DELAY,			\
		.data = {			\
			.delay = d,		\
		},				\
	}

#define term					\
	{					\
		.type = TERMINATOR,		\
	}

struct xterm_init_item {
	uint8_t type;
	union {
		const uint8_t *sequence;
		unsigned int delay;
		uint8_t bytes[4];
	} data;
};

static const uint8_t cmd_ed[] = {
	/* ??? */
	0xed, 0x64, 0x03, 0x12, 0x81,
};

static const uint8_t cmd_cb[] = {
	/* ??? */
	0xcb, 0x39, 0x2c, 0x00, 0x34, 0x02,
};

/* Display init sequence table */
static const struct xterm_init_item xterm_init_sequence[] = {
	short_seq(1, 0x01),
	delay(5),
	short_seq(1, CMD_DISPOFF),
	delay(20),
	short_seq(4, 0xcf, 0x00, 0x83, 0x30),
	long_seq(cmd_ed),
	short_seq(4, 0xe8, 0x85, 0x01, 0x79),
	long_seq(cmd_cb),
	short_seq(2, 0xf7, 0x20),
	short_seq(3, 0xEA, 0x00, 0x00),
	short_seq(2, 0xC0, 0x26),
	short_seq(2, 0xC1, 0x11),
	short_seq(3, 0xC5, 0x35, 0x3E),
	short_seq(2, 0xC7, 0xbe),
	short_seq(2, 0x3a, 0x55),
	short_seq(3, 0xb1, 0x00, 0x1b),
	short_seq(2, 0x26, 0x01),
	short_seq(2, 0xb7, 0x07),
	short_seq(1, 0x11),
	delay(100),
	short_seq(1, 0x29),
	delay(20),
	term,
};

static int send_spi_cmd_data(struct xterm_data *data, uint8_t cmd,
			     const void *buf, int datalen)
{
	uint8_t c = cmd;
	struct spi_obuf obuf;
	int stat;

	gpio_set(data->dc_gpio, 0);
	obuf.len = 1;
	obuf.buf = &c;
	stat = spi_xfer(data->xterm_dev, SPI_F_NOFINI, NULL, &obuf);
	if (stat < 0)
		return stat;

	gpio_set(data->dc_gpio, 1);
	obuf.len = datalen;
	obuf.buf = buf;
	return spi_xfer(data->xterm_dev, SPI_F_NOINIT, NULL, &obuf);
}

static int send_init_sequence(struct xterm_data *data,
			      const struct xterm_init_item *sequence)
{
	int stat;
	const struct xterm_init_item *it;

	for (it = sequence, stat = 0; it->type != TERMINATOR && stat >= 0;
	     it++) {
		const uint8_t *sequence;

		if (it->type == DELAY) {
			udelay(it->data.delay * 1000);
			continue;
		}
		/*
		 * Bytes sequence. If it's 1 to 4 bytes long, we store it
		 * directly in a struct xterm_init_item, otherwise a pointer
		 * is necessary
		 */
		sequence = it->type > FOUR_BYTES_SEQUENCE ? it->data.sequence :
			it->data.bytes;
		stat = send_spi_cmd_data(data, sequence[0], &sequence[1],
					 it->type - 1);
	}
	return stat;
}

int xterm_init(struct xterm_data *data, struct spi_dev *dev, int rst,
	       int xterm_mode)
{
	int ret = 0;
	/*
	 * see data sheet, p. 127/233, 8.2.29, command 0x36,
	 * memory access control:
	 * bits MY (row address morder), MX (column address order),
	 * MV (row/col exchange)
	 */
	static const uint8_t portrait_0x36[] = { 0x40 };
	static const uint8_t landscape_0x36[] = { 0xe0 };

	gpio_dir_af(data->cs_gpio, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	gpio_dir_af(data->dc_gpio, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	data->xterm_dev = dev;
	data->xterm_mode = xterm_mode;
	if (rst > 0) {
		gpio_dir_af(rst, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
		gpio_set(rst, 0);
		udelay(20);
		gpio_set(rst, 1);
	}
	udelay(500*1000L);

	ret = send_init_sequence(data, xterm_init_sequence);
	if (ret < 0)
		return ret;
	ret = send_spi_cmd_data(data, 0x36, xterm_mode & XTERM_MODE_LANDSCAPE ?
				landscape_0x36 : portrait_0x36, 2);
	if (ret < 0)
		return ret;
	if (data->xterm_mode & XTERM_MODE_LANDSCAPE) {
		data->wid = SCREEN_HEIGHT;
		data->hei = SCREEN_WIDTH;;
	} else {
		data->wid = SCREEN_WIDTH;
		data->hei = SCREEN_WIDTH;
	}

	return xterm_blank(data);
}

int xterm_set_font(struct xterm_data *data, const struct font *f)
{
	data->font = f;

	if (!f->wid || f->wid > MAX_GLYPH_WIDTH)
		return -EINVAL;
	data->ncols = data->wid / f->wid;
	data->nrows = data->hei / f->hei;
	return 0;
}

static uint16_t rgb888_to_bgr565be(int rgb)
{
	uint16_t ret = ((rgb & 0xf80000) >> 19)
		| ((rgb & 0xfc00) >> 5)
		| ((rgb & 0x00f8) << 8);
	return htons(ret);
}

void xterm_set_colors(struct xterm_data *data, uint16_t fg, uint16_t bg)
{
	data->fg_color = fg;
	data->bg_color = bg;
}

void xterm_set_colors888(struct xterm_data *data, int fg, int bg)
{
	data->fg_color = rgb888_to_bgr565be(fg);
	data->bg_color = rgb888_to_bgr565be(bg);
}

int xterm_goto_xy(struct xterm_data *data, unsigned short x, unsigned short y)
{
	if (x >= data->ncols || y >= data->nrows)
		return -EINVAL;
	data->currx = x;
	data->curry = y;
	return 0;
}

static int xterm_set_ca_pa(struct xterm_data *data, int ca,
			   unsigned short start, unsigned short end)
{
	union {
		unsigned char c[0];
		uint16_t w[2];
	} buf;

	buf.w[0] = htons(start);
	buf.w[1] = htons(end);

	return send_spi_cmd_data(data, ca ? CMD_CAS : CMD_PAS,
				 buf.c, sizeof(buf));
}

static int xterm_set_ca(struct xterm_data *data, unsigned short start,
			unsigned short end)
{
	return xterm_set_ca_pa(data, 1, start, end);
}

static int xterm_set_pa(struct xterm_data *data, unsigned short start,
			unsigned short end)
{
	return xterm_set_ca_pa(data, 0, start, end);
}

static int xterm_set_area(struct xterm_data *data,
			  int x0, int x1, int y0, int y1)
{
	int stat;
	stat = xterm_set_ca(data, x0, x1);
	if (stat < 0)
		return stat;
	return xterm_set_pa(data, y0, y1);
}

static int xterm_set_glyph_area(struct xterm_data *data)
{
	unsigned short sc, ec, sp, ep;
	int x, y;

	if (data->xterm_mode & XTERM_MODE_ROTATE180) {
		x = data->ncols - 1 - data->currx;
		y = data->nrows - 1 - data->curry;
	} else {
		x = data->currx;
		y = data->curry;
	}
	sc = x * data->font->wid;
	sp = y * data->font->hei;
	ec = sc + data->font->wid - 1;
	ep = sp + data->font->hei - 1;
	return xterm_set_area(data, sc, ec, sp, ep);
}

int xterm_putc(struct xterm_data *data, char c)
{
	const uint8_t *glyph = font_get_hv(data->font, c,
					   FONT_FLAG_H);
	int i, j, spi_flags, increment;
	struct spi_obuf obuf;


	if (!glyph)
		return -EINVAL;
	/* Set area */
	xterm_set_glyph_area(data);
	send_spi_cmd_data(data, CMD_RAMWR, NULL, 0);

	/* Send Memory write command */
	gpio_set(data->dc_gpio, 0);
	obuf.len = 1;
	obuf.buf = data->glyph_row_buf.c;
	data->glyph_row_buf.c[0] = 0x3c;
	spi_xfer(data->xterm_dev, SPI_F_NOFINI, NULL, &obuf);

	gpio_set(data->dc_gpio, 1);
	increment = data->font->wid / 8;
	if (data->xterm_mode & XTERM_MODE_ROTATE180) {
		glyph += (data->font->hei - 1) * increment;
		increment = - increment;
	}
	for (i = 0; i < data->font->hei; i++, glyph += increment) {
		/* Write pixels, send one font-line at a time */
		for (j = 0; j < data->font->wid; j++) {
			uint16_t *dest = data->glyph_row_buf.w;

			if (data->xterm_mode & XTERM_MODE_ROTATE180)
				dest += data->font->wid-1 - j;
			else
				dest += j;

			*dest = glyph[j / 8] & (1 << (7 - j%8))
				? data->fg_color : data->bg_color;
			}
		/*
		 * All rows but last one: no init and no fini, last row:
		 * just no init
		 */
		spi_flags = i == data->font->hei - 1 ? SPI_F_NOINIT :
		    SPI_F_NOINIT | SPI_F_NOFINI;
		obuf.len = data->font->wid * 2;
		spi_xfer(data->xterm_dev, spi_flags, NULL, &obuf);
	}
	return 1;
}

int xterm_puts(struct xterm_data *data, const char *s)
{
	const char *ptr;
	int stat;
	unsigned short newrow, newcol;

	for (ptr = s; ptr[0]; ptr++) {
		/* Manage newlines and backspaces */
		switch (ptr[0]) {
		case '\n':
			newrow = data->curry + 1;
			if (newrow >= data->nrows)
				newrow = 0;
			xterm_goto_xy(data, 0, newrow);
			break;
		case '\b':
			newcol = data->currx >= 1 ? data->currx - 1 :
			data->ncols - 1;
			newrow = data->currx >= 1 ? data->curry :
				(data->curry >= 1 ? data->curry - 1 :
				 data->nrows - 1);
			stat = xterm_goto_xy(data, newcol, newrow);
			if (stat < 0)
				return stat;
			break;
		default:
			stat = xterm_putc(data, ptr[0]);
			if (stat < 0)
				return stat;
			newcol = data->currx + 1;
			newrow = data->curry;
			if (newcol >= data->ncols) {
				newcol = 0;
				newrow++;
				if (newrow >= data->nrows)
					newrow = 0;
			}
			stat = xterm_goto_xy(data, newcol, newrow);
			if (stat < 0)
				return stat;
			break;
		}
	}
	return ptr - s;
}

int xterm_blank(struct xterm_data *data)
{
	int stat, i, cols, rows;
	static uint16_t bgdata[64];

	cols = data->wid;
	rows = data->hei;
	stat = xterm_set_ca(data, 0, cols);
	if (stat < 0)
		return -1;
	stat = xterm_set_pa(data, 0, rows);
	if (stat < 0)
		return -1;
	stat = send_spi_cmd_data(data, CMD_RAMWR, NULL, 0);
	if (stat < 0)
		return -1;
	for (i = 0; i < ARRAY_SIZE(bgdata); i++)
		bgdata[i] = data->bg_color;
	for (i = 0; i < ((cols * rows) / 64); i++) {
		stat = send_spi_cmd_data(data, 0x3c, bgdata, sizeof(bgdata));
		if (stat < 0)
			return stat;
	}
	return stat;
}

int xterm_draw_bgr565be(struct xterm_data *data, int x0, int y0,
		      const uint16_t *img)
{
	int stat, npix;

	stat = xterm_set_area(data, x0, x0 + img[0] - 1, y0, y0 + img[1] - 1);
	if (stat < 0)
		return -1;
	npix = img[0] * img[1];

	/* then, print the pixels as a single row */
	return send_spi_cmd_data(data, CMD_RAMWR, img + 2, 2 * npix);
}

static uint16_t get_rle_pixel(int cont,
			      const uint16_t *cols, const uint16_t *pixels)
{
	static const uint16_t *p;
	static int nibble, count;
	uint16_t val;

	if (!cont) { /* initialize static data */
		p = pixels;
		nibble = 0;
		count = 0;
		return 0;
	}
	val = *p;
	switch(nibble) {
	case 0:
		if (!count && (val & 0xf)) /* an RLE record */
			count = (val & 0xfff);
		if (count) { /* continue with previous pixel */
			count--;
			if (!count) /* if done, next value */
				p++;
			return cols[val >> 12];
		}
		nibble++;
		return cols[val >> 12];
	case 1:
		nibble++;
		return cols[(val >> 8) & 0xf];
	case 2:
		nibble = 0;
		p++;
		return cols[(val >> 4) & 0xf];
	}
	return 0;
}

int xterm_draw_rle(struct xterm_data *data, int x0, int y0,
		   const uint16_t *img)
{
	int stat, npix, fill;
	const uint16_t *cols, *pixels;
	uint16_t burst[32];
	uint8_t cmd = CMD_RAMWR;
	struct spi_obuf obuf;

	stat = xterm_set_area(data, x0, x0 + img[0] - 1, y0, y0 + img[1] - 1);
	if (stat < 0)
		return -1;
	npix = img[0] * img[1];
	cols = img + 2;
	pixels = cols + 16;
	get_rle_pixel(0, cols, pixels); /* init static data */

	/*
	 * Print the pixels as a single row, but in several writes.
	 * We can't use send_spi_cmd_data() as it restarts from pixel 0.
	 * So make a single SPI burst by unrolling the function here.
	 */
	gpio_set(data->dc_gpio, 0);
	obuf.len = 1;
	obuf.buf = &cmd;
	spi_xfer(data->xterm_dev, SPI_F_NOFINI, NULL, &obuf);
	gpio_set(data->dc_gpio, 1);

	while (npix) {
		for (fill = 0; npix && fill < ARRAY_SIZE(burst); ) {
			burst[fill++] = get_rle_pixel(1, cols, pixels);
			npix--;
		}
		/* send_spi_cmd_data(data, CMD_RAMWR, burst, 2 * fill); */
		obuf.len = 2 * fill;
		obuf.buf = (void *)burst;
		spi_xfer(data->xterm_dev,
			 SPI_F_NOINIT | (npix ? SPI_F_NOFINI : 0),
			 NULL, &obuf);
	}
	return 0;
}
