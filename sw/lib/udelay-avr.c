#include <time.h>
#include <stringify.h>

/*
 * Unfortunately, generic code takes 200usec as a constant overhead.
 * So rely on CPU_FREQ instead, with math at compile time.
 */

/* Moreover, in C we need "long", but avr-as complains for this spare "L" */
#define MHZ_ASM		((CPU_FREQ + 500  * 1000) / 1000 / 1000)
#define MHZ_C		((CPU_FREQ + 500L * 1000) / 1000 / 1000)
#define MHZ_STRING	__stringify(MHZ_ASM)

void __udelay(uint32_t usec)
{
	int32_t u = usec; /* Use signed as we may get below 0 */

	/* enter/leave with compare with 0  costs 18-20 cycles */
	u -= 20 / MHZ_C;

	if (MHZ_C >= 6) {
		/* Looping costs 6 cycles, so add nops if we are fast enough */
		while (u-- > 0)
			asm(".rep " MHZ_STRING " - 6\n\tnop\n\t.endr\n");
	} else if (MHZ_C >= 2) {
		/*
		 * We are too slow to loop on u. Count bigger steps.
		 * Shifting one bit is 4 instructions, so this is 8
		 * cycles, but we truncate micros so it's mostly ok
		 */
		u /= 4;
		while (u-- > 0)
			asm(".rep " MHZ_STRING " * 4 - 6\n\tnop\n\t.endr\n");
	} else {
		/* 1MHz? Waste 12 instructions to shift, and proceed */
		u /= 8;
		while (u-- > 0)
			asm(".rep " MHZ_STRING " * 8 - 6\n\tnop\n\t.endr\n");
	}
}
