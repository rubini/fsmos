#ifndef __TC_H__
#define __TC_H__

/* int32_t is 16.16 fixed point */

#define TC_K_MIN (-270)
#define TC_K_MAX 1370
extern int32_t tc_k_convert(int32_t cold_degs, int32_t millivolts);

#define TC_T_MIN (-270)
#define TC_T_MAX 400
extern int32_t tc_t_convert(int32_t cold_degs, int32_t millivolts);

#endif /* __TC_H__ */
