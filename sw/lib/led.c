#include <time.h>
#include <led.h>

void led_init(struct led *led)
{
	for (;led->name; led++)
		gpio_dir_af(led->gpio, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
}


void led_set_rate(struct led *led, int name, int t_on, int t_off)
{
	for (;led->name; led++) {
		if (led->name == name) {
			led->t_on = t_on;
			led->t_off = t_off;
		}
	}
}

void led_action(struct led *led)
{
	unsigned long next, j = jiffies;

	for (;led->name; led++) {
		if (led->last_action == 0) /* first time ever */
			led->last_action = j;

		if (led->t_on == 0) { /* if both 0, we are off */
			led->current = 0;
		} else if (led->t_off == 0) {
			led->current = 1;
		} else {
			if (led->current == 1) {
				next = led->last_action + led->t_on;
			} else {
				next = led->last_action + led->t_off;
				if (led->t_off == LED_FOREVER)
					next = j + 1;
			}
			if (time_before(j, next))
				continue; /* no action */
			led->current ^= 1;
		}
		/* activate */
		gpio_set(led->gpio, led->current);
		led->last_action = j;
	}
}

void led_tick(struct led *led, int name, int t_on)
{
	for (;led->name; led++) {
		if (led->name == name) {
			led->t_on = t_on;
			led->t_off = LED_FOREVER;
			led->current = 1;
			led->last_action = jiffies;
		}
	}
	led_action(led);
}

void led_sync(struct led *led, int target, int source)
{
	struct led *t;

	/* look for target */
	for (t = led; t->name; t++)
		if (t->name == target)
			break;
	if (!t->name)
		return;

	/* look for source */
	for (;led->name; led++)
		if (led->name == source)
			break;
	if (!led->name)
		return;

	t->current = led->current;
	t->last_action = led->last_action;
}
