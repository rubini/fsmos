
# FSM os (finite state machine operating system)

Actually, this is not an operating system. It's my collection of
procedures that I use as firmware in microcontrollers.

The idea is that everything is a state machine, and no interrupts
are ever used.  Time management is done by hardware, and "jiffies"
refers to an hardware register.

Some of this is used on other projects: for example White Rabbit
(developed and used by CERN and other laboratories) use this printf
and these onewire drivers. Actually, the network code comes from White
Rabbit, where I worked on it before picking it here.

## Configuration

I'm a fan of Kconfig, so I use it. 

## Supported architectures

I currently only support some LPC microcontrollers (see
sw/configs/*defconfig and sw/Kconfig*) and some AVR devices. I dislike
the 8-bit AVR, but once I was asked to replace the firmware on a board
already built by another company.

As said, most of this is very portable and used elsewhere too.

## Features

Not many, but I run this on microcontroller with a few kB of RAM.
Also, I will add a scheduler like in thos/bathos (see gitlab projects
for code), but it's not there yet.

* Printf is there, but small. It's the pp_printf project of mine

* Neopixel support

* Onewire support

* I2C as a gpio-based state machine

* eeprom support

* USB slave (on LPC11U and LPC17)

* USB serial base on USB slave

* USB network device: cdc_subset

* Network support (ARP/PING/UDP) based on usb serial or ENC28J60.

* SPI generic API

* SPI flash

* LTC2428 (20 bit ADC)

* TPI (to program attiny devices using gpio).

* Display support for N5110, and fonts (that I used on other displays
too, that I didn't clean up for publishing, yet).

* Panic and assert. Panic uses diag leds to "print" a panic code.


## Design choices

* Everyyhing is under sw/ so I can move commits across projects.

* Everything is built, then it's the linker that discards unneeded
code/data.

* No interrupts

* jiffies is an hardware register.

* All I/O access using regs[] (and sometimes readl/writel).

* Avoid ifdef as much as possible. I use Kconfig and ".rep" where applicable.
See neopixel code for example.

* The stack is before code, so the CPU hangs on stack overflow (rather
than misbehaving).

* Stack check support (not on AVR yet) to monitor stack use

* Programming to RAM for quick testing (see tools/program)

* .ramcode ELF section for faster execution of critical code
