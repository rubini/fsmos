#ifndef __BOARD_H__
#define __BOARD_H__

extern uint32_t board_get_clksel(void); /* a weak default is in lib/pll.c */

/*
 * Currently we support one board only. Later I'll split this
 */

#define BOARD_SYSOSCCTRL 0 /* No bypass system oscillator: we have xtal */


#endif /* __BOARD_H__ */
