/*
 * GPIO interface for LPC-11xx (derived from my work on 1143)
 * Alessandro Rubini, 2017
 */
#ifndef __GPIO_LPC17_H__
#define __GPIO_LPC17_H__

#define GPIO_NUMBER (5 * 32)

/* for gpio_dir() and gpio_dir_af() */
#define GPIO_DIR_IN     0
#define GPIO_DIR_OUT    1

/* for gpio_dir_af()  */
#define GPIO_AF_GPIO    0
#define GPIO_AF(x)      x
#define GPIO_AF_PULLDOWN   0x08
#define GPIO_AF_PULLUP     0x10
#define GPIO_AF_OPENDRAIN  0x20
#define GPIO_AF_ANALOG     0x40

/* We have 32 gpio bits per "port", this is hardwired */
#define GPIO_NR(port, bit)	((port) * 32 + (bit))
#define GPIO_PORT(nr)		((nr) / 32)
#define GPIO_BIT(nr)		((nr) % 32)


extern void gpio_init(void);

extern int gpio_dir_af(int gpio, int output, int value, int afnum);
extern void gpio_dir(int gpio, int output, int value);

extern int gpio_get(int gpio);
extern uint32_t __gpio_get(int gpio);

extern void gpio_set(int gpio, int value);
extern void __gpio_set(int gpio, uint32_t value);

#endif /* __GPIO_LPC17_H__ */
