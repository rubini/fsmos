/*
 * GPIO interface for LPC-1343
 * Alessandro Rubini, 2011-2012GNU GPL2 or later
 */
#include <init.h>

/*
 * This table is needed to turn gpio number to address. The hairy
 * macros are used to shrink the source code.
 */
#define CFG_REG(port, bit)  __GPIO_CFG_P ## port ## _ ## bit
#define __A(port, bit)  [GPIO_NR(port,bit)] = CFG_REG(port,bit)
static const uint8_t gpio_addr[] = {
	__A(0, 0), __A(0, 1), __A(0, 2), __A(0, 3), __A(0, 4), __A(0, 5),
	__A(0, 6), __A(0, 7), __A(0, 8), __A(0, 9), __A(0,10), __A(0,11),
	__A(1, 0), __A(1, 1), __A(1, 2), __A(1, 3), __A(1, 4), __A(1, 5),
	__A(1, 6), __A(1, 7), __A(1, 8), __A(1, 9), __A(1,10), __A(1,11),
	__A(2, 0), __A(2, 1), __A(2, 2), __A(2, 3), __A(2, 4), __A(2, 5),
	__A(2, 6), __A(2, 7), __A(2, 8), __A(2, 9), __A(2,10), __A(2,11),
	__A(3, 0), __A(3, 1), __A(3, 2), __A(3, 3), __A(3, 4), __A(3, 5),
};

#define GPIO_MAX  (ARRAY_SIZE(gpio_addr) - 1)

/* This other table marks the ones where AF0 is swapped with AF1 */
static const uint16_t gpio_weird[] = {
	__GPIO_WEIRDNESS_0, __GPIO_WEIRDNESS_1,
	__GPIO_WEIRDNESS_2, __GPIO_WEIRDNESS_3
};

/*
 * Initialization of the clock is done at initcall time, so it's automatic
 * whenever a function from this file is used by the application
 */
static int hw_gpio_init(void)
{
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_GPIO | REG_AHBCLKCTRL_IOCON;
	return 0;
}
subsys_initcall(hw_gpio_init);

/*
 * What follows is the public interface.
 * Note that only gpio_dir_af() checks the gpio is valid. Other
 * functions are expected to be called often and only after setting the mode.
 */

/* This is a documented part of the API, so offer it */
void gpio_init(void) {}

void gpio_dir(int gpio, int output, int value)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);
	uint32_t reg;

	reg = readl(__GPIO_DIR(port));
	if (output)
		reg |= (1 << bit);
	else
		reg &= ~(1 << bit);

	writel (reg , __GPIO_DIR(port));

	/* After changing the direction we must re-force the value */
	if (output)
		gpio_set(gpio, value);
}

int gpio_dir_af(int gpio, int output, int value, int afnum)
{
	int pull, digiobit;

	if (gpio > GPIO_MAX || gpio < 0)
		return -1;

	pull = afnum & (GPIO_AF_PULLDOWN | GPIO_AF_PULLUP);
	digiobit = afnum & GPIO_AF_ANALOG ? 0 : 0x80;
	afnum &= 0x07; /* max 8 alternate functions */

	if (afnum < 2) { /* if weird bit, swap AF0 and AF1 */
		int port = GPIO_PORT(gpio);
		int bit = GPIO_BIT(gpio);
		if (gpio_weird[port] & (1 << bit))
			afnum ^= 1;
	}
	/* First set dir to prevent glitches when moving to AF0 */
	gpio_dir(gpio, output, value);
	writel(afnum | pull | digiobit | 0x40 /* bit 6 reserved and set */,
	       __GPIO_CFG_BASE + gpio_addr[gpio]);
	/* Finally, dir again to force value when moving to gpio-out */
	gpio_dir(gpio, output, value);
	return 0;
}

/* The following functions don't check the gpio value, for speed */
int gpio_get(int gpio)
{
	return __gpio_get(gpio) ? 1 : 0;
}

void gpio_set(int gpio, int value)
{
	__gpio_set(gpio, value << GPIO_BIT(gpio));
}
