#include <io.h>
#include <errno.h>

/* Setup the uart, called by setup-avr.c (currently not an initcall) */

void uart_setup(void)
{
	/* This setup function should configure the uart pins and clock */

	/*
	 * Input clock is 16MHz, divided by 8 (using U2X) makes 2MHz.
	 * To get 2% precision we need 50 ticks per bit, so 40k bits,
	 * so let's try a 38400 rate:
	 *   2000000 / 38400 = 52.08
	 * [if clock is 8MHz we get 19200 baud]
	 */
	regs[REG_UCSR0C] = 0;
	regs[REG_UBRR0L] = 51;

	/* size is 011 for 8-bit symbols */
	regs[REG_UCSR0C] = REG_UCSR0C_UCSZ1 | REG_UCSR0C_UCSZ0;
	regs[REG_UCSR0A] = REG_UCSR0A_UDRE | REG_UCSR0A_U2X;
	/* enable the device */
	regs[REG_UCSR0B] = REG_UCSR0B_TXEN | REG_UCSR0B_RXEN;

	/* Note: we'll add REG_UCSR0B_RXCIE for async input */
}

/*
 * Then, actual users of the uart, so any use of them picks
 * into the link the initialization above
 */

void putc(int c)
{
	while ( !(regs[REG_UCSR0A] & REG_UCSR0A_UDRE) )
		;
	regs[REG_UDR0] = c;
}

/* no-flush version */
void __putc(int c)
{
	if (c == '\n')
		__putc('\r');
	regs[REG_UDR0] = c;
}

/* getc is blocking */
int getc(void)
{
	while (!(regs[REG_UCSR0A] & REG_UCSR0A_RXC))
		;
	return regs[REG_UDR0];
}

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
int pollc(void)
{
	int ret = -EAGAIN;
	while (!(regs[REG_UCSR0A] & REG_UCSR0A_RXC))
		return regs[REG_UDR0];
	return ret;
}

