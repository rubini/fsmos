#ifndef __REGS_STM32F0x0_H__
#define __REGS_STM32F0x0_H__

/* From Reference Manual RM0360 -- STM32F030, F070 */

#define REGBASE_GPIO_PORTA		(0x48000000 / 4)
#define REGBASE_GPIO_PORTB		(0x48000400 / 4)
#define REGBASE_GPIO_PORTC		(0x48000800 / 4)
#define REGBASE_GPIO_PORTD		(0x48000c00 / 4)
#define REGBASE_GPIO_PORTE		(0x48001000 / 4) /* not present! */
#define REGBASE_GPIO_PORTF		(0x48001400 / 4)

#define REGBASE_GPIO_PORT(x)		(REGBASE_GPIO_PORTA + (x) * 0x100)
#define GPIO_PORTS_MASK			0x2f /* F. DCBA */
#define BITS_GPIO_EN_SHIFT		17
#define REG_GPIO_EN			(0x40021014 / 4)


#define REGBASE_USART1		(0x40013800 / 4)
#define REGBASE_USART2		(0x40004400 / 4) /* Only F030[8C] + F070 */
#define REGBASE_USART3		(0x40004800 / 4) /* Only F030C and F070B */
#define REGBASE_USART4		(0x40004c00 / 4) /* Only F030C and F070B */
#define REGBASE_USART5		(0x40005000 / 4) /* Only F030C */
#define REGBASE_USART6		(0x40011400 / 4) /* Only F030C */

#define REG_USART2_EN		(0x4002101c / 4)
#define BIT_USART2_EN		(1 << 17)
#define USART2_PIN_TX		GPIO_NR(GPIO_PORTA, 2)
#define USART2_PIN_RX		GPIO_NR(GPIO_PORTA, 3)
#define USART2_PIN_xX_AF	1 /* Alternate function number */


#define REG_RCC_CR		(0x40021000 / 4)
#define REG_RCC_CFGR		(0x40021004 / 4)
#define REG_RCC_CIR		(0x40021008 / 4)
#define REG_RCC_APB2RSTR	(0x4002100c / 4)
#define REG_RCC_APB1RSTR	(0x40021010 / 4)
#define REG_RCC_AHBENR		(0x40021014 / 4)
#define REG_RCC_APB2ENR		(0x40021018 / 4)
#define REG_RCC_APB1ENR		(0x4002101c / 4)
#define REG_RCC_BDCR		(0x40021020 / 4)
#define REG_RCC_CSR		(0x40021024 / 4)
#define REG_RCC_AHBRSTR		(0x40021028 / 4)
/* and more... */


#define REGBASE_TIM3		(0x40000400 / 4)
#define REGBASE_TIM6		(0x40001000 / 4)
#define REGBASE_TIM7		(0x40001400 / 4) /* not in all models */
#define REGBASE_TIM14		(0x40002000 / 4)
#define REGBASE_TIM1		(0x40012c00 / 4)
#define REGBASE_TIM15		(0x40014000 / 4) /* not in all models */
#define REGBASE_TIM16		(0x40014400 / 4)
#define REGBASE_TIM17		(0x40014800 / 4)

/* Timer-related details for the jiffies timer. Use TIM6 */
#define REGBASE_TIMER		REGBASE_TIM6
#define REG_TIMER_RST		(0x40021010  / 4) /* APB1 */
#define REG_TIMER_EN		(0x4002101c  / 4) /* APB1 */
#define BIT_TIMER_EN		0x10


enum stm_irq {
	STM_IRQ_WWDG		= 0,
	/* reserved */
	STM_IRQ_RTC		= 2,
	STM_IRQ_FLASH		= 3,
	STM_IRQ_RCC		= 4,
	STM_IRQ_EXTI0_1		= 5,
	STM_IRQ_EXTI2_3		= 6,
	STM_IRQ_EXTI4_15	= 7,
	/* reserved */
	STM_IRQ_DMA_CH1		= 9,
	STM_IRQ_DMA_CH2_3	= 10,
	STM_IRQ_DMA_CH4_5	= 11,
	STM_IRQ_ADC		= 12,
	STM_IRQ_TIM1_BRK_UP_TRG_COM = 13,
	STM_IRQ_TIM1_CC		= 14,
	/* reserved */
	STM_IRQ_TIM3		= 16,
	STM_IRQ_TIM6		= 17,
	/* reserved */
	STM_IRQ_TIM14		= 19,
	STM_IRQ_TIM15		= 20,
	STM_IRQ_TIM16		= 21,
	STM_IRQ_TIM17		= 22,
	STM_IRQ_I2C1		= 23,
	STM_IRQ_I2C2		= 24,
	STM_IRQ_SPI1		= 25,
	STM_IRQ_SPI2		= 26,
	STM_IRQ_USART1		= 27,
	STM_IRQ_USART2		= 28,
	STM_IRQ_USART3_4_5_6	= 29,
	/* reserved */
	STM_IRQ_USB		= 31,
	__NR_IRQS,
};

#endif /* __REGS_STM32F0x0_H__ */
