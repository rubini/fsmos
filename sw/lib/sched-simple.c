#include <io.h>
#include <time.h>
#include <init.h>
#include <assert.h>
#include <sched-simple.h>

static inline void run(struct task *t)
{
	if (CONFIG_SCHED_IS_VERBOSE)
		printf("run %s\n", t->name);
	if (t->gpio > 0)
		__gpio_set(t->gpio, 1);
	t->arg = t->job(t->arg);
	if (t->gpio > 0)
		__gpio_set(t->gpio, 0);
}

static void sched_simple_once(void)
{
	struct task *t, *best = NULL;
	static struct task *ptask;

	for (t = __task_begin; t < __task_end; t++) {
		if (!t->period)
			continue;
		if (!best)
			best = t;
		if (t->release < best->release)
			best = t;
	}
	if (CONFIG_SCHED_IS_VERBOSE)
		printf("%s: best %p \"%s\" @%li, now %li\n", __func__,
		       best, best->name, best->release, jiffies);
	while (time_before(jiffies, best->release)) {
		/* look for a polling task */
		if (!ptask || ptask >= __task_end)
			ptask = __task_begin;
		while (ptask->period && ptask < __task_end)
			ptask++;
		if (ptask < __task_end)
			run(ptask++);
	}
	run(best);
	while (time_after_eq(jiffies, best->release))
		best->release += best->period;
}

void __attribute__((noreturn))
sched_simple(void)
{
	while (1)
		sched_simple_once();
}

void sched_simple_timeout(unsigned long timeout)
{
	while (time_before(jiffies, timeout))
		sched_simple_once();
}

static int sched_simple_init(void)
{
	struct task *t;
	unsigned long offset;

	for (t = __task_begin; t < __task_end; t++) {
		assert(t->version == SCHED_VERSION, ~0,
		       "wrong task @%p\n", t);
		if (CONFIG_SCHED_IS_VERBOSE) {
			puts("Task: "); puts(t->name); /* avoid printf */
			printf(": rate %li/%li, @%li",
			       t->period, (long)HZ, t->release);
		}
		if (t->init && t->init(t->arg))
			if (CONFIG_SCHED_IS_VERBOSE)
				puts(": error");
		if (t->gpio > 0)
			gpio_dir_af(t->gpio, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
		if (CONFIG_SCHED_IS_VERBOSE)
			putc('\n');
	}
	/* now update all release times, as we don't know current jiffies */
	offset = jiffies + 2;
	if (CONFIG_SCHED_IS_VERBOSE) {
		offset += HZ / 100; /* uart time */
		printf("Offset for tasks: %li\n", offset);
	}
	for (t = __task_begin; t < __task_end; t++)
		t->release += jiffies + 2;
	return 0;
}

task_initcall(sched_simple_init);
