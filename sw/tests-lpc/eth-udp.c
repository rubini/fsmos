#include <io.h>
#include <string.h>
#include <time.h>
#include <spi.h>
#include <assert.h>
#include <board.h>
#include <net.h>
#include <ipv4.h>
#include <usb.h>
#include <usb-eth.h>

#include "revision.h"

static struct usbeth usbeth = {
}, *ue = &usbeth;

/*
 * This is the backend that connects net/ip/udp to usb-eth
 */
static uint8_t macaddr[6] = {0x02, 0x04, 0x06, 0x08, 0x0a, 0x0c};

int net_get_mac(uint8_t *mac)
{
	memcpy(mac, macaddr, 6);
	return 0;
}

struct net_ll net_ll = {
	.rx = usbeth_frame_rx,
	.tx = usbeth_frame_tx,
	.get_mac = net_get_mac,
};

/*
 * Our own UDP port. Let's use a small buffer, but we need ethhdr,iphdr, ...
 */
static uint8_t __own_udp_queue[128];
static struct socket __static_own_udp_socket = {
	.queue.buff = __own_udp_queue,
	.queue.size = sizeof(__own_udp_queue),
};
static struct socket *own_udp_socket;

/*
 * Simple demo: open an udp port, where we turn lower-case to upper-case
 */
void main(void)
{
	/* Message out */
	printf("Network UDP demo\n");
	printf("Build: %s\n   (on %s, %s)\n", __gitc__, __date__, __time__);

	if (!CONFIG_HAS_USB)
		return;

	/* create ethernet device */
	ue = usbeth_init(ue);
	if (!ue)
		panic(0, "Can't init usb ethernet\n");

	if (0) {
		/* Wait for the host to configure the device */
		unsigned long j = jiffies + 5 * HZ;
		while (time_before(jiffies, j))
		       usbeth_poll(ue);
		printf("Now starting xmit\n");
	}
	/* prepare standard stuff (arp, ping, bootp) */
	arp_init();
	ipv4_init();

	/* create your own UDP port */
	own_udp_socket = socket_create(&__static_own_udp_socket, NULL,
				       SOCK_UDP, 666 /* don't say! */);

	/* And now loop forever */
	while (1) {
		struct sockaddr addr;
		int i, len;
		static uint8_t buf[64];

		/* Retrieve frames, and dispatch to proper sockets */
		net_update_rx_queues();

		/* Normal polling of ethernet for standard services */
		arp_poll();
		ipv4_poll();

		/* Own UDP port */
		len = socket_recvfrom(own_udp_socket, &addr, buf, sizeof(buf));
		if (len > 0) {
			printf("got %i bytes (%i)\n", len, len - UDP_END);
			for (i = UDP_END; i < len; i++) {
				if (buf[i] >= 'a' && buf[i] <= 'z')
					buf[i] += 'A' - 'a';
			}
			fill_udp(buf, len, NULL);
			socket_sendto(own_udp_socket, &addr, buf, len);
		}
	}
}
