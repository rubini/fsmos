#ifndef __EEPROM_LPC_H__
#define __EEPROM_LPC_H__
#include <iap.h>

#define lpc_eeprom ((void *)1) /* not null, but we don't need a "dev" pointer */

/* same prototype as i2cee functions */
extern int lpcee_read(void * unused, int offset, void *buf, int size);
extern int lpcee_write(void *unused, int offset, const void *buf, int size);

#endif /* __EEPROM_LPC_H__ */
