#ifndef __MGPIO_H__
#define __MGPIO_H__
/*
 * "multiple gpio". Use to hide gpio extenders
 */

/* This is simple: changing function, pullup/down etc is not included */
struct mgpio_chunk {
	int base;
	int nrgpio;
	void *drvdata;
	int (*dir)(void *drvdata, int gpio, int output, int value);
	int (*get)(void *drvdata, int gpio);
	int (*set)(void *drvdata, int gpio, int value);
	struct mgpio_chunk *next;
};

/* All of these returns 0 or -errno */
int mgpio_register(struct mgpio_chunk *chunk);
int mgpio_dir(int gpio, int output, int value);
int mgpio_get(int gpio);
int mgpio_set(int gpio, int value);

int command_mgpio(char **reply, int argc, char **argv);
#define COMMAND_MGPIO   {"mgpio",   command_mgpio,     2, 3}

#endif /* __MGPIO_H__ */
