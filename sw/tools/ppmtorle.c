#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#endif

uint16_t colors[16]; /* bgr565be */
int ncols;

uint16_t oimage[1024*1024];
int opos;

/* Return the bitmap index for this new r,g,b byte array */
int getpixel(unsigned char pix[3])
{
	uint16_t outpix;
	int i;

	outpix = (pix[2] >> 3) << 11;
	outpix |= (pix[1] >> 2) << 5;
	outpix |= (pix[0] >> 3);
	outpix = htons(outpix);

	/* now look it up */
	for (i = 0; i < ncols; i++)
		if (colors[i] == outpix)
			break;
	if (i < ARRAY_SIZE(colors))
		colors[i] = outpix;
	if (i == ncols)
		ncols++;
	return i;
}

/* Push this pixel to the output array */
void putpixel(int pix, int verbose)
{
	static int nibble, count, prevp;
	static uint16_t value;

	if (verbose)
		fprintf(stderr, "n %04i:%i, now %x prev %x, count %x, v %04x\n",
			opos, nibble, pix, prevp, count, (int)value);
	if (pix != -1) {
		/*
		 * Output format (nibbles):  P1 P2 P3 0
		 * or (RLE word):            P1 c  c  c  (count = 0..4095)
		 */
		switch(nibble) {
		case 0: /* new word: simple */
			value = pix << 12;
			prevp = pix;
			count = 1;
			nibble++;
			return;

		case 1: /* second nibble: accumulate repetitions */
			if (pix == prevp) {
				/* accumulate the counter, possibly flush */
				count++;
				if (count == 0xfff) {
					oimage[opos++] = value | count;
					count = value = nibble = 0;
				}
				return;
			}
			if (count > 1) {
				/* different pixel: flush previous counter */
				if (count == 2) {
					value |= prevp << 8;
					/* then same as 'case 2:' below */
					nibble = 2;
					putpixel(pix, 0);
					return;
				}
				if ((count & 0xf) == 0) {
					count--;
					value |= count;
					oimage[opos++] = value;
					/* then, re-put the old one and this */
					nibble = 0;
					putpixel(prevp, 0);
					putpixel(pix, 0);
					return;
				}
				value |= count;
				oimage[opos++] = value;
				nibble = 0;
				putpixel(pix, 0);
				return;
			}
			/* just add the pixel */
			value |= pix << 8;
			nibble++;
			return;
		case 2:
			value |= pix << 4;
			oimage[opos++] = value;
			count = value = nibble = 0;
			return;
		}
	} else {
		/* We must close up a partial word */
		switch(nibble) {
		case 0:
			return;
		case 1:
			if ((count & 0xf) == 0) {
				count--;
				value |= count;
				oimage[opos++] = value;
				oimage[opos++] = value << 12;
				return;
			}
			value |= count;
			oimage[opos++] = value;
			return;
		case 2:
			oimage[opos++] = value;
			return;
		}
	}
}

int main(int argc, char **argv)
{
	char head[256] = "";
	char s[80];
	int i, x, y, z, v, npix, p;
	unsigned char pix[3];

	if (argc > 1) {
		fprintf(stderr, "%s: please pass pgm into stdin channel\n",
			argv[0]);
		exit(1);
	}

	v = getenv("RLE_VERBOSE") != NULL;

	/* read header */
	while(1) {
		if (!fgets(s, 80, stdin))
			exit(1);
		if (s[0] == '#')
			continue;
		if (strlen(head) + strlen(s) > 255)
			exit(2);
		strcat(head, s);
		if (head[0]!='P' || head[1]!='6')
			exit(3);
		if (sscanf(head, "P6 %i %i %i", &x, &y, &z) == 3)
			break;
	}
	if (v)
		fprintf(stderr, "%i %i %i\n", x, y, z);
	i = 0; npix = x * y;

	while (fread(pix, 1, 3, stdin)==3) {
		if (i >= npix) {
			fprintf(stderr, "%s: file too long\n", argv[0]);
			exit(1);
		}
		i++;

		p = getpixel(pix);
		if (p >= ARRAY_SIZE(colors)) {
			fprintf(stderr, "%s: too many colors\n", argv[0]);
			exit(1);
		}
		if (0)
			printf("%i\n", p);
		putpixel(p, v);
	}
	putpixel(-1, v);

	/* First the size */
	printf("%i, %i,\n", x, y);
	/* Then the colormap */
	for (i = 0; i < ARRAY_SIZE(colors); i++)
		printf("0x%04x,%c", colors[i], (i+1) % 8 ? ' ' : '\n');
	/* And finally the data */
	for (i = 0; i < opos; i++)
		printf("0x%04x,%c", oimage[i], (i+1) % 8 ? ' ' : '\n');
	printf("\n");
	return 0;
}
