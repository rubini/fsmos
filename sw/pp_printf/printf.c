/*
 * Basic printf based on vprintf based on vsprintf
 *
 * Alessandro Rubini for CERN, 2011 -- public domain
 * (please note that the vsprintf is not public domain but GPL)
 */
#include <stdarg.h>
#include <pp-printf.h>

static char print_buf[CONFIG_PRINT_BUFSIZE];

int vprintf(const char *fmt, va_list args)
{
	int ret;

	ret = vsprintf(print_buf, fmt, args);
	puts(print_buf);
	return ret;
}

int sprintf(char *s, const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = vsprintf(s, fmt, args);
	va_end(args);
	return ret;
}


int printf(const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = vprintf(fmt, args);
	va_end(args);

	return ret;
}
