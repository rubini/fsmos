#ifndef __CPU_H__
#define __CPU_H__

#ifndef __ASSEMBLER__
#include <hw.h>
#include <stdint.h>
#include <regs-avr.h>

/* Jiffies is updated at interrupt time */
extern volatile unsigned long _jiffies;

/* But the update is not atomic, so we need to lock */
static inline unsigned long get_jiffies(void)
{
	unsigned long j;

	asm("cli");
	j = _jiffies;
	asm("sei");
	return j;
}

#define jiffies get_jiffies()



#define CPU_FREQ	CONFIG_CPU_FREQ

/* For HZ see setup-avr.c::timer_setup */
#define CONFIG_HZ  (122 * (CONFIG_CPU_FREQ / 1000 / 1000) / 8)


/*
 * The following is fake, by now, to allow building the rest
 */
#define __GPIO_WORD(x) 0x0000 /* placeholder, not used in avr */

#define GPIO_NUMBER (6 * 32) /* we have up to "portF" I recall */

/* for gpio_dir() and gpio_dir_af() */
#define GPIO_DIR_IN     0
#define GPIO_DIR_OUT    1

/* for gpio_dir_af() --although avr missed the typical AT mechanism */
#define GPIO_AF_GPIO    0
#define GPIO_AF(x)      x
#define GPIO_AF_PULLDOWN   0x08 /* Not present in hardware */
#define GPIO_AF_PULLUP     0x10
#define GPIO_AF_ANALOG     0x40 /* Unused, compat w/ LPC */

/* We have 32 gpio bits per "port", this is hardwired (and PORTA is 0) */
#define GPIO_NR(port, bit)	((port) * 32 + (bit))
#define GPIO_PORT(nr)		((nr) / 32)
#define GPIO_BIT(nr)		((nr) % 32)

extern void gpio_init(void);

extern int gpio_dir_af(int gpio, int output, int value, int afnum);
extern void gpio_dir(int gpio, int output, int value);

extern int gpio_get(int gpio);
extern uint32_t __gpio_get(int gpio);

extern void gpio_set(int gpio, int value);
extern void __gpio_set(int gpio, uint32_t value);

#endif /* __ASSEMBLER__ */
#endif /* __CPU_H__ */
