#include <time.h>
#include <io.h>

/* This program is used to verify how sharp udelay is */

void udelay_test(int usecs, int repeat)
{
	unsigned long j1, j2, expected, result, ratio;
	int i;

	printf("Test %i usecs, %i times\n", usecs, repeat);
	printf("+\n");
	j1 = jiffies;
	for (i = 0; i < repeat; i++)
		udelay(usecs);
	j2 = jiffies;
	printf("-\n");
	printf("%li jiffies @ %i Hz\n", j2 - j1, HZ);
	expected = usecs * repeat;
	result = (j2 - j1) * (1000000 / HZ);
	ratio = (result * 100 + expected/2) / expected;
	printf("usecs ~= %li (not %li: delta %li, ratio %li%%)\n",
	       result, expected, result - expected, ratio);
	printf("\n");
}

void main(void)
{
	while (1) {
		printf(" **** Start test iteration ****\n");

		/* 1 iteration */
		udelay_test(100, 1);
		udelay_test(1000, 1);
		udelay_test(10000, 1);
		udelay_test(100000, 1);

		/* 0.1 seconds in theory */
		udelay_test(1, 100 * 1000);
		udelay_test(2, 50 * 1000);
		udelay_test(4, 25 * 1000);
		udelay_test(5, 20 * 1000);
		udelay_test(10, 10 * 1000);
		printf("\n");

		/* 1 second in theory */
		udelay_test(20, 50 * 1000);
		udelay_test(50, 20 * 1000);
		udelay_test(100, 10 * 1000);
		udelay_test(1000, 1000);
		udelay_test(10000, 100);
		udelay_test(100000, 10);
		udelay_test(1000000, 1);
		printf("\n");

		/* 10 seconds */
		udelay_test(1000000, 10);
		udelay_test(10000000, 1);
		printf("\n\n");
	}
}
