/* CPU-specific include files for LPC11 and LPC13 */
#ifdef CONFIG_LPC1_OLD
#  include <regs-lpc1-old.h>
#  include <gpio-lpc1-old.h>
#else
#  include <regs-lpc1.h>
#  include <gpio-lpc1.h>
#endif
