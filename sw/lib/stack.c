#include <stack.h>
#include <io.h>

int stack_free(int verbose)
{
	static unsigned minfree =  ~0;
	unsigned free;
	uint32_t *p;

	for (p = _stack; p < _fstack; p++)
		if (*p != STACK_MAGIC)
			break;
	free = (p - _stack) * sizeof(*p);
	if (!verbose)
		return free;
	if (free < minfree) {
		printf("Stack: %i bytes unused\n", free);
		minfree = free;
	}
	return free;
}
