#include <stdio.h>

extern int pp_sscanf(const char *buf, const char *fmt, ...);

int main(int argc, char **argv)
{
	char *cmd = argv[1];
	int i;
	unsigned long begin, end;
	char fname[32], cmdname[32];
	char flashid;

	printf("Got command: \"%s\"\n", cmd);

	i = pp_sscanf(cmd, "%s %c %s %lx %lx", cmdname,
		   &flashid, fname, &begin, &end);
	printf("scan: %i\n", i);
	if (i > 0 && i < 5) {
		printf("Error in dump command\n");
		return 1;
	}
	if (i == 5) {
		return 0;
	}
	return 1;
}

