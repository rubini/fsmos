/*
 * Turn a pgm file to vertically-aligned bitmap for oled displays.
 * I currently requires 128x64 size.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define WID 128
#define HEI 64

static unsigned char img[WID * HEI];

#define PIX(x, y, invert) ((img[WID * (y) + (x)] > 128)^(invert))

int main(int argc, char **argv)
{
	FILE *in = stdin;
	char *fname = "<stdin>";
	int wid, hei, depth, x, line, bit, invert = 0;
	char separators[] = "       \n";
	char c;

	if(argc > 1 && !strcmp(argv[1], "-i")) {
		invert = 1;
		argv[1] = argv[0];
		argv++, argc--;
	}

	if (argc > 2) {
		fprintf(stderr, "%s: Use: \"%s [-i] [<file>]\"\n",
			argv[0], argv[0]);
		exit(1);
	}
	if (argc == 2) {
		fname = argv[2];
		in = fopen(argv[2], "r");
	}
	if (!in) {
		fprintf(stderr, "%s: %s: %s\n", argv[0],
			fname, strerror(errno));
		exit(1);
	}

	/* parse pgm header */
	if(fscanf(in, "P5 %i %i %i%c", &wid, &hei, &depth, &c) != 4) {
		fprintf(stderr, "%s: %s: not a PGM file\n",
			argv[0], fname);
		exit(1);
	}
	if (depth != 255) {
		fprintf(stderr, "%s: %s: It is PGM but not 0..255 per pixel\n",
			argv[0], fname);
		exit(1);
	}
	if (wid != WID || hei != HEI) {
		fprintf(stderr, "%s: %s: Wrong size: expected is 128x64\n",
			argv[0], fname);
	}

	if (fread(img, 1, wid * hei, in) != wid * hei) {
		fprintf(stderr, "%s: %s: Premature EOF\n",
			argv[0], fname);
		exit(1);
	}

	/* Output it the binary way, in hex, 8 bytes per line */
	for (line = 0; line < 8; line++) {
		for (x = 0; x < 128; x++) {
			unsigned char byte = 0;
			for (bit = 0; bit < 8; bit++) {
				int y = line * 8 + bit;
				byte |= PIX(x, y, invert) << bit;
			}
			printf("0x%02x,%c", byte,
			       separators[x % strlen(separators)]);
		}
	}
	exit(0);
}


