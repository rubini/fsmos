#ifndef __SCHED_SIMPLE_H__
#define __SCHED_SIMPLE_H__
/*
 * We have two task structures. The small one, for periodic tasks alone,
 * and a bigger one, with priorities and statistics support. Each application
 * can use one or the other. This is the simple one.
 */

#define SCHED_VERSION 0x0001 /* MSB is 0 for simple structure */
struct task {
	short version, gpio; /* gpio may or not be used; 0 == none */
	const char *name;
	int (*init)(void *);
	void *(*job)(void *);
	void *arg;
	unsigned long period;
	unsigned long release;
};

extern struct task __task_begin[], __task_end[];

#define __task __attribute__((section(".task"), \
			      aligned(__alignof__(long)), \
			      __used__))

extern void __attribute__((noreturn)) sched_simple(void);
extern void sched_simple_timeout(unsigned long timeout);

#endif /* __SCHED_SIMPLE_H__ */
