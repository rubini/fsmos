#include <init.h>
#include <panic.h>
#include <io.h>
#include <errno.h>
#include <device.h>
#include <uart-dev.h>
#include <irq.h>

/* Hack: define registers we refer to, in false conditionals */
#ifndef REG_UARTCLKDIV
#define REG_UARTCLKDIV 0
#endif
#ifndef REG_PCLKSEL0
#define REG_PCLKSEL0 0
#endif

extern int invalid_uart_speed;
extern int invalid_cpu_frequency_for_this_uart_speed;
extern int unsupported_uart_speed_for_this_cpu;

static int console_init(void)
{
	unsigned int val;

	/* Turn on the clock for the uart */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_UART;

	/* Turn on the clock for pin configuration, and gpio too */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_IOCON | REG_AHBCLKCTRL_GPIO;

	/* First fix pin configuration */
	if (CONFIG_CPU_IS_LPC1) {
		gpio_dir_af(GPIO_NR(0, 18),
			    GPIO_DIR_IN,  0, GPIO_AF(1)); /* 0-18: rx */
		gpio_dir_af(GPIO_NR(0, 19),
			    GPIO_DIR_OUT, 1, GPIO_AF(1)); /* 0-19: tx */
	} else if (CONFIG_CPU_IS_LPC1_OLD) {
		/* 1311, 1313, 1342, 1343 but also old series 11xx */
		gpio_dir_af(GPIO_NR(1, 6),
			    GPIO_DIR_IN,  0, GPIO_AF(1)); /* 1-6: rx */
		gpio_dir_af(GPIO_NR(1, 7),
			    GPIO_DIR_OUT, 1, GPIO_AF(1)); /* 1-7: tx */
	} else if (CONFIG_CPU_IS_LPC17) {
		gpio_dir_af(GPIO_NR(0, 3),
			    GPIO_DIR_IN,  0, GPIO_AF(1)); /* 0-3: rx */
		gpio_dir_af(GPIO_NR(0, 2),
			    GPIO_DIR_OUT, 1, GPIO_AF(1)); /* 0-2: tx */
	} else {
		/* Try to get a build error */
		(void)sizeof(char[ -1 +
				   CONFIG_CPU_IS_LPC1 + CONFIG_CPU_IS_LPC1_OLD
				   + CONFIG_CPU_IS_LPC17]);
		panic(0, "Unsupported CPU");
	}

	/*
	 * The clock divider must be set before turning on the clock.
	 * If we start from 12MHz, we would divide by 6.5 to get 115200:
	 * 12M / 115200 / 16 = 6.510416
	 */
	if (!CONFIG_CPU_IS_LPC17) {
		/* This register is missing in 17xx, so we multiply later */
		regs[REG_UARTCLKDIV] = CONFIG_PLL_CPU; /* Back to 12MHz */
	} else {
		/* Ensure we are *not* dividing the clock */
		regs[REG_PCLKSEL0] = (regs[REG_PCLKSEL0] & ~0xc0) | 0x40;
	}

	/* Disable interrupts and clear pending interrupts */
	regs[REG_U0IER] = 0;
	val = regs[REG_U0IIR];
	val = regs[REG_U0RBR];
	val = regs[REG_U0LSR];

	/* Set bit rate: enable DLAB bit and then divisor */
	regs[REG_U0LCR] = 0x80;

	if (115200 % CONFIG_UART_SPEED == 0) {
		/*
		 * Calculation is hairy: we need 6.5 or a multiple, but the
		 * fractional divisor makes (1 + A/B) where B is 1..15
		 * 6.510416 = 4 * 1.627604 =~ 4 * 1.625 = 4 * (1+5/8)
		 */
		val = 4;
		/* Support sub-multiples of 115200 */
		val *= (115200 / CONFIG_UART_SPEED);

		if (CONFIG_CPU_IS_LPC17)
			val *= CONFIG_PLL_CPU;

		regs[REG_U0DLL] = val & 0xff;
		regs[REG_U0DLM] = (val >> 8) & 0xff;
		regs[REG_U0FDR] = (8 << 4) | 5; /* 1 + 5/8 */
	} else if (CONFIG_UART_SPEED == 1562500) {
		/*
		 * This speed nedds the "oversampling register", that
		 * is missing in some versions of the logic cell. When
		 * missing (i.e. LPC17xx) the  name is defined to be 0.
		 * We force a build error, but the following calculations
		 * still buil, even if incorrect (they'd access address 0)
		 */
		if (REG_U0OSR == 0) {
			unsupported_uart_speed_for_this_cpu++;
		}
		/*
		 * We need SPEEED * 16 == 25MHz. Require 48 or 72MHz.
		 */
		regs[REG_UARTCLKDIV] = 1; /* Full PCLK for this rate */
		if (CPU_FREQ == 72 * 1000 * 1000) {
			/*
			 * 2.88 = 2 * 1.44  --- 1 + 4/9
			 */
			regs[REG_U0OSR] = 7 << 4; /* 8 cks per bit, not 16 */
			val = 4; /* So this can be made bigger */
			regs[REG_U0DLL] = val & 0xff;
			regs[REG_U0DLM] = (val >> 8) & 0xff;
			regs[REG_U0FDR] = (9 << 4) | 4; /* 1 + 4/9 */
		} else if (CPU_FREQ == 48 * 1000 * 1000) {
			/* 1.92 = 1 + 12/13 */
			regs[REG_U0OSR] = 3 << 4; /* 4 cks per bit, not 16 */
			val = 4; /* So this can be made bigger */
			regs[REG_U0DLL] = val & 0xff;
			regs[REG_U0DLM] = (val >> 8) & 0xff;
			regs[REG_U0FDR] = (13 << 4) | 12; /* 1 + 12/13 */
		} else {
			invalid_cpu_frequency_for_this_uart_speed++;
		}
	} else {
			/* We dont' support other values */
		invalid_uart_speed++;
	}

	/* clear DLAB and write mode (8bit, no parity) */
	regs[REG_U0LCR] = 0x3;
	regs[REG_U0FCR] = 0x1;
	return 0;
}

rom_initcall(console_init);

/*
 * Then, actual users of the uart, so any use of them picks
 * into the link the initialization above
 */


void putc(int c)
{
	while ( !(regs[REG_U0LSR] & REG_U0LSR_THRE) )
		;
	regs[REG_U0THR] = c;
}
void raw_putc(int c)
{
	regs[REG_U0THR] = c;
}

/* no-flush version */
void __putc(int c)
{
	if (c == '\n')
		__putc('\r');
	regs[REG_U0THR] = c;
}


/* getc is blocking */
int getc(void)
{
	while (!(regs[REG_U0LSR] & REG_U0LSR_RDR))
		;
	return regs[REG_U0RBR];
}

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
int pollc(void)
{
	int ret = -EAGAIN;
	if (regs[REG_U0LSR] & REG_U0LSR_RDR)
		return regs[REG_U0RBR];
	return ret;
}
/* The following one give warning for incompatible types
 *
 * int uart_pollc(const struct uart *u) __attribute__((weak, alias("pollc")));
 *
 * ... so we hack an asm thing
 */
asm(".weak uart_pollc; .thumb_set uart_pollc,pollc");

/*
 * What follow is code for device support: the irq handler and device
 * methods. The application must instantiate the driver_data and device
 * struture, relying on this code, hardware baseaddr/irq and gpio choices
 */
static irqreturn_t uartdev_irq(void *data, uint32_t fraction, uint32_t sp)
{
	struct device *dev = data;
	struct uart_driver_data *ddata = dev->driver_data;
	int c, moreout = 0;
	uint32_t base = ddata->baseaddr;

	/* diagnostic stuff: it takes time even if disabled, but we need it */
	if (ddata->gpio_irqtime)
		__gpio_set(ddata->gpio_irqtime, 1);
	if (ddata->gpio_irqtoggle) {
		ddata->gpio_toggleval ^= 1;
		__gpio_set(ddata->gpio_irqtoggle, ddata->gpio_toggleval);
	}

	/* deal with input data */
	while (regs[base + REGOFF_UARTLSR] & REGOFF_UARTLSR_RDR) {
		c = regs[base + REGOFF_UARTRBR];
		cbuf_putc_stamp(dev->icb, c, jiffies);
	}
	/* no out-irq means nothing to check */
	if (regs[base + REGOFF_UARTIER] == 1)
		goto done;

	/* deal with output data */
	moreout = !cbuf_is_empty(dev->ocb);
	while (moreout && regs[base + REGOFF_UARTLSR] & REGOFF_UARTLSR_THRE) {
		c = __cbuf_getc(dev->ocb);
		regs[base + REGOFF_UARTTHR] = c;
		moreout = !cbuf_is_empty(dev->ocb);
	}
	if (!moreout) /* disable further interrupts */
		regs[base + REGOFF_UARTIER] = 1;

done:
	if (ddata->gpio_irqtime)
		__gpio_set(ddata->gpio_irqtime, 0);
	return IRQ_HANDLED;
}

int uartdev_init(struct device *dev)
{
	struct uart_driver_data *ddata = dev->driver_data;

	if (ddata->gpio_irqtime)
		gpio_dir_af(ddata->gpio_irqtime,
			    GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	if (ddata->gpio_irqtoggle)
		gpio_dir_af(ddata->gpio_irqtoggle,
			    GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	irq_request(ddata->irqnr, 0, uartdev_irq, dev);
	regs[ddata->baseaddr + REGOFF_UARTIER] = 1; /* rx data available */
	return 0;
}

int uartdev_exit(struct device *dev)
{
	struct uart_driver_data *ddata = dev->driver_data;

	regs[ddata->baseaddr + REGOFF_UARTIER] = 0;
	irq_free(ddata->irqnr, dev);
	return 0;
}

int uartdev_write(struct device *dev,  unsigned long flags,
		  const void *src, int count)
{
	struct uart_driver_data *ddata = dev->driver_data;
	int ret = cbuf_write(dev->ocb, flags, src, count);

	regs[ddata->baseaddr + REGOFF_UARTIER] = 3; /* both tx and rx */
	return ret;
}
