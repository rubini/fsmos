#ifndef __PANIC_H__
#define __PANIC_H__

/*
 * This is used by assert too, for a warning-only iteration
 * Ledvalue is an integer number being bitbanged on the board's leds.
 */
extern void panic_leds(unsigned ledvalue);

extern void panic(unsigned ledvalue, const char *fmt, ...)
        __attribute__((format(printf,2,3), noreturn));

/* The application can intercept this to turn off stuff or reboot */
extern void panic_action(int iterations);

/* If the board has leds, define this. Must already be gpio-out */
struct panic_ledinfo {
	uint8_t gpio_clock, invert_clock;
	uint8_t gpio_data, invert_data;
};

extern struct panic_ledinfo panic_ledinfo;

#endif /* __PANIC_H__ */
