#include <stdarg.h>
#include <panic.h>
#include <time.h>
#include <io.h>
#include <board.h>

#define TICK_DELAY (250 * 1000L) /* on and off: two iterations per second */

/* change this according to your board */
struct panic_ledinfo panic_ledinfo __attribute__((weak))  = {
	.gpio_clock = 0xff, /* disabled */
};

void panic_leds(unsigned ledvalue)
{
	int i;

	for (i = 0; i < 10; i++) { /* 1 second of fast blinking */
		gpio_set(panic_ledinfo.gpio_clock, !panic_ledinfo.invert_clock);
		udelay(50 * 1000L);
		gpio_set(panic_ledinfo.gpio_clock, panic_ledinfo.invert_clock);
		udelay(50 * 1000L);
	}
	udelay(4 * TICK_DELAY);

	/* Now spit off the number, LSB first. A whole number of bytes */
	do {
		for (i = 0; i < 8; i++) {

			/* clock on, green = bit */
			gpio_set(panic_ledinfo.gpio_clock,
				 1 ^ panic_ledinfo.invert_clock);
			gpio_set(panic_ledinfo.gpio_data,
				 ledvalue & 1
				 ? !panic_ledinfo.invert_data
				 : panic_ledinfo.invert_data);
			udelay(TICK_DELAY);

			/* all off */
			gpio_set(panic_ledinfo.gpio_clock,
				 panic_ledinfo.invert_clock);
			gpio_set(panic_ledinfo.gpio_data,
				 panic_ledinfo.invert_data);

			udelay(TICK_DELAY);
			ledvalue >>= 1;

		}
		udelay(4 * TICK_DELAY);
	} while (ledvalue);
}

void  __attribute__((noreturn)) panic(unsigned ledvalue, const char *fmt, ...)
{
	va_list args;
	int iterations = 1;

	panic_action(0); /* first time: as soon as possible */

	if (panic_ledinfo.gpio_clock != 0xff) {
		gpio_dir_af(panic_ledinfo.gpio_clock, GPIO_DIR_OUT, 0,
			    GPIO_AF_GPIO);
		gpio_dir_af(panic_ledinfo.gpio_data, GPIO_DIR_OUT, 0,
			    GPIO_AF_GPIO);
	}

	while (1) {
		panic_action(iterations++);
		printf("Panic: ");
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);

		if (panic_ledinfo.gpio_clock != 0xff)
			panic_leds(ledvalue);
		else
			udelay(500 * 1000L);
	}
}

void __attribute__((weak)) panic_action (int iterations)
{
	return;
}

/* Lazily, assert goes here too */

void __assert(char *func, int line, int ledvalue, int forever,
		     char *fmt, ...)
{
	va_list args;

	do {
		printf("Assertion failed (%s:%i)", func, line);
		if (fmt && fmt[0]) {
			printf(": ");
			va_start(args, fmt);
			vprintf(fmt, args);
			va_end(args);
		} else {
			printf(".\n");
		}

		panic_leds(ledvalue);
	} while (forever);
}
