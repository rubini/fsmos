#include <font.h>
#include <panic.h>

const uint8_t *font_get(const struct font *f, int asc)
{
	int csize;
	const uint8_t *fail;

	if (f->flags & FONT_FLAG_V)
		csize = f->wid * ((f->hei + 7) / 8);
	else
		csize = f->hei * ((f->wid + 7) / 8);

	fail = f->data + csize * (f->last + 1 - f->first);

	if (asc < f->first || asc > f->last)
		return fail;
	return f->data + csize * (asc - f->first);
}

static void font_swap_bits(const uint8_t *from, uint8_t *to, int nb)
{
	int i, src, val;

	for (i = 0; i < nb; i++) {
		val = 0;
		for (src = 0; src < 8; src++)
			if (from[i] & (1 << src))
				val |= 0x80 >> src;
		to[i] = val;
	}
}

static void font_swap_xy(const uint8_t *from, uint8_t *to, int nb, int flags)
{
	int i, src, destbit, srcbit, val;

	for (i = 0; i < 8 && i < nb; i++) {
		srcbit = 0x80 >> i;
		val = 0;
		for (src = 0; src < 8; src++) {
			destbit = (flags & FONT_FLAG_LSB)
				? 0x01 << src : 0x80 >> src;
			if (from[src] &  srcbit)
				val |= destbit;
		}
		to[i] = val;
	}
	if (i == nb)
		return;

	/* we may have a second block, convert it as well */
	from += 8;
	for (srcbit = 0x80; i < nb; i++, srcbit >>= 1) {
		val = 0;
		for (src = 0; src < 8; src++) {
			destbit = (flags & FONT_FLAG_LSB)
				? 0x01 << src : 0x80 >> src;
			if (from[src] &  srcbit)
				val |= destbit;
		}
		to[i] = val;
	}
}

const uint8_t *font_get_hv(const struct font *f, int asc, int flags)
{
	static uint8_t gl_target[16];
	const uint8_t *gl_orig;
	const int hvflags = FONT_FLAG_H | FONT_FLAG_V;
	int nbytes;

	gl_orig = font_get(f, asc);
	if (f->flags == flags)
		return gl_orig;

	/* guess the number of bytes per glyph */
	if ((f->flags & hvflags) == FONT_FLAG_V)
		nbytes = f->wid * ((f->hei + 7) / 8);
	else
		nbytes = f->hei * ((f->wid + 7) / 8);

	if (nbytes > 16)
		panic(0xff, "Can't convert font %ix%i\n", f->wid, f->hei);

	/* So, we can be flipped or just bitorder-wrong */
	if ((f->flags & hvflags) == (flags & hvflags)) {
		if (f->wid > 8)
			panic(0xfe, "Can't convert font %ix%i\n",
			      f->wid, f->hei);
		font_swap_bits(gl_orig, gl_target, nbytes);
		return gl_target;
	}

	if (f->wid > 8)
		panic(0xfd, "Can't convert font %ix%i\n", f->wid, f->hei);

	/* Flip x/y and possibly bit-order too */
	font_swap_xy(gl_orig, gl_target, nbytes, flags);
	return gl_target;
}
