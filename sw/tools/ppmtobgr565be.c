#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>

int main(int argc, char **argv)
{
	char head[256] = "";
	char s[80];
	int i, x, y, z, npix;
	unsigned char pix[3];
	uint16_t outpix;

	if (argc > 1) {
		fprintf(stderr, "%s: please pass pgm into stdin channel\n",
			argv[0]);
		exit(1);
	}

	/* read header */
	while(1) {
		if (!fgets(s, 80, stdin))
			exit(1);
		if (s[0] == '#')
			continue;
		if (strlen(head) + strlen(s) > 255)
			exit(2);
		strcat(head, s);
		if (head[0]!='P' || head[1]!='6')
			exit(3);
		if (sscanf(head, "P6 %i %i %i", &x, &y, &z) == 3)
			break;
	}
	printf("%i, %i,\n", x, y);
	i = 0; npix = x * y;

	while (fread(pix, 1, 3, stdin)==3) {
		if (i >= npix) {
			fprintf(stderr, "%s: file too long\n",	argv[0]);
			exit(1);
		}
		i++;

		outpix = (pix[2] >> 3) << 11;
		outpix |= (pix[1] >> 2) << 5;
		outpix |= (pix[0] >> 3);

		printf("0x%04x,%c", htons(outpix), i % 8 ? ' ' : '\n');
	}
	if (i != npix) {
		fprintf(stderr, "%s: file too short\n",	argv[0]);
		exit(1);
	}
	exit(0);
}
