#include <io.h>
#include <init.h>
#include <time.h>
#include <board.h>

uint32_t __attribute__((weak)) board_get_clksel(void)
{
	return REG_SYSPLLCLKSEL_OSC; /* use oscillator, by default */
}

#if XTAL_FREQ != 12 * 1000 * 1000
#error "This code only works with 12MHz xtals"
#endif

static void feed(uint32_t freg)
{
	regs[freg] = 0xaa;
	regs[freg] = 0x55;
}

/* Program CPU PLL and USB PLL for LPC17xx */
static int pll_init(void)
{
	int m, n, cclkcfg, p;
	int fcco_min = 275 * 1000 * 1000;
	/*  fcco_max = 550 * 1000 * 1000; -- unused */
	int fout;

	fout = CPU_FREQ; /* xtal * PLL_CPU */

	/*  Fcco = (2 x M x Fin) / N  -- with N possibly small */
	n = 1;
	m = CONFIG_PLL_CPU;

	/* Now, raise m until we get in the range, we'll divide in cclkcfg */
	for (cclkcfg = 1; cclkcfg < 256; cclkcfg++)
		if (2 * m * cclkcfg * XTAL_FREQ / n > fcco_min)
			break;
	m *= cclkcfg;
	cclkcfg *= 2; /* account for the "2 *" above */
	if (0)
		printf("values: m = %i, n = %i, cclkcfg = %i\n", m, n, cclkcfg);

	/*
	 * Now follow 4.5.13 to set up PLL
	 */

	/* oscen! */
	regs[REG_SCS] = REG_SCS_OSCEN; /* range is 0: 1-20MHz */

	if (regs[REG_PLL0STAT] & (1 << 25)) {
		/* connected: disconnect */
		regs[REG_PLL0CON] = 1; /* still enabled */
		feed(REG_PLL0FEED);
		while (regs[REG_PLL0STAT] & (1 << 25))
			;
	}
	if (regs[REG_PLL0STAT] & (1 << 24)) {
		/* enabled: disable */
		regs[REG_PLL0CON] = 0;
		feed(REG_PLL0FEED);
		while (regs[REG_PLL0STAT] & (1 << 24))
			;
	}
	regs[REG_CCLKCFG] = cclkcfg - 1; /* divider as calculated */

	while (!(regs[REG_SCS] & REG_SCS_OSCSTAT))
		/* wait for osc, enabled above, to be ready */;

	regs[REG_CLKSRCSEL] = board_get_clksel();
	regs[REG_PLL0CFG] = ((n - 1) << 16) | (m - 1); feed(REG_PLL0FEED);
	regs[REG_PLL0CON] = 1; feed(REG_PLL0FEED); /* re-ena */
	while (!(regs[REG_PLL0STAT] & (1 << 24)))
		;
	while (!(regs[REG_PLL0STAT] & (1 << 26)))
		/* wait for lock (we are between 100kHz and 20MHz!) */;
	regs[REG_PLL0CON] = 3; feed(REG_PLL0FEED); /* connect */

	/*
	 * USB PLL, similar to above, but not really
	 */
	fcco_min = 156 * 1000 * 1000;
	/*  max = 320 * 1000 * 1000; */
	fout = 48 * 1000 * 1000;

	m = fout / XTAL_FREQ;
	p = 2; /* see 4.6.9: no choice, no need to loop */

	/* Now set up PLL1 */
	regs[REG_PLL1CFG] = ((p - 1) << 5) | (m - 1); feed(REG_PLL1FEED);
	regs[REG_PLL1CON] = 1; feed(REG_PLL1FEED); /* enable */
	while (!(regs[REG_PLL1STAT] & (1 << 8)))
		;
	while (!(regs[REG_PLL1STAT] & (1 << 10)))
		/* wait for lock (we are between 100kHz and 20MHz!) */;
	regs[REG_PLL1CON] = 3; feed(REG_PLL1FEED); /* connect */

	return 0;
}


core_initcall(pll_init);

unsigned char pll_hook;
