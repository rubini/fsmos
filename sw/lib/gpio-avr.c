/*
 * GPIO interface for atmega devices
 * Alessandro Rubini, 2018 -- GNU GPL2 or later
 */
#include <errno.h>
/* This table turns port numbers to addresses */


struct avr_gpio_regs {
	uint8_t data, direction, input;
};

static const struct avr_gpio_regs avr_gpio_regs[] = {
	{ 0, /* no port A is there */ },
	{ REG_PORTB, REG_DDRB, REG_PINB },
	{ REG_PORTC, REG_DDRC, REG_PINC },
	{ REG_PORTD, REG_DDRD, REG_PIND },
};

#define GPIO_MAX  (ARRAY_SIZE(avr_gpio_regs) * 32 - 1)

/*
 * What follows is the public interface.
 * Note that only gpio_dir_af() checks the gpio is valid. Other
 * functions are expected to be called often and only after setting the mode.
 */

/* This is a documented part of the API, so offer it */
void gpio_init(void) {}

void gpio_dir(int gpio, int output, int value)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);
	uint8_t reg;

	if (output)
		gpio_set(gpio, value);

	reg = regs[avr_gpio_regs[port].direction];
	if (output)
		reg |= (1 << bit);
	else
		reg &= ~(1 << bit);
	regs[avr_gpio_regs[port].direction] = reg;
}

int gpio_dir_af(int gpio, int output, int value, int afnum)
{
	uint8_t port = GPIO_PORT(gpio);
	uint8_t bit = GPIO_BIT(gpio);
	uint8_t pull;

	if (gpio > GPIO_MAX || gpio < 0 || bit > 7)
		return -1; /* should be -EINVAL, now compatible w/ LPC */

	pull = afnum & (GPIO_AF_PULLDOWN | GPIO_AF_PULLUP);
	afnum &= ~pull;

	if (afnum)
		return -ENOENT; /* no such thing as alternate funcs here */
	if (pull & GPIO_AF_PULLDOWN)
		return -ENODEV; /* not in hardware */

	/* Hmm.. how to avoid glitches with pull up? */
	gpio_dir(gpio, output, value);
	if (output == GPIO_DIR_IN && pull)
		regs[avr_gpio_regs[port].data] |= (1 << bit);
	return 0;
}

/* The following functions don't check the gpio value, for speed */
uint32_t __gpio_get(int gpio)
{
	uint8_t port = GPIO_PORT(gpio);
	uint8_t bit = GPIO_BIT(gpio);

	return regs[avr_gpio_regs[port].input] & (1 << bit);
}

int gpio_get(int gpio)
{
	return __gpio_get(gpio) ? 1 : 0;
}

void __gpio_set(int gpio, uint32_t value)
{
	uint8_t port = GPIO_PORT(gpio);
	uint8_t bit = GPIO_BIT(gpio);

	if (value)
		regs[avr_gpio_regs[port].data] |= (1 << bit);
	else
		regs[avr_gpio_regs[port].data] &= ~(1 << bit);
}

void gpio_set(int gpio, int value) /* same as above */
{
	return __gpio_set(gpio, 1);
}
