#include <time.h>
#include <io.h>

void __udelay(uint32_t usec)
{
	struct timespec ts = {
		.tv_sec = usec / (1000 * 1000),
		.tv_nsec = (usec % (1000 * 1000)) * 1000,
	};
	sys_nanosleep(&ts, NULL);
}

