#include <io.h>
#include <time.h>
#include <string.h>
#include <command.h>
#include <pwm.h>
#include "../revision.h"

/* Test the pwm driver */

static struct pwm_dev pwm_array[] = {
	{
		.gpio = GPIO_NR(0,22), /* 1MHz, 50% */
		.cycle_mr = 3, /* MR3 is not routed to a pin */
		.clock_hz = 4 * 1000 * 1000,
		.nclocks = 4,
		.t_on = 2,
	}, {
		.gpio = GPIO_NR(1, 13), /* 100kHz, 240 steps */
		.cycle_mr = 3, /* MR3 is not routed to a pin */
		.clock_hz = 24 * 1000 * 1000,
		.nclocks = 240,
		.t_on = 0,
	}, {
		.gpio = GPIO_NR(1, 14), /* 100kHz, 240 steps, too */
		.cycle_mr = 3, /* like previous one (same hw timer!) */
		.clock_hz = 24 * 1000 * 1000,
		.nclocks = 240,
		.t_on = 120,
	}, {
		.gpio = GPIO_NR(1, 25), /* 10kHz, 100 steps */
		.cycle_mr = 0, /* MR0 not used as a pin */
		.clock_hz = 1 * 1000 * 1000,
		.nclocks = 100,
		.t_on = 0,
	},
};

struct command pwmtest_commands[] = {
	COMMAND_COMMIT,
	COMMAND_STACK,
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	{}
};

void main(void)
{
	struct pwm_dev *dev;
	unsigned long j;
	int i, err;
	static char str[80];


	printf("%s: version %s (built on %s)\n", __FILE__,
	       __gitc__, __date__);

	if (CONFIG_CPU_IS_LPC17) {
		printf("No pwm support on this CPU\n");
		return;
	}

	/* Configure all pwms */
	for (i = 0; i < ARRAY_SIZE(pwm_array); i++) {
		if (!pwm_create(pwm_array + i, &err))
			printf("failed index %i: error %i\n", i, err);
	}

	j = jiffies + HZ/10;
	while (1) {

		if (time_after_eq(jiffies, j)) {
			j += HZ/10;

			/* increase each of them by 1 step (not first one) */
			for (i = 1; i < ARRAY_SIZE(pwm_array); i++) {
				dev = pwm_array + i;
				dev->t_on++;
				if (dev->t_on > dev->nclocks)
					dev->t_on = 0;
				__pwm_set(dev);
			}
		}

		/* And do the command interface, so I can read/write regs */
		if (polls(str, sizeof(str))) {
			if (str[strlen(str)-1] == '\n')
				str[strlen(str)-1] = '\0';
			if (str[strlen(str)-1] == '\r')
				str[strlen(str)-1] = '\0';
			command_parse(str, pwmtest_commands);
			puts(command_reply);
			str[0] = '\0';
		}
	}
}
