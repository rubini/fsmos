#include <io.h>
#include <time.h>
#include <timestamp-lpc.h>

struct timestamp samples[32];

void main(void)
{
	int i;
	uint32_t frac;
	unsigned long ticks, nanos; /* low half of 64-bits value */

	if (CONFIG_CPU_IS_LPC17) {
		printf("Timestamping not tested in LPC17, yet\n");
		while (1)
			;
	}

	while(1) {
		for (i = 0; i < ARRAY_SIZE(samples); i++) {
			frac = regs[REG_TMR32B1PC];
			__timestamp_complete(samples + i, frac);
		}
		for (i = 0; i < ARRAY_SIZE(samples); i++) {
			ticks = timestamp_to_64(samples + i);
			nanos = timestamp_to_ns(samples + i);
			printf("%08lx:%08lx = %9lu = %9lu\n",
			       (long)samples[i].counts,
			       (long)samples[i].fraction,
			       ticks, nanos);
		}
		printf("\n");
	}
}
