#include <io.h>
#include <cbuf.h>
#include <errno.h>

int cbuf_read(struct cbuf *cbuf, unsigned long flags,
		     void *dest, int count)
{
	int nbyte;
	uint8_t *bdest = dest;
	cbuf_size_t nr;

	if (count <= 0)
		return 0;

	/* not completely safe: input is async */
	cbuf_fix_overflow(cbuf);

	nbyte = cbuf_datasize(cbuf);
	if (!nbyte)
		return -EAGAIN;
	if (count < nbyte)
		nbyte = count;

	nr = cbuf->nr;
	for (count = 0; count < nbyte; count++)
		bdest[count] = cbuf->b[nr++ & cbuf->mask];

	if (!(flags & CBUF_R_PEEKONLY))
		cbuf->nr = nr;
	return count;
}

int cbuf_write(struct cbuf *cbuf, unsigned long flags,
		      const void *src, int count)
{
	int i, c, donlcr = flags & CBUF_W_ONLCR;
	const uint8_t *bsrc = src;

	/* FIXME: I don't check W_FORCE flag: I always force */
	for (i = 0; i < count; i++) {
		c = bsrc[i];
		if (c == '\n' && donlcr)
			cbuf_putc(cbuf, '\r');
		cbuf_putc(cbuf, c);
	}
	return count;
}


/* a command to dump buffer information (needs hex address) */
int command_cbuf(char **reply, int argc, char **argv)
{
	struct cbuf *cb;
	unsigned long addr;
	char c;
	int i, last;

	if (sscanf(argv[1], "%lx%c", &addr, &c) != 1)
		return -EINVAL;

	printf("dump of cbuf @ 0x%08lx\n", addr);
	cb = (void *)addr;
	printf("data = %08lx\nsize %i, mask 0x%04x\n",
	       (unsigned long)cb->b, cb->size, cb->mask);
	printf("nw %i (%04x), nr %i (%04x)\n", cb->nw, cb->nw, cb->nr, cb->nr);
	printf("last written:");
	last = cb->nw;
	i = last - 9;

	do {
		printf(" %02x", cb->b[i & cb->mask] & 0xff);
		i++;
	} while ((i & cb->mask) != (last & cb->mask));
	printf("\n");
	return 0;
}
