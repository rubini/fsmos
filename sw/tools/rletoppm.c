#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#endif

uint16_t colors[16]; /* bgr565be */
int ncols;

void putpixel(int col)
{
	col &= 0xf;
	col = ntohs(colors[col]);
	putchar((col & 0x001f) << 3);
	putchar((col & 0x07e0) >> 3);
	putchar((col & 0xf100) >> 8);
}


int main(int argc, char **argv)
{
	int i, x, y;
	uint16_t val;

	if (argc > 1) {
		fprintf(stderr, "%s: please pass pgm into stdin channel\n",
			argv[0]);
		exit(1);
	}
	/* Read size */
	if (scanf("%i, %i,", &x, &y) != 2)
		exit(1);
	printf("P6\n%i %i\n255\n", x, y);

	/* Read colormap */
	for (i = 0; i < ARRAY_SIZE(colors); i++) {
		if (scanf("%hx,", colors + i) != 1)
			exit(10+i);
	}

	/* read pixels */
	while (scanf("%hx,", &val) == 1) {
		if (val & 0xf) {
			for (i = 0; i < (val & 0xfff); i++)
				putpixel(val >> 12);
		} else {
			putpixel(val >> 12);
			putpixel(val >> 8);
			putpixel(val >> 4);
		}
	}
	return 0;
}
