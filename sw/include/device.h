/*
 * This was born as async_io or async_dev, but it's actually a standard
 * device with read and write methods. Designed for uart, may be reused
 */
#ifndef __DEVICE_H__
#define __DEVICE_H__
#include <cbuf.h>

struct device {
	struct cbuf *icb;
	struct cbuf *ocb;
	void *driver_data;
	int (*init)(struct device *dev);
	int (*exit)(struct device *dev);
	int (*write)(struct device *dev,  unsigned long flags,
			   const void *src, int count);
};

/* init and exit return 0 on success, as usual */
static inline int dev_init(struct device *dev)
{
	return dev->init ? dev->init(dev) : 0;
}
static inline int dev_exit(struct device *dev)
{
	return dev->exit ? dev->exit(dev) : 0;
}

/* read is just acting on the buffer, write must do something more */
static inline int dev_read(struct device *dev,  unsigned long flags,
			   void *dest, int count)
{
	return cbuf_read(dev->icb, flags, dest, count);
}
static inline int dev_write(struct device *dev,  unsigned long flags,
			   const void *src, int count)
{
	return dev->write(dev, flags, src, count);
}


#endif /* __DEVICE_H__ */
