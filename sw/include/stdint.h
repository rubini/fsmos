/*
 * We like to build with -nostdinc to be safe. Fake stdint.h...
 */
#ifndef __STDINT_H__
#define __STDINT_H__

typedef unsigned char	uint8_t;
typedef unsigned short	uint16_t;
#ifdef CONFIG_ARCH_64BITS
typedef unsigned int	uint32_t;
#else
typedef unsigned long	uint32_t;
#endif
typedef unsigned long long uint64_t;
typedef signed char	int8_t;
typedef signed short	int16_t;
typedef signed long	int32_t;
typedef signed long long int64_t;

#if CONFIG_ARCH_BITS == 16
typedef uint16_t	intptr_t;
#elif CONFIG_ARCH_BITS == 32
typedef uint32_t	intptr_t;
#else
typedef uint64_t	intptr_t;
#endif

typedef intptr_t size_t;

#endif /* __STDINT_H__ */
