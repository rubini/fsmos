#include <io.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <usb.h>
#include <usb-eth.h>

/* This is the setup callback */
static int ueth_setup_cb(struct usb_device *ud, void *buf, int len, int error)
{
	struct usb_setup *s = buf;

	if (0)
		printf("%s: %02x %02x %04x %04x %04x\n", __func__,
		       s->bRequestType,
		       s->bRequest,
		       s->wValue,
		       s->wIndex,
		       s->wLength);

	switch(s->bRequest) {

	default:
		printf("unhandled request %02x\n", s->bRequest);
	}

	return 0;
}

/* rd/recv and wr/send are from our POV, "out" and "in" from the host POV */
static uint8_t ueth_buffer[CONFIG_USBETH_BSIZE];
static int ueth_bsize;
static int ueth_rd_error;

/* Output callback: we got a frame from the host */
static int ueth_out_cb(struct usb_device *ud, void *buf, int len, int error)
{
	int i;
	uint8_t *b = buf;
	static int partial_len;

	if (error && error != EAGAIN) {
		ueth_rd_error = error > 0 ? error : -error; /* positive errno */
		return 0;
	}

	for (i = 0; 0 && i < len; i++)
		printf("%02x%c", b[i], i == len - 1 ? '\n' : ' ');
	if (len + partial_len > CONFIG_USBETH_BSIZE)
		len = CONFIG_USBETH_BSIZE - partial_len;
	memcpy(ueth_buffer + partial_len, buf, len);
	if (error == EAGAIN) {
		partial_len += len;
	} else {
		ueth_bsize = partial_len + len;
		partial_len = 0;
	}
	return 0;
}

/* Input callback: our frame went through */
static int ueth_in_cb(struct usb_device *ud, void *buf, int len, int error)
{
	if (0)
		printf("%s\n", __func__);
	return 0;
}

/* Configuration: we use usb-eth identifiers */
static struct usb_config usbc = {
	.vendor = 0x49f,
	.device = 0x505a,
	.devrelease = 0x0100,
};

/* Device: configuration and callbacks */
static struct usb_device usbd = {
	.cfg = &usbc,
	.setup_cb = ueth_setup_cb,
	.ep1_out_cb = ueth_out_cb,
	.ep1_in_cb = ueth_in_cb,
};

/*
 * External API
 */
static struct usbeth *usbeth_device; /* unfortunately, only one */

struct usbeth *usbeth_init(struct usbeth *userue)
{
	struct usbeth *ue;

	if (userue->ud == &usbd)
		return NULL; /* EBUSY: do not re-init */
	ue = userue; /* remember it */
	ue->ud = &usbd;

	usb_init(&usbd); /* Error check? */
	usbeth_device = ue;
	return ue;
}

/* Read methods work as polling too */
void usbeth_poll(struct usbeth *ue)
{
	int vbus;

	usb_poll(ue->ud);

	if (ue->ud->state == USB_DETACHED) {
		vbus = usb_attach(ue->ud, 0);
		if (vbus)
			usb_attach(ue->ud, 1);
		return;
	}
	vbus = usb_attach(ue->ud, 1); /* bah: no polling without setting */

	if (!vbus) {
		usb_attach(ue->ud, 0);
		return;
	}

	if (ue->ud->state != USB_CONFIGURED)
		return;

	/* Submit? */
}

static int usbeth_can_tx = 0; /* until we received */

/* Send is just a submit (FIXME: wait for previous ack) */
int usbeth_send(struct usbeth *ue, const void *buf, int len)
{
	if (!usbeth_can_tx)
		return -EAGAIN;
	usb_ep1_in_submit(ue->ud, buf, len);
	return len;
}

/* Recv: copy from temporary buffer */
int usbeth_recv(struct usbeth *us, void *buf, int len)
{
	usbeth_poll(us);

	if (len > ueth_bsize)
		len = ueth_bsize;
	if (!len)
		return len;
	memcpy(buf, ueth_buffer, len);
	ueth_bsize = 0;
	usbeth_can_tx = 1;
	return len;
}

/* And the following ones can be used in net_ll.tx and net_ll.rx */

static uint8_t usbeth_frame_buf[CONFIG_USBETH_BSIZE];

int usbeth_frame_rx(struct ethhdr *hdr, uint8_t *payload, uint32_t buf_size)
{
	int i;
	uint8_t *buf = usbeth_frame_buf;
	int size = sizeof(usbeth_frame_buf);

	if (!CONFIG_HAS_USB)
		return 0;

	i = usbeth_recv(usbeth_device, buf, size);
	if (i <= 0)
		return i; /* most likely nothing there yet */
	//printf("%s: size %i, got %i\n", __func__, size, i);
	size = i - sizeof(*hdr);
	if (size < 0)
		return -1;
	if (size > buf_size)
		size = buf_size;
	memcpy(hdr, buf, sizeof(*hdr));
	buf += sizeof(*hdr);
	memcpy(payload, buf, size);
	return size; /* not including header */
}

int usbeth_frame_tx(struct ethhdr_vlan *hdr, uint8_t *payload, uint32_t len)
{
	uint8_t *buf = usbeth_frame_buf;
	int size = sizeof(usbeth_frame_buf);
	int hsize = sizeof(struct ethhdr); /* ignore possible vlan */

	if (!CONFIG_HAS_USB)
		return 0;

	//printf("%s:    %i\n", __func__, (int)len);
	if (len + hsize > size)
		return -1;
	memcpy(buf, hdr, hsize);
	buf += hsize;
	memcpy(buf, payload, len);
	usbeth_send(usbeth_device, usbeth_frame_buf, hsize + len);
	return hsize + len;
}
