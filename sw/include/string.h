extern char *strcpy(char *d, char *s);
extern int strcmp(const char *s1, const char *s2);
extern int strlen(const char *s);
extern int strnlen(const char *s, int count);
extern void *memcpy(void *d, const void *s, int count);
extern void *memset(void *d, int c, int count);
extern int memcmp(const void *d, const void *s, int count);
extern const char *strerror(int errnum);
