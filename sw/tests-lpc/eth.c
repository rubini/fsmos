#include <io.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <panic.h>
#include <usb-eth.h>

/* A generic thing that uses usb-serial to actually test it */

static struct usbeth usbeth = {
}, *ue = &usbeth;

static unsigned char packet[] = {
	/* hardwired eth header */
	[0] = 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/* dest: broadcast */
	[6] = 0x02, 0x33, 0x44, 0x55, 0x66, 0x77,	/* source: us */
	[12] = 0x12, /* was 8 */ 0x00,			/* proto: crap */

	[14] = 0x41, 0x42, 0x43, 0x44, 0x45, 0x46	/* temporary hack */
};



void main(void)
{
	static char buffer[128];
	unsigned long j = jiffies + HZ;
	int i;

	if (!CONFIG_HAS_USB)
		panic(0, "No USB in this CPU\n");

	ue = usbeth_init(ue);
	if (!ue)
		panic(0, "Can't init usb ethernet\n");

	while (1) {
		i = usbeth_recv(ue, buffer, sizeof(buffer));
		if (i)
			printf("received %i bytes\n", i);

		if (time_before(jiffies, j))
			continue;

		usbeth_send(ue, packet, sizeof(packet));
		j += 2 * HZ;
	}
}
