#ifndef __IRQ_H__
#define __IRQ_H__
#include <errno.h>
#include <command.h>

/* This is the vector target. A weak symbol in boot.S calls irq_entry() */
extern void __irq_entry(void);

/* Called by the above weak default. It is weak in boot.S and panics*/
extern void irq_entry(int irq, uint32_t timestamp_fraction, uint32_t sp);

/* This is a short assembly function that panics. Or calls irq_diag()  */
extern void irq_unexpected(int irq, uint32_t timestamp_fraction, uint32_t sp);

/* A verbose replacement for irq_unexpected (called by boot.S) */
extern void irq_diag(int irq, uint32_t timestamp_fraction, uint32_t sp);

/* And svc_entry is another weak function in boot.S, that panics */
extern void svc_entry(void);

typedef void (*scheduler_f)(void *arg);
extern void __sched(scheduler_f sched, void *arg);

static inline void irq_enable(void)
{
	asm volatile("cpsie i");
}

static inline void irq_disable(void)
{
	asm volatile("cpsid i");
}

static inline int irq_ongoing(void)
{
	int ret;

	asm volatile("mrs %0, psr" : "=l" (ret));
	return ret & 0x3f;
}

/*
 * For the interrupt handler we follow best-practices.
 *
 * The return value is one of the three Linux values.
 */
enum irqreturn {
	IRQ_NONE                = (0 << 0),
	IRQ_HANDLED             = (1 << 0),
	IRQ_WAKE_THREAD         = (1 << 1),
};

typedef enum irqreturn irqreturn_t;
#define IRQ_RETVAL(x)   ((x) ? IRQ_HANDLED : IRQ_NONE)

/*
 * The data item can be used by the handler. It can be a pointer to
 * the object it works on, or whatever else. It should not be zero
 * because it is also used as a key  in irq_free().
 *
 * The second argument is the timestamp fraction we receive in the
 * lower level irq_enty() function: the interrupt handler can use it.
 */
typedef  irqreturn_t (*irq_handler_t)(void *data, uint32_t timestamp_fraction,
	uint32_t sp);

/*
 * This is the api for irq request and free. Please note that irq-acknowledge
 * step in the interrupt controller is done by the OS, not by the user.
 *
 * The irq number is the SoC irq (so e.g. 0..31, not the internal ones
 * like NMI and Timer tick).
 *
 * Flags is currently unused (must be zero), but we may add IRQ_SHARED or
 * other flags later. The function returns 0 or an error (e.g. -EBUSY)
 */
int irq_request(int irq, unsigned long flags, irq_handler_t func, void *data);
int irq_free(int irq, void *data);

/* a command to help in case if issues */
int command_irqstat(char **reply, int argc, char **argv);
#define COMMAND_IRQSTAT   {"irqstat", command_irqstat, 1, 1}

#endif /* __IRQ_H__ */
