#include <io.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <ps-korad.h>

int koradps_set_ch(struct koradps *ps, int ch)
{
	struct koradps_channel *c = ps->ch + ch;
	int docmd = 0; /* only send command if needed */
	char s[16];

	if (ch > ps->nchannels || ch > CONFIG_KORADPS_NCH)
		return -ENODEV;

	/* 0 is a special case, because internal fields are inited as 0 */
	if (c->_mV_set == 0 && c->_mA_set == 0)
		docmd = 1;

	if (docmd || c->mV_set != c->_mV_set) {
		if (c->mV_set < 0)
			return -EINVAL;
		sprintf(s, "VSET%i:%i.%03i\n", ch + 1,
			  c->mV_set / 1000, c->mV_set % 1000);
		uart_puts(ps->uart, s);
		c->_mV_set = c->mV_set;
	}
	if (docmd || c->mA_set != c->_mA_set) {
		if (c->mV_set < 0)
			return -EINVAL;
		sprintf(s, "ISET%i:%i.%03i\n", ch + 1,
			  c->mA_set / 1000, c->mA_set % 1000);
		uart_puts(ps->uart, s);
		c->_mA_set = c->mA_set;
	}
	return 0;
}

static int koradps_scanf(struct uart *uart)
{
	unsigned long j = jiffies + HZ/10 + 1;
	char s[10] = "";
	int i;
	char m1, m2, m3;

	/* get rid of leftover \r or \n */
	while (uart_pollc(uart) == '\r')
		;
	while (uart_pollc(uart) == '\n')
		;

	/* get and scan up to 3 decimal digits */
	while (time_before(jiffies, j)) {
		if (uart_polls_t(uart, s, "\r\n", sizeof(s)))
			break;
	}
	if (s[strlen(s)-1] == '\n')
		s[strlen(s)-1] = '\0';
	if (s[strlen(s)-1] == '\r')
		s[strlen(s)-1] = '\0';
	m1 = m2 = m3 = '0';
	if (sscanf(s, "%i.%c%c%c", &i, &m1, &m2, &m3) < 2)
		return -EINVAL;
	return i * 1000 + (m1-'0') * 100 + (m2-'0') * 10 + m3 - '0';
}

int koradps_get_ch(struct koradps *ps, int ch)
{
	struct koradps_channel *c = ps->ch + ch;
	char s[10] = "";

	if (ch > ps->nchannels || ch > CONFIG_KORADPS_NCH)
		return -ENODEV;

	sprintf(s, "VOUT%i?\n", ch + 1);
	uart_puts(ps->uart, s);
	c->mV_get = koradps_scanf(ps->uart);
	sprintf(s, "IOUT%i?\n", ch + 1);
	uart_puts(ps->uart, s);
	c->mA_get = koradps_scanf(ps->uart);
	return 0;
}

int koradps_getset_ch(struct koradps *ps, int ch)
{
	struct koradps_channel *c = ps->ch + ch;
	char s[10] = "";

	if (ch > ps->nchannels || ch > CONFIG_KORADPS_NCH)
		return -ENODEV;

	sprintf(s, "VSET%i?\n", ch + 1);
	uart_puts(ps->uart, s);
	c->mV_get = koradps_scanf(ps->uart);
	sprintf(s, "ISET%i?\n", ch + 1);
	uart_puts(ps->uart, s);
	c->mA_get = koradps_scanf(ps->uart);
	return 0;
}


/* ps-wide functions just loop in channels */
int koradps_set(struct koradps *ps)
{
	int i, ret;

	for (i = 0; i < ps->nchannels; i++) {
		ret = koradps_set_ch(ps, i);
		if (ret < 0)
			return ret;
	}
	return 0;
}

int koradps_get(struct koradps *ps)
{
	int i, ret;

	for (i = 0; i < ps->nchannels; i++) {
		ret = koradps_get_ch(ps, i);
		if (ret < 0)
			return ret;
	}
	return 0;
}

int koradps_getset(struct koradps *ps)
{
	int i, ret;

	for (i = 0; i < ps->nchannels; i++) {
		ret = koradps_getset_ch(ps, i);
		if (ret < 0)
			return ret;
	}
	return 0;
}
