#include <panic.h>
#include <time.h>
#include <io.h>

#ifdef CONFIG_LPC112x /* This code only works on 1124/1125, the multi-uart ones */

/* Ok, uart0 is already inited, but let's test the data structure */
const struct uart dev_uart0 = {
	.baseaddr = REGBASE_UART0,
	.baudrate = 38400, /* likely different from .config */
	.enable_reg = REG_AHBCLKCTRL,
	.enable_bit = REG_AHBCLKCTRL_UART0,
	.pin_tx = GPIO_NR(1,7), .af_tx = 1,
	.pin_rx = GPIO_NR(1,6), .af_rx = 1,
	.clock_div = REG_UART0CLKDIV,
};

const struct uart dev_uart1 = {
	.baseaddr = REGBASE_UART1,
	.baudrate = 19200, /* different */
	.enable_reg = REG_AHBCLKCTRL,
	.enable_bit = REG_AHBCLKCTRL_UART1,
	.pin_tx = GPIO_NR(0,6), .af_tx = 3,
	.pin_rx = GPIO_NR(0,7), .af_rx = 3,
	.clock_div = REG_UART1CLKDIV,
};

const struct uart dev_uart2 = {
	.baseaddr = REGBASE_UART2,
	.baudrate = 9600, /* different */
	.enable_reg = REG_AHBCLKCTRL,
	.enable_bit = REG_AHBCLKCTRL_UART2,
	.pin_tx = GPIO_NR(1,8), .af_tx = 3,
	.pin_rx = GPIO_NR(0,3), .af_rx = 3,
	.clock_div = REG_UART2CLKDIV,
};

void main(void)
{
	uart_init(&dev_uart0);
	uart_init(&dev_uart1);
	uart_init(&dev_uart2);
	while (1) {
		uart_puts(&dev_uart0, "uart0\r\n");
		uart_puts(&dev_uart1, "uart1\r\n");
		uart_puts(&dev_uart2, "uart2\r\n");
		udelay(500 * 1000);
	}
}

#else
void main(void)
{
	panic(0, "Not a multi-uart microcontroller\n");
}
#endif
