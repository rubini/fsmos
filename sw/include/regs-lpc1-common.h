/*
 * Hardware registers we use, defined for use in the regs[] abstraction
 */
#ifndef __REGS_LPC1_COMMON_H__ /* Common to old and new devices (11xx/13xx) */
#define __REGS_LPC1_COMMON_H__

/* system control */
#define REG_SYSMEMREMAP		(0x40048000 / 4)
#define REG_PRESETCTRL		(0x40048004 / 4)
#define REG_PRESETCTRL_SSP0RSTN		0x01
#define REG_PRESETCTRL_I2CRSTN		0x02
#define REG_PRESETCTRL_SSP1RSTN		0x04
#define REG_SYSPLLCTRL		(0x40048008 / 4)
#define REG_SYSPLLSTAT		(0x4004800c / 4)
#define REG_USBPLLCTRL		(0x40048010 / 4)
#define REG_USBPLLSTAT		(0x40048014 / 4)
#define REG_SYSOSCCTRL		(0x40048020 / 4)
#define REG_SYSOSCCTRL_ENABLE		0x00 /* no bypass: use osc for quartz */
#define REG_SYSOSCCTRL_DISABLE		0x01 /* bypass oscillator: ext signal */
#define REG_SYSOSCCTRL_FREQ_1_20	0x00 /* 1-20 MHz */
#define REG_SYSOSCCTRL_FREQ_15_25	0x02 /* 15-25 MHz */
#define REG_WDTOSCCTRL		(0x40048024 / 4)
#define REG_IRCCTRL		(0x40048028 / 4)
#define REG_SYSRESSTAT		(0x40048030 / 4)
#define REG_SYSPLLCLKSEL	(0x40048040 / 4)
#define REG_SYSPLLCLKSEL_IRC		0x0
#define REG_SYSPLLCLKSEL_OSC		0x1
#define REG_SYSPLLCLKUEN	(0x40048044 / 4)
#define REG_USBPLLCLKSEL	(0x40048048 / 4)
#define REG_USBPLLCLKUEN	(0x4004804c / 4)

#define REG_MAINCLKSEL		(0x40048070 / 4)
#define REG_MAINCLKUEN		(0x40048074 / 4)
#define REG_SYSAHBCLKDIV	(0x40048078 / 4)
#define REG_SYSAHBCLKCTRL	REG_AHBCLKCTRL /* shorter name, see later */
#define REG_SSP0CLKDIV		(0x40048094 / 4)
#define REG_UARTCLKDIV		(0x40048098 / 4)
#define REG_UART0CLKDIV		(0x40048098 / 4) /* alt name for 112x */
#define REG_SSP1CLKDIV		(0x4004809c / 4)
#define REG_UART1CLKDIV		(0x400480a0 / 4) /* 112x */
#define REG_UART2CLKDIV		(0x400480a4 / 4) /* 112x */
#define REG_TRACECLKDIV		(0x400480ac / 4) /* not on 11ux */
#define REG_SYSTICKCLKDIV	(0x400480b0 / 4) /* not on 11ux */
#define REG_USBCLKSEL		(0x400480c0 / 4)
#define REG_USBCLKUEN		(0x400480c4 / 4)
#define REG_USBCLKDIVN		(0x400480c8 / 4)
#define REG_USBCLKDIV		(0x400480c8 / 4)
   /* ... */
#define REG_BODCTRL		(0x40048150 / 4)
#define REG_BODCTRL_RSTLEV0		0x00
#define REG_BODCTRL_RSTLEV1		0x01
#define REG_BODCTRL_RSTLEV2		0x02
#define REG_BODCTRL_RSTLEV3		0x03
#define REG_BODCTRL_RSTENA		0x10

#define REG_USBCLKCTRL		(0x40048198 / 4)
#define REG_USBCLKST		(0x4004819c / 4)
   /* ... */
#define REG_PDRUNCFG		(0x40048238 / 4)
#define REG_PDRUNCFG_IRCOUT		(1 <<  0)
#define REG_PDRUNCFG_IRC		(1 <<  1)
#define REG_PDRUNCFG_FLASH		(1 <<  2)
#define REG_PDRUNCFG_BOD		(1 <<  3)
#define REG_PDRUNCFG_ADC		(1 <<  4)
#define REG_PDRUNCFG_SYSOSC		(1 <<  5)
#define REG_PDRUNCFG_WDTOSC		(1 <<  6)
#define REG_PDRUNCFG_SYSPLL		(1 <<  7)
#define REG_PDRUNCFG_USBPLL		(1 <<  8)
#define REG_PDRUNCFG_USBPAD		(1 << 10)

/* watchdog */
#define REG_WDMOD		(0x40004000 / 4)
#define REG_WDTC		(0x40004004 / 4)
#define REG_WDFEED		(0x40004008 / 4)

/* uart */
#define REG_U0THR		(0x40008000 / 4) /* write */
#define REG_U0RBR		(0x40008000 / 4) /* read */
#define REG_U0IER		(0x40008004 / 4)
#define REG_U0IIR		(0x40008008 / 4) /* read */
#define REG_U0FCR		(0x40008008 / 4) /* write */
#define REG_U0LCR		(0x4000800c / 4)
#define REG_U0LSR		(0x40008014 / 4)
#define REG_U0LSR_RDR		0x01
#define REG_U0LSR_THRE		0x20
#define REG_U0LSR_TEMT		0x40

#define REG_U0DLL		(0x40008000 / 4) /* when DLAB=1 */
#define REG_U0DLM		(0x40008004 / 4) /* when DLAB=1 */
#define REG_U0FDR		(0x40008028 / 4) /* fractional divider */

/* uart again, but with base+offet convention */
#define REGBASE_UART0		(0x40008000 / 4)
#define REGOFF_UARTTHR		(0x000 / 4) /* write */
#define REGOFF_UARTRBR		(0x000 / 4) /* read */
#define REGOFF_UARTIER		(0x004 / 4)
#define REGOFF_UARTIIR		(0x008 / 4) /* read */
#define REGOFF_UARTFCR		(0x008 / 4) /* write */
#define REGOFF_UARTLCR		(0x00c / 4)
#define REGOFF_UARTLSR		(0x014 / 4)
#define REGOFF_UARTLSR_RDR	0x01
#define REGOFF_UARTLSR_THRE	0x20
#define REGOFF_UARTLSR_TEMT	0x40
#define REGOFF_UARTDLL		(0x000 / 4) /* when DLAB=1 */
#define REGOFF_UARTDLM		(0x004 / 4) /* when DLAB=1 */
#define REGOFF_UARTFDR		(0x028 / 4) /* fractional divider */

/* clock control */
#define REG_AHBCLKCTRL		(0x40048080 / 4)
#define REG_AHBCLKCTRL_SYS	(1 << 0)
#define REG_AHBCLKCTRL_ROM	(1 << 1)
#define REG_AHBCLKCTRL_RAM	(1 << 2)
#define REG_AHBCLKCTRL_FLASHR	(1 << 3)
#define REG_AHBCLKCTRL_FLASHA	(1 << 4)
#define REG_AHBCLKCTRL_I2C	(1 << 5)
#define REG_AHBCLKCTRL_GPIO	(1 << 6)
#define REG_AHBCLKCTRL_CT16B0	(1 << 7)
#define REG_AHBCLKCTRL_CT16B1	(1 << 8)
#define REG_AHBCLKCTRL_CT32B0	(1 << 9)
#define REG_AHBCLKCTRL_CT32B1	(1 << 10)
#define REG_AHBCLKCTRL_SSP0	(1 << 11)
#define REG_AHBCLKCTRL_UART	(1 << 12)
#define REG_AHBCLKCTRL_UART0	(1 << 12) /* 112x have multiple uarts */
#define REG_AHBCLKCTRL_ADC	(1 << 13)
#define REG_AHBCLKCTRL_USBREG	(1 << 14)
#define REG_AHBCLKCTRL_WDT	(1 << 15)
#define REG_AHBCLKCTRL_IOCON	(1 << 16)
   /* nothing at 17 */
#define REG_AHBCLKCTRL_SSP1	(1 << 18)
#define REG_AHBCLKCTRL_PINT	(1 << 19)
#define REG_AHBCLKCTRL_UART1	(1 << 19) /* 112x */
#define REG_AHBCLKCTRL_UART2	(1 << 20) /* 112x */
   /* ... */
#define REG_AHBCLKCTRL_SRAM1	(1 << 26)
#define REG_AHBCLKCTRL_USBRAM	(1 << 27)

/* counter 0 */
#define REG_TMR32B0_BASE	(0x40014000 / 4)
#define REG_TMR32B0IR		(0x40014000 / 4)
#define REG_TMR32B0TCR		(0x40014004 / 4)
#define REG_TMR32B0TC		(0x40014008 / 4)
#define REG_TMR32B0PR		(0x4001400c / 4)
#define REG_TMR32B0PC		(0x40014010 / 4)
#define REG_TMR32B0MCR		(0x40014014 / 4)
#define REG_TMR32B0MCR_MR0R		(1 << 1)
#define REG_TMR32B0MCR_MR1R		(1 << 4)
#define REG_TMR32B0MCR_MR2R		(1 << 7)
#define REG_TMR32B0MCR_MR3R		(1 << 10)
#define REG_TMR32B0MR0		(0x40014018 / 4)
#define REG_TMR32B0MR1		(0x4001401c / 4)
#define REG_TMR32B0MR2		(0x40014020 / 4)
#define REG_TMR32B0MR3		(0x40014024 / 4)
#define REG_TMR32B0PWMC		(0x40014074 / 4)
#define REG_TMR32B0PWMC_PWMEN0		(1 << 0)
#define REG_TMR32B0PWMC_PWMEN1		(1 << 1)
#define REG_TMR32B0PWMC_PWMEN2		(1 << 2)
#define REG_TMR32B0PWMC_PWMEN3		(1 << 3)

/* counter 1 */
#define REG_TMR32B1_BASE	(0x40018000 / 4)
#define REG_TMR32B1IR		(0x40018000 / 4)
#define REG_TMR32B1TCR		(0x40018004 / 4)
#define REG_TMR32B1TC		(0x40018008 / 4)
#define REG_TMR32B1PR		(0x4001800c / 4)
#define REG_TMR32B1PC		(0x40018010 / 4)
#define REG_TMR32B1MCR		(0x40018014 / 4)
#define REG_TMR32B1MCR_MR0R		(1 << 1)
#define REG_TMR32B1MCR_MR1R		(1 << 4)
#define REG_TMR32B1MCR_MR2R		(1 << 7)
#define REG_TMR32B1MCR_MR3R		(1 << 10)
#define REG_TMR32B1MR0		(0x40018018 / 4)
#define REG_TMR32B1MR1		(0x4001801c / 4)
#define REG_TMR32B1MR2		(0x40018020 / 4)
#define REG_TMR32B1MR3		(0x40018024 / 4)
#define REG_TMR32B1PWMC		(0x40018074 / 4)
#define REG_TMR32B1PWMC_PWMEN0		(1 << 0)
#define REG_TMR32B1PWMC_PWMEN1		(1 << 1)
#define REG_TMR32B1PWMC_PWMEN2		(1 << 2)
#define REG_TMR32B1PWMC_PWMEN3		(1 << 3)

/* 16-bit counter 0 */
#define REG_TMR16B0_BASE	(0x4000c000 / 4)
#define REG_TMR16B0IR		(0x4000c000 / 4)
#define REG_TMR16B0TCR		(0x4000c004 / 4)
#define REG_TMR16B0TC		(0x4000c008 / 4)
#define REG_TMR16B0PR		(0x4000c00c / 4)
#define REG_TMR16B0PC		(0x4000c010 / 4)
#define REG_TMR16B0MCR		(0x4000c014 / 4)
#define REG_TMR16B0MCR_MR0R		(1 << 1)
#define REG_TMR16B0MCR_MR1R		(1 << 4)
#define REG_TMR16B0MCR_MR2R		(1 << 7)
#define REG_TMR16B0MCR_MR3R		(1 << 10)
#define REG_TMR16B0MR0		(0x4000c018 / 4)
#define REG_TMR16B0MR1		(0x4000c01c / 4)
#define REG_TMR16B0MR2		(0x4000c020 / 4)
#define REG_TMR16B0MR3		(0x4000c024 / 4)
#define REG_TMR16B0PWMC		(0x4000c074 / 4)
#define REG_TMR16B0PWMC_PWMEN0		(1 << 0)
#define REG_TMR16B0PWMC_PWMEN1		(1 << 1)
#define REG_TMR16B0PWMC_PWMEN2		(1 << 2)
#define REG_TMR16B0PWMC_PWMEN3		(1 << 3)

/* 16-bit counter 1 */
#define REG_TMR16B1_BASE	(0x40010000 / 4)
#define REG_TMR16B1IR		(0x40010000 / 4)
#define REG_TMR16B1TCR		(0x40010004 / 4)
#define REG_TMR16B1TC		(0x40010008 / 4)
#define REG_TMR16B1PR		(0x4001000c / 4)
#define REG_TMR16B1PC		(0x40010010 / 4)
#define REG_TMR16B1MCR		(0x40010014 / 4)
#define REG_TMR16B1MCR_MR0R		(1 << 1)
#define REG_TMR16B1MCR_MR1R		(1 << 4)
#define REG_TMR16B1MCR_MR2R		(1 << 7)
#define REG_TMR16B1MCR_MR3R		(1 << 10)
#define REG_TMR16B1MR0		(0x40010018 / 4)
#define REG_TMR16B1MR1		(0x4001001c / 4)
#define REG_TMR16B1MR2		(0x40010020 / 4)
#define REG_TMR16B1MR3		(0x40010024 / 4)
#define REG_TMR16B1PWMC		(0x40010074 / 4)
#define REG_TMR16B1PWMC_PWMEN0		(1 << 0)
#define REG_TMR16B1PWMC_PWMEN1		(1 << 1)
#define REG_TMR16B1PWMC_PWMEN2		(1 << 2)
#define REG_TMR16B1PWMC_PWMEN3		(1 << 3)

/* Same as above, but as offsets (used in generic pwm driver) */
#define REGOFF_TMRIR		(0x000 / 4)
#define REGOFF_TMRTCR		(0x004 / 4)
#define REGOFF_TMRTC		(0x008 / 4)
#define REGOFF_TMRPR		(0x00c / 4)
#define REGOFF_TMRPC		(0x010 / 4)
#define REGOFF_TMRMCR		(0x014 / 4)
#define REGOFF_TMRMCR_MR0R		(1 << 1)
#define REGOFF_TMRMCR_MR1R		(1 << 4)
#define REGOFF_TMRMCR_MR2R		(1 << 7)
#define REGOFF_TMRMCR_MR3R		(1 << 10)
#define REGOFF_TMRMR0		(0x018 / 4)
#define REGOFF_TMRMR1		(0x01c / 4)
#define REGOFF_TMRMR2		(0x020 / 4)
#define REGOFF_TMRMR3		(0x024 / 4)
#define REGOFF_TMRPWMC		(0x074 / 4)
#define REGOFF_TMRPWMC_PWMEN0		(1 << 0)
#define REGOFF_TMRPWMC_PWMEN1		(1 << 1)
#define REGOFF_TMRPWMC_PWMEN2		(1 << 2)
#define REGOFF_TMRPWMC_PWMEN3		(1 << 3)

/* SPI: we use base and offset */
#define REGOFF_SSPCR0		(0x000 / 4)
#define REGOFF_SSPCR1		(0x004 / 4)
#define REGOFF_SSPDR		(0x008 / 4)
#define REGOFF_SSPSR		(0x00C / 4)
#define REGOFF_SSPSR_TFE		0x0001
#define REGOFF_SSPSR_TNF		0x0002
#define REGOFF_SSPSR_RNE		0x0004
#define REGOFF_SSPSR_RFF		0x0008
#define REGOFF_SSPSR_BSY		0x0010
#define REGOFF_SSPCPSR		(0x010 / 4)
#define REGOFF_IMSC		(0x014 / 4)
#define REGOFF_IMSC_RORIM		0x0001
#define REGOFF_IMSC_RTIM		0x0002
#define REGOFF_IMSC_RXIM		0x0004
#define REGOFF_IMSC_TXIM		0x0008
#define REGOFF_RIS		(0x018 / 4)
#define REGOFF_MIS		(0x01c / 4)
#define REGOFF_ICR		(0x020 / 4)

/* ADC */
#define REG_ADCCR		(0x4001c000 / 4)
#define REG_ADCCR_SELMASK		0xff
#define REG_ADCCR_SELMASK_SHIFT		0
#define REG_ADCCR_CLKDIV		(0xff << 8)
#define REG_ADCCR_CLKDIV_SHIFT		8
#define REG_ADCCR_BURST			(1 << 16)
#define REG_ADCCR_LOWPOWER		(1 << 22)
#define REG_ADCCR_10BIT			(1 << 23)
#define REG_ADCCR_START_NOW		(0x01 << 24)
#define REG_ADCCR_START_16B0CAP0	(0x02 << 24)
#define REG_ADCCR_START_32B0CAP0	(0x03 << 24)
#define REG_ADCCR_START_32B0MAT0	(0x04 << 24)
#define REG_ADCCR_START_32B0MAT1	(0x05 << 24)
#define REG_ADCCR_START_16B0MAT0	(0x06 << 24)
#define REG_ADCCR_START_16B0MAT1	(0x07 << 24)
#define REG_ADCCR_RISINGEDGE		(0 << 27)
#define REG_ADCCR_FALLINGEDGE		(1 << 27)
#define REG_ADCGDR		(0x4001c004 / 4)
#define REG_ADCGDR_CHN			(7 << 24)
#define REG_ADCGDR_CHN_SHIFT		24
#define REG_ADCDR(x)		(0x4001c010 / 4 + (x))
/* DR below is both for GDR and DRx */
#define REG_ADCDR_DATA			0xfff0 /* use as 16-bit value */
#define REG_ADCDR_OVERRUN		(1 << 30)
#define REG_ADCDR_DONE			(1 << 31)
#define REG_ADCSTAT		(0x4001c030 / 4)
#define REG_ADCSTAT_DONEMASK		0xff
#define REG_ADCSTAT_DONEMASK_SHIFT	0
#define REG_ADCSTAT_OVERRUNMASK		0xff00
#define REG_ADCSTAT_OVERRUNMASK_SHIFT	8

/* ADC is different in 112x devices; define the registers I use */
#define REG_ADCCR_112x_CLKDIV		0xff
#define REG_ADCCR_CALMODE		(1 << 30)
#define REG_ADCSEQA		(0x4001c008 / 4)
#define REG_ADCSEQA_CHANNELS		(0x1fe)
#define REG_ADCSEQA_START		(1 << 26)
#define REG_ADCSEQA_ENA			(1 << 31)
#define REG_ADCDATx(x)		(0x4001c020 / 4 + (x))
#define REG_ADCDAT_DATA			0xfff0 /* use as 16-bit value */
#define REG_ADCDAT_OVERRUN		(1 << 30)
#define REG_ADCDAT_DONE			(1 << 31)

/* I2C */
#define REG_I2CCONSET		(0x40000000 / 4)
#define REG_I2CCON_AA			0x04
#define REG_I2CCON_SI			0x08
#define REG_I2CCON_STO			0x10
#define REG_I2CCON_STA			0x20
#define REG_I2CCON_I2EN			0x40
#define REG_I2CSTAT		(0x40000004 / 4)
#define REG_I2CDAT		(0x40000008 / 4)
#define REG_I2CADR0		(0x4000000c / 4)
#define REG_I2CSCLH		(0x40000010 / 4)
#define REG_I2CSCLL		(0x40000014 / 4)
#define REG_I2CCONCLR		(0x40000018 / 4)
#define REG_I2CMMCTRL		(0x4000001c / 4)
#define REG_I2CMMCTRL_ENA		0x01
#define REG_I2CMMCTRL_ENASCL		0x02
#define REG_I2CMMCTRL_MATCHALL		0x04
#define REG_I2CADR1		(0x40000020 / 4)
#define REG_I2CADR2		(0x40000024 / 4)
#define REG_I2CADR3		(0x40000028 / 4)
#define REG_I2CDATABUF		(0x4000002c / 4)
#define REG_I2CMASK0		(0x40000030 / 4)
#define REG_I2CMASK1		(0x40000034 / 4)
#define REG_I2CMASK2		(0x40000038 / 4)
#define REG_I2CMASK3		(0x4000003c / 4)

/* NVIC -- the "1" registers only in cores with > 32 irq sources */
#define REG_NVIC_ISER0		(0xe000e100 / 4)
#define REG_NVIC_ISER1		(0xe000e104 / 4)
#define REG_NVIC_ICER0		(0xe000e180 / 4)
#define REG_NVIC_ICER1		(0xe000e184 / 4)
#define REG_NVIC_ISPR0		(0xe000e200 / 4)
#define REG_NVIC_ISPR1		(0xe000e204 / 4)
#define REG_NVIC_ICPR0		(0xe000e280 / 4)
#define REG_NVIC_ICPR1		(0xe000e284 / 4)
#define REG_NVIC_IABR0		(0xe000e300 / 4)
#define REG_NVIC_IABR1		(0xe000e304 / 4)
/* and we ignore the priority stuff */

#define __NVIC_INTERNAL_INTERRUPTS	16

/*
 * Support for extra serial ports -- same structure as stm32.
 * I only know of 1124/1125 with multiple uart, but this makes no harm
 */
struct uart {
	uint32_t baseaddr;
	uint32_t baudrate;
	uint32_t enable_reg;
	uint32_t enable_bit;
	int pin_tx, af_tx;
	int pin_rx, af_rx;
	int clock_div;
};
extern const struct uart dev_uart0, *console;
extern const struct uart dev_uart1, dev_uart2;

#endif /*  __REGS_LPC1_COMMON_H__ */
