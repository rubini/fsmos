/*
 * A lockless circular buffer, based on n-written and n-read
 */
#ifndef __CBUF__H__
#define __CBUF_H__
#include <time.h>
#include <errno.h>
#include <command.h>

typedef uint16_t cbuf_size_t; /* so I can test both 16 and 32 */

struct cbuf {
	uint8_t *b;
	cbuf_size_t size, mask;
	cbuf_size_t nr, nw;
	unsigned long lastw; /* jiffies - approx by 1 */
};

/* a helper to create both data space and a consistent size/mask */
#define DECLARE_CBUF(_name, _size) \
	/* size & (size-1) is zero only for powers of 2: build_bug_on */ \
	static uint8_t _name ## _data [_size - 2 * (_size & (_size - 1))]; \
	struct cbuf _name = { \
		.b = _name ## _data, \
		.size = _size, \
		.mask = _size - 1, \
	}

/* trivial: store a byte (no overflow check: it's up to the reader) */
static inline void cbuf_putc(struct cbuf *cbuf, int c)
{
	int offset = cbuf->nw++ & cbuf->mask;
	cbuf->b[offset] = c;
}

/* trivial: store a byte and timestamp */
static inline void cbuf_putc_stamp(struct cbuf *cbuf, int c, unsigned long j)
{
	cbuf_putc(cbuf, c);
	cbuf->lastw = j | 1;
}

/* trivial: empty check and size check */
static inline int cbuf_is_empty(struct cbuf *cbuf)
{
	return cbuf->nr == cbuf->nw;
}
static inline int cbuf_datasize(struct cbuf *cbuf)
{
	return cbuf->nw - cbuf->nr;
}

/* trivial: update read-count in case of overflow */
static inline void cbuf_fix_overflow(struct cbuf *cbuf)
{
	while (cbuf->nr + cbuf->size < cbuf->nw)
		cbuf->nr += cbuf->size;
}

/* trivial: __getc assume there's one byte */
static inline int __cbuf_getc(struct cbuf *cbuf)
{
	int offset = cbuf->nr & cbuf->mask;

	cbuf->nr++;
	return cbuf->b[offset];
}

/* trivial: getc and ungetc (without overflow check) */
static inline int cbuf_getc(struct cbuf *cbuf)
{
	if (cbuf_is_empty(cbuf))
		return -EAGAIN;
	return __cbuf_getc(cbuf);
}
static inline void cbuf_ungetc(struct cbuf *cbuf)
{
	cbuf->nr--;
}

/* trivial: look at a byte, don't consume it (no overflow chk) */
static inline int cbuf_peek(struct cbuf *cbuf)
{
	int offset = cbuf->nr & cbuf->mask;

	if (cbuf_is_empty(cbuf))
		return -EAGAIN;
	return cbuf->b[offset];
}

/* check for minimum data size or idle time after data */
static inline int cbuf_data_or_timeout(struct cbuf *cbuf, int mindata,
				       unsigned long idletime)
{
	if (cbuf_datasize(cbuf) >= mindata)
		return 1;
	if (cbuf_datasize(cbuf) == 0)
		return 0;
	if (time_after_eq(cbuf->lastw + idletime, jiffies))
		return 1;
}

/* read may return -EAGAIN, write may return -ENOSPC unless forced */
extern int cbuf_read(struct cbuf *cbuf, unsigned long flags,
		     void *dest, int count);
extern int cbuf_write(struct cbuf *cbuf, unsigned long flags,
		      const void *src, int count);

#define CBUF_R_PEEKONLY 0x0001
#define CBUF_W_FORCE	0x0100 /* happily overflow, don't return -ENOSPC */
#define CBUF_W_ONLCR	0x0200 /* output: nl (\n) adds cr (\r): text uart */

/* a command to dump buffer information (needs hex address) */
int command_cbuf(char **reply, int argc, char **argv);
#define COMMAND_CBUF   {"cbuf", command_cbuf, 2, 2}

#endif /* __CBUF_H__ */
