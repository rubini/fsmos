#ifndef __MCP3428_H__
#define __MCP3428_H__
#include <i2c.h>

/*
 * This "driver" offers a data-based approach, to support
 * periodic acquisition and possibly a task. Sure you
 * can do without this stuff, which has some overhead binary
 * size and execution time. It only supports one-shot mode.
 */

struct mcp3428_channel {
	unsigned long tstamp; /* last successful acquisition, jiffies */
	uint8_t chflags, status; /* status as returned by device */
	int16_t raw; /* This is alway 16bit signed, driver shifts up */
	int32_t chvalue; /* Result of conversion, user-defined */
	int32_t (*convert)(struct mcp3428_channel *ch, int chnum);
};

/* Channel flags */
#define MCP3428_GAIN_1			0x00
#define MCP3428_GAIN_2			0x01
#define MCP3428_GAIN_4			0x02
#define MCP3428_GAIN_8			0x03
#define MCP3428_12B_240SPS		(0x00 << 2)
#define MCP3428_14B_60SPS		(0x01 << 2)
#define MCP3428_16B_15SPS		(0x02 << 2)
#define MCP3428_DISABLED_CHANNEL	0x80
/* Same with a shorter name */
#define MCP3428_12B			MCP3428_12B_240SPS
#define MCP3428_14B			MCP3428_14B_60SPS
#define MCP3428_16B			MCP3428_16B_15SPS

/* Channel selection, and not-ready, used by the driver */
#define MCP3428_SETCH(ch)		(((ch) & 3) << 5)
#define MCP3428_BUSY			0x80

/* Overall device */
struct mcp3428_dev {
	struct i2c_dev *i2c;
	short addr7; /* usually 0x68 */
	short fsm_iter; /* driver-internal */
	struct mcp3428_channel ch[4];
};

/* These two can be placed in a task, and get the  mcp3428_dev as argument */
extern int mcp3428_init(void *);
extern void *mcp3428_fsm(void *);

#endif /* __MCP3428_H__ */
