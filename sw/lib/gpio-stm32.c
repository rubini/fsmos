/*
 * GPIO interface for STM32 devices
 * Alessandro Rubini, 2021 -- GNU GPL2 or later
 */
#include <errno.h>
#include <init.h>

/*
 * What follows is the public interface.
 * Note that only gpio_dir_af() checks the gpio is valid. Other
 * functions are expected to be called often and only after setting the mode.
 */

void gpio_init(void)
{
}
static int hw_gpio_init(void)
{
	regs[REG_GPIO_EN] |= GPIO_PORTS_MASK << BITS_GPIO_EN_SHIFT;
	return 0;
}
core_initcall(hw_gpio_init);

void gpio_dir(int gpio, int output, int value)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);
	int baseaddr = REGBASE_GPIO_PORT(port);
	uint32_t reg;

	if (output)
		gpio_set(gpio, value);

	/* mode: 00 = input, 01 = output, 10 = AF, 11 = analog */
	reg = regs[baseaddr + REGOFF_GPIO_MODER];
	reg &= ~(3 << (2 * bit));
	if (output)
		reg |= (1 << (2 * bit));
	regs[baseaddr + REGOFF_GPIO_MODER] = reg;
}

int gpio_dir_af(int gpio, int output, int value, int afnum)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);
	int baseaddr = REGBASE_GPIO_PORT(port);
	int pull, analog;
	uint32_t reg, ra;

	if (gpio < 0 || bit > 15) /* FIXME: GPIO_MAX support */
		return -EINVAL;

	pull = (afnum & (GPIO_AF_PULLDOWN | GPIO_AF_PULLUP)) >> 4;
	analog = afnum & GPIO_AF_ANALOG;
	afnum &= 0x0f; /* max 15 alternate functions */

	/* First, pullup/down */
	reg = regs[baseaddr + REGOFF_GPIO_PUPDR];
	reg &= ~(3 << (2 * bit));
	reg |= (pull << (2 * bit));
	regs[baseaddr + REGOFF_GPIO_PUPDR] = reg;;

	if (!afnum && !analog) {
		gpio_dir(gpio, output, value);
	} else {
		/* set AF number first, then AF mode */
		if (bit < 8)
			ra = REGOFF_GPIO_AFRL;
		else
			ra = REGOFF_GPIO_AFRH;
		reg = regs[baseaddr + ra];
		reg &= ~(0xf << (4 * (bit & 0x7)));
		reg |= (afnum << (4 * (bit & 0x7)));
		regs[baseaddr + ra] = reg;

		reg = regs[baseaddr + REGOFF_GPIO_MODER];
		reg &= ~(3 << (2 * bit));
		reg |= (2 << (2 * bit));
		regs[baseaddr + REGOFF_GPIO_MODER] = reg;
	}

	/* FIXME: analog mode not supported */

	return 0;
}

/* The following functions don't check the gpio value, for speed */
int gpio_get(int gpio)
{
	return __gpio_get(gpio) ? 1 : 0;
}

void gpio_set(int gpio, int value)
{
	return __gpio_set(gpio, value);
}
