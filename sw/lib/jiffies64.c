/*
 * This is based on how jiffies is build out of jiffies16 for stm32
 */
#include <time.h>

union {
	jiffies64_t j64;
	struct {
		uint32_t jl;
		uint32_t jh;
	};
} jiffies64;


jiffies64_t get_jiffies64(void)
{
	uint32_t newj = jiffies;
	uint32_t oldj = jiffies64.jl;

	jiffies64.jl = newj;
	if (time_before(newj, oldj))
		jiffies64.jh++;
	return jiffies64.j64;
}

