/*
 * The organization and part of the structures come from
 *  wrpc-sw:3df9509 -- include/ptpd_netif.h
 */
#ifndef __NET_H__
#define __NET_H__
#include <stdint.h>

/* arp, bootp, ping, one udp port = 4; then be conservative */
#define NET_MAX_SOCKETS 8
#define NET_MAX_SKBUF_SIZE 512 /* We have small RAM space */

#define SOCK_UDP           0
#define SOCK_RAW_ETHERNET  1

#define ETH_HEADER_SIZE 14
#define ETH_ALEN 6

#define vlan_number 0 /* Build vlan code in, but do not emit instructions */

#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#error "Only little-endian is supported by now; please fix ntohs etc"
#endif
static inline uint16_t ntohs(uint16_t s) {return (s >> 8) | (s << 8);}
static inline uint16_t htons(uint16_t s) {return (s >> 8) | (s << 8);}
static inline uint32_t ntohl(uint32_t s)  { \
	return (uint32_t)ntohs(s >> 16) | ((uint32_t)ntohs(s) << 16);}
static inline uint32_t htonl(uint32_t s)  { \
	return (uint32_t)htons(s >> 16) | ((uint32_t)htons(s) << 16);}


#ifdef CONFIG_NET_VERBOSE
#define NET_IS_VERBOSE 1
#else
#define NET_IS_VERBOSE 0
#endif
#define net_verbose(...) \
        ({if (NET_IS_VERBOSE) printf(__VA_ARGS__);})

static inline int min(int a, int b) {return a < b ? a : b;}

int net_update_rx_queues(void); /* to be polled */

struct ethhdr {
	uint8_t dstmac[6];
	uint8_t srcmac[6];
	uint16_t ethtype;
};

struct ethhdr_vlan {
	uint8_t dstmac[6];
	uint8_t srcmac[6];
	uint16_t ethtype;
	uint16_t tag;
	uint16_t ethtype_2;
};

struct sockaddr {
	uint8_t mac[6];
	uint8_t mac_dest[6];
	uint16_t ethertype;
	uint16_t udpport;
        uint16_t vlan;
};

struct sockq {
        uint16_t head, tail, avail, size;
        uint16_t n;
        uint8_t *buff;
};

struct socket {
	struct sockaddr bind_addr;
	uint8_t local_mac[6];
	struct sockq queue;
};

struct socket *socket_create(struct socket *s,
			     const struct sockaddr *bind_addr,
			     int udp_or_raw, int udpport);
int socket_close(struct socket *sock);

int socket_sendto(const struct socket *s, const struct sockaddr *to,
		  void *data, int len);
int socket_recvfrom(struct socket *s, struct sockaddr *from,
		    void *data, int len);


/* The following structure must be provided by the application */
struct net_ll {
	int (*rx)(struct ethhdr *hdr, uint8_t * payload, uint32_t buf_size);
	int (*tx)(struct ethhdr_vlan *hdr, uint8_t * payload, uint32_t size);
	int (*get_mac)(uint8_t *mac);
};

extern struct net_ll net_ll;

#endif /* __NET_H__ */
