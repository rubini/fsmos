/*
 * xterm display on alfio function prototypes and global declarations.
 * This file will be hopefully moved to lib/ when stable
 */
#ifndef __ALFIO_XTERM_H__
#define __ALFIO_XTERM_H__

#include <font.h>

#define MAX_GLYPH_WIDTH 16

struct spi_dev;

struct xterm_data {
	/* Number of rows */
	unsigned short int nrows;
	/* Number of columns */
	unsigned short int ncols;
	/* Pixel size */
	unsigned short wid, hei;
	/* Current x, y */
	unsigned short int currx;
	unsigned short int curry;
	/* Current foreground color */
	uint16_t fg_color;
	/* Current background color */
	uint16_t bg_color;
	/* Reset gpio number */
	int rst_gpio;
	/* Chip select gpio number */
	int cs_gpio;
	/* DC gpio number */
	int dc_gpio;
	/* xterm mode (bits) */
#define XTERM_MODE_PORTRAIT	0x00
#define XTERM_MODE_LANDSCAPE	0x01
#define XTERM_MODE_ROTATE180	0x02
	int xterm_mode;
	/* Pointer to current font */
	const struct font *font;
	/* Buffer used to send a row of pixels */
	union {
		unsigned char c[2 * MAX_GLYPH_WIDTH];
		uint16_t w[MAX_GLYPH_WIDTH];
	} glyph_row_buf;
	/* Pointer to spi device */
	struct spi_dev *xterm_dev;
};

/*
 * Initialize display
 *
 * @data: pointer to an instance's data
 * @dev: pointer to spi device
 * @rst: number of reset gpio or -1
 * @mode: either XTERM_MODE_PORTRAIT or XTERM_MODE_LANDSCAPE
 *
 * xterm_init() initializes the display using @data.
 */
int xterm_init(struct xterm_data *data, struct spi_dev *dev, int rst, int mode);

/*
 * Blank screen
 *
 * @data: pointer to an instance's data
 */
int xterm_blank(struct xterm_data *data);

/*
 * Set font
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @font: pointer to font data struct
 */
int xterm_set_font(struct xterm_data *data, const struct font *f);

/*
 * Set foreground and background colors
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @fg: foreground color (bgr565)
 * @bg: background color (bgr565)
 */
void xterm_set_colors(struct xterm_data *data, uint16_t fg, uint16_t bg);

/*
 * Set foreground and background colors
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @fg: foreground color (rgb888)
 * @bg: background color (rgb888)
 */
void xterm_set_colors888(struct xterm_data *data, int fg, int bg);

/*
 * Set cursor position
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @x: cursor x position
 * @y: cursor y position
 */
int xterm_goto_xy(struct xterm_data *data, unsigned short x, unsigned short y);

/*
 * Print a character
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @c: character to be printed out
 */
int xterm_putc(struct xterm_data *data, char c);

/*
 * Print a string
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @s: pointer to string to be printed out. \n and \b are interpreted in @s.
 */
int xterm_puts(struct xterm_data *data, const char *s);

int xterm_draw_bgr565be(struct xterm_data *data, int x0, int y0,
		      const uint16_t *img);
/*
 * Draw an image
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @x0: initial X position
 * @x1: initial Y position
 * @img: an array of width and height followed by bgr565 big-endian pixels
 */

int xterm_draw_rle(struct xterm_data *data, int x0, int y0,
		      const uint16_t *img);
/*
 * Draw an image, RLE and LUT compressed.
 *
 * @data: pointer to a struct xterm_data, already initialized by xterm_init()
 * @x0: initial X position
 * @x1: initial Y position
 * @img: an array: width and height, 16 bgr565colors, then RLE pixels
 */


#endif /* __ALFIO_XTERM_H__ */
