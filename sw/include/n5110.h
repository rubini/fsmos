#ifndef __N5110_H__
#define __N5110_H__
#include <spi.h>
#include <font.h>

#define N5110_WID 84
#define N5110_HEI 48

/* This is meant to be read-only data */
struct n5110_cfg {
	int gpio_backlight; /* -1 if unconnected */
	int gpio_dc;
	const struct font *font;
	uint32_t font_flags;
};

/* This one has r/w fields */
struct n5110_dev {
	const struct n5110_cfg *cfg;
	struct spi_dev spi;
	int x, y; /* current position */
};

extern struct n5110_dev *n5110_create(struct n5110_dev *dev, int *errno);
extern void n5110_init(struct n5110_dev *dev);
extern void n5110_goto(struct n5110_dev *dev, int x, int y);
extern void n5110_puts(struct n5110_dev *dev, const char *s);

/* these are exported so we can test with different contrast etc values */
extern uint8_t n5110_init_commands[8];
extern int n5110_init_commands_len;

#endif /* __N5110_H__ */
