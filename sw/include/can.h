#ifndef __CAN_H__
#define __CAN_H__

struct can_cfg {
	int bitrate;
};

struct can_dev {
	struct can_cfg *cfg;
	unsigned long flags;
	int devn;
};

struct can_dev *can_create(struct can_dev *);
void can_destroy(struct can_dev *);
int can_send(struct can_dev *dev, uint32_t can_id, int len, void *data);
int can_recv(struct can_dev *dev, uint32_t *can_id, int len, void *data);

#endif /* __CAN_H__ */
