#ifndef __TIME_H__
#define __TIME_H__
#include <stdint.h>

#define HZ CONFIG_HZ

#ifndef jiffies
    #ifdef CONFIG_JIFFIES_16B
	extern volatile uint16_t jiffies;
    #else
	extern volatile unsigned long jiffies;
    #endif
#endif

/* macros for structure initialization time */
#define ms_to_jiffies_const(ms) (((ms) * HZ / 1000) ?: (ms) ? 1 : 0)
#define jiffies_to_ms_const(j) (((j) * 1000 + HZ/2) / HZ)

/* inline for type checking etc */
static inline unsigned long ms_to_jiffies(int ms)
{
	return ms_to_jiffies_const(ms);
}

static inline int jiffies_to_ms(unsigned long j)
{
	return jiffies_to_ms_const(j);
}


extern void __udelay(uint32_t usec);
static inline void udelay(uint32_t usec)
{
	if (__builtin_constant_p(usec) && !usec)
		return;
	__udelay(usec);
}

/* Get a 64 bit timestamp, by extending jiffies */
typedef uint64_t jiffies64_t;
extern jiffies64_t get_jiffies64(void);

#ifndef CONFIG_JIFFIES_16B
/* The following ones come from the kernel, but simplified */
#define time_after(a,b)         \
        ((long)(b) - (long)(a) < 0)
#define time_before(a,b)        time_after(b,a)
#define time_after_eq(a,b)      \
         ((long)(a) - (long)(b) >= 0)
#define time_before_eq(a,b)     time_after_eq(b,a)

#define time_in_range(a,b,c) \
        (time_after_eq(a,b) && \
         time_before_eq(a,c))

#else /* Jiffies is 16-bit */
#define time_after(a,b)         \
        ((int16_t)(b) - (int16_t)(a) < 0)
#define time_before(a,b)        time_after(b,a)
#define time_after_eq(a,b)      \
         ((int16_t)(a) - (int16_t)(b) >= 0)
#define time_before_eq(a,b)     time_after_eq(b,a)

#define time_in_range(a,b,c) \
        (time_after_eq(a,b) && \
         time_before_eq(a,c))

#endif

/* The timestamp is architecture-specific, but based on a consistent struct */
struct timestamp {
	uint32_t counts;
	uint32_t fraction;
};

/* And the functions, too, must obey the same prototype */
extern void timestamp_get(struct timestamp *t);
extern uint64_t timestamp_to_64(struct timestamp *t);
extern uint64_t timestamp_to_ns(struct timestamp *t);
extern uint64_t timestamp_get_64(void);
extern uint64_t timestamp_get_ns(void);

/* We may have the fraction already (e.g. irq) and want to complete it */
extern void timestamp_complete(struct timestamp *t, uint32_t frac);

#endif /* __TIME_H__ */
