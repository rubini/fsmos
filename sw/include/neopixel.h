#ifndef __NEOPIXEL_H__
#define __NEOPIXEL_H__
#include <stringify.h>
#include <stdint.h>

extern void neopix_rgb(uint8_t r, uint8_t g, uint8_t b);
extern void neopix_reset(void);
/* Warning: order in array is G, R, B */
extern void neopix_array(const uint8_t *a, int len, int fire);

/*
 * multi-gpio version, with 565 mode added and internal brightness-reduction
 */
struct neopix_bit_functions {
	void (*bit0)(void);
	void (*bit1)(void);
	void (*reset)(void);
};

extern int neopix_reduce; /* a bit shift */

extern void neopix_array_rgb(const struct neopix_bit_functions *b,
			     const uint8_t *rgb, const int nled);
extern void neopix_array_565(const struct neopix_bit_functions *b,
			     const uint16_t *pix, const int nled);

/* The following helpers are for the caller who writes raise/lower code */

#define CPU_FREQ_S		__stringify(CPU_FREQ)

#define NEOPIX_DEFINE_TIMES						\
	asm(".set __neopixel_count_1, " CPU_FREQ_S "/1000/1000/2 + 1\n"); \
	asm(".set __neopixel_count_0, " CPU_FREQ_S "/1000/1000/5 - 1\n")

#define NEOPIX_WAIT_T0						\
	asm(".rep __neopixel_count_0\n\tnop\n\t.endr\n")

#define NEOPIX_WAIT_T1						\
	asm(".rep __neopixel_count_1\n\tnop\n\t.endr\n")

#endif /* __NEOPIXEL_H__ */
