#include <io.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <usb.h>
#include <usb-serial.h>

static const uint8_t cp2101_properties[0x40] = { /* see AN571 */
	0x40, 0x00, /* offset 0: length */
	0x00, 0x01, /* offset 2: version 1.00 */
	0x01, 0x00, 0x00, 0x00, /* offset 4: service mask */
	0,0,0,0, /* offset 8: reserved */
	0x00, 0x02, 0x00, 0x00, /* offset 12: max tx queue is 512 */
	0x00, 0x02, 0x00, 0x00, /* offset 16: max rx queue is 512 */
	115200 & 0xff, (115200 >> 8) & 0xff, 115200 >> 16, 115200 >> 24,
	1,0,0,0, /* offset 24: kind: rs232 is number 1 */
	7,0,0,0, /* offset 28: caabilities */
	2,0,0,0, /* offset 32: settable parameters */
	0xff,0xff,0x3,0, /* offset 36: settable baud rates */
	8,0,0,0, /* offset 40: settable bitsize */
	[60] = 'a', 'b', 'c',
};

static const uint8_t cp2101_comm_status[0x13] = { /* see AN571 */
	0,0,0,0, /* errrors */
	0,0,0,0, /* holding reason */
	0,0,0,0, /* bytes in input queue */
	0,0,0,0, /* bytes in output queue */
	0,0,0, /* bah! */
};


/* This is the setup callback; we reply to CP2102 requests */
static int us_setup_cb(struct usb_device *ud, void *buf, int len, int error)
{
	struct usb_setup *s = buf;
	static uint32_t baud = 9600;
	static const uint16_t line_control = 0x800;
	static const uint8_t flow_control[16];
	static const uint16_t modem_status = 0x03;
	char tmp;

	if (0)
		printf("USB: %s: %02x %02x %04x %04x %04x\n", __func__,
		       s->bRequestType,
		       s->bRequest,
		       s->wValue,
		       s->wIndex,
		       s->wLength);
	else
		udelay(500); /* don't know why, but windows would be unhappy */

	/* See AN571 for protocol */
	switch(s->bRequest) {
	case 0x00: /* enable or disable: ignore */
	case 0x07: /* set modem handshake: ignore */
	case 0x12: /* purge: ignore */
	case 0x19: /* set chars: ignore */
	case 0x13: /* set flow: ignore */
	case 0x03: /* set line control: ignore (/me lazy) */
		usb_setup_reply(ud, NULL, 0);
		break;
	case 0x1d: /* get baud date */
		usb_setup_reply(ud, &baud, 4);
		break;
	case 0x04: /* get line control */
		usb_setup_reply(ud, &line_control, 2);
		break;
	case 0x14: /* get flow control */
		usb_setup_reply(ud, flow_control, 16);
		break;
	case 0x08: /* get modem status */
		usb_setup_reply(ud, &modem_status, 1);
		break;

	case 0x1e: /* set baud rate: we got 4 bytes */
		memcpy(&baud, ud->ep0_out_buffer, 4);
		printf("USB: baud now %li\n", baud);
		usb_setup_reply(ud, NULL, 0);
		break;
	case 0x0f: /* "get properties". Bah! */
		usb_setup_reply(ud, &cp2101_properties,
				sizeof(cp2101_properties));
		break;
	case 0x10: /* "get comm status". Bah! */
		usb_setup_reply(ud, &cp2101_comm_status,
				sizeof(cp2101_comm_status));
		break;
	case 0xff: /* fucking vendor driver */
		switch(s->wValue) {
		case 0x370b: /* eeprom part number? */
			tmp = 1;
			usb_setup_reply(ud, &tmp, 1);
			break;
		default:
			printf("USB: unhandled request %02x:%04x\n", s->bRequest,
			       s->wValue);
		}
		break;
	default:
		printf("USB: unhandled request %02x\n", s->bRequest);
	}

	return 0;
}

/* Note; "rd" and "wr" are from our POV, "out" and "in" from the host POV */

static struct usbser *us; /* Maybe the following should be inside the device */
static char us_rd_buffer[CONFIG_USBSERIAL_BSIZE];
static char us_wr_buffer[CONFIG_USBSERIAL_BSIZE];
static int us_rd_bsize;
static int us_wr_bsize;
static int us_rd_error;
static int us_wr_busy;

/* Output callback: we got data from the host, we accumulate in the buffer */
static int us_out_cb(struct usb_device *ud, void *buf, int len, int error)
{
	struct usbser *us = ud->client;
	char *us_b = us_rd_buffer + us_rd_bsize;
	int maxsize;
	unsigned long flags;

	if (error) {
		us_rd_error = error > 0 ? error : -error; /* positive errno */
		return 0;
	}
	maxsize = CONFIG_USBSERIAL_BSIZE - us_rd_bsize;
	if (len > maxsize) {
		len = maxsize;
		us_rd_error = ENOSPC;
	}
	memcpy(us_b, buf, len);
	us_rd_bsize += len;

	flags = us->flags; us->flags |= USBSER_FLAG_CBACK;
	if (us->flags & USBSER_FLAG_ECHO)
		usbser_write(us, buf, len);
	us->flags = flags;

	return 0;
}

/* Input callback: our frame went through. Proceed with busy/idle FSM */
static int us_in_cb(struct usb_device *ud, void *buf, int len, int error)
{
	unsigned long flags;
	struct usbser *us = ud->client;

	if (!us_wr_bsize) {
		us_wr_busy = 0;
		return 0;
	}
	/* send the current buffer content and keep busy on */
	flags = us->flags; us->flags |= USBSER_FLAG_CBACK;
	usb_ep1_in_submit(ud, us_wr_buffer, us_wr_bsize);
	us->flags = flags;

	us_wr_bsize = 0;
	return 0;
}

/* Configuration: we use CP2102 vendor and device ID */
static struct usb_config us_usbc = {
	.vendor = 0x10c4,
	.device = 0xea60,
	.devrelease = 0x0100,
};

/* Device: configuration and callbacks */
static struct usb_device us_usbd = {
	.cfg = &us_usbc,
	.setup_cb = us_setup_cb,
	.ep1_out_cb = us_out_cb,
	.ep1_in_cb = us_in_cb,
};

/*
 * External API
 */
struct usbser *usbser_init(struct usbser *userus)
{
	if (us->flags == USBSER_FLAG_INITED)
		return NULL; /* EBUSY: do not re-init */
	us = userus; /* remember it */
	us->ud = &us_usbd;
	us->flags &= USBSER_USER_FLAGS;

	usb_init(&us_usbd); /* Error check? */
	us_usbd.client = us;
	us->flags |= USBSER_FLAG_INITED;
	return us;
}

/* Read methods work as polling too */
static void usbser_poll(struct usbser *us)
{
	int vbus;

	usb_poll(us->ud);

	if (us->ud->state == USB_DETACHED) {
		vbus = usb_attach(us->ud, 0);
		if (vbus)
			usb_attach(us->ud, 1);
		return;
	}
	vbus = usb_attach(us->ud, 1); /* bah: no polling without setting */

	if (!vbus) {
		usb_attach(us->ud, 0);
		return;
	}

	if (us->ud->state != USB_CONFIGURED)
		return;

	/* If we failed writing last time, do it now */
	if (us_wr_bsize && !us_wr_busy) {
		us_wr_busy = 1;
		usb_ep1_in_submit(us->ud, us_wr_buffer, us_wr_bsize);
		us_wr_bsize = 0;
	}
	/* There no "output submit (for our read), as usb.c does it */

}

void usbser_flush(struct usbser *us)
{
	static char debugs[40];
	unsigned long timeout = jiffies + 1 + HZ / 1000;
	while (us_wr_bsize && time_before(jiffies, timeout)) {
		if (CONFIG_USBSER_IS_VERBOSE) {
			sprintf(debugs, "flush: bsize = %i\n", us_wr_bsize);
			console_puts(debugs);
		}
		usbser_poll(us);
	}
}
/* Write: fill the buffer and possibly send it out */
int usbser_write(struct usbser *us, const void *buf, int len)
{
	static char debugs[80];
	int maxlen = CONFIG_USBSERIAL_BSIZE - us_wr_bsize;

	if (CONFIG_USBSER_IS_VERBOSE) {
		sprintf(debugs, "len %i, maxlen %i\ndata: ", len, maxlen);
		console_puts(debugs);
		console_puts(buf);
	}

	/* FIXME: loop over if len > buffersize */
	if (len > maxlen) len = maxlen;

	memcpy(us_wr_buffer + us_wr_bsize, buf, len);
	us_wr_bsize += len;

	/* If we are not in a callback (e.g. echo mode), flush the buffer */
	if (!(us->flags & USBSER_FLAG_CBACK))
		usbser_flush(us);
	return len;
}



/* FIXME: rd_error (as accumulated ENOSPC) is not used */
int usbser_read(struct usbser *us, void *buf, int len)
{
	usbser_poll(us);

	if (len > us_rd_bsize)
		len = us_rd_bsize;
	if (!len)
		return -EAGAIN;
	memcpy(buf, us_rd_buffer, len);
	/* move back the trailing part, if any */
	us_rd_bsize -= len;
	memcpy(us_rd_buffer, us_rd_buffer + len, us_rd_bsize);
	return len;
}

/* consider \r like \n; if \r\n the reader will get an empty line too */
int usbser_gets(struct usbser *us, void *buf, int len)
{
	int i;

	usbser_poll(us);

	for (i = 0; i < us_rd_bsize; i++) {
		if (us_rd_buffer[i] == '\r')
			us_rd_buffer[i] = '\n';
		if (us_rd_buffer[i] == '\n')
			break;
	}
	if (i == us_rd_bsize)
		return -EAGAIN;
	i++; /* the newline itself */

	/* So, there is newline. If the user buffer won't fit, ENOSPC */
	if (i > len)
		return -ENOSPC;
	i = usbser_read(us, buf, i);
	if (i >= 0)
		((char *)buf)[i] = '\0';
	return i;
}

/* convert \n into \r\n untile end-of-string */
int usbser_puts(struct usbser *us, const char *s)
{
	while (us_wr_bsize < (CONFIG_USBSERIAL_BSIZE - 1) && *s) {
		if (*s == '\n')
			us_wr_buffer[us_wr_bsize++] = '\r';
		us_wr_buffer[us_wr_bsize++] = *s++;
	}

	/* Finally, use usbser_write to flush the buffer */
	usbser_write(us, "", 0);
	return 0;
}
