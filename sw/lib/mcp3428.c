#include <io.h>
#include <time.h>
#include <i2c.h>
#include <mcp3428.h>
#include <assert.h>

int32_t mcp3428_microvolts(struct mcp3428_channel *ch, int chnum)
{
	int32_t res;
	/*
	 * The raw value is from -2.048V (0x8000) to 2.048V (0x7fff):
	 * the LSB is 1/16 of a millivolt. 
	 */
	res = ch->raw * 1000 / 16;
	/* fortunately, the two low bits mean gain 1,2,4,8, use this */
	res <<= (ch->chflags & 0x03);
	return res;
}

/* Actually, there is no init: the caller has probed i2c already */
int mcp3428_init(void *data)
{
	struct mcp3428_dev *dev = data;

	/* We should return error if the data rate is too fast */
	assert (dev->i2c, 1, "mc3428 i2c uninitialized");
	return 0;
}

/* we always align to 16 bits, so shift if we acquired fewer bits */
static uint8_t sample_shift[4] = {4, 2, 0, 0};

void *mcp3428_fsm(void *data)
{
	int32_t (*convert_fn)(struct mcp3428_channel *ch, int chnum);
	struct mcp3428_dev *dev = data;
	int c, getres, srate;;
	struct mcp3428_channel *ch;
	uint8_t buf[3];

	/* Run in 8 loops: first ask for channel, then read result */
	c = dev->fsm_iter / 2;
	getres = dev->fsm_iter & 1;
	ch = dev->ch + c;

	if (ch->chflags & MCP3428_DISABLED_CHANNEL)
		goto next;
	if (!getres) { /* request */
		buf[0] = (ch->chflags & 0xf) /* gain and sample */
			| (c << 5) | 0x80; /* one-shot for this channel */
		if (0)
			printf("c %i write %02x\n", c, buf[0]);
		i2c_write(dev->i2c, dev->addr7, buf, 1, 0);
		goto next;
	}
	/* retrieve result and convert */
	dev->i2c->nak_count = 0;
	i2c_read(dev->i2c, dev->addr7, buf, 3, 0);
	if (0)
		printf("c %i read (%i nak)x\n", c, dev->i2c->nak_count);
	if (dev->i2c->nak_count)
		goto next;
	ch->tstamp = jiffies;
	ch->status = buf[2];
	srate = (ch->chflags >> 2) & 3;
	ch->raw = ((buf[0] << 8) | buf[1]) << sample_shift[srate];
	convert_fn = mcp3428_microvolts;
	if (ch->convert)
		convert_fn = ch->convert;
	ch->chvalue = convert_fn(ch, c);

next:
	dev->fsm_iter++;
	dev->fsm_iter &= 7;
	return dev;
}
