OUTPUT_FORMAT("elf64-x86-64")
OUTPUT_ARCH(i386:x86-64)
ENTRY(_entry)

MEMORY
{
    stack : ORIGIN = 0x10000000, LENGTH = 0x1000 /* 1 page  */
    ram :   ORIGIN = 0x10001000, LENGTH = 0x100000 - LENGTH(stack) /* 1M */
};

_fstack = ORIGIN(stack) + LENGTH(stack) - 4;
_stack = ORIGIN(stack);
regs = 0;

SECTIONS
{
	.stack : {
		KEEP(*(.stack));
	} > stack
	.text.boot : {
		*(.text.boot);
	} > ram
	.text : {
		*(.text .text.*)
	} > ram
	.rodata : {
		*(.rodata*)
		. = ALIGN(8);
		romcall_begin = .;
		romcall_end = .;
		initcall_begin = .;
		KEEP(*(.init1));
		KEEP(*(.init2));
		KEEP(*(.init3));
		KEEP(*(.init4));
		KEEP(*(.init5));
		KEEP(*(.init6));
		KEEP(*(.init7));
		KEEP(*(.init8));
		initcall_end = .;
	} > ram
	.data : {
		. = ALIGN(8);
		__task_begin = .;
		KEEP(*(.task0))
		KEEP(*(.task))
		. = ALIGN(8);
		__task_end = .;
		*(.data .data.*)
		*(.ramcode)
	} > ram
	.bss : {
		. = ALIGN(16);
		__bss_start = .;
		*(.bss .bss.* COMMON);
		. = ALIGN(16);
		__bss_end = .;
	} > ram
	/DISCARD/ : { *(.init0); }
	/DISCARD/ : { *(.note.gnu.build-id) }
}
