/* CPU-specific include files */

#define JIFFIES_ADDR 0x40094008 /* REG_TMR32B1TC == TIMER4 */

#ifndef __ASSEMBLER__
#include <regs-lpc17.h>
#include <gpio-lpc17.h>
#endif
