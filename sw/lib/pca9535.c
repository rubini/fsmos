#include <io.h>
#include <i2c.h>
#include <errno.h>
#include <gpio-pca9535.h>

static int __pca_rd16(struct pca9535 *pca, int basereg)
{
	int r0, r1;
	r0 = i2c_readreg(pca->dev, pca->addr7, basereg + 0);
	r1 = i2c_readreg(pca->dev, pca->addr7, basereg + 1);
	if (r0 < 0)
		return r0;
	if (r1 < 0)
		return r1;
	return (r1 << 8) | r0;
}

static int __pca_wr16(struct pca9535 *pca, int basereg, int val)
{
	int r0, r1, ret;
	r0 = val & 0xff;
	r1 = val >> 8;
	ret = i2c_writereg(pca->dev, pca->addr7, basereg + 0, r0);
	if (ret < 0)
		return ret;
	ret = i2c_writereg(pca->dev, pca->addr7, basereg + 1, r1);
	if (ret < 0)
		return ret;
	return 0;
}

/* creation: read current status and store it in pca struct */
struct pca9535 *pca9535_create(struct pca9535 *pca)
{
	int i, inbits, outbits;

	pca->dev->nak_count = 0;
	i = __pca_rd16(pca, PCA9535_DIRIN0);
	if (i < 0)
		return NULL;
	if (pca->dev->nak_count)
		return NULL;
	pca->inmask = i;

	inbits = __pca_rd16(pca, PCA9535_INPUT0);
	if (inbits < 0)
		return NULL;
	outbits = __pca_rd16(pca, PCA9535_INPUT0);
	if (outbits < 0)
		return NULL;
	pca->values = (inbits & pca->inmask) | (outbits & ~pca->inmask);
	return pca;
}

/* change all directions (and values) */
int pca9535_set_outmask(struct pca9535 *pca, int outmask, int values)
{
	int ret;

	pca->values = values;
	pca->inmask = outmask ^ 0xffff;
	/* first values, then direction */
	ret = __pca_wr16(pca, PCA9535_OUTPUT0, pca->values);
	if (ret < 0)
		return ret;
	ret = __pca_wr16(pca, PCA9535_DIRIN0, pca->inmask);
	if (ret < 0)
		return ret;
	return 0;
}

/* change one direction, with error check on arguments */
int pca9535_dir(struct pca9535 *pca, int gpio, int output, int value)
{

	int values, outmask;

	if (gpio < 0 || gpio > 15)
		return -EINVAL;
	if (output != GPIO_DIR_OUT && output != GPIO_DIR_IN)
		return -EINVAL;
	if (value < 0 || value > 1)
		return -EINVAL;

	values = (pca->values & ~(1 << gpio)) | (value << gpio);
	outmask = ((~pca->inmask & 0xffff) & ~(1 << gpio)) |
		((output == GPIO_DIR_OUT) << gpio);
	return pca9535_set_outmask(pca, outmask, values);
}

/* I/O of cached data */
int pca9535_rd_values(struct pca9535 *pca)
{
	int ret;

	ret = __pca_rd16(pca, PCA9535_INPUT0);
	if (ret < 0)
		return ret;
	pca->values = (pca->values & ~pca->inmask) | (ret &pca->inmask);
	return 0;
}

int pca9535_wr_values(struct pca9535 *pca)
{
	return __pca_wr16(pca, PCA9535_OUTPUT0, pca->values);
}

/* get and set, based on above ones, w/ error check on arguments */
int pca9535_get(struct pca9535 *pca, int gpio)
{
	int ret;

	if (gpio < 0 || gpio > 15)
		return -EINVAL;
	if ((pca->inmask & (1 << gpio)) == 0)
		return -EBUSY;

	ret = pca9535_rd_values(pca);
	if (ret < 0)
		return ret;
	return (pca->values >> gpio) & 1;
}

int pca9535_set(struct pca9535 *pca, int gpio, int value)
{
	if (value < 0 || value > 1)
		return -EINVAL;
	if (gpio < 0 || gpio > 15)
		return -EINVAL;
	if ((pca->inmask & (1 << gpio)) != 0)
		return -EBUSY;
	pca->values = (pca->values & ~(1 << gpio)) | (value << gpio);
	return pca9535_wr_values(pca);
}

/* Three aliases for the functions that can be used in mgpio */
int __attribute__((alias("pca9535_dir")))
   pca9535_mgpio_dir(void *dev, int gpio, int output, int value);
int __attribute__((alias("pca9535_get")))
   pca9535_mgpio_get(void *dev, int gpio);
int __attribute__((alias("pca9535_set")))
   pca9535_mgpio_set(void *dev, int gpio, int value);
