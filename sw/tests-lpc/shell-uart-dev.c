#include <io.h>
#include <time.h>
#include <string.h>
#include <command.h>
#include <device.h>
#include <uart-dev.h>
#include <errno.h>
#include <irq.h>

/* gpio pins of tdc board */
#define DIAG1			GPIO_NR(0,7)
#define DIAG2			GPIO_NR(1,28)
#define DIAG3			GPIO_NR(1,31)

/* no usb: we may have no oscillator */
uint32_t board_get_clksel(void)
{
	return REG_SYSPLLCLKSEL_IRC;
}

/*
 * Define the console device with its own driver_data -- we have diags
 */
static struct uart_driver_data ddata = {
	.baseaddr = REGBASE_UART0,
	.irqnr = LPC_IRQ_USART0,
	.gpio_irqtime = DIAG1,
	//.gpio_irqtoggle = DIAG2,
};

DECLARE_CBUF(cbuf_out, 128);
DECLARE_CBUF(cbuf_in, 64);

struct device console_dev = {
	.icb = &cbuf_in,
	.ocb = &cbuf_out,
	.driver_data = &ddata,
	.init = uartdev_init,
	.exit = uartdev_exit,
	.write = uartdev_write,
};

/* we want the first message to got straight, in case driver code fails */
int shell_is_async;

int puts(const char *s)
{
	if (!shell_is_async) /* e.g.: panic pre-init */
		return console_puts(s);

	return dev_write(&console_dev, CBUF_W_ONLCR, s, strlen(s));
}

static struct command shell_commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	COMMAND_COMMIT,
	COMMAND_STACK,
	COMMAND_HELP,
	{}
};

void main(void)
{
	char s[80] = {0,};
	char *sptr = s;

	gpio_dir_af(DIAG1, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	gpio_dir_af(DIAG2, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	gpio_dir_af(DIAG3, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);
	udelay(100 * 1000);
	gpio_set(DIAG1, 0);
	gpio_set(DIAG2, 0);
	gpio_set(DIAG3, 0);

	/* initialize the async device */
	if (dev_init(&console_dev) < 0)
		panic(3, "can't create console device\n");

	if (1)
		shell_is_async++; /* this enable async write */

	irq_enable();
	while (1) {
		if (dev_read(&console_dev, 0, sptr, 1) <= 0)
			continue;
		if (*sptr != '\n' && *sptr != '\r') {
			sptr++;
			continue;
		}
		command_parse(s, shell_commands);
		puts(command_reply);
		memset(s, 0, sizeof(s));
		sptr = s;
	}
}
