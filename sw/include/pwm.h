#ifndef __PWM_H__
#define __PWM_H__


struct pwm_hw; /* defined by the driver */

struct pwm_dev {
	int gpio;
	int cycle_mr;		/* LPC-specific, but I need it (set by user) */
	uint32_t clock_hz;	/* requested (set by user) */
	uint32_t real_clock_hz;	/* achieved (set by driver) */
	unsigned nclocks;	/* set by user at init time */
	unsigned t_on;		/* clocks: inited by user, changed by set() */

	/* following fields are private to the driver */
	const struct pwm_hw *hw;
	struct pwm_dev *next;
	uint32_t reg;
};

/*
 * can claim EINVAL if gpio or frequency is wrong, or EBUSY if timer
 * already in use by pwm with a different frequency
 */
extern struct pwm_dev *pwm_create(struct pwm_dev *dev, int *errno);

extern void pwm_destroy(struct pwm_dev *dev);

/*
 * Following calls offer no error check: clocks and ratio must fit 0..1.
 * They are all inline, because it's a few clock each. The "ratio" one
 * allows constant-math optimization, e.g. use a power of 2 for Ttot.
 */
static inline void __pwm_set(struct pwm_dev *dev)
{
	if (CONFIG_ARCH_IS_LPC) {
		regs[dev->reg] = dev->nclocks - dev->t_on;
	} else {
		extern int no_pwm_backend_for_this_arch;
		no_pwm_backend_for_this_arch = 1;
	}
}
static inline void pwm_set(struct pwm_dev *dev, int t_on)
{
	dev->t_on = t_on;
	return __pwm_set(dev);
}
static inline void pwm_ratio(struct pwm_dev *dev, int Ton, int Ttot)
{
	/* we assume nclocks fits 16 bits: no overflow expected */
	return pwm_set(dev, dev->nclocks * Ton / Ttot);
}

#endif /* __PWM_H__ */
