#include <errno.h>
#include <pwm.h>
#include <io.h>

#ifdef CONFIG_LPC112x
#  define CONFIG_IS_LPC112x 1
#else
#  define CONFIG_IS_LPC112x 0
#endif

struct pwm_timer {
	uint32_t mask; /* 16b or 32b bitmask */
	uint32_t reg_base;
	uint32_t clkctrl_bit;
};

/* Note: 17xx has the same tiemr block, but a different set of timers */
enum timer_id {
	TIMER_32B0,
	TIMER_32B1,
	TIMER_16B0,
	TIMER_16B1
};

static const struct pwm_timer pwm_timer_info[] = {
	[TIMER_32B0] = {0xffffffff, 0x40014000 / 4, REG_AHBCLKCTRL_CT32B0},
	[TIMER_32B1] = {0xffffffff, 0x40018000 / 4, REG_AHBCLKCTRL_CT32B1},
	[TIMER_16B0] = {0x0000ffff, 0x4000c000 / 4, REG_AHBCLKCTRL_CT16B0},
	[TIMER_16B1] = {0x0000ffff, 0x40010000 / 4, REG_AHBCLKCTRL_CT16B1},
};

static uint32_t pwm_mr_offset[] = {
	REGOFF_TMRMR0, REGOFF_TMRMR1, REGOFF_TMRMR2, REGOFF_TMRMR3,
};

struct pwm_hw {
	/* timer_id and match register index allow to find collisions, too */
	int timer_id;
	int mr_index;
	/* gpio is what the user is requesting */
	int gpio;
	int af_num;
};

static const struct pwm_hw pwm_hw_info_generic[] = {
#if !CONFIG_JIFFIES_USES_CT32B0
	{TIMER_32B0, 0, GPIO_NR(0, 18), GPIO_AF(2)}, /* pin 46 */
	{TIMER_32B0, 0, GPIO_NR(1, 24), GPIO_AF(1)}, /* pin 21 */
	{TIMER_32B0, 1, GPIO_NR(0, 19), GPIO_AF(2)}, /* pin 47 */
	{TIMER_32B0, 1, GPIO_NR(1, 25), GPIO_AF(1)}, /* pin 1 */
	{TIMER_32B0, 2, GPIO_NR(0,  1), GPIO_AF(2)}, /* pin 4 */
	{TIMER_32B0, 2, GPIO_NR(1, 26), GPIO_AF(1)}, /* pin 11 */
	{TIMER_32B0, 3, GPIO_NR(0, 11), GPIO_AF(3)}, /* pin 32 */
	{TIMER_32B0, 3, GPIO_NR(1, 27), GPIO_AF(1)}, /* pin 12 */
#endif
#if !CONFIG_JIFFIES_USES_CT32B1
	{TIMER_32B1, 0, GPIO_NR(0, 13), GPIO_AF(3)}, /* pin 34 */
	{TIMER_32B1, 1, GPIO_NR(0, 14), GPIO_AF(3)}, /* pin 35 */
	{TIMER_32B1, 2, GPIO_NR(0, 15), GPIO_AF(3)}, /* pin 39 */
	{TIMER_32B1, 3, GPIO_NR(0, 16), GPIO_AF(2)}, /* pin 40 */
#endif
	{TIMER_16B0, 0, GPIO_NR(0,  8), GPIO_AF(2)}, /* pin 27 */
	{TIMER_16B0, 0, GPIO_NR(1, 13), GPIO_AF(2)}, /* pin 36 */
	{TIMER_16B0, 1, GPIO_NR(0,  9), GPIO_AF(2)}, /* pin 28 */
	{TIMER_16B0, 1, GPIO_NR(1, 14), GPIO_AF(2)}, /* pin 37 */
	{TIMER_16B0, 2, GPIO_NR(0, 10), GPIO_AF(3)}, /* pin 29 */
	{TIMER_16B0, 2, GPIO_NR(1, 15), GPIO_AF(2)}, /* pin 43 */

	{TIMER_16B1, 0, GPIO_NR(0, 21), GPIO_AF(1)}, /* pin 17 */
	{TIMER_16B1, 1, GPIO_NR(0, 22), GPIO_AF(2)}, /* pin 30 */
	{TIMER_16B1, 1, GPIO_NR(1, 23), GPIO_AF(1)}, /* pin 18 */
};

/* LPC1124/1125 have different pins */
static const struct pwm_hw pwm_hw_info_112x[] = {
#if !CONFIG_JIFFIES_USES_CT32B0
	{TIMER_32B0, 2, GPIO_NR(0,  1), GPIO_AF(2)}, /* pin 4 */
	{TIMER_32B0, 3, GPIO_NR(0, 11), GPIO_AF(3)}, /* pin 30 */
	{TIMER_32B0, 0, GPIO_NR(1,  6), GPIO_AF(2)}, /* pin 46 */
	{TIMER_32B0, 1, GPIO_NR(1,  7), GPIO_AF(2)}, /* pin 47 */
	{TIMER_32B0, 0, GPIO_NR(2,  5), GPIO_AF(1)}, /* pin 20 */
	{TIMER_32B0, 1, GPIO_NR(2,  6), GPIO_AF(1)}, /* pin 1 */
	{TIMER_32B0, 2, GPIO_NR(2,  7), GPIO_AF(1)}, /* pin 11 */
	{TIMER_32B0, 3, GPIO_NR(2,  8), GPIO_AF(1)}, /* pin 12 */
#endif
#if !CONFIG_JIFFIES_USES_CT32B1
	{TIMER_32B1, 0, GPIO_NR(1,  1), GPIO_AF(3)}, /* pin 32 */
	{TIMER_32B1, 1, GPIO_NR(1,  2), GPIO_AF(3)}, /* pin 35 */
	{TIMER_32B1, 2, GPIO_NR(1,  3), GPIO_AF(3)}, /* pin 38 */
	{TIMER_32B1, 3, GPIO_NR(1,  4), GPIO_AF(2)}, /* pin 39 */
#endif
	{TIMER_16B0, 0, GPIO_NR(0,  8), GPIO_AF(2)}, /* pin 26 */
	{TIMER_16B0, 1, GPIO_NR(0,  9), GPIO_AF(2)}, /* pin 27 */
	{TIMER_16B0, 2, GPIO_NR(0, 10), GPIO_AF(3)}, /* pin 28 */
	{TIMER_16B0, 0, GPIO_NR(3,  0), GPIO_AF(2)}, /* pin 36 */

	{TIMER_16B1, 0, GPIO_NR(1,  9), GPIO_AF(1)}, /* pin 17 */
	{TIMER_16B1, 1, GPIO_NR(1, 10), GPIO_AF(2)}, /* pin 29 */
	{TIMER_16B1, 1, GPIO_NR(2,  4), GPIO_AF(1)}, /* pin 19 */
};

static struct pwm_dev *pwm_list_head;

struct pwm_dev *pwm_create(struct pwm_dev *dev, int *errno)
{
	struct pwm_dev *otherd;
	const struct pwm_hw *hw, *hwlist;
	const struct pwm_timer *timer;
	uint32_t reg_base, mr;
	int prescale, i, array_size;

	if (CONFIG_IS_LPC112x) {
		hwlist = pwm_hw_info_112x;
		array_size = ARRAY_SIZE(pwm_hw_info_112x);
	} else {
		hwlist = pwm_hw_info_generic;
		array_size = ARRAY_SIZE(pwm_hw_info_generic);
	}

	/* Find this device */
	*errno = ENODEV;
	for (i = 0, hw = NULL; i < array_size; i++)
		if (hwlist[i].gpio == dev->gpio)
			hw = hwlist + i;
	if (!hw)
		return NULL;;

	/* Look for conflicts */
	*errno = EINVAL;
	if (hw->mr_index == dev->cycle_mr)
		return NULL;
	*errno = EBUSY;
	for (otherd = pwm_list_head; otherd; otherd = otherd->next) {
		if (hw->timer_id == otherd->hw->timer_id) {
			/* can't be same match register */
			if (hw->mr_index == otherd->hw->mr_index)
				return NULL;
			/* can't rely on different cycle match register */
			if (dev->cycle_mr != otherd->cycle_mr)
				return NULL;
			/* can't have different frequency nor nclocks */
			if (dev->clock_hz != otherd->clock_hz)
				return NULL;
			if (otherd->nclocks != dev->nclocks)
				return NULL;
			break;
		}
	}
	dev->hw = hw;
	timer = pwm_timer_info + hw->timer_id;
	reg_base = timer->reg_base;

	if (otherd) {
		dev->real_clock_hz = otherd->real_clock_hz;
	} else {
		/* New timer: enable, prescaler... */
		regs[REG_AHBCLKCTRL] |= timer->clkctrl_bit;
		prescale = CPU_FREQ / dev->clock_hz;
		regs[reg_base + REGOFF_TMRTCR] = 1;
		regs[reg_base + REGOFF_TMRPR] = prescale - 1;
		dev->real_clock_hz = CPU_FREQ / prescale;

		/* ... prepare match register for complete cycle ... */
		mr = reg_base + pwm_mr_offset[dev->cycle_mr];
		regs[mr] = dev->nclocks - 1;
		/* ... and reset timer on "match-on-that-one" */
		regs[reg_base + REGOFF_TMRMCR] =
			REGOFF_TMRMCR_MR0R << (3 * dev->cycle_mr);
		/* reset counter block to start it again from 0 */
		regs[reg_base + REGOFF_TMRTCR] = 3;
		regs[reg_base + REGOFF_TMRTCR] = 1;
	}
	/* Prepare this match-register setup */
	dev->reg = reg_base + pwm_mr_offset[hw->mr_index];
	regs[dev->reg] = dev->nclocks - dev->t_on;
	regs[reg_base + REGOFF_TMRPWMC] |=
		REGOFF_TMRPWMC_PWMEN0 << hw->mr_index;
	/* Prepare gpio */
	gpio_dir_af(dev->gpio, GPIO_DIR_OUT, 0, hw->af_num);

	/* Done */
	dev->next = pwm_list_head;
	pwm_list_head = dev;
	return dev;
}

void pwm_destroy(struct pwm_dev *dev)
{
	struct pwm_dev **pptr;
	gpio_dir_af(dev->gpio, GPIO_DIR_IN, 0, GPIO_AF_GPIO);

	/* detach from the list */
	for (pptr = &pwm_list_head; *pptr; pptr = & (*pptr)->next) {
		if (*pptr == dev) {
			*pptr = dev->next;
			dev->next = NULL;
		}
	}
	regs[dev->reg + REGOFF_TMRPWMC] &=
		~(REGOFF_TMRPWMC_PWMEN0 << dev->hw->mr_index);

	/* do not turn off power or whatever, it's not fsmos' use case */
}
