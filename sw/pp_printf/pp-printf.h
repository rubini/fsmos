#include <stdarg.h>

/* These used to pp_printf but i had to turn them to standard names */
extern int printf(const char *fmt, ...)
        __attribute__((format(printf,1,2)));

extern int sprintf(char *s, const char *fmt, ...)
        __attribute__((format(printf,2,3)));

extern int vprintf(const char *fmt, va_list args);

extern int vsprintf(char *buf, const char *, va_list);

#define pp_printf printf /* so the examples still build */

/* This is what we rely on for output */
extern int puts(const char *s);


