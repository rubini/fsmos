#ifndef __LTC2428_H__
#define __LTC2428_H__
#include <spi.h>

/*
 * This code manages an array of ltc2428, with the same MUX.
 * It is described by an array of spi devices. First is mux
 */

struct ltc2428_result {
	int index; /* allows renumbering (used by caller) */
	char *name; /* optional (used by caller) */
	int error; /* caller can set to -EAGAIN when waiting for result */
	uint32_t raw;
	int32_t val;
	int32_t mVs; /* 16.16 fixed point */
	int32_t avg;
	int32_t cvt; /* converted by caller-provided function */
	int niter; /* internal, for average */
};

enum { /* steps for internal fsm */
	LTC2428_STEP_MUX = 0,
	LTC2428_STEP_DATA,
	LTC2428_STEP_WAIT,
};

struct ltc2428 {
	struct spi_dev *devs; /* an array starting with mux */
	struct ltc2428_result *res; /* 8 * ndev */
	void (*convert)(struct ltc2428_result *, int index);
	int ndev; /* actual devices, exluding mux */
	int miso_pin;
	int navg;
	/* internal data follows */
	int fsm_ch;
	int fsm_dev;
	int fsm_step;
};

extern int ltc2428_init(struct ltc2428 *);
extern void ltc2428_fsm(struct ltc2428 *);

#endif /* __LTC2428_H__ */
