/*
 * Serial protocol for the korad power supply.
 */
#ifndef __PS_KORAD_H__
#define __PS_KORAD_H__

/*
 * The power supply device-set has 1 or 2 channels. Let's use 1 by
 * now, to save space (mine has 1 only), we can Kconfig this later
 */
#define CONFIG_KORADPS_NCH 1

struct koradps_channel {
	short mV_set, mA_set, mV_get, mA_get;

	/* lib-internal fields, to check for changes */
	short _mV_set, _mA_set;
};

struct koradps {
	struct uart *uart;
	int nchannels;
	struct koradps_channel ch[CONFIG_KORADPS_NCH];
};


/*
 * Only act on a whole ps or channel, no need to do V-only or I-only.
 * The "get" function can be used as a check for -ENODEV,
 * and "getset" returns the current settings.
  */
extern int koradps_set(struct koradps *ps);
extern int koradps_get(struct koradps *ps);
extern int koradps_getset(struct koradps *ps);
extern int koradps_set_ch(struct koradps *ps, int ch);
extern int koradps_get_ch(struct koradps *ps, int ch);
extern int koradps_getset_ch(struct koradps *ps, int ch);

#endif /* __PS_KORAD_H__ */
