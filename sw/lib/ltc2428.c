#include <io.h>
#include <spi.h>
#include <ltc2428.h>

int ltc2428_init(struct ltc2428 *dev)
{
	int i;

	for (i = 0; i <= dev->ndev; i++) {
		if (spi_create(dev->devs + i) == NULL)
			return i + 1;
		/* set gpio inactive: 0 for mux, 1 for others */
		gpio_dir_af(dev->devs[1].cfg->gpio_cs, GPIO_DIR_OUT,
			    (i != 0), GPIO_AF_GPIO);
	}
	/* the clock input, if any, must be prepared by the caller */
	return 0;
}

static void ltc2428_convert(struct ltc2428 *dev)
{
	int i, value;
	struct ltc2428_result *res;

	for (i = 0, res = dev->res; i < 8 * dev->ndev; i++, res++) {

		/*
		 * Bit 21: sign (1 for positive)
		 * Bit 20: extended range (> Vref or < 0)
		 * Bits 19..0: converted value
		 */
		if (res->raw == 0) /* special case: minus 0 (!!) */
			res->raw = 0x200000;
		value = res->raw & 0x1fffff;
		if ((res->raw & (1L<<21)) == 0)
			value |= 0xffe00000;
		res->val = value;

		/* Now, (1<<20) == 1.024V -> (1<<10) = 1mV */
		res->mVs = res->val << 6;

		/* First iteration is discarded; then average */
		res->niter++;
		switch(res->niter) {
		case 1:
			res->avg = 0;
			continue;
		case 2:
			res->avg = res->mVs;
			continue;
		default:
			res->avg = (res->avg * (dev->navg - 1) + res->mVs)
				/ dev->navg;
		}

		/* So, we have a valid average. Ask the caller to convert */
		res->error = 0;
		dev->convert(res, i);
	}
}


void ltc2428_fsm(struct ltc2428 *dev)
{
	unsigned char indata[3];
	unsigned char outdata[1];
	struct ltc2428_result *res;
	struct spi_ibuf ibuf;
	struct spi_obuf obuf;
	int i;

	if (0) {
		printf("%i %i %i\n", dev->fsm_ch, dev->fsm_dev,
		       dev->fsm_step);
	}

	obuf.buf = outdata;
	ibuf.buf = indata;

	/*
	 * It looks like the convertion starts after the end of data
	 * phase. So there are three steps: write next mux; pick this
	 * data, and *then* wait for conversion done
	 */
	#define NEXT_CH(ch) ((ch + 1) % 8)

	switch(dev->fsm_step) {
	case LTC2428_STEP_MUX:
		if (0)
			printf("%i:\n", dev->fsm_ch);
		gpio_set(dev->devs[0].cfg->gpio_cs, 1); /* active */
		outdata[0] = 0x8 | NEXT_CH(dev->fsm_ch);
		obuf.len = 1;
		spi_xfer(dev->devs + 0, SPI_F_NOCS, NULL, &obuf);
		gpio_set(dev->devs[0].cfg->gpio_cs, 0); /* inactive */

		dev->fsm_step = LTC2428_STEP_DATA;
		dev->fsm_dev = 0;
		/* fall through: be fast */

	case LTC2428_STEP_DATA:
		while (dev->fsm_dev < dev->ndev) {
			ibuf.len = 3;
			ibuf.buf = indata;

			spi_xfer(dev->devs + (dev->fsm_dev + 1),
				 SPI_F_DEFAULT, &ibuf, NULL);

			res = dev->res + 8 * dev->fsm_dev + dev->fsm_ch;
			res->raw = (uint32_t)indata[0] << 16
				| indata[1] << 8 | indata[2];
			dev->fsm_dev++;
		}
		dev->fsm_step = LTC2428_STEP_WAIT;
		dev->fsm_dev = 0;

		if (0) {
			/* Verbosely report what we got, before waiting */
			for (i = 0; i < dev->ndev; i++) {
				res = dev->res + 8 * dev->fsm_dev + dev->fsm_ch;
				printf("data: %06x for ch %i\n",
				       (int)res->raw, (int)(res - dev->res));
			}
		}
		return;

	case LTC2428_STEP_WAIT: /* Loop: wait for all chips to be ready */
		while (dev->fsm_dev < dev->ndev) {
			/* activate CS of current device */
			gpio_set(dev->devs[1 + dev->fsm_dev].cfg->gpio_cs, 0);
			/* Poll for ~conversion-done bit */
			i = gpio_get(dev->miso_pin);
			gpio_set(dev->devs[1 + dev->fsm_dev].cfg->gpio_cs, 1);
			if (i == 1)
				return;
			dev->fsm_dev++;
		}

		/* Ok, new conversion is done. Convert after all channels */
		if (dev->fsm_ch == 7)
			ltc2428_convert(dev);
		dev->fsm_ch = NEXT_CH(dev->fsm_ch);
		dev->fsm_step = LTC2428_STEP_MUX;
		dev->fsm_dev = 0;
	default: /* never taken */
		return;
	}
}
