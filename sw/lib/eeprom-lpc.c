#include <eeprom-lpc.h>

static uint32_t lpcee_args[5], lpcee_ret[1];

/* same prototype as i2cee functions */
int lpcee_read(void * unused, int offset, void *buf, int size)
{
	lpcee_args[0] = IAP_CMD_EEPROM_RD;
	lpcee_args[1] = offset;
	lpcee_args[2] = (int)buf;
	lpcee_args[3] = size;
	lpcee_args[4] = CPU_FREQ / 1000;
	return iap_call(lpcee_args, lpcee_ret);
}

int lpcee_write(void *unused, int offset, const void *buf, int size)
{
	lpcee_args[0] = IAP_CMD_EEPROM_WR;
	lpcee_args[1] = offset;
	lpcee_args[2] = (int)buf;
	lpcee_args[3] = size;
	lpcee_args[4] = CPU_FREQ / 1000;
	return iap_call(lpcee_args, lpcee_ret);
}

