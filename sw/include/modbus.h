/*
 * Generic support for modbus over serial line, RTU mode
 */
#ifndef __MODBUS_H__
#define __MODBUS_H__
#include <stdint.h>

struct modbus_header { /* uart frame, for TCP we'll see */
	uint8_t address;
	uint8_t func;
};

struct modbus_req3 { /* section 6.3: read holding register */
	struct modbus_header h;
	uint16_t reg_name;
	uint16_t nregs;
	uint16_t crc;
} __attribute__((packed)) ;

struct modbus_req4 { /* section 6.4: read input register */
	struct modbus_header h;
	uint16_t reg_name;
	uint16_t nregs;
	uint16_t crc;
} __attribute__((packed)) ;

struct modbus_resp34 {
	struct modbus_header h;
	uint8_t byte_count;
	uint16_t data[];
	/* and crc */
} __attribute__((packed)) ;

struct modbus_req6 { /* section 6.6: write single register */
	struct modbus_header h;
	uint16_t reg_name;
	uint16_t reg_value;
	uint16_t crc;
} __attribute__((packed)) ;

struct modbus_resp6 { /* exact echo of request */
	struct modbus_header h;
	uint16_t reg_name;
	uint16_t reg_value;
	uint16_t crc;
} __attribute__((packed)) ;

struct modbus_req10 { /* section 6.12: write multiple registers */
	struct modbus_header h;
	uint16_t reg_name;
	uint16_t reg_count;
	uint8_t nbyte; /* braindead! */
	uint16_t data[];
	/* and crc */
} __attribute__((packed)) ;

struct modbus_resp10 {
	struct modbus_header h;
	uint16_t reg_name;
	uint16_t reg_count;
	uint16_t crc;
} __attribute__((packed)) ;

/* all values are big-endian, like network values */
static inline uint16_t ntohs(uint16_t s)
{
	return ((s >> 8) | (s << 8)) & 0xffff;
}
#define htons ntohs

#define MODBUS_READ   0x01
#define MODBUS_WRITE  0x02

/* Slave: define registers and/or counters */
struct modbus_register {
	uint16_t reg_name; /* only one or first one */
	uint8_t count;     /* if > 1, it is an array */
	uint8_t flags;     /* MODBUS_READ | MODBUS_WRITE (slave may use both) */
	uint16_t *reg_addr;
	/* the callback is invoked before reading or writing */
	int (*callback)(struct modbus_register *self,
			int regname, int action, int value);
};

struct modbus_device {
	int address;
	struct modbus_register *regarray;
	int regcount;
	void *data; /* the uart or whatever send/recv may need */
	int (*send)(struct modbus_device *dev, void *msg, int msglen,
		unsigned long when);
	unsigned long resp_delay; /* jiffies, for master it is max allowed */
	unsigned long last_byte; /* last tx or rx, to measure timeouts */
	unsigned long inter_frame; /* 3.5 symbol lenght, in theory */
	/* recv is only used by the master */
	int (*recv)(struct modbus_device *dev, void *msg, int msglen,
		unsigned long timeout);
};

uint16_t modbus_crc(void *msg, int msglen);
int modbus_crc_check(void *msg, int msglen, int okretval);


/*
 * Slave: the validation function returns one of:
 *      0 (incomplete -- better than -EGAIN)
 *      1 (complete with checksum ok, for us and supported)
 *      -EINVAL (format error or checksum error)
 *      -ENXIO (ok but not for us)
 *      -ENOENT (ok but unsupported request)
 * The caller should trim the message on error, use it on true, wait on false.
 */
int modbus_request_valid(struct modbus_device *dev, void *msg, int msglen);

/*
 * Slave: the parsing code returns 0 on success, or -EAGAIN if message
 * is incomplete.  Other errors may come from message_valid(), -ENOENT
 * for unsupported requests or anything non-0 returned by callback().
 * So the caller should trim the message on everything but -EAGAIN.
 */
int modbus_parse(struct modbus_device *dev, void *msg,
		 int msglen, int buflen);

/*
  * Like above, but we skip the request_valid() step: the message is valid
  * and must be trimmed after the fact
  */
int __modbus_parse(struct modbus_device *dev, void *msg,
		   int msglen, int buflen);

/* Master, currently only for testing: read or write a register or rset */
int modbus_master(struct modbus_device *dev, struct modbus_register *reg);

#endif /* __MODBUS_H__ */
