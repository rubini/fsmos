#ifndef __PARAM_H__
#define __PARAM_H__

/* The read-write structure, hosted in RAM. "int" so it's 16 bits in AVR */
struct param_values {
	int current;
	int measured;
	int saved; /* eeprom */
};

/* The read-only structure, can be const */
struct param_desc {
	char *tag;  /* a short name */
	char *desc; /* a sentence */
	unsigned long flags;
	int defval;
	struct param_values *vals;
};

/* And the overall description of a parameter group (there may be several) */
struct param_group {
	const struct param_desc *desc;
	void *eeprom;
	short eeprom_type, nparam;
	int eeoffset, version;
};
enum param_eeprom_type {
	PARAM_EEPROM_I2C = 0, /* default */
	PARAM_EEPROM_IAP,
	PARAM_SPI_FLASH,
	PARAM_CUSTOM_CODE,
};

#define PARAM_FLAG_INT     0x0001 /* Show as signed integer */
#define PARAM_FLAG_HEX     0x0002 /* Show as hex */
#define PARAM_FLAG_HIDDEN  0x0004 /* Do not show */
#define PARAM_FLAG_PRECIOUS 0x0008 /* Use saved value even if wrong version */
#define PARAM_FLAG_MEASURE 0x1000 /* The value is both set and measured */

/* Library functions, may need the above information */
extern int param_init(const struct param_group *);
extern void param_reset(const struct param_group *, int which);
#define PARAM_RESET_DEFAULT 0
#define PARAM_RESET_EEPROM 1
extern int param_load(const struct param_group *, int apply); /* from eeprom */
#define PARAM_LOAD_ONLY 0
#define PARAM_LOAD_APPLY 1
extern int param_save(const struct param_group *); /* to eeprom */
extern void param_list(const struct param_group *); /* tag and description */
extern void param_show(const struct param_group *, int all); /* show values */

/* If you base parameter in custom storage, define these */
int param_read_custom(void *dev, int offset, void *buf, int size);
int param_write_custom(void *dev, int offset, const void *buf, int size);


/* If you use commands, this may be what you want. Needs param_group pointer */
extern int param_generic_cmd(char **reply, int argc, char **argv);
extern const struct param_group *param_current_group;

/* Shortcuts to access a parameter, based on current group */
static inline int param_get_int(int param)
{
	return param_current_group->desc[param].vals->current;
}
static inline void param_set_int(int param, int value)
{
	param_current_group->desc[param].vals->current = value;
}


#endif /* __PARAM_H__ */
