#ifndef __I2C_H__
#define __I2C_H__

#include <stdint.h>

/* We only do bitbanging by now */
struct i2c_cfg {
	int gpio_sda;
	int gpio_scl;
	int usec_delay;
	/* HW_NONBLOCK and HW_OPENDRAIN are per-device */
	unsigned long flags;
};

struct i2c_dev {
	const struct i2c_cfg *cfg;
	int nak_count;
	/* The following keeps track of async operation */
	int actindex;
	int bufindex;
	int opindex;
	int value; /* the current value being shifted */
};

extern struct i2c_dev *i2c_create(struct i2c_dev *dev);

/* The following ones return 0 or negative error, or -EAGAIN */
extern int i2c_reset(struct i2c_dev *dev);
extern int i2c_write(struct i2c_dev *dev, int addr7, const uint8_t *buf,
		     int count, unsigned long flags);
extern int i2c_read(struct i2c_dev *dev, int addr7, uint8_t *buf,
		    int count, unsigned long flags);

#define I2C_FLAG_HW_NONBLOCK	0x01
#define I2C_FLAG_HW_OPENDRAIN	0x02
#define I2C_FLAG_HW_INTPULLUP	0x04
#define I2C_FLAG_NO_STOP	0x10
#define I2C_FLAG_NO_START	0x20
#define I2C_FLAG_REPSTART	0x40

/* Catch-all calls: they return negative error, or -EAGAIN, or unsigned ret */
extern int i2c_readreg(struct i2c_dev *dev, uint8_t addr7, uint8_t reg);
extern int i2c_writereg(struct i2c_dev *dev, uint8_t addr7, uint8_t reg,
			uint8_t val);

/* Multi-reg: first byte of buffer is starting register */
extern int i2c_readregs(struct i2c_dev *dev, uint8_t addr7, uint8_t *buf,
			int nregs);
extern int i2c_writeregs(struct i2c_dev *dev, uint8_t addr7,
			 const uint8_t *buf, int nregs);

#endif /* __I2C_H__ */
