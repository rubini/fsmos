/*
 * Trivial program to fix the checksum, needed for LPC1343
 * Alessandro Rubini, 2010 GNU GPL2 or later
 * -- added crc and "tail version" for use with IAP
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "../lib/crc32.c" /* awful! */

char buf[64 * 1024];

int main(int argc, char **argv)
{
    int i, fd;
    void *mapaddr;
    uint32_t sum=0, *data;
    struct stat st;

    if (argc != 2) {
	fprintf(stderr, "%s: pass a binary filename please\n", argv[0]);
	exit(1);
    }
    fd = open(argv[1], O_RDWR);
    if (fd < 0) {
	fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
	exit(1);
    }
    mapaddr = mmap(0, 0x20, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (mapaddr == (typeof(mapaddr))-1) {
	fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1], strerror(errno));
	exit(1);
    }
    data = mapaddr;

    for (i = 0; i < 7; i++)
	sum += data[i];
    data[i] = -sum;
    munmap(mapaddr, 0x20);

    /*
     * Then, to be able to store the binary in flash and verify it,
     * we crc32 the file and add both the crc and "1" (structure version)
     * to the end, after aligning at a 4-byte multiple.
     */
    if (stat(argv[1], &st) < 0) {
	fprintf(stderr, "%s: stat(%s): %s\n", argv[0], argv[1],
		strerror(errno));
	exit(1);
    }
    /* align to 8 (so crc and marker are on the same page) */
    i = st.st_size;
    lseek(fd, 0, SEEK_END);
    while (i%8) {
	write(fd, "", 1);
	i++;
    }
    st.st_size = i;

    /* readit all for crc32 */
    lseek(fd, 0, SEEK_SET);
    i = read(fd, buf, sizeof(buf));
    if (i != st.st_size) {
	fprintf(stderr, "%s: %s: read error (expected %i, read %i)\n",
		argv[0], argv[1], (int)st.st_size, i);
	exit(1);
    }
    sum = crc32(buf, i);

    /* write crc and version */
    lseek(fd, 0, SEEK_END);
    write(fd, &sum, sizeof(sum));
    sum = 1;
    write(fd, &sum, sizeof(sum));
    close(fd);
    exit(0);
}
