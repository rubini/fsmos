#include <uuencode.h>

void uuencode_line(const void *inptr, int count, char *out)
{
	int i, j; uint32_t l;
	const unsigned char *in = inptr;

	*out = ' ' + count;
	for (i = 0, j = 1; i < count; i += 3) {
		l = (uint32_t)in[i]<<16 | in[i+1]<<8 | in[i+2];
		/* this uses '`' instead of ' '	 */
		out[j++] = ' ' + (((l>>18)-1) & 0x3f) + 1;
		out[j++] = ' ' + (((l>>12)-1) & 0x3f) + 1;
		if (count - i > 1)
			out[j++] = ' ' + (((l>> 6)-1) & 0x3f) + 1;
		if (count - i > 2)
			out[j++] = ' ' + (((l>> 0)-1) & 0x3f) + 1;
	}
	out[j] = '\0';
	return;
}

int uudecode_line(const char *in, void *outptr)
{
	int i, j; uint32_t l;
	unsigned char *out = outptr;
	int count = (in[0]-' ') & 0x3f;

	for (i = 0, j = 1; i < count; j += 4) {
		l = ((uint32_t)((in[j]-' ')&0x3f) << 18)
			| ((uint32_t)((in[j+1]-' ')&0x3f) << 12)
			| (((in[j+2]-' ')&0x3f) << 6)
			| ((in[j+3]-' ')&0x3f);
		out[i++] = (l>>16) & 0xff;
		if (i < count)
			out[i++] = (l>>8) & 0xff;
		if (i < count)
			out[i++] = l&0xff;
	}
	return count;
}
