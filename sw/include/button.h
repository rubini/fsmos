#ifndef __BUTTON_H__
#define __BUTTON_H__

struct button {
	int gpio;
	unsigned long debounce_j; /* jiffies */
	unsigned long last_event; /* internal field, jiffies */
	uint16_t flags, status;
};
#define BUTTON_LOW_ACTIVE	0x0000 /* default: most common case */
#define BUTTON_HIGH_ACTIVE	0x0001
#define BUTTON_LEVEL_FLAG	0x0001 /* bitmask of levels */

#define BUTTON_PRESS_ONLY	0x0002 /* only leading edge is reported */

/* button_get() returns 0 or 1 as status, or 1 on leading edge if EDGE_ONLY */
extern int button_get(struct button *);

/* button_get_edge() return 0 (no edge), +1 or -1 */
extern int button_get_edge(struct button *);

#endif /* __BUTTON_H__ */
