#ifndef __REGS_LPC17_H__
#define __REGS_LPC17_H__
/*
 * 17xx is mostly different from 11xx/13xx.
 */

/* system control -- FIXME */

/* uart0 */
#define REG_U0THR		(0x4000C000 / 4) /* write */
#define REG_U0RBR		(0x4000C000 / 4) /* read */
#define REG_U0IER		(0x4000C004 / 4)
#define REG_U0IIR		(0x4000C008 / 4) /* read */
#define REG_U0FCR		(0x4000C008 / 4) /* write */
#define REG_U0LCR		(0x4000C00c / 4)
#define REG_U0LSR		(0x4000C014 / 4)
#define REG_U0LSR_RDR		0x01
#define REG_U0LSR_THRE		0x20

#define REG_U0DLL		(0x4000C000 / 4) /* when DLAB=1 */
#define REG_U0DLM		(0x4000C004 / 4) /* when DLAB=1 */
#define REG_U0FDR		(0x4000C028 / 4) /* fractional divider */
#define REG_U0OSR		(0x0 ) /* this register does not exist */

/* uart again, but with base+offet convention */
#define REGBASE_UART0		(0x4000c000 / 4)
#define REGOFF_UARTTHR		(0x000 / 4) /* write */
#define REGOFF_UARTRBR		(0x000 / 4) /* read */
#define REGOFF_UARTIER		(0x004 / 4)
#define REGOFF_UARTIIR		(0x008 / 4) /* read */
#define REGOFF_UARTFCR		(0x008 / 4) /* write */
#define REGOFF_UARTLCR		(0x00c / 4)
#define REGOFF_UARTLSR		(0x014 / 4)
#define REGOFF_UARTLSR_RDR	0x01
#define REGOFF_UARTLSR_THRE	0x20
#define REGOFF_UARTDLL		(0x000 / 4) /* when DLAB=1 */
#define REGOFF_UARTDLM		(0x004 / 4) /* when DLAB=1 */
#define REGOFF_UARTFDR		(0x028 / 4) /* fractional divider */

/* GPIO ("fast" gpio == FIO) */
#define REG_FIO0DIR		(0x2009c000 / 4)
#define REG_FIO0MASK		(0x2009c010 / 4)
#define REG_FIO0PIN		(0x2009c014 / 4)
#define REG_FIO0SET		(0x2009c018 / 4)
#define REG_FIO0CLR		(0x2009c01c / 4)
#define REG_FIOnDIR(n)		(REG_FIO0DIR + 0x20/4 * (n))
#define REG_FIOnMASK(n)		(REG_FIO0MASK + 0x20/4 * (n))
#define REG_FIOnPIN(n)		(REG_FIO0PIN + 0x20/4 * (n))
#define REG_FIOnSET(n)		(REG_FIO0SET + 0x20/4 * (n))
#define REG_FIOnCLR(n)		(REG_FIO0CLR + 0x20/4 * (n))

#define REG_PINSEL0		(0x4002c000 / 4)
#define REG_PINMODE0		(0x4002c040 / 4)
#define REG_PINMODEOD0		(0x4002c068 / 4)
#define REG_PINSEL(n)		(REG_PINSEL0 + (n)) /* Warning: 16-bits each */
#define REG_PINMODE(n)		(REG_PINMODE0 + (n)) /* Warning: 16-bits each */
#define REG_PINMODEOD(n)	(REG_PINMODEOD0 + (n)) /* 32-bits each */

/* system control registers */
#define REG_CLKSRCSEL		(0x400fc10c / 4)
#define REG_CLKSRCSEL_IRC		0
#define REG_CLKSRCSEL_OSC		1
#define REG_PLL0CON		(0x400fc080 / 4)
#define REG_PLL0CFG		(0x400fc084 / 4)
#define REG_PLL0STAT		(0x400fc088 / 4)
#define REG_PLL0FEED		(0x400fc08c / 4)
#define REG_PLL1CON		(0x400fc0a0 / 4)
#define REG_PLL1CFG		(0x400fc0a4 / 4)
#define REG_PLL1STAT		(0x400fc0a8 / 4)
#define REG_PLL1FEED		(0x400fc0ac / 4)
#define REG_CCLKCFG		(0x400fc104 / 4)

/* compatibility with LPC11/13 (which I use more often): define same names */
#define REG_SYSPLLCLKSEL	REG_CLKSRCSEL
#define REG_SYSPLLCLKSEL_IRC	REG_CLKSRCSEL_IRC
#define REG_SYSPLLCLKSEL_OSC	REG_CLKSRCSEL_OSC

/* clock control */
#define REG_PCONP		(0x400fc0c4 / 4)
#define REG_PCONP_PCTIM0	(1 << 1)
#define REG_PCONP_PCTIM1	(1 << 2)
#define REG_PCONP_PCUART0	(1 << 3)
#define REG_PCONP_PCUART1	(1 << 4)
#define REG_PCONP_PCPWM1	(1 << 6)
#define REG_PCONP_PCI2C0	(1 << 7)
#define REG_PCONP_PCSPI		(1 << 8)
#define REG_PCONP_PCRTC		(1 << 9)
#define REG_PCONP_PCSSP1	(1 << 10)
#define REG_PCONP_PCADC		(1 << 12)
#define REG_PCONP_PCCAN1	(1 << 13)
#define REG_PCONP_PCCAN2	(1 << 14)
#define REG_PCONP_PCGPIO	(1 << 15)
#define REG_PCONP_PCRIT		(1 << 16)
#define REG_PCONP_PCMCPWM	(1 << 17)
#define REG_PCONP_PCQUEI	(1 << 18)
#define REG_PCONP_PCI2C1	(1 << 19)
#define REG_PCONP_PCSSP0	(1 << 21)
#define REG_PCONP_PCTIM2	(1 << 22)
#define REG_PCONP_PCTIM3	(1 << 23)
#define REG_PCONP_PCUART2	(1 << 24)
#define REG_PCONP_PCUART3	(1 << 25)
#define REG_PCONP_PCI2C2	(1 << 26)
#define REG_PCONP_PCI2S		(1 << 27)
#define REG_PCONP_PCGPDMA	(1 << 29)
#define REG_PCONP_PCENET	(1 << 30)
#define REG_PCONP_PCUSB		(1 << 31)

/* compatibility macros, to build 11xx/13xx code */
#define REG_AHBCLKCTRL		REG_PCONP
#define REG_AHBCLKCTRL_SYS	0
#define REG_AHBCLKCTRL_ROM	0
#define REG_AHBCLKCTRL_RAM	0
#define REG_AHBCLKCTRL_FLASHR	0
#define REG_AHBCLKCTRL_FLASHA	0
#define REG_AHBCLKCTRL_I2C	REG_PCONP_PCI2C0
#define REG_AHBCLKCTRL_GPIO	REG_PCONP_PCGPIO
#define REG_AHBCLKCTRL_CT16B0	REG_PCONP_PCTIM0
#define REG_AHBCLKCTRL_CT16B1	REG_PCONP_PCTIM1
#define REG_AHBCLKCTRL_CT32B0	REG_PCONP_PCTIM2
#define REG_AHBCLKCTRL_CT32B1	REG_PCONP_PCTIM3
#define REG_AHBCLKCTRL_SSP0	REG_PCONP_PCSPI
#define REG_AHBCLKCTRL_UART	REG_PCONP_PCUART0
#define REG_AHBCLKCTRL_ADC	REG_PCONP_PCADC
#define REG_AHBCLKCTRL_USBREG	REG_PCONP_PCUSB
#define REG_AHBCLKCTRL_WDT	0
#define REG_AHBCLKCTRL_IOCON	0
#define REG_AHBCLKCTRL_SSP1	REG_PCONP_SSP1
#define REG_AHBCLKCTRL_USBRAM	REG_PCONP_PCUSB

/* oscen, isn't it? */
#define REG_SCS			(0x400fc1a0 / 4)
#define REG_SCS_OSCRANGE		(1 << 4)
#define REG_SCS_OSCEN			(1 << 5)
#define REG_SCS_OSCSTAT			(1 << 6)

/* peripheral clock selection (00 = /4; 01 = /1; 10 = /2; 11 = /8 or /6) */
#define REG_PCLKSEL0		(0x400fc1a8 / 4)
#define REG_PCLKSEL1		(0x400fc1ac / 4)

/* clock out pin (P1.27) */
#define REG_CLKOUTCFG		(0x400fc1c8 / 4)
#define REG_CLKOUTCFG_CPU		0
#define REG_CLKOUTCFG_MAIN		1
#define REG_CLKOUTCFG_RC		2
#define REG_CLKOUTCFG_USB		3
#define REG_CLKOUTCFG_RTC		4
#define REG_CLKOUTCFG_DIV(n)		((n-1 & 0x0f) << 4)
#define REG_CLKOUTCFG_EN		(1 << 8)

/* counter 0 -- names from LPC11xx, mapped to TIMER2 on LPC17xx */
#define REG_TMR32B0_BASE	(0x40090000 / 4)
#define REG_TMR32B0TCR		(0x40090004 / 4)
#define REG_TMR32B0TC		(0x40090008 / 4)
#define REG_TMR32B0PR		(0x4009000c / 4)
#define REG_TMR32B0PC		(0x40090010 / 4)
#define REG_TMR32B0MCR		(0x40090014 / 4)
#define REG_TMR32B0MCR_MR0R		(1 << 1)
#define REG_TMR32B0MCR_MR1R		(1 << 4)
#define REG_TMR32B0MCR_MR2R		(1 << 7)
#define REG_TMR32B0MCR_MR3R		(1 << 10)
#define REG_TMR32B0MR0		(0x40090018 / 4)
#define REG_TMR32B0MR1		(0x4009001c / 4)
#define REG_TMR32B0MR2		(0x40090020 / 4)
#define REG_TMR32B0MR3		(0x40090024 / 4)

/* counter 1 -- like above, mapped on TIMER3 */
#define REG_TMR32B1_BASE	(0x40094000 / 4)
#define REG_TMR32B1TCR		(0x40094004 / 4)
#define REG_TMR32B1TC		(0x40094008 / 4)
#define REG_TMR32B1PR		(0x4009400c / 4)
#define REG_TMR32B1PC		(0x40094010 / 4)

#define JIFFIES_ADDR		0x40094008

/* Same as above, but as offsets (used in generic pwm driver) */
#define REGOFF_TMRIR		(0x000 / 4)
#define REGOFF_TMRTCR		(0x004 / 4)
#define REGOFF_TMRTC		(0x008 / 4)
#define REGOFF_TMRPR		(0x00c / 4)
#define REGOFF_TMRPC		(0x010 / 4)
#define REGOFF_TMRMCR		(0x014 / 4)
#define REGOFF_TMRMCR_MR0R		(1 << 1)
#define REGOFF_TMRMCR_MR1R		(1 << 4)
#define REGOFF_TMRMCR_MR2R		(1 << 7)
#define REGOFF_TMRMCR_MR3R		(1 << 10)
#define REGOFF_TMRMR0		(0x018 / 4)
#define REGOFF_TMRMR1		(0x01c / 4)
#define REGOFF_TMRMR2		(0x020 / 4)
#define REGOFF_TMRMR3		(0x024 / 4)
#define REGOFF_TMRPWMC		(0x074 / 4)
#define REGOFF_TMRPWMC_PWMEN0		(1 << 0)
#define REGOFF_TMRPWMC_PWMEN1		(1 << 1)
#define REGOFF_TMRPWMC_PWMEN2		(1 << 2)
#define REGOFF_TMRPWMC_PWMEN3		(1 << 3)

/* usb device */
#define REG_USBIntSt		(0x400fc1c0 / 4)

#define REG_USBClkCtrl		(0x5000cff4 / 4)
#define REG_USBClkSt		(0x5000cff8 / 4)
#define REG_USBClkSt		(0x5000cff8 / 4)
#define REG_USBClk_DEV			(1 << 1)
#define REG_USBClk_AHB			(1 << 4)

#define REG_USBDevIntSt		(0x5000c200 / 4)
#define REG_USBDevIntEn		(0x5000c204 / 4)
#define REG_USBDevIntClr	(0x5000c208 / 4)
#define REG_USBDevIntSet	(0x5000c20c / 4)
#define REG_USBDevIntPri	(0x5000c22c / 4)
#define REG_USBDevInt_EPFAST		(1 << 1)
#define REG_USBDevInt_EPSLOW		(1 << 2)
#define REG_USBDevInt_DEVSTAT		(1 << 3)
#define REG_USBDevInt_CCEMPTY		(1 << 4)
#define REG_USBDevInt_CDFULL		(1 << 5)
#define REG_USBDevInt_RxEND		(1 << 6)
#define REG_USBDevInt_TxEND		(1 << 7)
#define REG_USBDevInt_EPRLZED		(1 << 8)
#define REG_USBDevInt_ERROR		(1 << 9)

#define REG_USBEpIntSt		(0x5000c230 / 4)
#define REG_USBEpIntEn		(0x5000c234 / 4)
#define REG_USBEpIntClr		(0x5000c238 / 4)
#define REG_USBEpIntSet		(0x5000c23c / 4)
#define REG_USBEpIntPri		(0x5000c240 / 4)

#define REG_USBReEp		(0x5000c244 / 4)
#define REG_USBEpIn		(0x5000c248 / 4)
#define REG_USBMaxPSize		(0x5000c24c / 4)

#define REG_USBRxData		(0x5000c218 / 4)
#define REG_USBRxPLen		(0x5000c220 / 4)
#define REG_USBRxPLen_LENMASK		0x3ff
#define REG_USBRxPLen_DV		(1 << 10)
#define REG_USBRxPLen_RDY		(1 << 11)
#define REG_USBTxData		(0x5000c21c / 4)
#define REG_USBTxPLen		(0x5000c224 / 4)
#define REG_USBTxPLen_LENMASK		0x3ff
#define REG_USBCtrl		(0x5000c228 / 4)
#define REG_USBCtrl_RDEN		(1 << 0)
#define REG_USBCtrl_WREN		(1 << 1)
#define REG_USBCtrl_EPMASK		0x2c
#define REG_USBCtrl_EPSHIFT		2

#define REG_USBCmdCode		(0x5000c210 / 4)
#define REG_USBCmdCode_WRITE		0x100
#define REG_USBCmdCode_READ		0x200
#define REG_USBCmdCode_COMMAND		0x500
#define REG_USBCmdCode_CODE(x)		((x) << 16)
#define REG_USBCmdData		(0x5000c214 / 4)

#define SIE_SETADDR	0xd0 /* w1 */
#define SIE_CONFIG	0xd8 /* w1 */
#define SIE_SETMODE	0xf3 /* w1 */
#define SIE_READFN	0xf5 /* r1-r2 */
#define SIE_READTEST	0xfd /* r2 */
#define SIE_SETSTATUS	0xfe /* w1 */
#define SIE_GETSTATUS	0xfe /* r1 */
#define SIE_GETECODE	0xff /* r1 */
#define SIE_GETESTATUS	0xfb /* r1 */
#define SIE_SELECT(ep)	(ep) /* r0-r1 */
#define SIE_SEL_CLI(ep)	(0x40 + (ep)) /* r1 */
#define SIE_EP_ST(ep)	(0x40 + (ep)) /* w1 */
#define SIE_CLEARBUF	0xf2 /* r0-r1 (selected endpoint) */
#define SIE_VALIDATEB	0xfa /* - (selected endpoint) */

#define REG_USBDMARSt		(0x5000c250 / 4)
#define REG_USBDMARClr		(0x5000c254 / 4)
#define REG_USBDMARSet		(0x5000c258 / 4)
#define REG_USBUDCAH		(0x5000c280 / 4)
#define REG_USBEpDMASt		(0x5000c284 / 4)
#define REG_USBEpDMAEn		(0x5000c288 / 4)
#define REG_USBEpDMADis		(0x5000c28c / 4)
#define REG_USBDMAIntSt		(0x5000c290 / 4)
#define REG_USBDMAIntEn		(0x5000c294 / 4)
#define REG_USBEoTIntSt		(0x5000c2a0 / 4)
#define REG_USBEoTIntClr	(0x5000c2a4 / 4)
#define REG_USBEoTIntSet	(0x5000c2a8 / 4)
#define REG_USBNDDRIntSt	(0x5000c2ac / 4)
#define REG_USBNDDRIntClr	(0x5000c2b0 / 4)
#define REG_USBNDDRIntSet	(0x5000c2b4 / 4)
#define REG_USBSysErrIntSt	(0x5000c2b8 / 4)
#define REG_USBSysErrIntClr	(0x5000c2bc / 4)
#define REG_USBSysErrIntSet	(0x5000c2c0 / 4)

/* Can registers */
#define REG_CANTxSR		(0x40040000 / 4)
#define REG_CANRxSR		(0x40040004 / 4)
#define REG_CANMSR		(0x40040008 / 4)
#define REG_CAN1BASE		(0x40044000 / 4)
#define REG_CAN2BASE		(0x40048000 / 4)
#define CANx_MOD			(0x00 / 4)
#define CANx_MOD_RM				0x01
#define CANx_CMR			(0x04 / 4)
#define CANx_CMR_TR				0x01
#define CANx_CMR_AT				0x02
#define CANx_CMR_RRB				0x04
#define CANx_CMR_CDO				0x08
#define CANx_CMR_SRR				0x10
#define CANx_CMR_TB1				0x20
#define CANx_CMR_TB2				0x40
#define CANx_CMR_TB3				0x80
#define CANx_GSR			(0x08 / 4)
#define CANx_GSR_RBS				0x01
#define CANx_GSR_DOS				0x02
#define CANx_GSR_TBS				0x04
#define CANx_GSR_TCS				0x08
#define CANx_GSR_RS				0x10
#define CANx_GSR_TS				0x20
#define CANx_GSR_ES				0x40
#define CANx_GSR_BS				0x80
#define CANx_ICR			(0x0c / 4)
#define CANx_IER			(0x10 / 4)
#define CANx_BTR			(0x14 / 4)
#define CANx_BTR_BRP_MASK			0x1ff
#define CANx_BTR_BRP_SHIFT			0
#define CANx_BTR_SJW_MASK			0xc000
#define CANx_BTR_SJW_SHIFT			14
#define CANx_BTR_TSEG1_MASK			0xf0000
#define CANx_BTR_TSEG1_SHIFT			16
#define CANx_BTR_TSEG2_MASK			0x700000
#define CANx_BTR_TSEG2_SHIFT			20
#define CANx_BTR_SAM				(1 << 23)
#define CANx_EWL			(0x18 / 4)
#define CANx_SR				(0x1c / 4)
#define CANx_SR_RBS				0x1
#define CANx_SR_DOS				0x2
#define CANx_RFS			(0x20 / 4)
#define CANx_RFS_DLC_MASK			0xf0000
#define CANx_RFS_DLC_SHIFT			16
#define CANx_RFS_EID				(1 << 31)
#define CANx_RID			(0x24 / 4)
#define CANx_RDA			(0x28 / 4)
#define CANx_RDB			(0x2c / 4)
#define CANx_TFI1			(0x30 / 4)
#define CANx_TID1			(0x34 / 4)
#define CANx_TDA1			(0x38 / 4)
#define CANx_TDB1			(0x3c / 4)
#define CANx_TFI2			(0x40 / 4)
#define CANx_TID2			(0x44 / 4)
#define CANx_TDA2			(0x48 / 4)
#define CANx_TDB2			(0x4c / 4)
#define CANx_TFI3			(0x50 / 4)
#define CANx_TID3			(0x54 / 4)
#define CANx_TDA3			(0x58 / 4)
#define CANx_TDB3			(0x5c / 4)
#define REG_AFMR		(0x4003c000 / 4)

/* NVIC -- the "1" registers only in cores with > 32 irq sources */
#define REG_NVIC_ISER0		(0xe000e100 / 4)
#define REG_NVIC_ISER1		(0xe000e104 / 4)
#define REG_NVIC_ICER0		(0xe000e180 / 4)
#define REG_NVIC_ICER1		(0xe000e184 / 4)
#define REG_NVIC_ISPR0		(0xe000e200 / 4)
#define REG_NVIC_ISPR1		(0xe000e204 / 4)
#define REG_NVIC_ICPR0		(0xe000e280 / 4)
#define REG_NVIC_ICPR1		(0xe000e284 / 4)
#define REG_NVIC_IABR0		(0xe000e300 / 4)
#define REG_NVIC_IABR1		(0xe000e304 / 4)
/* and we ignore the priority stuff */

#define __NVIC_INTERNAL_INTERRUPTS	16

/* Interrupts for LPC17xx */
enum lpc_irq {
	LPC_IRQ_WDT		= 0,
	LPC_IRQ_TIMER0		= 1,
	LPC_IRQ_TIMER1		= 2,
	LPC_IRQ_TIMER2		= 3,
	LPC_IRQ_TIMER3		= 4,
	LPC_IRQ_UART0		= 5,
	LPC_IRQ_UART1		= 6,
	LPC_IRQ_UART2		= 7,
	LPC_IRQ_UART3		= 8,
	LPC_IRQ_PWM1		= 9,
	LPC_IRQ_I2C0		= 10,
	LPC_IRQ_I2C1		= 11,
	LPC_IRQ_I2C2		= 12,
	LPC_IRQ_SP1		= 13,
	LPC_IRQ_SSP0		= 14,
	LPC_IRQ_SSP1		= 15,
	LPC_IRQ_PLL0		= 16,
	LPC_IRQ_RTC		= 17,
	LPC_IRQ_EINT0		= 18,
	LPC_IRQ_EINT1		= 19,
	LPC_IRQ_EINT2		= 20,
	LPC_IRQ_EINT3		= 21,
	LPC_IRQ_ADC		= 22,
	LPC_IRQ_BOD		= 23,
	LPC_IRQ_USB		= 24,
	LPC_IRQ_CAN		= 25,
	LPC_IRQ_GPDAM		= 26,
	LPC_IRQ_I2S		= 27,
	LPC_IRQ_ETH		= 28,
	LPC_IRQ_RIT		= 29,
	LPC_IRQ_MCPWM		= 30,
	LPC_IRQ_QUADENC		= 31,
	LPC_IRQ_PLL1		= 32,
	LPC_IRQ_USBACT		= 33,
	LPC_IRQ_CANACT		= 34,
	__NR_IRQS,
};
#define LPC_IRQ_USART0 LPC_IRQ_UART0 /* compatible name */

#endif /* __REGS_LPC17_H__ */
