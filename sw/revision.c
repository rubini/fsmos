#include "revision.h"

const char __date__[] = __DATE__;
const char __time__[] = __TIME__;
const char __gitc__[] = __GITC__;

/* This can be grepped for, but we must reference it from main somehow */
const char __gitc__for_identification__[] = "\nCOMMIT:" __GITC__ "\n";
