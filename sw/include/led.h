#ifndef __LED_H__
#define __LED_H__

/* The user array is terminated by an empty item (name == 0) */
struct led {
	short name, gpio;
	int t_on, t_off, current /* 1 or 0  */;
	unsigned long last_action;
};
#define LED_FOREVER (~0)

extern void led_init(struct led *led);
extern void led_set_rate(struct led *led, int name, int t_on, int t_off);
extern void led_action(struct led *led);
extern void led_tick(struct led *led, int name, int t_on);
extern void led_sync(struct led *led, int target, int source);

#endif /* __LED_H__ */
