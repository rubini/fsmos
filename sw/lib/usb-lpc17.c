#include <assert.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <io.h>
#include <usb.h>
#include <usb-int.h>

/*
 * This file is currently a mixup of generic and cpu-specific stuff
 */
static struct usb_hw_device usb_hw_device;

static struct usb_dev_desc usb_dev_desc = {
	.bLength = sizeof(struct usb_dev_desc),
	.bDescriptorType = USB_DT_DEVICE,
	.bcdUSB = 0x100, /* might be 0200, but this is easier it seems */
	.bDeviceClass = USB_CLASS_PER_INTERFACE,
	.bDeviceProtocol = 0,
	.bMaxPacketSize0 = 64,
	/* idVendor, idProduct, bcdDevice set at run time (CONFIG_ + const?) */
	.iManufacturer = 0, /* no strings by now */
	.iProduct = 0, /* no strings by now */
	.iSerialNumber = 0, /* no strings by now */
	.bNumConfigurations = 1,
};

static const struct usb_cfg_desc usb_cfg_desc = {
	.bLength = sizeof(struct usb_cfg_desc),
	.bDescriptorType = USB_DT_CONFIG,
	.wTotalLength = 32, /* this + 1 interface + 2 endpoints = 9+9+7+7 */
	.bNumInterfaces = 1,
	.bConfigurationValue = 1,
	.iConfiguration = 0, /* no string for this cfg */
	.bmAttributes = USB_CONFIG_ATT_ONE | USB_CONFIG_ATT_SELFPOWER,
	.bMaxPower = 5, /* 10mA -- just a value */
};

static const struct usb_if_desc usb_if_desc = {
	.bLength = sizeof(struct usb_if_desc),
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = 0,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = 255,
	.bInterfaceSubClass = 0,
	.bInterfaceProtocol = 0,
	.iInterface = 0,
};

static const struct usb_ep_desc usb_ep_desc[2] = {
	{
		.bLength = sizeof(struct usb_ep_desc),
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = 0x82,
		.bmAttributes = USB_ENDPOINT_XFER_BULK,
		.wMaxPacketSize = 64,
		.bInterval = 0,
	}, {
		.bLength = sizeof(struct usb_ep_desc),
		.bDescriptorType = USB_DT_ENDPOINT,
		.bEndpointAddress = 0x02,
		.bmAttributes = USB_ENDPOINT_XFER_BULK,
		.wMaxPacketSize = 64,
		.bInterval = 0,
	},
};

static void usb17_clock_on(void)
{
	const int reqbits = (REG_USBClk_DEV | REG_USBClk_AHB);

	regs[REG_USBClkCtrl] |= reqbits;
	while ((regs[REG_USBClkSt] & reqbits) != reqbits)
		;
}

enum {SIE_RD = 0, SIE_WR, SIE_NORW};
static uint32_t sie_cmd(int cmd, int dir, int dlen, uint32_t data /* 8b */ )
{
	uint32_t res = 0;
	int i;

	/* clear irq; send command; wait for irq, clear irq */
	regs[REG_USBDevIntClr] = REG_USBDevInt_CCEMPTY | REG_USBDevInt_CDFULL;
	regs[REG_USBCmdCode] = REG_USBCmdCode_COMMAND
		| REG_USBCmdCode_CODE(cmd);
	while(!(regs[REG_USBDevIntSt] & REG_USBDevInt_CCEMPTY))
		;
	regs[REG_USBDevIntClr] = REG_USBDevInt_CCEMPTY;

	if (dir == SIE_RD) {
		/* write RD phase; wait for full; pick byte, clear full */
		regs[REG_USBCmdCode] = REG_USBCmdCode_READ
			| REG_USBCmdCode_CODE(cmd);
		for (i = 0; i < dlen; i++) {
			while(!(regs[REG_USBDevIntSt] & REG_USBDevInt_CDFULL))
				;
			res |= (regs[REG_USBCmdData] << (8 * i));
			regs[REG_USBDevIntClr] = REG_USBDevInt_CDFULL;
		}
		return res;
	}
	if (dlen) {
		/* write: just write it; wait for ccempty; clear irq */
		regs[REG_USBCmdCode] = REG_USBCmdCode_WRITE
			| REG_USBCmdCode_CODE(data);
		while(!(regs[REG_USBDevIntSt] & REG_USBDevInt_CCEMPTY))
			;
		regs[REG_USBDevIntClr] = REG_USBDevInt_CCEMPTY;
	}
	return 0;
}

static int usb17_diag_irq(void)
{
	int devi = regs[REG_USBDevIntSt];
	int epi = regs[REG_USBEpIntSt];
	if (0) {
		printf("realized %08x, dev_int %08x ep_int %08x\n",
		       (int)regs[REG_USBReEp],
		       (unsigned)devi, (unsigned)epi);
	}
	if (devi & REG_USBDevInt_ERROR) {
		regs[REG_USBDevIntClr] = REG_USBDevInt_ERROR;
		int ecode = sie_cmd(SIE_GETECODE, SIE_RD, 1, 0);
		int estatus = sie_cmd(SIE_GETESTATUS, SIE_RD, 1, 0);
		printf("error code %02x, status %02x\n", ecode, estatus);
		return  ecode << 8 | estatus;
	}
	return 0;
}

static void usb17_realize(int ep /* HW number: 0..31 */)
{
	int max_psize;

	if (ep < 1)
		max_psize = 8; /* control EP, out (host to us) */
	else
		max_psize = 64; /* control-in or other ones */
	/* max_psize up to 1023 only for isochronous... */

	regs[REG_USBDevIntClr] = REG_USBDevInt_EPRLZED;
	if (0)
		printf("ReEp: was %08x, irq %08x\n",
		       (unsigned)regs[REG_USBReEp],
		       (unsigned)regs[REG_USBDevIntSt]);
	regs[REG_USBReEp] |= 1 << ep;
	regs[REG_USBEpIn] = ep;
	regs[REG_USBMaxPSize] = max_psize;
	while(!(regs[REG_USBDevIntSt] & REG_USBDevInt_EPRLZED))
		usb17_diag_irq();
}

static void usb17_may_realize_bulk(void)
{
	if (regs[REG_USBReEp] == 0x03) {
		/* If we are not realized (or not any more...) */
		printf("R!\n");
		usb17_realize(4); usb17_realize(5);
		/* Also, enable the endpoint with SIE */
		sie_cmd(SIE_EP_ST(4), SIE_WR, 1, 0x00);
		sie_cmd(SIE_EP_ST(5), SIE_WR, 1, 0x00);
		/* And say we are done */
		sie_cmd(SIE_CONFIG, SIE_WR, 1, 0x01);
	}
}


/* This cannot be an initcall, as we need the config -- FIXME: really? */
int usb_init(struct usb_device *ud) /* pages 262 and 263 (user manual v4.1) */
{
	struct usb_hw_device *hd = &usb_hw_device;

	/* Power on */
	regs[REG_PCONP] |= REG_PCONP_PCUSB;

	usb17_clock_on();

	/* Fix GPIO AF */
	gpio_dir_af(GPIO_NR(1, 30), GPIO_DIR_IN, 0, GPIO_AF(1)); /* Vbus */
	gpio_dir_af(GPIO_NR(2, 9), GPIO_DIR_OUT, 1, GPIO_AF(1)); /* connect */
	gpio_dir_af(GPIO_NR(0, 29), GPIO_DIR_IN, 0, GPIO_AF(1)); /* DP */
	gpio_dir_af(GPIO_NR(0, 30), GPIO_DIR_IN, 0, GPIO_AF(1)); /* DM */
	gpio_dir_af(GPIO_NR(1, 18), GPIO_DIR_OUT, 1, GPIO_AF(1)); /* led */

	/* Realize control endpoint (out and in) */
	usb17_realize(0);
	usb17_realize(1);

	/* FIXME: when are other endpoints to be realized (4,5 and 10,11) ? */

	/* Enable endpoint interrupts for slave mode */
	regs[REG_USBEpIntClr] = ~0; /* clear all */
	regs[REG_USBDevIntClr] = 0x3f; /* clear all */
	regs[REG_USBEpIntEn] = 0x31; /* control in and bulk I/O (EP2) */
	regs[REG_USBEpIntPri] = 0; /* All equal, "slow" */
	/* They suggest to use SIE_SETMODE, but I try delaying it */
	/* They suggest to enable interrupts, I'd better not */

	/* Configure DMA -- we currently do without DMA */
	regs[REG_USBEpDMADis] = ~0;
	regs[REG_USBDMARClr] = ~0;
	regs[REG_USBEoTIntClr] = ~0;
	regs[REG_USBNDDRIntClr] = ~0;
	regs[REG_USBSysErrIntClr] = ~0;
	/* They suggest to prepare UDCA, and enable DMA (p. 263). Not now */

	/* Set address to 0 and connect bit: this is better done below */

	/* Finally, copy configuration information to our structure */
	ud->hd = hd;
	usb_dev_desc.idVendor = ud->cfg->vendor;
	usb_dev_desc.idProduct = ud->cfg->device;
	usb_dev_desc.bcdDevice = ud->cfg->devrelease;

	/* IF we realize the endpoints now, they are lost at connect time... */

	return 0;
}

/* This requires the user to poll, to get vbus back (bad API) */
int usb_attach(struct usb_device *ud, int on)
{
	if (on && ud->state == USB_DETACHED) {
		/* This is the end of the initialization phase */
		sie_cmd(SIE_SETADDR, SIE_WR, 1, 0x80); /* address 0, enabled */
		/*
		 * We have a problem here below without nack we timeout.
		 * So let's keep irq-on-nack for control endpoints
		 */
		sie_cmd(SIE_SETMODE, SIE_WR, 1, 0x07); /* clk and some nack */
		sie_cmd(SIE_SETSTATUS, SIE_WR, 1, 0x01); /* con */
		ud->state = USB_ATTACHED;
	}

	if (!on && ud->state != USB_DETACHED) {
		/* Clear connect bit using SIE */
		sie_cmd(SIE_SETSTATUS, SIE_WR, 1, 0x0);
		ud->state = USB_DETACHED;
	}
	/* FIXME: what about CON_CH bit in sie_getstatus? */
	return gpio_get(GPIO_NR(1, 30)); /* Vbus */
}


static uint8_t ep0_buf[64];
static uint32_t *ep0_words = (void *)ep0_buf;


/*
 * This function, called by us and by outside code, must send back the buffer
 */
void usb_setup_reply(struct usb_device *ud, const void *buf, int len)
{
	/* Page 264 about how to send output */
	uint32_t *v;

	//printf("%s, %p %i\n", __func__, buf, len);
	if (buf != ep0_buf)
		memcpy(ep0_words, buf, len);

	regs[REG_USBCtrl] = REG_USBCtrl_WREN | (0 << REG_USBCtrl_EPSHIFT);
	regs[REG_USBTxPLen] = len;
	/* Bah. Little endian... */
	v = ep0_words;
	while (len > 0) {
		//printf("%08x\n", (int)*v);
		regs[REG_USBTxData] = *v;
		len -= 4;
		v++;
	}
	//printf("written, intsts now %08x\n", (unsigned)regs[REG_USBDevIntSt]);

	/* Finally, validate the frame -- and this SIE_SELECT is mandatory */
	sie_cmd(SIE_SELECT(1), SIE_NORW, 0, 0);
	sie_cmd(SIE_VALIDATEB, SIE_NORW, 0, 0);
}


static void usb_handle_setup(struct usb_device *ud)
{
	uint8_t setup_buf[8];
	struct usb_setup *s = (void *)setup_buf;
	struct usb_hw_device *hd = ud->hd;
	uint32_t r, v;
	int i, j, len;
	uint8_t *buf = ep0_buf;

	/* If poll() calls us, there is data. Pick it out, MSB first */
	regs[REG_USBCtrl] = REG_USBCtrl_RDEN | (0 << REG_USBCtrl_EPSHIFT);

	/* Page 264 about how to get input */
	i = 0;
	do {
		/* Sometimes we get 0 at first loop */
		r = regs[REG_USBRxPLen];
		i++;
	} while ((r & 0xc00) != 0xc00 && i < 15);
	if (0)
		printf("%i: %04x\n", i, (unsigned)r);
	if (!(r & REG_USBRxPLen_RDY)) /* if no data, ignore */
		return;
	if (!(r & REG_USBRxPLen_DV)) /* if not valid (why?), ignore */
		return;

	len = r & REG_USBRxPLen_LENMASK; /* len must be 8! */
	if (len == 0) {
		usb_setup_reply(ud, buf, 0); /* send empty reply. It works, but why? */
		return;
	}

	for (i = 0; i < len; i += 4) {
		v = regs[REG_USBRxData];
		for (j = 0; j < 4; j++) {
			setup_buf[i + j] = v & 0xff;
			v >>= 8;
		}
	}
	sie_cmd(SIE_CLEARBUF, SIE_NORW, 0, 0);

	if (0) {
		printf("setup: %02x:%02x:%04x:%04x:%04x\n",
		       s->bRequestType, s->bRequest, s->wValue,
		       s->wIndex, s->wLength);
	}
	/* select the action */
	if ((s->bRequestType & 0x60) != 0) {
		/* class or vendor */
		if (ud->setup_cb) {
			ud->setup_cb(ud, setup_buf, 8, 0);
			return;
		}
		printf("ERROR: request %02x and no handler\n",
		       s->bRequestType);
		return;
	}

	if (s->bRequest == USB_REQ_GET_DESCRIPTOR) {
		if ((s->wValue >> 8) == USB_DT_DEVICE) {
			/*
			 * We got first setup frame (80 06 00 01 00 00 40 00)
			 * This is "get descriptor", "device", 0, 0x40 bytes.
			 */
			memcpy(buf, &usb_dev_desc, sizeof(usb_dev_desc));
			usb_setup_reply(ud, buf, s->wLength);
			return;
		}
		if ((s->wValue >> 8) == USB_DT_CONFIG) {
			uint8_t *target = buf;
			/* Get descriptors: fill the whole 32 byte thing */
			memcpy(target, &usb_cfg_desc, sizeof(usb_cfg_desc));
			target += sizeof(usb_cfg_desc);
			memcpy(target, &usb_if_desc, sizeof(usb_if_desc));
			target += sizeof(usb_if_desc);
			/* Next is an array, so no "&" is there */
			memcpy(target, usb_ep_desc, sizeof(usb_ep_desc));
			target += sizeof(usb_ep_desc);
			assert(target - buf == usb_cfg_desc.wTotalLength,
			       0, "Wrong size in datastruct");

			/* Send 9 or 32, according to the request */
			usb_setup_reply(ud, buf, s->wLength);
			return;
		}
	}

	if (s->bRequest == USB_REQ_SET_ADDRESS) {
		/* set address: caller will use ud->addr to complete */
		ud->addr = s->wValue;
		usb_setup_reply(ud, NULL, 0);
		/* And now save our address */
		sie_cmd(SIE_SETADDR, SIE_WR, 1, 0x80 | ud->addr);
		return;
	}

	if (s->bRequest == USB_REQ_SET_CONFIGURATION
		|| s->bRequest == USB_REQ_SET_INTERFACE) {
		/* like above: accept and send no data back */
		usb_setup_reply(ud, NULL, 0);
		/* but realize your bulk endpoints */
		if (0) {
			/*
			 * It may be too early to realize,: it causes
			 * a timeout in communication. Likely because
			 * we'll change ourselved before retransmitted
			 * SET_INTERFACE reaches us.			 
			 */
			usb17_may_realize_bulk();
			ud->state = USB_CONFIGURED;
		} else {
			hd->tout_pending = 1;
			hd->cfg_timeout = jiffies + HZ/2;
		}

		return;
	}

	printf("Unhandled setup request %02x %02x\n",
	       s->bRequestType, s->bRequest);
}

void usb17_collect_data(struct usb_device *ud)
{
	static uint8_t buffer[64]; /* we won't get more than 64 */
	uint32_t r, *b = (void *)buffer;
	int i, len;

	/* similar to setup collection above (see page 264) */
	regs[REG_USBCtrl] = REG_USBCtrl_RDEN | (2 << REG_USBCtrl_EPSHIFT);
	i = 0;
	do {
		/* Sometimes we get 0 at first loop */
		r = regs[REG_USBRxPLen];
		i++;
	} while ((r & 0xc00) != 0xc00 && i < 15);
	if (0)
		printf("%i: %04x\n", i, (unsigned)r);
	if (!(r & REG_USBRxPLen_RDY)) /* if no data, ignore */
		return;
	if (!(r & REG_USBRxPLen_DV)) /* if not valid (why?), ignore */
		return;
	len = r & REG_USBRxPLen_LENMASK;
	if (len > sizeof(buffer)) {
		printf("Error: len = %i\n", len);
		len = sizeof(buffer);
	}
	for (i = 0; i < len; i += 4)
		*b++ = regs[REG_USBRxData];
	sie_cmd(SIE_CLEARBUF, SIE_NORW, 0, 0);
	/* Now, it looks like if size is 64, this is a partial message */
	if (ud->ep1_out_cb)
		ud->ep1_out_cb(ud, buffer, len, len == 64 ? EAGAIN : 0);
}

int usb_poll(struct usb_device *ud)
{
	static unsigned long j, iterations;
	struct usb_hw_device *hd = &usb_hw_device;
	unsigned devi, epi;
	int timely_print = 0;
	int i;

	if (timely_print) {
		if (j == 0)
			j = jiffies + 3 * HZ;
		iterations++;
		if (time_after(jiffies, j)) {
			printf("%s: %li iterations\n", __func__, iterations);
			printf("%s: epi, devi: %08x %08x\n", __func__,
			       (int)regs[REG_USBEpIntSt], (int)regs[REG_USBDevIntSt]);
			j += 3 * HZ;
		}
	}

	/* We had to delay realization of endpoints... */
	if (hd->tout_pending && time_after(jiffies, hd->cfg_timeout)) {
		usb17_may_realize_bulk();
		ud->state = USB_CONFIGURED;
		hd->tout_pending = 0;
	} else  if (ud->state == USB_CONFIGURED) {
		/* Check if we lost sth... */
		usb17_may_realize_bulk();
	}

	if (0) {
		printf("irq dev %08x ep %08x dma %08x\n",
		       (unsigned)regs[REG_USBDevIntSt],
		       (unsigned)regs[REG_USBEpIntSt],
		       (unsigned)regs[REG_USBDMAIntSt]);
	}

	devi = regs[REG_USBDevIntSt];
	if (devi & REG_USBDevInt_DEVSTAT) {
		regs[REG_USBDevIntClr] = REG_USBDevInt_DEVSTAT;
	}
	if (devi & REG_USBDevInt_ERROR) {
		usb17_diag_irq();
	}

	epi = regs[REG_USBEpIntSt];
	if (!epi)
		return 0; /* fast (well, in a wat) return track */

	if (epi & 0x01) { /* We got setup (control-out) */
		regs[REG_USBEpIntClr] = 1;
		usb_handle_setup(ud);
	}
	if (epi & 0x02) { /* The control-in went through: ack it */
		regs[REG_USBEpIntClr] = 2;
	}

	if (epi & 0x10) {
		if (0)
			printf("EP 2-out irq (%08x)\n", epi);
		regs[REG_USBEpIntClr] = 0x10;
		usb17_collect_data(ud);
	}
	if (epi & 0x20) {
		if (0) {
			i = sie_cmd(SIE_SEL_CLI(5), SIE_RD, 1, 0);
			printf("EP 2-in irq (%08x -- %02x)\n", epi, i);
		}
		regs[REG_USBEpIntClr] = 0x20;
	}
	return 0;
}

/* This is not ep1 but ep2 (4,5 physical). Who cares */
int usb_ep1_in_submit(struct usb_device *ud, const void *buf, int len)
{
	uint32_t val;
	const uint32_t *valp;
	int status, nw;
	int iterations;

	if (ud->state != USB_CONFIGURED)
		return -ENODEV; /* Do not send if not yet configured */

	/* similar to setup_reply above. It's sending, after all */
	while (len) { /* FIXME: multiple-of-64 issue */

		iterations = 0;
		do {
			status =  sie_cmd(SIE_SELECT(5), SIE_RD, 1, 0);
			if (iterations++ > 1000) {
				int error = usb17_diag_irq();
				printf("%s: status %02x, still to write %i"
				       " (error 0x%04x)\n", __func__, status,
				       len, error);
				return -EBUSY;
			}
		} while (status & 0x01);

		nw = len;
		if (nw > 64)
			nw = 64; /* max packet size */
		len -= nw;

		regs[REG_USBCtrl] = REG_USBCtrl_WREN
			| (2 << REG_USBCtrl_EPSHIFT);
		udelay(4);
		regs[REG_USBTxPLen] = nw;
		if (((int)buf & 3) == 0) {
			/* aligned: proceed */
			valp = buf;
			while (nw > 0) {
				regs[REG_USBTxData] = *valp++;
				nw -= 4;
				buf += 4;
			}
		} else {
			/* not aligned: memcpy() (FIXME: __packed__) */
			while (nw > 0) {
				memcpy(&val, buf, 4);
				regs[REG_USBTxData] = val;
				nw -= 4;
				buf += 4;
			}
		}
		/* Finally, validate the frame (we selected above already) */
		sie_cmd(SIE_VALIDATEB, SIE_NORW, 0, 0);
	}
	return 0;

}
