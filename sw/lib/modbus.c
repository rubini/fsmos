#include <io.h>
#include <time.h>
#include <errno.h>
#include <modbus.h>
#include <unaligned.h>
#include <crc.h>

#define MODBUS_FAKE_CRC 0 /* 0xaaaa is useful when looking at dumps */
#define MODBUS_VERBOSE_CRC 0 /* just in case */

uint16_t modbus_crc(void *msg, int msglen)
{
	if (MODBUS_FAKE_CRC)
		return 0xaaaa;

	if (MODBUS_VERBOSE_CRC)
		printf("make crc on %i bytes\n", msglen);
	return crc16(0xa001, msg, msglen);
}

int modbus_crc_check(void *msg, int msglen, int okretval)
{
	uint16_t crc;
	uint8_t *crcpos = (uint8_t *)msg + msglen - 2;

	if (msglen < 4)
		return -EINVAL;

	crc = modbus_crc(msg, msglen - 2);
	if (MODBUS_VERBOSE_CRC) {
		printf("check crc on %i bytes: ", msglen);
		printf("got 0x%04x, expected 0x%04x\n",
		       crcpos[0] | (crcpos[1] << 8), crc);
	}
	if (crc != (crcpos[0] | (crcpos[1] << 8)))
		return -EINVAL;
	return okretval;
}

static const uint8_t req_len[] = {
	[3] = sizeof(struct modbus_req3),
	[4] = sizeof(struct modbus_req4),
	[6] = sizeof(struct modbus_req6),
};

int modbus_request_valid(struct modbus_device *dev, void *msg, int msglen)
{
	struct modbus_header *h = msg;
	struct modbus_req10 *r10 = msg;
	int func;
	int ret = -ENXIO, len = 0;

	if (msglen < 4)
		return 0;
	/* prepare return value if the frame is complete and crc-ok */
	if (h->address == dev->address)
		ret = 1;
	if (h->address == 0) /* broadcast) */
		ret = 1;

	func = h->func;

	/* function 10 is variable length */
	if (func == 0x10) {
		int nreg;
		if (msglen < 9)
			return 0;
		nreg = ntohs(r10->reg_count);
		len = sizeof(*r10) + 2 + 2 * nreg;
		if (msglen < len)
			return 0;
		return modbus_crc_check(msg, msglen, ret);
	}
	if (func < ARRAY_SIZE(req_len))
		len = req_len[func];
	if (!len) {
		/* unsupported function, wait for inter-frame timeout */
		if (time_before(jiffies, dev->last_byte + dev->inter_frame))
			return 0;
		if (ret == 1) ret = -ENOENT; /* for us, but not supported */
		return modbus_crc_check(msg, msglen, ret);
	}
	if (msglen < len)
		return 0;
	return modbus_crc_check(msg, msglen, ret);
}

/* find the register, for read or write access */
static struct modbus_register *modbus_findreg(struct modbus_device *dev,
					      int regname, int action)
{
	struct modbus_register *r;
	int i;

	for (r = dev->regarray, i = 0; r && i < dev->regcount; r++, i++) {
		if (regname == r->reg_name)
			break;
		if (regname > r->reg_name &&
		    regname < r->reg_name + r->count)
			break;
		}
	if (!r ||i == dev->regcount)
		return NULL; /* regname not supported */
	if (!(r->flags & action))
		return NULL; /* action not supported */
	return r;
}

/* function 3 and 4 behave in the same way: they read some registers */
static int modbus_parse_34(struct modbus_device *dev, void *msg,
			   int msglen, int buflen)
{
	struct modbus_req3 *req = msg;
	struct modbus_resp34 *resp = msg; /* reuse data space */
	struct modbus_register *r;
	unsigned long resp_time = jiffies + dev->resp_delay;
	int first, nregs, last, maxn, reg, ok;
	uint16_t val;

	first = ntohs(req->reg_name);
	nregs = ntohs(req->nregs);

	/* prevent overflow in the reply buffer */
	maxn = (buflen - 2 - offsetof(struct modbus_resp34, data)) / 2;
	if (nregs > maxn)
		nregs = maxn;

	last = first + nregs;

	resp->byte_count = 2 * nregs;
	msglen = 3 + 2 * nregs; /* before crc */

	for (reg = first; reg < last; reg++) {
		val = 0; /* return 0 on failure, it's easier */
		r = modbus_findreg(dev, reg, MODBUS_READ);
		ok = r != NULL;
		/* FIXME: return error if not a valid register */
		if (ok && r->callback) {
			/* the callback, if any, should not fail */
			ok = r->callback(r, reg, MODBUS_READ, 0) >= 0;
		}
		if (ok)
			val = r->reg_addr[reg - r->reg_name];
		else return 0; /* if at least one fails, we won't reply */
		unaligned_writew_be(resp->data + (reg - first), val);
	}
	/* crc is little-endian, so no htons */
	resp->data[reg - first] = modbus_crc(msg, msglen);
	if (resp->h.address) /* no reply for broadcast (addr == 0) */
		dev->send(dev, msg, msglen + 2, resp_time);
	return 0;
}

/* write single */
static int modbus_parse_6(struct modbus_device *dev, void *msg,
			  int msglen, int buflen)
{
	struct modbus_req6 *req = msg;
	struct modbus_register *r;
	unsigned long resp_time = jiffies + dev->resp_delay;
	int reg, ok;
	uint16_t val;

	reg = ntohs(req->reg_name);
	val = ntohs(req->reg_value);

	r = modbus_findreg(dev, reg, MODBUS_WRITE);
	ok = r != NULL;
	/* FIXME: return error if not a valid register */
	if (ok && r->callback) {
		/* the callback, if any, should not fail */
		ok = r->callback(r, reg, MODBUS_WRITE, val) >= 0;
	}
	if (ok)
		r->reg_addr[reg - r->reg_name] = val;
	else return 0; /* if at least one fails, we won't reply */

	if (req->h.address) /* no reply for broadcast (addr == 0) */
		dev->send(dev, msg, msglen, resp_time);
	return 0;
}

/* write multiple */
static int modbus_parse_10(struct modbus_device *dev, void *msg,
			   int msglen, int buflen)
{
	struct modbus_req10 *req = msg;
	struct modbus_resp10 *resp = msg;
	struct modbus_register *r;
	uint16_t *ptr;
	unsigned long resp_time = jiffies + dev->resp_delay;
	int reg, count, ok;
	uint16_t val;

	reg = ntohs(req->reg_name);
	count = ntohs(req->reg_count);
	ptr = req->data;
	for (; count; reg++, ptr++, count--) {
		val = unaligned_readw_be(ptr);
		r = modbus_findreg(dev, reg, MODBUS_WRITE);
		ok = r != NULL;
		/* FIXME: return error if not a valid register */
		if (ok && r->callback) {
			/* the callback, if any, should not fail */
			ok = r->callback(r, reg, MODBUS_WRITE, val) >= 0;
		}
		if (ok)
			r->reg_addr[reg - r->reg_name] = val;
		else return 0; /* if at least one fails, we won't reply */
	}

	/* the reply is like the request, trimmed to regcount */
	resp->crc = modbus_crc(resp, sizeof(*resp) - sizeof(resp->crc));
	if (resp->h.address) /* no reply for broadcast (addr == 0) */
		dev->send(dev, resp, sizeof(*resp), resp_time);
	return 0;
}

static const int (*parse_fn[])(struct modbus_device *dev,
			     void *msg, int msglen, int buflen) = {
	[3] modbus_parse_34,
	[4] modbus_parse_34,
	[6] modbus_parse_6,
};

int __modbus_parse(struct modbus_device *dev, void *msg,
		   int msglen, int buflen)
{
	struct modbus_header *h = msg;
	int func = h->func;

	/* We know for sure that the message is complete and supported */
	if (func < ARRAY_SIZE(parse_fn) && parse_fn[func])
		return parse_fn[func](dev, msg, msglen, buflen);
	if (func == 0x10)
		return modbus_parse_10(dev, msg, msglen, buflen);
	/* FIXME: exception 01, function not supported */
	return 0;
}

int modbus_parse(struct modbus_device *dev, void *msg, int msglen, int buflen)
{
	int ret = modbus_request_valid(dev, msg, msglen);

	if (ret == 0)
		return -EAGAIN;
	if (ret < 0)
		return ret;
	return __modbus_parse(dev, msg, msglen, buflen);
}

int modbus_master(struct modbus_device *dev, struct modbus_register *reg)
{
	return 0; /* FIXME */
}
