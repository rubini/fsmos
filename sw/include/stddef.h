#ifndef __STDDEF_H__
#define __STDDEF_H__

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#define offsetof(TYPE, MEMBER) ((unsigned) &((TYPE *)0)->MEMBER)

#endif /* __STDDEF_H__ */
