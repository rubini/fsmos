/*
 * Limited sscanf version: only %s, %c, %[width]i, %[width]x
 *
 * Alessandro Rubini, 2017 -- public domain
 */
#include <stdarg.h>

static int space(char c)
{
	return c == ' ' || c == '\t' || c == '\n';
}

/* hack: return -1 if it is not, or value. One function for two purposes */
static int digit(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	return -1;
}

static int xdigit(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	return -1;
}

int vsscanf(const char *buf, const char *fmt, va_list args)
{
	int ret = 0, minus = 0;
	int mod_l, mod_base, mod_fsize;

	while (*fmt) {
		mod_l = mod_base = mod_fsize = 0;

		if (space(*fmt)) {
			while (space(*buf))
				buf++;
			fmt++;
			continue;
		}

		if (*fmt != '%') {
			if (*buf == *fmt) {
				buf++; fmt++; continue;
			}
			return ret; /* mismatch */
		}

		fmt++;
		while (digit(*fmt) >= 0) {
			mod_fsize = mod_fsize * 10 + digit(*fmt);
			fmt++;
		}
		if (*fmt == 'h' || *fmt == 'l') {
			/* note: 'll' or 'L' not supported */
			mod_l = *fmt;
			fmt++;
		}

		switch(*fmt) {
		case '%':
			if (*buf++ != '%')
				return ret;
			break;

		case 'c': {
			char *cp = va_arg(args, char *);
			if (mod_fsize || mod_l)
				return -1; /* "%5c" not supported */
			if (!*buf)
				return ret;

			*cp = *buf++;
			ret++;
			break;
		}

		case 's': {
			char *s = va_arg(args, char *);
			if (mod_fsize || mod_l)
				return -1; /* "%5s" not supported */
			while (space(*buf))
				buf++;
			if (!*buf)
				return ret;
			while (*buf && !space(*buf))
				*s++ = *buf++;
			*s = '\0';
			ret++;
			break;
		}

		case 'd':
			mod_base = 10;
			/* fall through */
		case 'x':
			if (*fmt == 'x')
				mod_base = 16;
			/* fall through */
		case 'i': {
			long value = 0;
			int ndigit = 0;

			while (space(*buf))
				buf++;

			if (mod_fsize == 0)
				mod_fsize = 0xffff;

			/* accept the sign */
			if (buf[0] == '-') {
				minus = 1;
				buf++;
				mod_fsize--;
			}

			/* special case: if '%i' accept 0x or 0X */
			if (mod_base == 0) {
				if (mod_fsize > 2
				    && buf[0] == '0'
				    && (buf[1] == 'x' || buf[1] == 'X')) {
					mod_base = 16; buf += 2; mod_fsize -= 2;
				} else {
					mod_base = 10;
				}
			}
			/* finally convert */
			while (mod_fsize--) {
				if (mod_base == 10 && digit(*buf) >= 0) {
					value = value * 10 + digit(*buf++);
					ndigit++;
					continue;
				}
				if (mod_base == 16 && xdigit(*buf) >= 0) {
					value = value * 16 + xdigit(*buf++);
					ndigit++;
					continue;
				}
				break;
			}
			if (!ndigit) { /* nothing converted */
				return ret;
			}
			if (minus)
				value = -value;
			if (mod_l == 'l')
				*va_arg(args, long *) = value;
			else if (mod_l == 'h')
				*va_arg(args, short *) = value;
			else
				*va_arg(args, int *) = value;
			ret++;
			break;
		}

		default: return -1; /* wrong format */

		}
		fmt++;
	}
	return ret;
}

int sscanf(const char *buf, const char *fmt, ...)
{
        va_list args;
        int i;

        va_start(args, fmt);
        i = vsscanf(buf, fmt, args);
        va_end(args);

        return i;
}
