#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
/* The three below for open(2) according to man page */
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

unsigned char *binary;

unsigned char header[2*1024]; /* FAT header before binary image */

int main(int argc, char **argv)
{
	FILE *f, *parts;
	int i, size, found, devfd;
	char s[80], name[40] = "/dev/";
	int verbose;

	verbose = getenv("NEWFWVERBOSE") != NULL;

	if (argc != 2) {
		fprintf(stderr, "%s: use \"%s <binary-name>\"\n", argv[0],
			argv[0]);
		exit(1);
	}

	/* Step 1: check /proc/partitions */
	parts = fopen("/proc/partitions", "r");
	if (!parts) {
		fprintf(stderr, "%s: /proc/partitions: %s\n", argv[0],
			strerror(errno));
		exit(3);
	}
	found = 0;
	while (fgets(s, sizeof(s), parts)) {
		if (sscanf(s, "%*i %*i %i %s", &size, name+5) != 2)
			continue;
		if (size > 66)
			continue;
		if (size < 10)
			continue;
		if (isdigit(name[strlen(name)-1]))
			continue;
		found = 1;
		break;
	}
	fclose(parts);
	if (verbose)
		printf("Detected \"%s\": size %i\n", name, size);
	if (!found) {
		fprintf(stderr, "%s: no suitable device detected\n", argv[0]);
		exit(2);
	}

	/* Step 2: allocate memory */
	size = 1024 * (size - 2);
	binary = malloc(size + 1);
	memset(binary, 0xff, size);

	/* Step 3: read binary */
	if (!(f = fopen(argv[1], "rb"))) {
		fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1],
			strerror(errno));
		exit(1);
	}
	i = fread(binary, 1, size+1, f);
	if (i < 0) {
		fprintf(stderr, "%s: read(%s): %s\n", argv[0], argv[1],
			strerror(errno));
		exit(1);
	}
	if (i > size) {
		fprintf(stderr, "%s: %s: file too large\n", argv[0], argv[1]);
		exit(1);
	}
	size = (i + 4095) & ~4095;
	if (verbose)
		printf("Binary size %i -> %i (0x%x)\n", i, size, size);

	/* Step 4: open device and check it */
	devfd = open(name, O_RDWR);
	if (devfd < 0) {
		fprintf(stderr, "%s: %s: %s\n", argv[0], name,
			strerror(errno));
		exit(2);
	}
	i = read(devfd, header, sizeof(header));
	if (i < 0) {
		fprintf(stderr, "%s: read(%s): %s\n", argv[0], name,
			strerror(errno));
		exit(2);
	}
	if (i != sizeof(header)) {
		fprintf(stderr, "%s: %s: short read\n", argv[0], name);
		exit(2);
	}
	if (memcmp(header + 3, "MSDOS", 5)) {
		fprintf(stderr, "%s: %s: wrong file format\n", argv[0], name);
		exit(2);
	}
	if (memcmp(header + 0x620, "FIRMWAREBIN", 11)) {
		fprintf(stderr, "%s: %s: wrong file format\n", argv[0], name);
		exit(2);
	}

	/* Now we are safe. Write the binary */
	lseek(devfd, 0x800, SEEK_SET);
	i = write(devfd, binary, size);
	if (i < 0) {
		fprintf(stderr, "%s: write(%s): %s\n", argv[0], name,
			strerror(errno));
		exit(2);
	}
	if (i != size) {
		fprintf(stderr, "%s: %s: short write\n", argv[0], name);
		exit(2);
	}

	return 0;
}
