/*
 * Support for Solomon SSD1307 (oled controller, usually 64x128)
 */
#ifndef __DISPLAY_SSD130x_H__
#define __DISPLAY_SSD130x_H__
#include <i2c.h>
#include <font.h>

/* First byte must be one of these */
#define SSD13_CMD	0x00
#define SSD13_DATA	0x40

/* Define some commands for the application to use with write method */
#define SSD13_CMD_ONOFF(on)	((on) ? 0xaf : 0xae)
#define SSD13_CMD_INVERT(on)	((on) ? 0xa7 : 0xa6)
#define SSD13_CMD_FLIPY(on)	((on) ? 0xc0 : 0xc8)
#define SSD13_CMD_FLIPX(on)	((on) ? 0xa0 : 0xa1)
#define SSD13_CMD_ALLON		0xa5
#define SSD13_CMD_FOLLOWRAM	0xa4
#define SSD13_CMD_CONTRAST	0x81 /* next byte: 0..255 */

/* This is meant to be read-only data */
struct ssd13_cfg {
	short wid, hei;
	struct i2c_dev *i2c;
	short addr7; /* usually 0x3c */

	short init_len;
	const uint8_t *init_string; /* if null default applies */

	/* font */
	const struct font *font;
	uint32_t font_flags;
};

extern const uint8_t ssd13_default_init[];
extern const int ssd13_default_init_len;

/* And this is read-write */
struct ssd13_dev {
	const struct ssd13_cfg *cfg;
	int x, y; /* current position */
};

/* all integer functions return 0 or -EIO */
extern struct ssd13_dev *ssd13_create(struct ssd13_dev *dev, int *errno);
extern int ssd13_reinit(struct ssd13_dev *dev);

/* low level access -- first byte must be SSD13_CMD or SSD13_DATA */
extern int ssd13_write(struct ssd13_dev *dev, const uint8_t *buf, int len);

/*
 * A few helpers for simple commands
 */
static inline int __ssd13_cmd1(struct ssd13_dev *dev, uint8_t cmd)
{
	uint8_t msg[2] = {SSD13_CMD, cmd};
	return ssd13_write(dev, msg, 2);
}

static inline int ssd13_onoff(struct ssd13_dev *dev, int ison)
{ return __ssd13_cmd1(dev, SSD13_CMD_ONOFF(ison)); }

static inline int ssd13_invert(struct ssd13_dev *dev, int ison)
{ return __ssd13_cmd1(dev, SSD13_CMD_INVERT(ison)); }

static inline int ssd13_rotate180(struct ssd13_dev *dev, int ison)
{
	__ssd13_cmd1(dev, SSD13_CMD_FLIPX(ison));
	return __ssd13_cmd1(dev, SSD13_CMD_FLIPY(ison));
}

static inline int ssd13_allon(struct ssd13_dev *dev)
{ return __ssd13_cmd1(dev, SSD13_CMD_ALLON); }

static inline int ssd13_followram(struct ssd13_dev *dev)
{ return __ssd13_cmd1(dev, SSD13_CMD_FOLLOWRAM); }

static inline int ssd13_contrast(struct ssd13_dev *dev, uint8_t contrast)
{
	uint8_t msg[3] = {SSD13_CMD, SSD13_CMD_CONTRAST, contrast};
	return ssd13_write(dev, msg, 3);
}

/*
 * String access, with a font (not all fonts are supported: it panics on putc)
 */
extern void ssd13_goto(struct ssd13_dev *dev, int x, int y);
extern void ssd13_putc(struct ssd13_dev *dev, char c);

static inline void ssd13_puts(struct ssd13_dev *dev, const char *s)
{
	while (*s)
		ssd13_putc(dev, *s++);
}

/*
 * Bitmap support (must be in proper format, not all positions work, etc)
 */

struct ssd13_bitmap {
	short wid, hei;
	unsigned char data[];
};

extern void ssd13_image(struct ssd13_dev *dev, int x, int y,
			 struct ssd13_bitmap *bmap);

/*
 * Waiting for the above image support, let's do raw graphics
 * WARNING: the data array *must* begin widh SSD13_DATA
 */
extern void ssd13_raw_graphics(struct ssd13_dev *dev, int x, int line,
			const uint8_t *data, int datalen);

#endif /* __DISPLAY_SSD13_H__ */
