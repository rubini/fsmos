#ifndef __CPU_H__
#define __CPU_H__

#ifdef __ASSEMBLER__

#if CONFIG_CPU_IS_F0x0
#  define JIFFIES16_ADDR	0x40001024
#elif CONFIG_CPU_IS_F205
#  define JIFFIES16_ADDR	0x40014424
#elif CONFIG_CPU_IS_L010
#  define JIFFIES16_ADDR	0x40010824
#elif CONFIG_CPU_IS_L031
#  define JIFFIES16_ADDR	0x40010824
#elif CONFIG_CPU_IS_L4R
#  define JIFFIES16_ADDR	0x40001024
#else
#  error "Unknonw STM32 family"
#endif

#else /* not assembler */

#include <stdint.h>
#include <hw.h>

/* Common registers (see below for ifdef and special registers */
#include <regs-stm32.h>

/* Our hardware jiffies is 16 bits long. We may use it, in special cases */
extern volatile uint16_t jiffies16;
extern uint32_t get_jiffies(void);
#define jiffies get_jiffies()


/* The system clock can be driven by these sources */
enum stm32_clocks {
	STM_CLK_HSI, /* sometimes HSI_16, sometimes different */
	STM_CLK_MSI,
	STM_CLK_HSE,
	STM_CLK_PLL,
	STM_CLK_LSE, /* some devices only */
};

#if CONFIG_CPU_IS_F0x0
#include <regs-stm32f0x0.h>
#define CPU_FREQ (8 * 1000 * 1000) /* At reset, HSI at 8MHz */
#elif CONFIG_CPU_IS_F205
#include <regs-stm32f205.h>
#define CPU_FREQ (16 * 1000 * 1000) /* At reset, MSI at 16MHz */
#elif CONFIG_CPU_IS_L010
#include <regs-stm32l010.h>
#define CPU_FREQ (2100 * 1000) /* At reset, MSI at 2.1MHz */
#elif CONFIG_CPU_IS_L031
#include <regs-stm32l031.h>
#define CPU_FREQ (2100 * 1000) /* At reset, MSI at 2.1MHz */
#elif CONFIG_CPU_IS_L4R
#include <regs-stm32l4r.h>
#define CPU_FREQ (4000 * 1000) /* At reset, MSI at 4MHz */
#else
#error "Unknonw STM32 family"
#endif

/* We have one set of gpio mechanisms */
#include <gpio-stm32.h>

#endif /* __ASSEMBLER__ */
#endif /* __CPU_H__ */

