#include "../pp_printf/pp-printf.h"
#include <stdarg.h>

#define NULL 0

extern int console_puts(const char *s);
extern int puts(const char *s);
extern void putc(int c);
extern void raw_putc(int c);

/* __putc and __puts don't flush the fifo */
extern int __puts(const char *s);
extern void __putc(int c);

/* serial input: getc/gets are blocking (beware!) */
extern int getc(void);
extern char *gets(char *s); /* includes trailing newline */

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
extern int pollc(void);
extern char *polls(char *s, int bsize); /* pass empty string! */
extern char *polls_t(char *s, char *terminators, int bsize);

/* equivalent operations, but for a specific uart */
struct uart;
extern int uart_init(const struct uart *u);
extern int uart_puts(const struct uart *u, const char *s);
extern void uart_putc(const struct uart *u, int c);
extern void uart_raw_putc(const struct uart *u, int c);
extern int uart_getc(const struct uart *u);
extern char *uart_gets(const struct uart *u, char *s);
extern int uart_pollc(const struct uart *u);
extern char *uart_polls(const struct uart *u, char *s, int bsize);
extern char *uart_polls_t(const struct uart *u, char *s, char *terminators,
			  int bsize);

/* "device" code, for asynchronous irq-driven uart code */
struct device;
extern int uartdev_init(struct device *dev);
extern int uartdev_exit(struct device *dev);
extern int uartdev_write(struct device *dev,  unsigned long flags,
			 const void *src, int count);

/* local sscanf implementations */
extern int vsscanf(const char *buf, const char *fmt, va_list args);
extern int sscanf(const char *buf, const char *fmt, ...)
	__attribute__((format(scanf, 2, 3)));
