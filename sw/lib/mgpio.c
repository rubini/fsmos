#include <io.h>
#include <mgpio.h>
#include <errno.h>

static struct mgpio_chunk *mgpio_list;

int mgpio_register(struct mgpio_chunk *new)
{
	struct mgpio_chunk *p;
	int first, last;

	if (new->base < GPIO_NUMBER)
		return -EINVAL;

	if (new->nrgpio == 0)
		return 0;

	first = new->base;
	last = first + new->nrgpio;

	for (p = mgpio_list; p; p = p->next) {
		if (first >= p->base && first < p->base + p->nrgpio)
			return -EBUSY;
		if (first < p->base && last > p->base)
			return -EBUSY;
	}
	new->next = mgpio_list;
	mgpio_list = new;
	return 0;
}

/* run-time interfaces, with an helper */
static struct mgpio_chunk *mgpio_find(int gpio)
{
	struct mgpio_chunk *p;

	for (p = mgpio_list; p; p = p->next)
		if (gpio >= p->base && gpio < p->base + p->nrgpio)
			return p;
	return NULL;
}

int mgpio_dir(int gpio, int output, int value)
{
	struct mgpio_chunk *p;

	if (gpio < GPIO_NUMBER) {
		gpio_dir(gpio, output, value);
		return 0;
	}
	p = mgpio_find(gpio);
	if (!p)
		return -ENODEV;
	return p->dir(p->drvdata, gpio - p->base, output, value);
}

int mgpio_get(int gpio)
{
	struct mgpio_chunk *p;

	if (gpio < GPIO_NUMBER)
		return __gpio_get(gpio);
	p = mgpio_find(gpio);
	if (!p)
		return -ENODEV;
	return p->get(p->drvdata, gpio - p->base);
}

int mgpio_set(int gpio, int value)
{
	struct mgpio_chunk *p;

	if (gpio < GPIO_NUMBER) {
		__gpio_set(gpio, value);
		return 0;
	}
	p = mgpio_find(gpio);
	if (!p)
		return -ENODEV;
	return p->set(p->drvdata, gpio - p->base, value);
}

/* a command interface */
int command_mgpio(char **reply, int argc, char **argv)
{
	int gpio, val;
	char c;

	if (sscanf(argv[1], "%i%c", &gpio, &c) != 1)
		return -EINVAL;
	if (argc == 3) {
		/* "gpio <gpio> <value>" */
		if(sscanf(argv[2], "%i%c", &val, &c) != 1)
			return -EINVAL;
		if (val < 0 || val > 1)
			return -EINVAL;
		mgpio_dir(gpio, GPIO_DIR_OUT, val);
		sprintf(*reply, "mgpio %i := %i", gpio, val);
		return 0;
	}
	mgpio_dir(gpio, GPIO_DIR_IN, 0);
	sprintf(*reply, "mgpio %i = %i", gpio, mgpio_get(gpio));
	return 0;
}
