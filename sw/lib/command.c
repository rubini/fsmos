#include <io.h>
#include <string.h>
#include <errno.h>
#include <revision.h>
#include <stack.h>
#include <command.h>

/*
 * Split a line into argc/argv
 */
void command_split_argv(char *s, int *argcp, char **argv, int argvsize)
{
	char *t = s;
	int argc = 0;

	while (argc < argvsize) {
		/* skip blanks, save pointer, skip non-blanks */
		while (*t && (*t == ' ' || *t == '\t'))
			t++;
		if (!*t)
			break;
		argv[argc++] = t;
		if (*t == '"') {
			/* special case: quoted string */
			argv[argc-1] = ++t;
			while (*t && *t != '"')
				t++;
		} else {
			while (*t && (*t != ' ' && *t != '\t'))
				t++;
		}
		if (*t == '\0')
			break;
		*t++ = '\0';
	}
	*argcp = argc;

	if (0) { /* verify what we got */
		int i;
		for (i = 0; i < argc; i++)
			printf("argv[%i] = \"%s\"\n", i, argv[i]);
	}
}

/*
 * Some generic commands that applications may use
 */
int command_commit(char **reply, int argc, char **argv)
{
	*reply = (char *)__gitc__;
	return 0;
}

int command_stack(char **reply, int argc, char **argv)
{
	int sf = stack_free(0);
	sprintf(*reply, "%i free", sf);
	return 0;
}

int command_r(char **reply, int argc, char **argv)
{
	unsigned long *regptr; /* uint32_t, but we need to sscanf it */
	unsigned long reg;

	if (sscanf(argv[1], "%lx", &reg) != 1)
		return -EINVAL;
	regptr = (void *)reg;
	reg = *regptr;
	sprintf(*reply, "0x%08lx = 0x%08lx", (unsigned long)regptr, reg);
	return 0;
}

int command_w(char **reply, int argc, char **argv)
{
	unsigned long *regptr; /* uint32_t, but we need to sscanf it */
	unsigned long reg;

	if (sscanf(argv[1], "%lx", &reg) != 1)
		return -EINVAL;
	regptr = (void *)reg;
	if (sscanf(argv[2], "%lx", &reg) != 1)
		return -EINVAL;
	*(uint32_t *)regptr = reg;
	sprintf(*reply, "0x%08lx := 0x%08lx", (unsigned long)regptr, reg);
	return 0;
}

int command_gpio(char **reply, int argc, char **argv)
{
	int gpio, val, flags = GPIO_AF_GPIO;
	char c;

	if (sscanf(argv[1], "%i%c", &gpio, &c) != 1)
		return -EINVAL;
	while (argc == 3) { /* "while" to avoid "goto" */
		if (!strcmp(argv[2], "pu")) {
			flags |= GPIO_AF_PULLUP;
			break;
		}
		if (!strcmp(argv[2], "pd")) {
			flags |= GPIO_AF_PULLDOWN;
			break;
		}
		/* "gpio <gpio> <value>" */
		if(sscanf(argv[2], "%i%c", &val, &c) != 1)
			return -EINVAL;
		if (val < 0 || val > 1)
			return -EINVAL;
		gpio_dir_af(gpio, GPIO_DIR_OUT, val, GPIO_AF_GPIO);
		sprintf(*reply, "gpio %i := %i", gpio, val);
		return 0;
	}
	gpio_dir_af(gpio, GPIO_DIR_IN, 0, flags);
	sprintf(*reply, "gpio %i = %i", gpio, gpio_get(gpio));
	return 0;
}

static const struct command *current_command_list;
int command_help(char **reply, int argc, char **argv)
{
	const struct command *cp;

	printf("Available commands:\n");
	for (cp = current_command_list; cp->cmd; cp++)
		printf("  %s\n", cp->cmd);
	return 0;
}

extern char *__current_config;
int command_config(char **reply, int argc, char **argv)
{
	printf("%s", __current_config);
	return 0;
}

char *command_args[COMMAND_NARGS];
char command_reply[CONFIG_COMMAND_REPLY_SIZE];

int command_parse(char *s, const struct command *cmds)
{
	const struct command *cp;
	char **argv;
	int argc, ret;
	static char reply_buf[CONFIG_COMMAND_REPLY_SIZE - 4];
	char *reply = reply_buf;

	current_command_list = cmds; /* for help */

	/* trim return+newline */
	if (s[strlen(s)-1] == '\n')
		s[strlen(s)-1] = '\0';
	if (s[strlen(s)-1] == '\r')
		s[strlen(s)-1] = '\0';

	/* argv[-1] will be the identifier, so start at args+1 */
	command_args[0] = ""; argv = command_args + 1;
	command_split_argv(s, &argc, argv, ARRAY_SIZE(command_args) - 1);
	if (!argc || argv[0][0] == '#')
		return 0;

	if (argv[0][0] == '@') {
		argv[0]++; /* identifier, without 'at' */
		argv++; argc--;
	}

	for (cp = cmds; cp->cmd; cp++)
		if (!strcmp(argv[0], cp->cmd))
			break;

	reply[0] = '\0';
	if (cp->cmd_f) {
		if (argc < cp->argc_min || argc > cp->argc_max) {
			sprintf(reply, "%s: argc %i: min %i, max %i",
				cp->cmd, argc, cp->argc_min, cp->argc_max);
			ret = -EINVAL;
		} else {
			ret = cp->cmd_f(&reply, argc, argv);
		}
	} else {
		sprintf(reply, "%s: No such command", argv[0]);
		ret = -ENOENT;
	}

	/* Panic if size of reply is too big? */
	if (ret == COMMAND_RETURN_SILENT)
		ret = 0;
	else if (ret < 0)
		sprintf(command_reply, "KO %s %s %s\n", argv[-1],
			strerror(-ret), reply);
	else
		sprintf(command_reply, "OK %s %s\n", argv[-1], reply);
	return ret;
}
