#include <time.h>
#include <io.h>

int gpio = GPIO_NR(GPIO_PORTC, 14); /* green */
int rpio = GPIO_NR(GPIO_PORTC, 15); /* red */

void main(void)
{
	volatile int i;
	unsigned long j;

	printf("%s\n", __FILE__);
	printf("Clock: %i\n", CPU_FREQ);
	gpio_dir_af(gpio, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	gpio_dir_af(rpio, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);

	while (1) {
		gpio_set(gpio, 1);
		for (i = 0; i < CPU_FREQ / 12; i++)
			;
		printf("jiffies: %li", jiffies);
		gpio_set(gpio, 0);
		udelay(1000 * 1000);
		printf(" %li\n", jiffies);

		/* and another loop based on jiffies */
		gpio_set(rpio, 1);
		j = jiffies + HZ;
		while (time_before(jiffies, j))
			;
		printf("jiffies2: %li", jiffies);
		gpio_set(rpio, 0);
		j += HZ;
		while (time_before(jiffies, j))
			;
		printf(" %li\n", jiffies);

	}
}
