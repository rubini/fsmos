#include <io.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <i2c.h>
#include <i2c-eeprom.h>

int __i2cee_read(struct i2cee_dev *dev, int offset, void *buf, int size)
{
	int ret, offsize = 2;
	uint8_t wbuf[2];
	const struct i2cee_cfg *cfg = dev->cfg;

	/* if flags is zero, previous default of 2b applies */
	if (dev->flags & I2CEE_F_OFF1B)
		offsize = 1;

	if (offset == I2CEE_CONT)
		offset = dev->offset;

	wbuf[0] = offset & 0xff;
	if (offsize == 2) {
		wbuf[0] = offset >> 8;
		wbuf[1] = offset & 0xff;
	}
	cfg->i2c->nak_count = 0;
	ret = i2c_write(cfg->i2c, cfg->addr7, wbuf, offsize,
			I2C_FLAG_NO_STOP);
	if (ret)
		return ret;
	if (cfg->i2c->nak_count)
		return -EIO;
	ret = i2c_read(cfg->i2c, cfg->addr7, buf, size,
		       I2C_FLAG_NO_START | I2C_FLAG_REPSTART);
	if (ret)
		return ret;
	if (cfg->i2c->nak_count)
		return -EIO;
	dev->offset = offset + size;
	return 0;
}

int __i2cee_write(struct i2cee_dev *dev, int offset, const void *buf, int size)
{
	int ret, offsize = 2;
	static uint8_t wbuf[34];
	const struct i2cee_cfg *cfg = dev->cfg;

	if (size > ARRAY_SIZE(wbuf) - 2)
		return -E2BIG;

	/* if flags is zero, previous default of 2b applies */
	if (dev->flags & I2CEE_F_OFF1B)
		offsize = 1;

	if (offset == I2CEE_CONT)
		offset = dev->offset;

	wbuf[0] = offset & 0xff;
	if (offsize == 2) {
		wbuf[0] = offset >> 8;
		wbuf[1] = offset & 0xff;
	}

	memcpy(wbuf + offsize, buf, size);
	cfg->i2c->nak_count = 0;
	ret = i2c_write(cfg->i2c, cfg->addr7, wbuf, offsize + size, 0);
	if (ret)
		return ret;
	if (cfg->i2c->nak_count)
		return -EIO;
	dev->offset = offset + size;
	return ret;
}

/* Same prototype as above, but considering pagesize. Return nread/nwritten */
int i2cee_read(struct i2cee_dev *dev, int offset, void *buf, int size)
{
	int pagesize, len, err;

	pagesize = dev->cfg->pagesize;
	if (!pagesize)
		pagesize = 16; /* minimum, "safe" value */
	while (size) {
		len = pagesize - (offset & (pagesize - 1));
		if (len > size)
			len = size;
		err = __i2cee_read(dev, offset, buf, len);
		if (err < 0)
			return err;
		buf += len;
		offset += len;
		size -= len;
	}
	return 0;
}

int i2cee_write(struct i2cee_dev *dev, int offset, const void *buf, int size)
{
	int pagesize, len, err;

	pagesize = dev->cfg->pagesize;
	if (!pagesize)
		pagesize = 16; /* minimum, "safe" value */
	while (size) {
		len = pagesize - (offset & (pagesize - 1));
		if (len > size)
			len = size;
		err = __i2cee_write(dev, offset, buf, len);
		if (err < 0)
			return err;
		buf += len;
		offset += len;
		size -= len;
		if (size)
			udelay(5500); /* 5ms max, in theory */
	}
	return 0;
}

/* Provide two aliases, receiving a void pointer, used in param.c */
int i2cee_read_vp(void *, int offset, void *buf, int size)
	__attribute__((alias("i2cee_read")));
int i2cee_write_vp(void *, int offset, const void *buf, int size)
	__attribute__((alias("i2cee_write")));



/* Support function for below */
static void printbufs(uint8_t b[4][8], int line)
{
	int i;
	printf("dump @ line %i:\n", line);
	for (i = 0; i < 4; i++) {
		printf(" %02x %02x %02x %02x",
		       b[i][0], b[i][1], b[i][2], b[i][3]);
		printf(" %02x %02x %02x %02x\n",
		       b[i][4], b[i][5], b[i][6], b[i][7]);
	}
}

/*
 * This long autodetect procedure checks if there is something at the
 * address, and possibly whether it takes 1b or 2b offset bytes.
 * It may need to write one byte. With 1M writes endurance, it's acceptable.
 */
#define CONFIG_DEBUG_EEAUTO 0

int i2cee_autodetect_offset_size(struct i2cee_dev *dev)
{
	static uint8_t buf[4][8]; /* 4 test cases, 8 bytes each */
	uint8_t save1[2];
	memset(buf, 0xff, sizeof(buf));

	dev->cfg->i2c->nak_count = 0;
	/* read as 2B: if 2B we get different ranges, if 1B the same (@1) */
	dev->flags = I2CEE_F_OFF2B;
	i2cee_read(dev, 0x0000, buf[0], 8);
	i2cee_read(dev, 0x0001, buf[1], 8);
	/* and reset the counter to "2" so possibly next read is offset by 2 */
	i2cee_read(dev, 0x0000, buf[0], 2);
	/* read as 1B: if 2B we get different ranges (sequential) */
	dev->flags = I2CEE_F_OFF1B;
	i2cee_read(dev, 0x00, buf[2], 8);
	i2cee_read(dev, 0x00, buf[3], 8);
	if (dev->cfg->i2c->nak_count)
		return -ENODEV;

	if (dev->cfg->i2c->nak_count)
		return -ENODEV;

	if (CONFIG_DEBUG_EEAUTO)
		printbufs(buf, __LINE__);

	if (memcmp(buf[0], buf[1], 8) || memcmp(buf[2], buf[3], 8)) {
		dev->flags = I2CEE_F_OFF2B;
		return 0;
	}

	/* Look for a missing exptected offset: if so, it's the other kind */
	if (memcmp(buf[0] + 2, buf[2], 6)) {
		dev->flags = I2CEE_F_OFF1B;
		return 0;
	}
	if (memcmp(buf[0] , buf[2] + 1, 7)) {
		dev->flags = I2CEE_F_OFF2B;
		return 0;
	}

	/* All of them are equal. We know nothing. So, save bytes and write */
	save1[0] = buf[2][0]; save1[1] = buf[2][1];

	buf[0][4] ^= 0xff;
	dev->flags = I2CEE_F_OFF2B;
	/* If really 2B, write to offset 4. Otherwise it writes to 0 and 1 */
	i2cee_write(dev, 0x0004, buf[0]+4, 1);

	dev->flags = I2CEE_F_OFF2B;
	i2cee_read(dev, 0x0001, buf[1], 8);
	i2cee_read(dev, 0x0000, buf[0], 2); /* and the counter is 2 */
	dev->flags = I2CEE_F_OFF1B;
	i2cee_read(dev, 0x00, buf[2], 8);
	i2cee_read(dev, 0x00, buf[3], 8);

	if (CONFIG_DEBUG_EEAUTO)
		printbufs(buf, __LINE__);

	/* restore eeprom, and then return the buffer to changed state */

	/* and compare as before: there must be the offset here */
	if (memcmp(buf[0] + 2, buf[2], 6)) {
		dev->flags = I2CEE_F_OFF1B;
		buf[2][0] = save1[0]; buf[2][1] = save1[1];
		i2cee_write(dev, 0x00, buf[2], 2); /* restore! */
		return 0;
	}
	if (memcmp(buf[0] , buf[2] + 1, 7)) {
		dev->flags = I2CEE_F_OFF2B;
		buf[0][4] ^= 0xff; /* restore! */
		i2cee_write(dev, 0x0004, buf[0]+4, 1);
		return 0;
	}
	return -ENODEV;
}
