#include <io.h>
#include <errno.h>
#include <string.h>
#include <net.h>
#include <ipv4.h>
#include <syslog.h>

/* syslog: a tx-only socket: no queue is there */
static struct socket __static_syslog_socket = {
        .queue.buff = NULL,
        .queue.size = 0,
}, *syslog_socket;

/* These addresses are all broadcast until configured by the user */
static struct sockaddr syslog_sockaddr = {
	.mac = {0xff,0xff,0xff,0xff,0xff,0xff},
};
static struct udp_addr syslog_udpaddr = {
	.saddr = 0xffffffff,
	.daddr = 0xffffffff,
	/* ports are not constant, set at run time */
};

int syslog_config_dst(uint32_t daddr, uint8_t *mac)
{
	syslog_udpaddr.daddr = daddr;
	if (mac)
		memcpy(syslog_sockaddr.mac, mac, sizeof(syslog_sockaddr.mac));
	return 0;
}

int syslog_send(int level, const char *fmt, ...)
{
	static uint8_t xfer_buf[UDP_END + CONFIG_PRINT_BUFSIZE];
	static char *msg_buf = (void *)xfer_buf + UDP_END;
	va_list args;
	int len, ret;
	unsigned char ip[4];
	uint32_t *ip_int = (void *)ip;

	getIP(ip);
	syslog_udpaddr.saddr = *ip_int;

	len = sprintf(msg_buf, "<%i> ", level);

	va_start(args, fmt);
	ret = vsprintf(msg_buf + len, fmt, args);
	va_end(args);

	getIP(ip);
	syslog_udpaddr.saddr = *ip_int;

	len = UDP_END + len + ret;
	fill_udp(xfer_buf, len, &syslog_udpaddr);
	socket_sendto(syslog_socket, &syslog_sockaddr, xfer_buf, len);
	return ret;
}

/* This can't be an initcall, as it depends on network setup */
int syslog_init(void)
{
	syslog_socket = socket_create(&__static_syslog_socket, NULL,
				      SOCK_UDP, 514);
	if (!syslog_socket)
		return -EIO;
	syslog_udpaddr.dport = syslog_udpaddr.sport = htons(514);
	return 0;
}
