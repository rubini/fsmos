/*
 * SPI generic interface
 * Alessandro Rubini, 2012 GNU GPL2 or later
 */
#ifndef __BATHOS_SPI_H__
#define __BATHOS_SPI_H__

#include <stdint.h>
#include <io.h>

/* This is an interface for SPI, master and slave */
struct spi_cfg {
	int gpio_cs;		/* Either GPIO number or SPI_CS(x) */
	int freq;		/* Suggested frequency */
	int timeout;		/* Jiffies */
	unsigned long flags;
	uint8_t pol, phase, bits, devn; /* devn selects spi0 or spi1 or more */
};

#define SPI_CS(x)		(-1 - (x)) /* arg is 0.., value is negative */
#define __SPI_IS_HW_CS(x)	((x) < 0)
#define __SPI_GET_HW_CS(x)	(-(x) - 1)

#define SPI_CFG_SLAVE		0x8000
#define SPI_CFG_VERBOSE		0x4000
#define SPI_CFG_ALT_SCK(x)	((x) & 3) << 2) /* 0..3 -> 0x04..0x0c */
#define SPI_CFG_ALT_MISO(x)	((x) & 3) << 4) /* 0..3 -> 0x10..0x30 */
#define SPI_CFG_ALT_MOSI(x)	((x) & 3) << 6) /* 0..3 -> 0x40..0xc0 */

#define __SPI_ALT_SCK(flags)	(((flags) >> 2) & 3)
#define __SPI_ALT_MISO(flags)	(((flags) >> 4) & 3)
#define __SPI_ALT_MOSI(flags)	(((flags) >> 6) & 3)

#define SPI_DEFAULT_TIMEOUT  (HZ/10) /* as previously hardwired */

/* Data transfers include read and write, either or both */
struct spi_ibuf {
	int len;
	union {
		uint8_t *buf;
		uint16_t *buf16;
	};
};

struct spi_obuf {
	int len;
	union {
		const uint8_t *buf;
		const uint16_t *buf16;
	};
};

/* Usually a data transfer is a single buffer, but we may need to chain more */
enum spi_flags {
    SPI_F_DEFAULT = 0,
    SPI_F_NOINIT = 1,	/* Don't lower CS (and don't take the lock) */
    SPI_F_NOFINI = 2,	/* Don't raise CS (and don't release the lock) */
    SPI_F_NOCS = 3,	/* Dont' act on CS at all (nor on the lock) */
};

/* This would be opaque if we had malloc, but we miss it by now */
struct spi_dev {
	const	struct spi_cfg *cfg;
	unsigned long base;
	int	current_freq;
};

extern struct spi_dev *spi_create(struct spi_dev *cfg);
extern void spi_destroy(struct spi_dev *dev);
extern int spi_xfer(struct spi_dev *dev,
		    enum spi_flags flags,
		    const struct spi_ibuf *ibuf,
		    struct spi_obuf *obuf);

/* Sometimes we want to only read or only write */
static inline int spi_read(struct spi_dev *dev,
			   enum spi_flags flags,
			   const struct spi_ibuf *ibuf)
{
	return spi_xfer(dev, flags, ibuf, NULL);
}

static inline int spi_write(struct spi_dev *dev, enum spi_flags flags,
			    struct spi_obuf *obuf)
{
	return spi_xfer(dev, flags, NULL, obuf);
}

/* And sometimes we want raw-read  and write (with no buffer) */
static inline int spi_raw_read(struct spi_dev *dev, enum spi_flags flags,
			       int len, void *buf)
{
	struct spi_ibuf b = {len, .buf = buf};
	return spi_read(dev, flags, &b);
}

static inline int spi_raw_write(struct spi_dev *dev, enum spi_flags flags,
				int len, const void *buf)
{
	struct spi_obuf b = {len, .buf = buf};
	return spi_write(dev, flags, &b);
}

#endif /* __BATHOS_SPI_H__ */
