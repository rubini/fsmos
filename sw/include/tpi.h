/*
 *  Atmel "Tiny Programmer Interface" for attiny
 */
#ifndef __TPI_H__
#define __TPI_H__

struct tpi_pin {
	int gpio;
	int flags;
};
#define TPI_FLAG_INVERT			1
/* data out, if different from data in, should be forced to 0 or 1 */
#define TPI_FLAG_RECESSIVE_IS_0		2
#define TPI_FLAG_RECESSIVE_IS_1		4
#define TPI_FLAG_PULLUP			8


struct tpi_hw {
	struct tpi_pin reset;
	struct tpi_pin clock;
	struct tpi_pin data_in;
	struct tpi_pin data_out;
};

struct tpi_data {
	int address;
	int len;
	uint8_t data [32];
};

enum tpi_command {
	TPI_COMMAND_INIT = 0,
	TPI_COMMAND_FINI,
	TPI_COMMAND_LOAD,
	TPI_COMMAND_STORE,
	TPI_COMMAND_CHIPERASE,
	TPI_COMMAND_IN,
	TPI_COMMAND_OUT,
};

/* addresses for read/write */
#define TPI_ADDR_SRAM	0x0040
#define TPI_ADDR_LOCK	0x3f00
#define TPI_ADDR_CFG	0x3f40
#define TPI_ADDR_CALIB	0x3f80
#define TPI_ADDR_DEVID	0x3fc0
#define TPI_ADDR_FLASH	0x4000

/* internal protocol bytes */
#define TPI_PROTO_SLD		0x20
#define TPI_PROTO_SLDI		0x24
#define TPI_PROTO_SST		0x60
#define TPI_PROTO_SSTI		0x64
#define TPI_PROTO_SSTPR(a)	(0x68 | (a)) /* a == 0 or 1 */
#define TPI_PROTO_SIN(a)	(0x10 | (((a) & 0x30) << 1) | (a & 0xf))
#define TPI_PROTO_SOUT(a)	(0x90 | (((a) & 0x30) << 1) | (a & 0xf))
#define TPI_PROTO_SLDCS(a)	(0x80 | (a)) /* 0..f */
#define TPI_PROTO_SSTCS(a)	(0xc0 | (a)) /* 0..f */
#define TPI_PROTO_SKEY		0xe0

/* This uses the TPI_COMMAND commands above */
int tpi_action(struct tpi_hw *hw, enum tpi_command, struct tpi_data *data);

/* The following ones are higher level. They INIT and FINI by default */
#define TPI_DEFAULT  0x00
#define TPI_NO_INIT  0x40
#define TPI_NO_FINI  0x80

/* returns number of differences, or negative error */
int tpi_check_binary(struct tpi_hw *hw, int flags, const void *code,
		     int codesize);
/* returns 0 or negative error */
int tpi_write_binary(struct tpi_hw *hw, int flags, const void *code,
		     int codesize);

#endif /* __TPI_H__ */
