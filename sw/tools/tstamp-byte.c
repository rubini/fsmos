#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
	int fd, i;
	char buf[16];

	if (argc != 2) {
		fprintf(stderr, "%s: Use \"%s <fname>\"\n", argv[0], argv[0]);
		exit(1);
	}
	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1],
			strerror(errno));
		exit(1);
	}

	/* if uart, it is already configured */
	while (1) {
		char s[80];
		struct timeval tv;
		int diff, n;
		static int prev_usec;

		i = read(fd, buf, sizeof(buf));
		gettimeofday(&tv, NULL);
		if (i < 0) {
			fprintf(stderr, "%s: read(): %s\n", argv[0],
				strerror(errno));
			exit(1);
		}
		if (!i)
			exit(0);
		diff = tv.tv_usec - prev_usec;
		if (diff < 0) diff += 1000 * 1000;
		n = sprintf(s, ": %3i.%03i (delta %3i.%03i)\n",
			(int)tv.tv_usec / 1000, (int)tv.tv_usec % 1000,
			diff / 1000, diff % 1000);
		prev_usec = tv.tv_usec;
		write(STDOUT_FILENO, buf, i);
		write(STDOUT_FILENO, s, n);
	}
}
