#include <io.h>
#include <stdint.h>
#include <iap.h>
#include <errno.h>

const unsigned char iap_errno[__IAP_NR_ERRORS] = {
	[IAP_ERR_SUCCESS] = 0,
	[IAP_ERR_INVALID_CMD] = ENOENT,
	[IAP_ERR_SRC_ALIGN] = EIO,
	[IAP_ERR_DST_ALIGN] = EIO,
	[IAP_ERR_SRC_UNMAP] = EFAULT,
	[IAP_ERR_DST_UNMAP] = EFAULT,
	[IAP_ERR_COUNT] = EINVAL,
	[IAP_ERR_INVALID_SEC] = EINVAL,
	[IAP_ERR_NOT_BLANK] = EBUSY,
	[IAP_ERR_NOT_PREPARED] = EBUSY,
	[IAP_ERR_COMPARE] = EAGAIN,
	[IAP_ERR_BUSY] = EBUSY,
	/* In theory, the rest are ISP only, but compare can return unmap */
	[IAP_ERR_PARAM] = EINVAL,
	[IAP_ERR_ADDR_ALIGN] = EIO,
	[IAP_ERR_ADDR_UNMAP] = EFAULT,
	[IAP_ERR_LOCKED] = EPERM,
	[IAP_ERR_CODE] = EPERM,
	[IAP_ERR_BAUD] = EINVAL,
	[IAP_ERR_STOP] = EINVAL,
	[IAP_ERR_PROTECTION] = EPERM,
};

/* Some cores can erase page, all of them can erase sector and copy page */
#define LPC_SECTOR_SIZE 4096
#define LPC_PAGE_SIZE 256

static uint32_t iap_args[5], iap_ret[1];


int iap_erase_flash(unsigned long start, int count)
{
	int ret;

	if ((start % LPC_SECTOR_SIZE) || (count % LPC_SECTOR_SIZE))
		return -EINVAL;
	iap_args[0] = IAP_CMD_PREPARE;
	iap_args[1] = start / LPC_SECTOR_SIZE;
	iap_args[2] = (count / LPC_SECTOR_SIZE) - 1 + iap_args[1]; /* end sec */
	iap_args[3] = CPU_FREQ / 1000;
	ret = iap_call(iap_args, iap_ret);
	if (ret < 0)
		return ret;
	iap_args[0] = IAP_CMD_ERASESEC;
	ret = iap_call(iap_args, iap_ret);
	if (ret < 0)
		return ret;
	return 0;
}

int iap_copy_to_flash(unsigned long dest, void *src, int count)
{
	unsigned long lsrc = (unsigned long)src;
	int ret;

	/* actually, count can only be 256/512/1024/4096 */
	if ((dest % LPC_PAGE_SIZE) || (count % LPC_PAGE_SIZE))
		return -EINVAL;
	if (lsrc & 0x3)
		return -EINVAL;

	/* Bah! we must "prepare" again. Assume it's within one sector */
	iap_args[0] = IAP_CMD_PREPARE;
	iap_args[1] = dest / LPC_SECTOR_SIZE;
	iap_args[2] = iap_args[1];
	iap_args[3] = CPU_FREQ / 1000;
	ret = iap_call(iap_args, iap_ret);
	if (ret < 0)
		return ret;
	iap_args[0] = IAP_CMD_CPFLASH;
	iap_args[1] = dest;
	iap_args[2] = lsrc;
	iap_args[3] = count;
	iap_args[4] = CPU_FREQ / 1000;
	ret = iap_call(iap_args, iap_ret);
	if (ret < 0)
		return ret;
	return ret;
}

void __attribute__((noreturn)) iap_reset(void)
{
	*(uint32_t *)0xe000ed0c = 0x05fa0004;
	while(1); /* bah! */
}
