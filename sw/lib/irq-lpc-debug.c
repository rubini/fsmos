#include <irq.h>
#include <panic.h>
#include <debug-registers.h>
#include <timestamp-lpc.h>

struct irq_frame {
	uint32_t r0, r1, r2, r3, r12, lr, pc, psr;
};

static __attribute__((noinline)) void irq_saveregs(void)
{
	registers_save();
}

static __attribute__((noinline,noreturn)) void irq_printregs(void)
{
	int irq;
	uint32_t fraction, fnew, sp;
	unsigned long j;
	struct irq_frame *stack;
	unsigned long *p;

	/* First, recover the timestamp */
	j = jiffies;
	fnew = regs[REG_TMR32B1PC];
	fraction = cpu_regs.r[1];
	if (fnew < fraction)
		j = jiffies - 1;

	/* Then the other arguments we didn't receive in C */
	irq = cpu_regs.r[0];
	sp = cpu_regs.r[2];
	stack = (void *)sp;

	/* And now report */
	printf("*** IRQ %i (nvic irq %i) @ 0x%08lx\n",
	       irq, irq + 16, stack->pc);
	printf("Jiffies: %li + %li / %i clocks (%08lx:%08lx)\n",
	       j, fraction, CPU_FREQ / HZ, j, fraction);
	printf("  r0 %08lx   r1 %08lx   r2 %08lx   r3 %08lx\n",
	       stack->r0, stack->r1, stack->r2, stack->r3);
	printf(" (r4 %08lx   r5 %08lx   r6 %08lx   r7 %08lx)\n",
	       cpu_regs.r[4], cpu_regs.r[5], cpu_regs.r[6], cpu_regs.r[7]);
	printf(" (r8 %08lx   r9 %08lx  r10 %08lx  r11 %08lx)\n",
	       cpu_regs.r[8], cpu_regs.r[9], cpu_regs.r[10], cpu_regs.r[11]);
	printf(" r12 %08lx   sp %08lx   lr %08lx   pc %08lx\n",
	       stack->r12, sp + 32, stack->lr, stack->pc);
	printf(" psr %08lx\n", stack->psr);

	/* And some stack words */
	printf("Stack:\n");
	sp += 32;
	for (j = sp, p = (void *)sp; j < sp + 128; j += 32, p += 4) {
		printf("%p: %08lx %08lx %08lx %08lx\n", p,
		       p[0], p[1], p[2], p[3]);
	}

	panic(129, "Unexpected irq %i\n", irq);
}

/*
 * The following code only saves the link register before calling
 * irq_saveregs() -- actually it could save nothing at all!
 * So, irq_printregs() has a full register set. Besides those that
 * were saved and modified by the callse (i.e. irq_entry() ).
 */
void irq_diag(int irq, uint32_t timestamp_fraction, uint32_t sp)
{
	irq_saveregs();
	irq_printregs();
}
