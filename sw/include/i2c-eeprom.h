#ifndef __I2C_EEPROM_H__
#define __I2C_EEPROM_H__
#include <i2c.h>

struct i2cee_cfg {
	struct i2c_dev *i2c;
	int addr7;
	int pagesize;
};

struct i2cee_dev {
	const struct i2cee_cfg *cfg;
	int offset;
	int flags;
};

#define I2CEE_CONT -1

#define I2CEE_F_OFF1B	0x0001
#define I2CEE_F_OFF2B	0x0002

int i2cee_autodetect_offset_size(struct i2cee_dev *dev);

/* The following functions take care of dev->pagesize (return: 0 or negative) */
int i2cee_read(struct i2cee_dev *dev, int offset, void *buf, int size);
int i2cee_write(struct i2cee_dev *dev, int offset, const void *buf, int size);

/* The following two are the same, with a weaker prototype (void pointer) */
int i2cee_read_vp(void *dev, int offset, void *buf, int size);
int i2cee_write_vp(void *dev, int offset, const void *buf, int size);

/* The following functions go straight to the eeprom (return: 0 or negative) */
int __i2cee_read(struct i2cee_dev *dev, int offset, void *buf, int size);
int __i2cee_write(struct i2cee_dev *dev, int offset, const void *buf, int size);

#endif /* __I2C_EEPROM_H__ */
