#include <io.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <command.h>
#include <usb.h>
#include <usb-serial.h>
#include <modbus.h>

/* the host board I use for testing */
#define GPIO_RX_ENAL	GPIO_NR(1,22)
#define GPIO_TX_ENAH	GPIO_NR(0,15)
#define GPIO_LEDY	GPIO_NR(1,31)
#define GPIO_LEDG	GPIO_NR(1,21)

struct usbser usbser = {
	.flags = 0,
}, *us;

/* Override puts -- and never use the uart for diagnostics */
int puts(const char *s)
{
	if (!CONFIG_HAS_USB)
		return console_puts(s);
	if (!memcmp("USB: ", s, 5))
		return 0; /* usb diagnostic message */
	if (us->ud->state != USB_CONFIGURED)
		return 0;
	return usbser_puts(us, s);
}

/*
 * Definition of registers, to be read/written via modbus
 */
uint16_t reg5[1]; /* write */
/* reg4 (speed) is same as 256 */
uint16_t regs255_265[11] = {1, 0, 1000, 500, 520, 0x9000, 70, 9, 10, 20, 30};

static int mbs_cb(struct modbus_register *self,
		  int regname, int action, int value)
{
	printf("reg %i (%p) = %i (action %i)\n", regname, self, value, action);
	return 0;
}


struct modbus_register mbs_regs[] = {
	{
		.reg_name = 4,
		.count = 1,
		.flags = MODBUS_READ | MODBUS_WRITE,
		.reg_addr = regs255_265 + 1, /* 256 */
		.callback = mbs_cb,
	}, {
		.reg_name = 5,
		.count = 1,
		.flags = MODBUS_READ | MODBUS_WRITE,
		.reg_addr = reg5,
		.callback = mbs_cb,
	}, {
		.reg_name = 255,
		.count = ARRAY_SIZE(regs255_265),
		.flags = MODBUS_READ,
		.reg_addr = regs255_265,
		.callback = mbs_cb,
	}
};

/*
 * Modbus infrastructure
 */
uint8_t mbs_recv_buf[sizeof(struct modbus_req3) + 32]; /* just extra space */
uint8_t mbs_send_buf[sizeof(struct modbus_resp34) + 32]; /* data, crc + xtra */

struct modbus_req3 *req3 = (void *)mbs_recv_buf;
struct modbus_resp34 *resp3 = (void *)mbs_send_buf;

/* This is a synchronous send, and we read back what we write */
static int mbs_send(struct modbus_device *dev, void *msg, int msglen,
		unsigned long when)
{
	uint8_t *s = msg;
	int i;

	printf("send %i bytes\n", msglen);
	while (time_before(jiffies, when))
		;

	gpio_set(GPIO_TX_ENAH, 1);
	for (i = 0; i < msglen; i++) {
		putc(s[i]);
		getc();
	}
	dev->last_byte = jiffies;
	gpio_set(GPIO_TX_ENAH, 0);
	return 0;
}

struct modbus_device mbs_dev = {
	.address = 1,
	.regarray = mbs_regs,
	.regcount = ARRAY_SIZE(mbs_regs),
	.send = mbs_send,
	.resp_delay = ms_to_jiffies_const(10),
	.inter_frame = ms_to_jiffies_const(5), /* 9600 baud */
};

/*
 * And the polling/parsing function
 */
void mbs_poll(struct modbus_device *dev)
{
	static int nb; /* nbyte */
	int i, c;

	c = pollc();
	if (c >= 0) { /* one byte: enqueue */
		mbs_recv_buf[nb++] = c;
		dev->last_byte = jiffies;
		if (nb == sizeof(mbs_recv_buf)) {
			nb = 0; /* overflow: start anew */
			return;
		}
	}
	/* no byte: if beginning of buffer, ignore */
	if (!nb)
		return;

	/* no byte and some data enqueued: check timeout */
	if (time_before(jiffies, dev->last_byte + dev->inter_frame))
		return;

	/* inter-frame timeout: check request */
	i = modbus_request_valid(dev, mbs_recv_buf, nb);
	if (i != -ENXIO)
		printf("got %i bytes @%li, valid = %i\n", nb, jiffies, i);
	i = modbus_parse(dev, mbs_recv_buf, nb, sizeof(mbs_recv_buf));
	if (i < 0 && i != -ENXIO)
		printf("   parse = %s\n", strerror(-i));
	else
		printf("   parse = %i\n", i);
	/* trim anyways: we already got inter-frame delay */
	nb = 0;
}

/* we have no commands, we are just slaves */
void main(void)
{
	static char str[80];

	if (!CONFIG_HAS_USB) {
		panic(1, "No usb, this test can't run\n");
	}
	us = usbser_init(&usbser);
	if (!us)
		panic(15, "Can't init usb serial");

	gpio_dir_af(GPIO_LEDY, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(GPIO_LEDG, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	/* rx enable is on, tx enable is off */
	gpio_dir_af(GPIO_RX_ENAL, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(GPIO_TX_ENAH, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);

	while (1) {
		gpio_set(GPIO_LEDG, (jiffies / (HZ / 4)) & 1);
		if (usbser_gets(us, str, sizeof(str)-1) > 0) {
			printf("no input\n");
		}
		/* just accumulate a request, and obey to it */
		mbs_poll(&mbs_dev);
	}
}
