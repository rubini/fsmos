#ifndef __REGS_LPC1_OLD_H__
#define __REGS_LPC1_OLD_H__
/*
 * 11xx and 13xx feature two generations. Older processors have
 * a 12 bit wide GPIO port, and their own USB slave logic cell.
 * Newer ones have a 32 bit wide GPIO port and a different USB slave.
 *
 * Then, uart0 vs. uart, ssp0 vs. ssp
 */
#include <regs-lpc1-common.h>

#define REGBASE_SPI0		(0x40040000 / 4)
#define REGBASE_SPI1		(0x40058000 / 4) /* only 112x, apparently */

#define REG_FLASHTIM		(0x4003c010 / 4)

#define REG_U0OSR		0x0 /* this register does not exist */
#define REGOFF_UARTOSR		0x0 /* again, prevent a build error */

#ifdef CONFIG_LPC112x /* These have 3 uarts */
#define REGBASE_UART1		(0x40020000 / 4)
#define REGBASE_UART2		(0x40024000 / 4)
#endif


#ifndef CONFIG_LPC112x
/* Interrupts for LPC11xx and LPC13xx, old flavour -- but not 1124/1125*/
enum lpc_irq {
	LPC_IRQ_PIN0		= 0,
#define LPC_IRQ_PIN(x)  (LPC_IRQ_PIN0 + (x))
	LPC_IRQ_PINLAST		= 39,
	LPC_IRQ_I2C0		= 40,
	LPC_IRQ_CT16B0		= 41,
	LPC_IRQ_CT16B1		= 42,
	LPC_IRQ_CT32B0		= 43,
	LPC_IRQ_CT32B1		= 44,
	LPC_IRQ_SSP		= 45,
	LPC_IRQ_USART		= 46,
	LPC_IRQ_USART0		= 46, /* alt name, compat */
	LPC_IRQ_USB_IRQ		= 47,
	LPC_IRQ_USB_FIQ		= 48,
	LPC_IRQ_ADC		= 49,
	LPC_IRQ_WWDT		= 50,
	LPC_IRQ_BOD		= 51,
	/* 52 reserved */
	LPC_IRQ_PIO3		= 53,
	LPC_IRQ_PIO2		= 54,
	LPC_IRQ_PIO1		= 55,
	LPC_IRQ_PIO0		= 56,
	__NR_IRQS,
};
#else /* LPC1124 and LPC1125 have a different interrupt list */
enum lpc_irq {
	LPC_IRQ_PI0		= 0,
#define LPC_IRQ_PIO(x)  (LPC_IRQ_PI0 + (x))
	LPC_IRQ_PIOLAST		= 12,
	LPC_IRQ_ADC_B		= 13,
	LPC_IRQ_SSP1		= 14,
	LPC_IRQ_I2C		= 15,
	LPC_IRQ_CT16B0		= 16,
	LPC_IRQ_CT16B1		= 17,
	LPC_IRQ_CT32B0		= 18,
	LPC_IRQ_CT32B1		= 19,
	LPC_IRQ_SSP0		= 20,
	LPC_IRQ_UART0		= 21,
	LPC_IRQ_USART0		= 21, /* alt name, compat */
	LPC_IRQ_UART1		= 22,
	LPC_IRQ_UART2		= 23,
	LPC_IRQ_ADC_A		= 24,
	LPC_IRQ_WDT		= 25,
	LPC_IRQ_WWDT		= 25, /* alt name, compat */
	LPC_IRQ_BOD		= 26,
	/* 27 reserved */
	LPC_IRQ_PIO3		= 28,
	LPC_IRQ_PIO2		= 29,
	LPC_IRQ_PIO1		= 30,
	LPC_IRQ_PIO0		= 31,
	__NR_IRQS,
};
#endif /* lpc flavour */

#endif /* __REGS_LPC1_OLD_H__ */
