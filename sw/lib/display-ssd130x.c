/*
 * Oled controller: Solomon SSD1306 and SSD1307
 */
#include <io.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <display-ssd130x.h>
#define SSD_VERBOSE 0

const uint8_t ssd13_default_init[] = {
	SSD13_CMD,
	0xd5, 0x80, /* clock divide ratio */
	0xd9, 0x22, /* precharge period */
	0xdb, 0x20, /* adjust vcom reg output */
	0x8d, /* ? */
	0x14, /* ? */
	0xae, /* display off */
	0xc8, /* output scan direction */
	0xa8, 0x3f, /* mux ratio */
	0xd3, 0x00, /* vertical shift */
	0x81, 0xff, /* contrast (max) */
	0x20, 0x00, /* horizontal addressing mode */
	0xda, 0x12, /* set pin configuration */
	0xa1, /* mapping of column address */
	0x21, 0x00, 0x7f, /* columnt start and end */
	0x22, 0x00, 0x07, /* page start and end */
	0x40, /* ram display offset 0 */
	0xd3, 0x00, /* vertical shift is 0 */
	0xa6, /* normal (not inverse) */
	0x2e, /* deactivate scrolling */
	0xa4, /* follow ram (a5 is all-on) */
	/* 0xaf = turn on, but we'd better keep off until written */
};
const int ssd13_default_init_len = ARRAY_SIZE(ssd13_default_init);

/* low level access: first by is 0x00 or 0x40 */
int ssd13_write(struct ssd13_dev *dev, const uint8_t *buf, int len)
{
	const struct ssd13_cfg *c = dev->cfg;

	if (buf[0] != SSD13_CMD && buf[0] != SSD13_DATA)
		return -EINVAL;
	c->i2c->nak_count = 0;
	if (SSD_VERBOSE)
		printf("%s: %p (len %i): ", __func__, buf, len);
	i2c_writeregs(c->i2c, c->addr7, buf, len-1);
	if (SSD_VERBOSE)
		printf("%i naks\n ", c->i2c->nak_count);
	return c->i2c->nak_count ?  -EIO : 0;
}

int ssd13_reinit(struct ssd13_dev *dev)
{
	const uint8_t *buf = dev->cfg->init_string;
	int len = dev->cfg->init_len;

	if (!buf) {
		buf = ssd13_default_init;
		len = ssd13_default_init_len;
	}
	return ssd13_write(dev, buf, len);
}

struct ssd13_dev *ssd13_create(struct ssd13_dev *dev, int *errno)
{
	int ret;

	ret = ssd13_reinit(dev);
	if (errno)
		*errno = ret;
	return ret < 0 ? NULL : dev;
}

/* Font support follows, it is not complete (we miss 8x16h) */
void ssd13_goto(struct ssd13_dev *dev, int x, int y)
{
	dev->x = x;
	dev->y = y;
}

void ssd13_putc(struct ssd13_dev *dev, char c)
{
	const uint8_t *fdata;
	const struct ssd13_cfg *cfg = dev->cfg;
	const struct font *font = cfg->font;
	int pix_x, pix_y, max, fonth;
	int wid, hei; /* may be fake, see below */
	uint8_t databuf[17] = {SSD13_DATA,};
	uint8_t cmdbuf[5] = {SSD13_CMD,};

	if (!font)
		return;

	/* Align y: we can only write at a multiple of 8 pixels */
	fonth = (font->hei + 7) & ~7;

	wid = font->wid;
	hei = fonth;

	/* We have 16 + 48 pixy: a big fonr is better placed on a 8x8 grid */
	if (hei > 16) {
		wid = 8;
		hei = 8;
	}

	max = cfg->wid - font->wid;
	pix_x = dev->x * wid;
	if (pix_x > max) pix_x = max;


	max = cfg->hei - ((font->hei + 7) & ~7);
	pix_y = dev->y * hei;

	if (pix_y > max) pix_y = max;

	dev->x += (font->wid / wid); /* for big font on 8x8, increment more */

	fdata = font_get_hv(font, c, FONT_FLAG_LSB | FONT_FLAG_V);
	do {
		/* goto position */
		cmdbuf[1] = 0xb0 + pix_y / 8;
		cmdbuf[3] = 0x10 + pix_x / 0x10;
		cmdbuf[2] = 0x00 + pix_x % 0x10;
		ssd13_write(dev, cmdbuf, 4);

		/* data */
		memcpy(databuf + 1, fdata, font->wid);
		ssd13_write(dev, databuf, font->wid + 1);

		fdata += font->wid;
		pix_y += 8;
		fonth -= 8;
	} while (fonth);
}

/* A trivial low-level thing to draw lines of graphics instead of text */
void ssd13_raw_graphics(struct ssd13_dev *dev, int x, int line,
			const uint8_t *data, int datalen)
{
	uint8_t cmdbuf[5] = {SSD13_CMD,};

	cmdbuf[1] = 0xb0 + line;
	cmdbuf[3] = 0x10 + x / 0x10;
	cmdbuf[2] = 0x00 + x % 0x10;
	ssd13_write(dev, cmdbuf, 4);
	ssd13_write(dev, data, datalen);
}

