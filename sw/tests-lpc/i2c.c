#include <io.h>
#include <i2c.h>
#include <i2c-eeprom.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <command.h>


struct i2c_cfg i2c_cfg = {
	.gpio_sda = GPIO_NR(0,5),
	.gpio_scl = GPIO_NR(0,4),
#ifndef CONFIG_LPC1_OLD
	.flags = I2C_FLAG_HW_OPENDRAIN,
#else
	.flags = 0, /* 1113, w/ 1343 gpio cell, can't read unless input */
#endif
};
struct i2c_dev i2c_dev = {
	.cfg = &i2c_cfg,
}, *d;

/* negative return value is -(1000 + argc) of the argument in error */
int command_i2c(char **reply, int argc, char **argv)
{
	uint8_t buffer[32];
	int i, n, addr7, byte, ret;
	char c;

	if (sscanf(argv[1], "%x%c", &addr7, &c) != 1)
		return -1001;

	if (strcmp(argv[2], "d") == 0) {

		/* detect, even if we need to pass fake arguments */
		static struct i2cee_cfg eecfg = {
		};
		static struct i2cee_dev ee = {
			.cfg = &eecfg,
		};

		eecfg.addr7 = addr7;
		eecfg.i2c = d;
		i2cee_autodetect_offset_size(&ee);
		printf("flags now 0x%x\n", ee.flags);
		return 0;
	}

	if (strcmp(argv[2], "w") == 0) {
		/* i2c <addr7> w <byte> [<byte> ...] */
		for (i = 3; i < argc; i++) {
			if (sscanf(argv[i], "%x%c", &byte, &c) != 1)
				return -1000 - i;
			buffer[i - 3] = byte;
		}
		d->nak_count = 0;
		ret = i2c_write(d, addr7, buffer, i - 3, 0);
		if (ret < 0)
			return ret;
		sprintf(*reply, "written %i bytes to %x (got %i nak)\n",
		       i - 3, addr7, d->nak_count);
		return 0;
	}

	memset(buffer, 0x55, sizeof(buffer));
	if (strcmp(argv[2], "r") == 0) {
		/* i2c <addr7> r <n> [<byte> ...] */
		if (sscanf(argv[3], "%i%c", &n, &c) != 1)
			return -1003;
		if (n < 1)
			return -EINVAL;
		for (i = 4; i < argc; i++) {
			if (sscanf(argv[i], "%x%c", &byte, &c) != 1)
				return -1000 - i;
			buffer[i - 4] = byte;
		}
		d->nak_count = 0;
		if (argc > 4) {
			ret = i2c_write(d, addr7, buffer, i - 4,
					I2C_FLAG_NO_STOP);
			if (ret < 0)
				return ret;
			ret = i2c_read(d, addr7, buffer, n, I2C_FLAG_NO_START
				       | I2C_FLAG_REPSTART);
		} else {
			ret = i2c_read(d, addr7, buffer, n, 0);
		}
		if (ret < 0)
			return ret;
		for (i = 0; i < n; i++)
			printf("%02x ", buffer[i]);
		printf("\n");
		sprintf(*reply, "read %i bytes from %x (got %i nak)\n",
		       n, addr7, d->nak_count);
		return 0;
	}
	return -1002;
}

/* scan the bus reading register 0. Print number of nak might be ok */
int command_scan(char **reply, int argc, char **argv)
{
	int addr, ret;
	unsigned char buffer[4];

	for (addr = 0x8; addr < 0x78; addr++) {
		printf("%02x: ", addr);
		d->nak_count = 0;
		buffer[0] = 0;
		ret = i2c_write(d, addr, buffer, 1,
				I2C_FLAG_NO_STOP);
		if (ret < 0) {
			i2c_reset(d);
			printf("error %i while writing addr\n", ret);
			continue;
		}
		ret = i2c_read(d, addr, buffer, 1, I2C_FLAG_NO_START
				       | I2C_FLAG_REPSTART);
		if (ret < 0) {
			i2c_reset(d);
			printf("error %i while reading reg\n", ret);
			continue;
		}
		printf ("reg 0 = %02x (%i nak)\n", buffer[0], d->nak_count);
	}
	return 0;
}



static struct command i2ctest_commands[] = {
	COMMAND_COMMIT,
	COMMAND_STACK,
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	{"i2c",    command_i2c,      4, 10},
	{"scan",    command_scan,    1, 1},
	{}
};

/* 17xx has a different PLL. we only support oscillator, there (so far) */
#ifndef CONFIG_LPC17
/* This board may have no oscillator: use internal RC */
uint32_t  board_get_clksel(void)
{
        return REG_SYSPLLCLKSEL_IRC;
}
#endif /* !CONFIG_LPC17 */

void main(void)
{
	char str[80] = "";

	printf("i2c test program\n");

        d = i2c_create(&i2c_dev);
        if (!d)
                panic(0, "Can't create I2C device\n");

	while (1) {
		if (!polls(str, sizeof(str)))
			continue;
		if (str[strlen(str)-1] == '\n')
			str[strlen(str)-1] = '\0';
		if (str[strlen(str)-1] == '\r')
			str[strlen(str)-1] = '\0';
		command_parse(str, i2ctest_commands);
		puts(command_reply);
		str[0] = '\0';
	}
}
