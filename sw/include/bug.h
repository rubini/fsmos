#ifndef __BUG_H__
#define __BUG_H__

/* The mandatory BUILD_BUG */
#define BUILD_BUG_ON(condition) ((void)sizeof(char[1 - 2*!!(condition)]))
#define BUILD_BUG()     BUILD_BUG_ON(1)

#endif
