#include <stdint.h>

static inline void crc32_init(uint32_t *crc)
{
	*crc = ~0L;
}

extern void crc32_partial(void *area, int len, uint32_t *crc);
extern uint32_t crc32(void *buf, int len);

static inline void crc32_fini(uint32_t *crc)
{
	*crc ^= ~0L;
}

extern uint16_t crc16(uint16_t poly, uint8_t *buf, int len);
