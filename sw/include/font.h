#ifndef __FONT_H__
#define __FONT_H__

#include <stdint.h>

struct font {
	unsigned long flags;
	short wid, hei;
	int first, last; /* ascii */
	const uint8_t *data;
};
#define FONT_FLAG_H	0x0001	/* MSB is left */
#define FONT_FLAG_V	0x0002	/* MSB is top */
#define FONT_FLAG_LSB	0x0004	/* The opposite */
/* NOTE: fonts are not expected to be LSB, but in special cases */

extern const struct font font_5x8v;
extern const struct font font_5x7v;
extern const struct font font_4x6v;
extern const struct font font_8x16h;
extern const struct font font_8x8h;
extern const struct font font_16x32h;
extern const struct font font_12x16v;

extern const uint8_t *font_get(const struct font *f, int asc);
extern const uint8_t *font_get_hv(const struct font *f, int asc, int flags);

#endif /* __FONT_H__ */

