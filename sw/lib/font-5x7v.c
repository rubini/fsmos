#include <font.h>

static const uint8_t font_5x7v_data[] = {
	0x7c, 0x82, 0x82, 0x7c, 0x00, /* 0 */
	0x00, 0x42, 0xfe, 0x02, 0x00, /* 1 */
	0x4c, 0x8a, 0x92, 0x62, 0x00, /* 2 */
	0x44, 0x82, 0x92, 0x6c, 0x00, /* 3 */
	0x1c, 0x64, 0xce, 0x04, 0x00, /* 4 */
	0xe4, 0xa2, 0xa2, 0x9c, 0x00, /* 5 */
	0x3c, 0x52, 0x92, 0x8c, 0x00, /* 6 */
	0x86, 0x98, 0xe0, 0xc0, 0x00, /* 7 */
	0x6c, 0x92, 0x92, 0x6c, 0x00, /* 8 */
	0x62, 0x92, 0x94, 0x78, 0x00, /* 9 */
	0x24, 0x18, 0x18, 0x24, 0x00, /* small x for missing glyphs */
};

const struct font font_5x7v = {
	.flags = FONT_FLAG_V,
	.wid = 5, .hei = 7,
	.first = '0', .last = '9',
	.data = font_5x7v_data,
};
