/*
 * In Application Programming (and other rom-based stuff)
 */
#ifndef __IAP_H__
#define __IAP_H__
#include <stdint.h>

#ifdef CONFIG_ARCH_LPC
#  include "iap-lpc.h"
#else
  /* default empty implementation */
  static inline void iap_call(uint32_t *args, uint32_t *res) {}
#endif

extern int iap_erase_flash(unsigned long start, int count);
extern int iap_copy_to_flash(unsigned long dest, void *src, int count);
extern void __attribute__((noreturn)) iap_reset(void);

#endif /* __IAP_H__ */
