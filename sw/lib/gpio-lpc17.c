/*
 * GPIO interface for LPC-17xx
 * Alessandro Rubini, 2017
 */
#include <init.h>

#define GPIO_MAX  (32 * 5)

/*
 * Initialization of the clock is done at initcall time, so it's automatic
 * whenever a function from this file is used by the application
 */
static int hw_gpio_init(void)
{
	regs[REG_PCONP] |= REG_PCONP_PCGPIO; /* on by default anyways */
	return 0;
}
subsys_initcall(hw_gpio_init);

/*
 * What follows is the public interface.
 * Note that only gpio_dir_af() checks the gpio is valid. Other
 * functions are expected to be called often and only after setting the mode.
 */

/* This is a documented part of the API, so offer it */
void gpio_init(void) {}

void gpio_dir(int gpio, int output, int value)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);
	uint32_t reg;

	reg = regs[REG_FIOnDIR(port)];
	if (output)
		reg |= (1 << bit);
	else
		reg &= ~(1 << bit);
	regs[REG_FIOnDIR(port)] = reg;
}

int gpio_dir_af(int gpio, int output, int value, int afnum)
{
	int af = afnum & 0x7; /* mask out pullup/down bits */
	int afpull = afnum & (GPIO_AF_PULLDOWN | GPIO_AF_PULLUP);
	uint32_t reg;
	int val;
	int gpio_hp = gpio / 16; /* half-port number */
	int gpio_hpb = gpio % 16; /* half-port bit number */

	if (gpio > GPIO_MAX || gpio < 0 || afnum < 0 || afnum > 3)
		return -1;

	/* First set dir to prevent glitches when moving to AF0 */
	gpio_dir(gpio, output, value);

	/* pull-up/down (ignore silly repeater mode) */
	switch (afpull) {
	case GPIO_AF_PULLUP:
		val = 0; break;
	case GPIO_AF_PULLDOWN:
		val = 3; break;
	default:
		val = 2; break;
	}
	reg = regs[REG_PINMODE(gpio_hp)];
	reg &= ~(3 << (gpio_hpb * 2));
	reg |= (val << (gpio_hpb * 2));
	regs[REG_PINMODE(gpio_hp)] = reg;

	/* open drain? */
	if (afnum & GPIO_AF_OPENDRAIN)
		regs[REG_PINMODEOD(GPIO_PORT(gpio))] |= (1 << GPIO_BIT(gpio));
	else
		regs[REG_PINMODEOD(GPIO_PORT(gpio))] &= ~(1 << GPIO_BIT(gpio));

	/* AF */
	reg = regs[REG_PINSEL(gpio_hp)];
	reg &= ~(3 << (gpio_hpb * 2));
	reg |= (af << (gpio_hpb * 2));
	regs[REG_PINSEL(gpio_hp)] = reg;

	return 0;
}

/* The following functions don't check the gpio value, for speed */
uint32_t __gpio_get(int gpio)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);

	return regs[REG_FIOnPIN(port)] & (1 << bit);
}

int gpio_get(int gpio)
{
	return __gpio_get(gpio) ? 1 : 0;
}

void __gpio_set(int gpio, uint32_t value)
{
	gpio_set(gpio, value); /* same as below, no way to optimize */
}

void gpio_set(int gpio, int value)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);

	if (value)
		regs[REG_FIOnSET(port)] = 1 << bit;
	else
		regs[REG_FIOnCLR(port)] = 1 << bit;
}
