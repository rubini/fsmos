#include <init.h>
#include <panic.h>
#include <io.h>
#include <errno.h>

/* the default console is u(s)art2, but parameters are family-dependent */
const struct uart dev_uart2 = {
	.baseaddr = REGBASE_USART2,
	.baudrate = CONFIG_UART_SPEED,
	.enable_reg = REG_USART2_EN,
	.enable_bit = BIT_USART2_EN,
	.pin_tx = USART2_PIN_TX, .af_tx = USART2_PIN_xX_AF,
	.pin_rx = USART2_PIN_RX, .af_rx = USART2_PIN_xX_AF,
};

const struct uart __attribute__((weak)) *console = &dev_uart2;

/* The console initcall relies on generic uart initialization code */
static int console_init(void)
{
	return uart_init(console);
}
subsys_initcall(console_init);

/* putc, and only putc, has a special "fast" console version */
void putc(int c)
{
	while (!(regs[console->baseaddr + REGOFF_UART_ISR] & BIT_UART_ISR_TXE))
		;
	regs[console->baseaddr + REGOFF_UART_TDR] = c;
}

/* no-flush version */
void __putc(int c)
{
	if (c == '\n')
		__putc('\r');
	regs[console->baseaddr + REGOFF_UART_TDR] = c;
}

/* Other "console" functions rely on generic uart code, defined later */
int getc(void)
{
	return uart_getc(console);
}
int pollc(void)
{
	return uart_pollc(console);
}

/*
 * And here are the generic versions, expected to work with any stm32 uart
 */
int uart_init(const struct uart *u)
{
	unsigned int i, m, d;

	/* Turn on the clock for the uart */
	regs[u->enable_reg] |= u->enable_bit;

	/* Select I/O pins */
	gpio_dir_af(u->pin_tx, 0, 0, u->af_tx);
	gpio_dir_af(u->pin_rx, 0, 0, u->af_rx);

	/* Ensure the uart is disabled (UE bit), so we can change the BRR */
	regs[u->baseaddr + REGOFF_UART_CR1] = 0;

	if(REGOFF_UART_ICR >= 0) {/* F205 has no interrupt clear register */
		/* Clear all interrupts that may have accumulated */
		regs[u->baseaddr + REGOFF_UART_ICR] = ~0;
	}
	/* Baud rate selection: assume oversampling by 16 */
	m = u->clock_mult ?: 1;
	d = u->clock_div ?: 1;
	i = (CPU_FREQ * m + u->baudrate/2) / (d * u->baudrate);
	regs[u->baseaddr + REGOFF_UART_BRR] = i;

	/* Enable transmitter, receiver and uart overall (UE) */
	regs[u->baseaddr + REGOFF_UART_CR1] =
		BIT_UART_CR1_UE | BIT_UART_CR1_TE | BIT_UART_CR1_RE;

	return 0;
}

void uart_putc(const struct uart *u, int c)
{
	while ( !(regs[u->baseaddr + REGOFF_UART_ISR] & BIT_UART_ISR_TXE) )
		;
	regs[u->baseaddr + REGOFF_UART_TDR] = c;
}

int uart_getc(const struct uart *u) /* blocking */
{
	if(REGOFF_UART_ICR >= 0) /* F205 has no such register */
		regs[u->baseaddr + REGOFF_UART_ICR] = BIT_UART_ISR_ORE;
	while (!(regs[u->baseaddr + REGOFF_UART_ISR] & BIT_UART_ISR_RXNE))
		;
	return regs[u->baseaddr + REGOFF_UART_RDR];
}

int uart_pollc(const struct uart *u) /* non-blocking */
{
	int ret = -EAGAIN;
	if(REGOFF_UART_ICR >= 0) /* F205 has no such register */
		regs[u->baseaddr + REGOFF_UART_ICR] = BIT_UART_ISR_ORE;
	if (regs[u->baseaddr + REGOFF_UART_ISR] & BIT_UART_ISR_RXNE)
		return regs[u->baseaddr + REGOFF_UART_RDR];
	return ret;
}
