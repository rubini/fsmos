#ifndef __REGS_STM32L031_H__
#define __REGS_STM32L031_H__

/* From Reference Manual RM0377 -- for STM32L0x1 */

#define REGBASE_GPIO_PORTA		(0x50000000 / 4)
#define REGBASE_GPIO_PORTB		(0x50000400 / 4)
#define REGBASE_GPIO_PORTC		(0x50000800 / 4)
#define REGBASE_GPIO_PORTD		(0x50000c00 / 4)
#define REGBASE_GPIO_PORTE		(0x50001000 / 4)
#define REGBASE_GPIO_PORTH		(0x50001c00 / 4)

#define REGBASE_GPIO_PORT(x)		(REGBASE_GPIO_PORTA + (x) * 0x100)
#define GPIO_PORTS_MASK			0x9f /* H..E DCBA */
#define BITS_GPIO_EN_SHIFT		0
#define REG_GPIO_EN			(0x4002102c / 4)


#define REGBASE_USART2		(0x40004400 / 4)
#define REGBASE_LPUART1		(0x40004800 / 4)

#define REG_USART2_EN		(0x40021038 / 4)
#define BIT_USART2_EN		(1 << 17)
#define USART2_PIN_TX		GPIO_NR(GPIO_PORTA, 9) /* NOTE: best is PA2 */
#define USART2_PIN_RX		GPIO_NR(GPIO_PORTA, 10) /* NOTE: best is PA3 */
#define USART2_PIN_xX_AF	4 /* Alternate function number */


#define REG_RCC_CR		(0x40021000 / 4)
#define REG_RCC_ICSR		(0x40021004 / 4)
#define REG_RCC_CFGR		(0x4002100c / 4)
#define REG_RCC_CIER		(0x40021010 / 4)
#define REG_RCC_CIFR		(0x40021014 / 4)
#define REG_RCC_CICR		(0x40021018 / 4)
#define REG_RCC_IOPRSTR		(0x4002101c / 4)
#define REG_RCC_AHBRSTR		(0x40021020 / 4)
#define REG_RCC_APB2RSTR	(0x40021024 / 4)
#define REG_RCC_APB1RSTR	(0x40021028 / 4)
#define REG_RCC_IOPENR		(0x4002102c / 4)
#define REG_RCC_AHBENR		(0x40021030 / 4)
#define REG_RCC_APB2ENR		(0x40021034 / 4)
#define REG_RCC_APB1ENR		(0x40021038 / 4)
#define REG_RCC_IOPSMEN		(0x4002103c / 4)
/* and more... */


#define REGBASE_TIMER2		(0x40000000 / 4)
#define REGBASE_TIM21		(0x40010800 / 4)
#define REGBASE_TIM22		(0x40011400 / 4)

/* Timer-related details for the jiffies timer. Use TIM21 */
#define REGBASE_TIMER		REGBASE_TIM21
#define REG_TIMER_RST		(0x40021024 / 4) /* APB2 */
#define REG_TIMER_EN		(0x40021034 / 4) /* APB2 */
#define BIT_TIMER_EN		0x04


enum stm_irq {
	STM_IRQ_WWDG		= 0,
	STM_IRQ_PVD		= 1,
	STM_IRQ_RTC		= 2,
	STM_IRQ_FLASH		= 3,
	STM_IRQ_RCC_CRS		= 4,
	STM_IRQ_EXT_1_0		= 5,
	STM_IRQ_EXT_3_2		= 6,
	STM_IRQ_EXT_15_4	= 7,
	/* resvd */
	STM_IRQ_DMA1_1		= 9,
	STM_IRQ_DMA1_3_2	= 10,
	STM_IRQ_DMA1_7_4	= 11,
	STM_IRQ_ADC		= 12,
	STM_IRQ_LPTIM1		= 13,
	/* resvd - usart 4/5 */
	STM_IRQ_TIM2		= 15,
	/* resvd - timers... */
	STM_IRQ_TIM21		= 20,
	/* resvd - i2c3 */
	STM_IRQ_TIM22		= 22,
	STM_IRQ_I2C1		= 23,
	/* resvd - i2c2 */
	STM_IRQ_SPI1		= 25,
	/* resvd - spi1, usart1 */
	STM_IRQ_USART2		= 28,
	STM_IRQ_LPUART1		= 29,
	__NR_IRQS,
};

#endif /* __REGS_STM32L031_H__ */
