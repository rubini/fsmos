#include <init.h>
#include <time.h>
#include <io.h>

extern int __invalid_timer_selection;

static int timer_setup(void)
{
	int prvalue;
	int baseaddr;

	if (CONFIG_JIFFIES_USES_CT32B1) {
		regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT32B1;
		baseaddr = REG_TMR32B1_BASE;
	} else if (CONFIG_JIFFIES_USES_CT32B0) {
		regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT32B0;
		baseaddr = REG_TMR32B0_BASE;
	} else {
		baseaddr = 0;
		__invalid_timer_selection++;
	}

	prvalue = (CPU_FREQ / HZ) - 1;
	if (CONFIG_CPU_IS_LPC17) {
		/* unless we change PCLKSEL0, by default we run at 1/4f */
		prvalue = (CPU_FREQ / 4 / HZ) -1;
	}

	/* enable selected timer, and count at HZ Hz (e.g.  1000) */
	regs[baseaddr + REGOFF_TMRTCR] = 1;
	regs[baseaddr + REGOFF_TMRTCR] = 3;
	regs[baseaddr + REGOFF_TMRTCR] = 1;
	regs[baseaddr + REGOFF_TMRPR] = prvalue;
	return 0;
}

core_initcall(timer_setup);

unsigned char jiffies_hook;
