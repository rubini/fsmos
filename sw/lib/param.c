#include <io.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <param.h>
#include <i2c.h>
#include <i2c-eeprom.h>
#include <spi-nor.h>
#include <eeprom-lpc.h>

static int param_xfer_buffer[CONFIG_MAX_N_PARAM + 1]; /* 1 for version */

struct param_ll_ops {
	int (*read)(void *eeprom, int offset, void *buf, int size);
	int (*write)(void *eeprom, int offset, const void *buf, int size);
};

static const struct param_ll_ops param_ops[] = {
#ifdef CONFIG_PARAM_I2C
	[PARAM_EEPROM_I2C] = {i2cee_read_vp, i2cee_write_vp},
#endif
#ifdef CONFIG_PARAM_LPC_EEPROM
	[PARAM_EEPROM_IAP] = {lpcee_read, lpcee_write},
#endif
#ifdef CONFIG_PARAM_SPIFLASH
	[PARAM_SPI_FLASH] = {spinor_read_param, spinor_write_param},
#endif
#ifdef CONFIG_PARAM_CUSTOM_CODE
	[PARAM_CUSTOM_CODE] = {param_read_custom, param_write_custom},
#endif
};

int param_init(const struct param_group *g)
{
	if (g->nparam > CONFIG_MAX_N_PARAM)
		return -ENOMEM;
	param_reset(g, PARAM_RESET_DEFAULT);
	if (g->eeprom)
		param_load(g, PARAM_LOAD_APPLY);
	return 0;
}

void param_reset(const struct param_group *g, int which)
{
	int i;
	const struct param_desc *d = g->desc;

	for (i = 0; i < g->nparam; i++) {
		if (which == PARAM_RESET_DEFAULT)
			d[i].vals->current = d[i].defval;
		else
			d[i].vals->current = d[i].vals->saved;
	}
}

static int param_ll_read(const struct param_group *g)
{
	const struct param_ll_ops *ops = param_ops + g->eeprom_type;
	int size = (g->nparam + 1) * sizeof(int);

	return ops->read(g->eeprom, g->eeoffset, param_xfer_buffer, size);
}


int param_load(const struct param_group *g, int apply)
{
	int i, okversion;
	const struct param_desc *d = g->desc;

	if (g->nparam > CONFIG_MAX_N_PARAM)
		return -ENOMEM;

	if (g->eeprom) {
		i = param_ll_read(g);
		if (i < 0)
			return i;
	} else {
		/* fake a buffer with "saved" values */
		param_xfer_buffer[0] = g->version;
		for (i = 0; i < g->nparam; i++)
			param_xfer_buffer[i + 1] = d[i].vals->saved;
	}

	/* If the version is different, ignore values but for PRECIOUS ones */
	okversion = param_xfer_buffer[0] == g->version;
	for (i = 0; i < g->nparam; i++) {
		if (!okversion && !(d[i].flags & PARAM_FLAG_PRECIOUS))
			continue;
		d[i].vals->saved = param_xfer_buffer[i + 1];
		if (apply == PARAM_LOAD_APPLY)
			d[i].vals->current = param_xfer_buffer[i + 1];
	}
	return okversion ? 0 : -EINVAL;
}

static int param_ll_write(const struct param_group *g)
{
	const struct param_ll_ops *ops = param_ops + g->eeprom_type;
	int size = (g->nparam + 1) * sizeof(int);

	return ops->write(g->eeprom, g->eeoffset, param_xfer_buffer, size);
}

int param_save(const struct param_group *g)
{
	int i;
	const struct param_desc *d = g->desc;

	if (g->nparam > CONFIG_MAX_N_PARAM)
		return -ENOMEM;

	param_xfer_buffer[0] = g->version;
	for (i = 0; i < g->nparam; i++)
		param_xfer_buffer[i + 1] =
			d[i].vals->saved = d[i].vals->current;
	if (g->eeprom)
		return param_ll_write(g);
	return 0;
}

void param_list(const struct param_group *g) /* tag and description */
{
	int i;
	const struct param_desc *d;

	printf("Index   Name        Description\n");
	for (i = 0; i < g->nparam; i++) {
		d = g->desc + i;
		if (!d->tag || d->flags & PARAM_FLAG_HIDDEN)
			continue;
		printf(" %3i  %-10s", i, d->tag);
		printf("  %s\n", d->desc);
	}
}

void param_show(const struct param_group *g, int all) /* show values */
{
	int i;
	const struct param_desc *d;
	static const char showint[] = " %3i  %-10s  %9i %9i %9i ";
	static const char showhex[] = " %3i  %-10s 0x%08x  %08x  %08x ";
	const char *fmt;

	printf("Index Name            Default     Saved   Current  Measured\n");
	for (i = 0; i < g->nparam; i++) {
		d = g->desc + i;
		if (!d->tag)
			continue;
		if (!all && d->flags & PARAM_FLAG_HIDDEN)
			continue;
		fmt = showint;
		if (d->flags & PARAM_FLAG_HEX)
			fmt = showhex;
		printf(fmt, i, d->tag,
		       d->defval, d->vals->saved, d->vals->current);
		if (d->flags & PARAM_FLAG_MEASURE)
			printf("%9i\n", d->vals->measured);
		else
			printf("%9s\n", "-");
	}
}

const struct param_group *param_current_group;

int param_generic_cmd(char **reply, int argc, char **argv)
{
	const struct param_group *g = param_current_group;
	int i, v, number = 0;

	if (argc == 1) { /* show un-hidden parameters */
		param_show(g, 0);
		return 0;
	}
	if (sscanf(argv[1], "%i", &i) == 1)
		number = 1;
	if (number && (i < 0 || i >= g->nparam))
		return -ERANGE;
	if (argc == 3) { /*change parameter */
		if (!number || sscanf(argv[2], "%i", &v) != 1)
			return -EINVAL;
		g->desc[i].vals->current = v;
		return 0;
	}
	if (number) {
		/* show one */
		if (g->desc[i].flags & PARAM_FLAG_HEX)
			sprintf(*reply, "0x%x", g->desc[i].vals->current);
		else
			sprintf(*reply, "%i", g->desc[i].vals->current);
		return 0;
	}
	/* argc = 2: subcommand */
	if (!strcmp(argv[1], "save"))
		return param_save(g);
	if (!strcmp(argv[1], "load")) {
		return param_load(g, PARAM_LOAD_APPLY);
	}
	if (!strcmp(argv[1], "all")) {
		param_show(g, 1);
		return 0;
	}
	if (!strcmp(argv[1], "help")) {
		param_list(g); return 0;
	}
	if (!strcmp(argv[1], "reset")) {
		param_reset(g, PARAM_RESET_DEFAULT); return 0;
	}
	return -EINVAL;
}
