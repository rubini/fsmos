#ifndef __GPIO_STM32_H__
#define __GPIO_STM32_H__

#define GPIO_NUMBER (8 * 32)

/* STM32 documentation uses PORTA,B,... naming, not 0,1,,, */
#define GPIO_PORTA 0
#define GPIO_PORTB 1
#define GPIO_PORTC 2
#define GPIO_PORTD 3
#define GPIO_PORTE 4
#define GPIO_PORTF 5
#define GPIO_PORTG 6
#define GPIO_PORTH 7
#define GPIO_PORTI 8

/* Accept only constant uppercase names. Build-fail on error */
#define __PORT(asc) __port(asc)

static inline int __port(int asc)
{
	extern void invalid_port_name(void);
	extern void non_constant_port_name(void);

	if (!__builtin_constant_p(asc)) {
		non_constant_port_name();
		return 0;
	}
	if (asc < 'A' || asc > 'I') {
		invalid_port_name();
		return 0;
	}
	return asc - 'A';
}

/* for gpio_dir() and gpio_dir_af() */
#define GPIO_DIR_IN     0
#define GPIO_DIR_OUT    1

/* for gpio_dir_af()  */
#define GPIO_AF_GPIO    0
#define GPIO_AF(x)      x
#define GPIO_AF_PULLDOWN   0x20 /* value for REG_PUPDR << 4 */
#define GPIO_AF_PULLUP     0x10 /* value for REG_PUPDR << 4 */
#define GPIO_AF_ANALOG     0x40

/* We have 32 gpio bits per "port", this is hardwired */
#define GPIO_NR(port, bit)	((port) * 32 + (bit))
#define GPIO_PORT(nr)		((nr) / 32)
#define GPIO_BIT(nr)		((nr) % 32)


extern void gpio_init(void);

extern int gpio_dir_af(int gpio, int output, int value, int afnum);
extern void gpio_dir(int gpio, int output, int value);

extern int gpio_get(int gpio);

extern void gpio_set(int gpio, int value);

/* Fast inline versions, to be defined */
static inline uint32_t __gpio_get(int gpio)
{
	int port = GPIO_PORT(gpio);
	int bit = GPIO_BIT(gpio);
	int baseaddr = REGBASE_GPIO_PORT(port);

	return regs[baseaddr + REGOFF_GPIO_IDR] & (1 << bit);
}

static inline void __gpio_set(int gpio, uint32_t value)
{
	uint8_t port = GPIO_PORT(gpio);
	uint8_t bit = GPIO_BIT(gpio);
	int baseaddr = REGBASE_GPIO_PORT(port);

	if (value)
		regs[baseaddr + REGOFF_GPIO_BSRR] = 1 << bit;
	else
		regs[baseaddr + REGOFF_GPIO_BSRR] = 1 << (bit + 16);
}

#endif /* __GPIO_STM32_H__ */
