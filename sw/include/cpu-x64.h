#ifndef __CPU_H__
#define __CPU_H__

#ifndef __ASSEMBLER__
#include <hw.h>
#include <stdint.h>

/* Jiffies comes from a system call and some calculation */
extern unsigned long get_jiffies(void);

#define jiffies get_jiffies()

#define CPU_FREQ	(100 * 1000 * 1000) /* fake */

/* other system calls */
struct timeval {
	signed long tv_sec; signed long tv_usec;
};
struct timespec {
	signed long tv_sec; signed long tv_nsec;
};
extern int sys_gettimeofday(struct timeval *tv, void *tz);
extern int sys_nanosleep(struct timespec *req, struct timespec *rem);
extern int sys_write(int fd, const void *buf, int count);


/*
 * The following is fake, by now, to allow building the rest
 */
#define GPIO_NUMBER 32

/* for gpio_dir() and gpio_dir_af() */
#define GPIO_DIR_IN     0
#define GPIO_DIR_OUT    1

/* for gpio_dir_af() --although avr missed the typical AT mechanism */
#define GPIO_AF_GPIO    0
#define GPIO_AF(x)      x
#define GPIO_AF_PULLDOWN   0x08 /* Not present in hardware */
#define GPIO_AF_PULLUP     0x10
#define GPIO_AF_ANALOG     0x40 /* Unused, compat w/ LPC */

/* We have 32 gpio bits per "port", this is hardwired */
#define GPIO_NR(port, bit)	((port) * 32 + (bit))
#define GPIO_PORT(nr)		((nr) / 32)
#define GPIO_BIT(nr)		((nr) % 32)

extern void gpio_init(void);

extern int gpio_dir_af(int gpio, int output, int value, int afnum);
extern void gpio_dir(int gpio, int output, int value);

extern int gpio_get(int gpio);
extern uint32_t __gpio_get(int gpio);

extern void gpio_set(int gpio, int value);
extern void __gpio_set(int gpio, uint32_t value);

#endif /* __ASSEMBLER__ */
#endif /* __CPU_H__ */
