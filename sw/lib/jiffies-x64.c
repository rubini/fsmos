#include <io.h>
#include <time.h>

unsigned char jiffies_hook;

unsigned long get_jiffies(void)
{
	static struct timeval tv0;
	struct timeval tv;
	unsigned long usec;

	sys_gettimeofday(&tv, NULL);
	if (!tv0.tv_sec)
		tv0.tv_sec = tv.tv_sec;
	usec = (tv.tv_sec - tv0.tv_sec) * 1000 * 1000;
	usec += tv.tv_usec;
	return usec / (1000 * 1000 / HZ);
}
