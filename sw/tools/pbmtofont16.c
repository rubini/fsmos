/*
 * Turn a ppm file to source hex suitable for a font (eg font-8x16v)
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define HEI 16

static unsigned char img[16 * 16];

static inline int getpix(unsigned char *img, int wid, int x, int y, int invert)
{
	unsigned char *p = img + (wid/8) * y + x/8;
	int bit = *p & (1 << (7 - x%8));

	bit = !!bit; /* just 0 or 1 */
	return bit ^ invert;
}

#define PIX(x, y, invert) ((img[wid/8 * (y) + (x)] > 128)^(invert))

int main(int argc, char **argv)
{
	FILE *in = stdin;
	char *fname = "<stdin>";
	int wid, hei, x, y, item, line, invert = 0;
	char separators[] = "       \n       \n";
	char c;

	if(argc > 1 && !strcmp(argv[1], "-i")) {
		invert = 1;
		argv[1] = argv[0];
		argv++, argc--;
	}

	if (argc > 2) {
		fprintf(stderr, "%s: Use: \"%s [-i] [<file>]\"\n",
			argv[0], argv[0]);
		exit(1);
	}
	if (argc == 2) {
		fname = argv[2];
		in = fopen(argv[2], "r");
	}
	if (!in) {
		fprintf(stderr, "%s: %s: %s\n", argv[0],
			fname, strerror(errno));
		exit(1);
	}

	/* parse pgm header */
	if(fscanf(in, "P4 %i %i%c", &wid, &hei, &c) != 3) {
		fprintf(stderr, "%s: %s: not a PBM file\n",
			argv[0], fname);
		exit(1);
	}
	if (hei != HEI) {
		fprintf(stderr, "%s: %s: Wrong heigth: must be 16\n",
			argv[0], fname);
	}
	if (wid != 16 && wid != 32) {
		fprintf(stderr, "%s: %s: Wrong width: must be 16 or 32\n",
			argv[0], fname);
	}

	if (fread(img, 1, wid * hei / 8, in) != wid * hei / 8) {
		fprintf(stderr, "%s: %s: Premature EOF\n",
			argv[0], fname);
		exit(1);
	}

	/* Output it in vertical first line first (msb is bottom pixel) */
	for (item = 0; item < wid; item += 16) {
		for (line = 0; line < 2; line++) {
			for (x = 0; x < 16; x++) {
				unsigned char byte = 0;
				for (y = 7; y >= 0; y--) {
					byte |= getpix(img, wid,
						       item + x,
						       line * 8 + y,
						       invert) << y;
				}
				printf("0x%02x,%c", byte, separators[x]);
			}
		}
		printf("\n");
	}
	exit(0);
}


