#include <string.h>
#include <errno.h>
#include <io.h>

/* CONFIG_STRERROR_NONE: so we use strerror() with minimal overhead */
const char *strerror_none(int errnum)
{
	static char errstr[10];
	sprintf(errstr, "-%i", errnum);
	return errstr;
}

/* CONFIG_STRERROR_SMALL: the default: only a few values */
struct errlist {
	int num;
	const char *str;
};

static const struct errlist errlist[] = {
	{EINVAL, "Invalid argument"},
	{ENODEV, "No such device"},
	{EAGAIN, "Try again"},
	{EBUSY, "Device or resource busy"},
	{EIO, "I/O error"},
	{ENOMEM, "Out of memory"},
	{0, "No error"},
	{-1,},
};

const char *strerror_small(int errnum)
{
	const struct errlist *p;
	for (p = errlist; p->num != errnum && p->str; p++)
		;
	if (p->str)
		return p->str;
	return strerror_none(errnum);
}

#ifdef CONFIG_STRERROR_FULL /* ugly ifdef. See commit message */

/* CONFIG_STRERROR_FULL: all strings, without index field */
static const char *errnames[] = {
	[0] = "No error",
	[EPERM] = "Operation not permitted",
	[ENOENT] = "No such file or directory",
	[ESRCH] = "No such process",
	[EINTR] = "Interrupted system call",
	[EIO] = "I/O error",
	[ENXIO] = "No such device or address",
	[E2BIG] = "Argument list too long",
	[ENOEXEC] = "Exec format error",
	[EBADF] = "Bad file number",
	[ECHILD] = "No child processes",
	[EAGAIN] = "Try again",
	[ENOMEM] = "Out of memory",
	[EACCES] = "Permission denied",
	[EFAULT] = "Bad address",
	[ENOTBLK] = "Block device required",
	[EBUSY] = "Device or resource busy",
	[EEXIST] = "File exists",
	[EXDEV] = "Cross-device link",
	[ENODEV] = "No such device",
	[ENOTDIR] = "Not a directory",
	[EISDIR] = "Is a directory",
	[EINVAL] = "Invalid argument",
	[ENFILE] = "File table overflow",
	[EMFILE] = "Too many open files",
	[ENOTTY] = "Not a typewriter",
	[ETXTBSY] = "Text file busy",
	[EFBIG] = "File too large",
	[ENOSPC] = "No space left on device",
	[ESPIPE] = "Illegal seek",
	[EROFS] = "Read-only file system",
	[EMLINK] = "Too many links",
	[EPIPE] = "Broken pipe",
	[EDOM] = "Math argument out of domain of func",
	[ERANGE] = "Math result not representable",
	/* [ETIMEDOUT] = "Connection timed out", -- this is "110", too big */
};

const char *strerror_full(int errnum)
{
	if (errnum < ARRAY_SIZE(errnames) && errnames[errnum])
		return errnames[errnum];
	return strerror_none(errnum);
}

#endif /* CONFIG_STRERROR_FULL */




#ifdef CONFIG_STRERROR_NONE
const char *strerror(int errnum) __attribute__((alias("strerror_none")));
#elif defined CONFIG_STRERROR_FULL
const char *strerror(int errnum) __attribute__((alias("strerror_full")));
#else /* CONFIG_STRERROR_SMALL */
const char *strerror(int errnum) __attribute__((alias("strerror_small")));
#endif
