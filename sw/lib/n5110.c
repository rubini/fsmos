#include <spi.h>
#include <font.h>
#include <errno.h>
#include <string.h>
#include <n5110.h>

static struct spi_obuf n5110_buf;

static void n51_cmd(struct n5110_dev *dev, unsigned char cmd)
{
	n5110_buf.len = 1;
	n5110_buf.buf = &cmd;
	gpio_set(dev->cfg->gpio_dc, 0);
	spi_write(&dev->spi, 0, &n5110_buf);
}


static void n51_data(struct n5110_dev *dev, int len, const void *data)
{
	n5110_buf.len = len;
	n5110_buf.buf = data;
	gpio_set(dev->cfg->gpio_dc, 1);
	spi_write(&dev->spi, 0, &n5110_buf);
}

/* init commands. Not const so I can test different values and re-init */
uint8_t n5110_init_commands[8] = {
	0x21,    /* LCD extended commands */
	0xB8,    /* set LCD Vop (contrast) */
	0x04,    /* set temp coefficient */
	0x14,    /* set biad mode 1:40 */
	0x20,    /* LCD basic commands */
	0x0c,    /* normal */
	0x00, 0x00 /* fillers, to be able to test more bytes */
};
int n5110_init_commands_len = 6;

void n5110_init(struct n5110_dev *dev)
{
	int i;

	for (i = 0; i < n5110_init_commands_len; i++)
		n51_cmd(dev, n5110_init_commands[i]);
	n5110_goto(dev, 0, 0);
	for (i = 0; i < N5110_WID * N5110_HEI / 4; i++)
		n51_data(dev, 4, "\0\0\0");
	n5110_goto(dev, 0, 0);
}

struct n5110_dev *n5110_create(struct n5110_dev *dev, int *errno)
{
	struct spi_dev *spi;

	spi = spi_create(&dev->spi);
	if (!spi) {
		*errno = EINVAL;
		return NULL;
	}
	if (dev->cfg->gpio_backlight >= 0)
		gpio_dir_af(dev->cfg->gpio_backlight,
			    GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(dev->cfg->gpio_dc, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	n5110_init(dev);
	return dev;
}

void n5110_goto(struct n5110_dev *dev, int x, int y)
{
	int pixx, line;
	int hei;

	/* FIXME: wid and hei should consider font flags (rotation etc) */
	pixx = x * dev->cfg->font->wid;
	/* each byte is placed vertically, so use multiples of 8 pixels */
	hei = dev->cfg->font->hei;
	hei = (hei + 7) / 8;
	line = y * hei;
	n51_cmd(dev, 0x80 + pixx);
	n51_cmd(dev, 0x40 + line);

}

void n5110_puts(struct n5110_dev *dev, const char *s)
{
	const uint8_t *glyph;
	int i;

	for (i = 0; i < strlen(s); i++) {
		glyph = font_get_hv(dev->cfg->font, s[i], dev->cfg->font_flags);
		n51_data(dev, dev->cfg->font->wid, glyph);
	}
}
