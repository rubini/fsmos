-include $(CURDIR)/.config

# Remove quotes (horrible!)
ARCH=$(shell echo $(CONFIG_ARCH))

ifdef CONFIG_ARCH_AVR
  CROSS_COMPILE ?= avr-
else
  CROSS_COMPILE ?= arm-none-eabi-
endif
ifdef CONFIG_ARCH_X64
  CROSS_COMPILE :=
endif

TESTS := $(wildcard tests-$(ARCH)/*.c)
BINARIES-y = $(TESTS:.c=.bin) $(TESTS:.c=.ihex)

# BINARIES-y += list-of-real-programs ...

OBJECTS  = $(BINARIES-y:.bin=.o)
ELFS     = $(BINARIES-y:.bin=)

# The library
OSLIB   = libfsmos.a

CC =		$(CROSS_COMPILE)gcc
LD =		$(CROSS_COMPILE)ld
OBJDUMP =	$(CROSS_COMPILE)objdump
OBJCOPY =	$(CROSS_COMPILE)objcopy
SIZE =		$(CROSS_COMPILE)size
AUTOCONF = $(CURDIR)/include/generated/autoconf.h

GIT_VER = $(shell git describe --always --long --abbrev=7 --dirty)

# linker configuration and rule (we preprocess it). ram/flash is only for lpc
LDS-$(CONFIG_LPC_RAM_BOOT) = lds/lpc-ram.lds
LDS-$(CONFIG_LPC_FLASH_BOOT) = lds/lpc-flash.lds
LDS-$(CONFIG_ARCH_AVR) = lds/avr-flash.lds
LDS-$(CONFIG_STM_RAM_BOOT) = lds/stm-ram.lds
LDS-$(CONFIG_STM_FLASH_BOOT) = lds/stm-flash.lds
LDS-$(CONFIG_ARCH_X64) = lds/x64-ram.lds

ldflags-$(CONFIG_FLASH_BOOT) = --entry=_entry_rom
ldflags-$(CONFIG_RAM_BOOT) = --entry=_entry_ram
ldflags-$(CONFIG_ARCH_X64) = -static
ldflags-y	+= -Wl,--build-id=none
ldflags-y	+= -nostdlib -T $(LDS-y) -Wl,--gc-sections -Os
ldflags-y	+= -L. -lfsmos
ldflags-$(CONFIG_USE_LIBGCC) += -lgcc

# compiler flags
cflags-y		= -g -Wall -ffreestanding -O2 -Wstrict-prototypes
cflags-y		+= -include $(AUTOCONF) -include include/stddef.h
cflags-y		+= -include include/cpu-$(ARCH).h
cflags-y		+= -ffunction-sections -fdata-sections -fno-common
cflags-y		+= -Wno-format-zero-length -Wno-date-time
cflags-y		+= -Wno-zero-length-bounds -Wno-stringop-overflow
cflags-$(CONFIG_CM0)	+= -march=armv6-m -mthumb -mtune=cortex-m0
cflags-$(CONFIG_CM0P)	+= -march=armv6-m -mthumb -mtune=cortex-m0plus
cflags-$(CONFIG_CM3)	+= -march=armv7-m -mthumb -mtune=cortex-m3
cflags-$(CONFIG_CM4)	+= -march=armv7-m -mthumb -mtune=cortex-m4
cflags-$(CONFIG_ARCH_AVR) += -mmcu=$(CONFIG_AVR_MCU)
cflags-$(CONFIG_ARCH_X64) += -fPIE
cflags-y +=  -Iinclude -Ipp_printf -I.

### object file selection
# generic
obj-y			= boot-$(ARCH).o setup.o setup-$(ARCH).o board.o
obj-y			+= config.o lib/sched-simple.o
obj-y			+= lib/io.o lib/string.o lib/strerror.o
obj-y			+= lib/udelay-$(ARCH).o lib/jiffies-$(ARCH).o
obj-y			+= lib/jiffies64.o
obj-y			+= lib/ctype.o lib/sscanf.o lib/uuencode.o
obj-y			+= lib/rand.o lib/regs.o lib/panic.o
obj-y			+= pp_printf/printf.o pp_printf/vsprintf-full.o
obj-y			+= lib/stack.o lib/crc32.o lib/crc16.o
obj-y			+= lib/font.o lib/font-5x7v.o lib/font-5x8v.o
obj-y			+= lib/font-8x8h.o lib/font-8x16h.o lib/font-4x6v.o
obj-y			+= lib/font-16x32h.o lib/font-12x16v.o
obj-y			+= lib/w1.o lib/w1-gpio.o lib/w1-temp.o lib/w1-eeprom.o
obj-y			+= lib/tc.o lib/ltc2428.o
obj-y			+= lib/i2c-eeprom.o lib/tpi.o
obj-y			+= lib/n5110.o lib/led.o lib/button.o
obj-y			+= lib/pca9535.o lib/mgpio.o
obj-y			+= lib/display-ili9341.o lib/display-ssd130x.o
obj-y			+= lib/mcp3428.o
obj-y			+= lib/ps-korad.o

# logic cells (drivers)
obj-y			+= lib/uart-$(ARCH).o

obj-$(CONFIG_LPC1)	+= lib/pll-lpc1.o lib/i2c.o lib/spi.o
obj-$(CONFIG_LPC1_OLD)	+= lib/pll-lpc1.o lib/i2c.o lib/spi.o
obj-$(CONFIG_LPC1)	+= lib/pwm-lpc.o lib/iap-lpc.o lib/timestamp-lpc.o
obj-$(CONFIG_LPC1_OLD)	+= lib/pwm-lpc.o lib/iap-lpc.o lib/timestamp-lpc.o


obj-$(CONFIG_LPC1)	+= lib/irq-lpc-debug.o lib/gpio-lpc1.o lib/usb-lpc1.o
obj-$(CONFIG_LPC1_OLD)	+= lib/irq-lpc-debug.o lib/gpio-lpc1-old.o
obj-$(CONFIG_LPC17)	+= lib/irq-lpc-debug.o lib/gpio-lpc17.o

obj-$(CONFIG_LPC1)	+= lib/adc-lpc.o lib/eeprom-lpc.o
obj-$(CONFIG_LPC1125)	+= lib/adc-lpc.o

obj-$(CONFIG_LPC_MUART)	+= lib/uart-lpc-multi.o

obj-$(CONFIG_LPC17)	+= lib/i2c.o
obj-$(CONFIG_LPC17)	+= lib/pll-lpc17.o
obj-$(CONFIG_LPC17)	+= lib/usb-lpc17.o
obj-$(CONFIG_LPC17)	+= lib/can-lpc17.o

obj-$(CONFIG_ARCH_AVR)	+= lib/gpio-avr.o

obj-$(CONFIG_ARCH_STM)	+= lib/gpio-stm32.o

obj-$(CONFIG_ARCH_LPC)	+= lib/irq-cortex-m.o
obj-$(CONFIG_ARCH_STM)	+= lib/irq-cortex-m.o

obj-$(CONFIG_CM0)	+= lib/debug-registers.o
obj-$(CONFIG_CM0P)	+= lib/debug-registers.o
obj-$(CONFIG_CM3)	+= lib/debug-registers.o

obj-$(CONFIG_ARCH_X64)	+= lib/syscalls-x64.o
obj-$(CONFIG_ARCH_X64)	+= lib/gpio-x64.o

# higher level libraries (that won't fit in AVR memory)
obj-$(CONFIG_USB)	+= lib/usb-serial.o lib/usb-eth.o
obj-$(CONFIG_ARCH_32BITS) += lib/neopixel.o lib/spi-nor.o
obj-$(CONFIG_ARCH_32BITS) += lib/net.o lib/icmp.o lib/arp.o
obj-$(CONFIG_ARCH_32BITS) += lib/udp.o lib/ipv4.o lib/bootp.o lib/syslog.o
obj-$(CONFIG_ARCH_32BITS) += lib/command.o lib/param.o lib/modbus.o
obj-$(CONFIG_ARCH_32BITS) += lib/cbuf.o

# eventually, really build rules
all: tests-$(ARCH)/.gitignore tools $(OSLIB) $(LDS-y) $(ELFS) $(BINARIES-y)

.PHONY: all tools clean

tests-$(ARCH)/.gitignore: $(TESTS)
	@cp tests-$(ARCH)/.gitignore.head $@
	@cd tests-$(ARCH) && ls -1 *.c | sed s'/.c$$//' >> .gitignore

tools:
	$(MAKE) -s -C tools

$(ELFS): $(OSLIB) $(LDS-y)

$(OSLIB): $(obj-y)
	@rm -f $@
	$(AR) -r $@ $^

clean:
	$(MAKE) -C tools clean
	rm -f *.a *.o */*.o *.map */*.map $(BINARIES-y) $(ELFS) $(LDS-y)
	rm -f *.bin tests-$(ARCH)/*.bin $(AUTOCONF)

%.ihex: %.bin
	${OBJCOPY} -I binary -O ihex $*.bin $@

%.hex: %.bin
	od -v -w8 -t x1 -An $^ | \
		sed -e 's/ /, 0x/g' -e 's/^, //' -e 's/$$/,/' > $@

%.bin: %
	${OBJCOPY} -O binary $* $@
	if [ "$(CONFIG_LPC_FLASH_BOOT)" = "y" ]; then \
		./tools/fix-checksum $@; \
	fi

%: %.o $(OSLIB) $(LDS-y) revision.o
	$(CC) $(cflags-y) -o $@ revision.o $*.o \
		-Wl,-Map=$@.map $(ldflags-y)

%: %.c revision.o
	$(CC) $(cflags-y) -o $@ revision.o $*.c \
		-Wl,-Map=$@.map $(ldflags-y)

revision.o: revision.c $(AUTOCONF)
	$(CC) $(cflags-y) -D__GITC__="\"$(GIT_VER)\"" -c revision.c

config.c: .config
	echo "char *__current_config = \"\\" > $@
	grep CONFIG .config | sed -e 's/$$/\\n\\/' -e 's/"/\\"/g' >> $@
	echo "\";" >> $@

# And mostly standard rules, but I'd better override the default ones
%.o: %.c
	$(CC) $(cflags-y) -c $*.c -o $@

%.o: %.S
	$(CC) $(cflags-y) -c $*.S -o $@

%.i: %.c
	$(CC) $(cflags-y) -E $*.c -o $@

%.S: %.c
	$(CC) $(cflags-y) -S $*.c -o $@

lds/%.lds: lds/%.lds.S .config
	$(CC) $(cflags-y) -E -P lds/$*.lds.S -o $@

# following targets from Makefile.kconfig
silentoldconfig:
	@mkdir -p include/config
	$(MAKE) quiet=quiet_ -f Makefile.kconfig $@

scripts_basic config:
	$(MAKE) quiet=quiet_ -f Makefile.kconfig $@

%config:
	$(MAKE) quiet=quiet_ -f Makefile.kconfig $@

defconfig:
	$(MAKE) quiet=quiet_ -f Makefile.kconfig lpc11u_defconfig

.config $(AUTOCONF): silentoldconfig

# This forces more compilations than needed, but it's useful
# (we depend on .config and not on include/generated/autoconf.h
# because the latter is touched by silentoldconfig at each build)
$(obj-y): $(AUTOCONF) .config $(wildcard include/*.h)

