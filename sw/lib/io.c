#include <io.h>
#include <errno.h>

/* This is the default puts, but can be overridden by application code */
int console_puts(const char *s)
{
	while (*s) {
		if (*s == '\n')
			putc('\r');
		putc (*s++);
	}
	return 0;
}
int __attribute__((weak,alias("console_puts"))) puts(const char *s);

/* No-flush, for no-delay logging */
int __puts(const char *s)
{
	while (*s)
		__putc (*s++);
	return 0;
}

/*
 * Not all architecutres have the uart abstraction, thus e.g. gets
 * can't call uart_gets(console). As a result, we have duplicated code.
 */
int uart_puts(const struct uart *u, const char *s)
{
	while (*s) {
		if (*s == '\n')
			uart_putc(u, '\r');
		uart_putc(u, *s++);
	}
	return 0;
}

/* gets is blocking, because getc is */
char *gets(char *s)
{
	int c, i = 0;
	do {
		s[i++] = c = getc();
		s[i] = '\0';
	}
	while (c != '\n' && c != '\r');
	return s;
}
char *uart_gets(const struct uart *u, char *s)
{
	int c, i = 0;
	do {
		s[i++] = c = uart_getc(u);
		s[i] = '\0';
	}
	while (c != '\n' && c != '\r');
	return s;
}

/*
 * The polls code is long, I don't want to duplicate so much code.
 * So we call uart_pollc() anyways, which archs make make the same as pollc()
 */
char *polls(char *s, int bsize)
{
	return uart_polls_t(NULL, s, "\n\r", bsize);
}
char *uart_polls(const struct uart *u, char *s, int bsize)
{
	return uart_polls_t(u, s, "\n\r", bsize);
}

char *polls_t(char *s, char *terminators, int bsize)
{
	return uart_polls_t(NULL, s, terminators, bsize);
}
char *uart_polls_t(const struct uart *u, char *s, char *terminators, int bsize)
{
	int c, t, i = 0;

	if (bsize < 2)
		return NULL;
	while (1) {
		c = u ? uart_pollc(u) : pollc();
		if (c == -EAGAIN)
			return NULL; /* no news since last call */

		/* go to end of currently-accumulated string */
		while (s[i] && i < bsize - 1)
			i++;
		if (i == bsize - 1) /* caller did sth wrong, start fresh */
			i = 0;
		s[i++] = c;
		s[i] = '\0';
		if (i == bsize - 1)
			return s;
		for (t = 0; terminators[t]; t++)
			if (c == terminators[t])
				return s;
	}
	return NULL;
}
