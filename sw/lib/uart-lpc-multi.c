/*
 * Some LPC devices have multiple uart ports. Console is auto-initialized
 * if putc/puts/pollc are used, other uarts must be requested.
 *
 * NOTE: altermate function assignments of the pins are up to the user
 */
#include <io.h>
#include <errno.h>
#include <panic.h>

int uart_init(const struct uart *u)
{
	unsigned int val;

	if (115200 % u->baudrate != 0)
		panic(15, "Unsupported UART speed\n");

	/* Turn on the clock for the uart */
	regs[u->enable_reg] |= u->enable_bit;

	/* Turn on the clock for pin configuration, and gpio too */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_IOCON | REG_AHBCLKCTRL_GPIO;

	/* First fix pin configuration */
	gpio_dir_af(u->pin_rx, GPIO_DIR_IN, 0, u->af_rx);
	gpio_dir_af(u->pin_tx, GPIO_DIR_OUT, 0, u->af_tx);

	/*
	 * The clock divider must be set before turning on the clock.
	 * If we start from 12MHz, we would divide by 6.5 to get 115200:
	 * 12M / 115200 / 16 = 6.510416
	 */

	regs[u->clock_div] = CONFIG_PLL_CPU; /* Back to 12MHz */

	/* Disable interrupts and clear pending interrupts */
	regs[u->baseaddr + REGOFF_UARTIER] = 0;
	val = regs[u->baseaddr + REGOFF_UARTIIR];
	val = regs[u->baseaddr + REGOFF_UARTRBR];
	val = regs[u->baseaddr + REGOFF_UARTLSR];

	/* Set bit rate: enable DLAB bit and then divisor */
	regs[u->baseaddr + REGOFF_UARTLCR] = 0x80;

	/*
	 * Calculation is hairy: we need 6.5 or a multiple, but the
	 * fractional divisor makes (1 + A/B) where B is 1..15
	 * 6.510416 = 4 * 1.627604 =~ 4 * 1.625 = 4 * (1+5/8)
	 */
	val = 4;
	/* Support sub-multiples of 115200 */
	val *= (115200 / u->baudrate);


	regs[u->baseaddr + REGOFF_UARTDLL] = val & 0xff;
	regs[u->baseaddr + REGOFF_UARTDLM] = (val >> 8) & 0xff;
	regs[u->baseaddr + REGOFF_UARTFDR] = (8 << 4) | 5; /* 1 + 5/8 */

	/* clear DLAB and write mode (8bit, no parity) */
	regs[u->baseaddr + REGOFF_UARTLCR] = 0x3;
	regs[u->baseaddr + REGOFF_UARTFCR] = 0x1;
	return 0;
}

/* This does not turn \n into \r\n -- it's a puts thing now */
void uart_putc(const struct uart *u, int c)
{
	while ( !(regs[u->baseaddr + REGOFF_UARTLSR] & REGOFF_UARTLSR_THRE) )
		;
	regs[u->baseaddr + REGOFF_UARTTHR] = c;
}
void uart_raw_putc(const struct uart *u, int c)
{
	regs[u->baseaddr + REGOFF_UARTTHR] = c;
}

/* getc is blocking */
int uart_getc(const struct uart *u)
{
	while (!(regs[u->baseaddr + REGOFF_UARTLSR] & REGOFF_UARTLSR_RDR))
		;
	return regs[u->baseaddr + REGOFF_UARTRBR];
}

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
int uart_pollc(const struct uart *u)
{
	int ret = -EAGAIN;
	if (regs[u->baseaddr + REGOFF_UARTLSR] & REGOFF_UARTLSR_RDR)
		return regs[u->baseaddr + REGOFF_UARTRBR];
	return ret;
}
