.text

/* the ABI says argument in rdi, rsi, rdx (then rcx, r8, r9) */

/* syscalls get rax (nr) rdi, rsi, etc (same registers) */	

.globl sys_write
.type sys_write,%function
sys_write:
	movq $1, %rax /* 1 == __NR_write */
	syscall
	ret
.size sys_write,.-sys_write

.globl sys_exit
.type sys_exit,%function
sys_exit:
	movq $60, %rax /* 60 == __NR__exit */
	syscall
	ret
.size sys_exit,.-sys_exit

.globl sys_gettimeofday
.type sys_gettimeofday,%function
sys_gettimeofday:
        movq $96, %rax /* 96 == __NR_gettimeofday */
        syscall
        ret
.size sys_gettimeofday,.-sys_gettimeofday

.globl sys_nanosleep
.type sys_nanosleep,%function
sys_nanosleep:
        movq $35, %rax /* 35 == __NR_nanosleep */
        syscall
        ret
.size sys_nanosleep,.-sys_nanosleep
