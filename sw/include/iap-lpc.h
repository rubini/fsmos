/*
 * In Application Programming (and other rom-based stuff)
 */
#ifndef __IAP_H__
#error "Pleae include <iap.h>, not <iap-lpc.h>"
#endif

#define IAP_ADDRESS 0x1fff1ff1

enum iap_commands {
	IAP_CMD_PREPARE = 50,
	IAP_CMD_CPFLASH = 51,
	IAP_CMD_ERASESEC = 52,
	IAP_CMD_BLANKSEC = 53,
	IAP_CMD_PARTID = 54,
	IAP_CMD_ROMVERS = 55,
	IAP_CMD_COMPARE = 56,
	IAP_CMD_ROMBOOT = 57,
	IAP_CMD_UID = 58,
	IAP_CMD_ERASEPAGE = 59,
	IAP_CMD_EEPROM_WR = 61,
	IAP_CMD_EEPROM_RD = 62,
};

enum iap_errors {
	IAP_ERR_SUCCESS = 0,
	IAP_ERR_INVALID_CMD = 1,
	IAP_ERR_SRC_ALIGN = 2,
	IAP_ERR_DST_ALIGN = 3,
	IAP_ERR_SRC_UNMAP = 4,
	IAP_ERR_DST_UNMAP = 5,
	IAP_ERR_COUNT = 6,
	IAP_ERR_INVALID_SEC = 7,
	IAP_ERR_NOT_BLANK = 8,
	IAP_ERR_NOT_PREPARED = 9,
	IAP_ERR_COMPARE = 10,
	IAP_ERR_BUSY = 11,
	/* In theory, the rest are ISP only, but compare can return unmap */
	IAP_ERR_PARAM = 12,
	IAP_ERR_ADDR_ALIGN = 13,
	IAP_ERR_ADDR_UNMAP = 14,
	IAP_ERR_LOCKED = 15,
	IAP_ERR_CODE = 16,
	IAP_ERR_BAUD = 17,
	IAP_ERR_STOP = 18,
	IAP_ERR_PROTECTION = 19,
	__IAP_NR_ERRORS
};

extern const unsigned char iap_errno[__IAP_NR_ERRORS];

typedef void (*__iap)(uint32_t *args, uint32_t *results);

static inline int iap_call(uint32_t *args, uint32_t *res)
{
	__iap iap_ptr = (__iap)IAP_ADDRESS;
	iap_ptr(args, res);

	/* most commands return the error in position 0, so use it */
	if (res[0] < __IAP_NR_ERRORS)
		return -iap_errno[res[0]];
	return 0;
}
