#include <time.h>
#include <button.h>

int button_get(struct button *b)
{
	button_get_edge(b);
	return b->status;
}

int button_get_edge(struct button *b)
{
	int value = gpio_get(b->gpio);

	if ((b->flags & BUTTON_LEVEL_FLAG) == BUTTON_LOW_ACTIVE)
		value = !value;

	if (value == b->status)
		return 0; /* no edge */
	if (time_before(jiffies, b->last_event + b->debounce_j))
		return 0;

	b->last_event = jiffies;
	b->status = value;
	if (value) return 1; /* positive edge */
	if (b->flags & BUTTON_PRESS_ONLY)
		return 0; /* no negative edge reported */
	return -1;
}
