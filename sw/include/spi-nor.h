#ifndef __SPINOR_H__
#define __SPINOR_H__

struct spinor_dev {
	struct spi_dev *sdev;
	unsigned size;
	unsigned blocksize;
	unsigned nblocks;
	unsigned pagesize;
	uint8_t mfid;
	uint8_t devid[2];
	uint8_t unused;
	/* FIXME: cfi description */
};

struct spinor_dev *spinor_detect(struct spinor_dev *sn, struct spi_dev *s);
int spinor_read(struct spinor_dev *sn, void *buf, uint32_t pos, int count);
int spinor_write(struct spinor_dev *sn, void *buf, uint32_t pos, int count);
int spinor_erase(struct spinor_dev *sn, uint32_t pos);
int spinor_erase_sector(struct spinor_dev *sn, uint32_t pos);
int spinor_write_in_progress(struct spinor_dev *sn);

/* The following ones are used for parameters, the prototype is different */
int spinor_read_param(void *dev, int offset, void *buf, int size);
int spinor_write_param(void *dev, int offset, const void *buf, int size);

#endif
