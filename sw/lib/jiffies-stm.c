#include <init.h>
#include <time.h>
#include <io.h>

extern int __hz_value_is_too_small;

int timer_setup(void)
{
	int prvalue;

	/* Power on this timer, reset and enable it */
	regs[REG_TIMER_EN] |= BIT_TIMER_EN;
	regs[REG_TIMER_RST] = BIT_TIMER_EN;
	regs[REG_TIMER_RST] = 0;
	regs[REGBASE_TIMER + REGOFF_TIM_CR1] = BIT_TIM_CR1_CEN;

	prvalue = (CPU_FREQ / HZ) - 1;
	if (prvalue >= 0xffff)
		__hz_value_is_too_small++;
	regs[REGBASE_TIMER + REGOFF_TIM_PSC] = prvalue;
	regs[REGBASE_TIMER + REGOFF_TIM_EGR] = BIT_TIM_EGR_UG;
	return 0;
}

core_initcall(timer_setup);

unsigned char jiffies_hook;

/*
 * Our hardware counter is 16 bits, so offer an helper function.
 * This is desgined to have conditionals, so it executes in constant time.
 * Other approaches I tried are slower/longer. I might make it inline, later.
 */
union {
	uint32_t j;
	struct {
		uint16_t jl;
		uint16_t jh;
	};
} jiffies32;

uint32_t get_jiffies(void)
{
	uint32_t c = jiffies16;
	uint32_t jl = jiffies32.jl;

	jiffies32.jl = c;
	c = c - jl;    /* posive if ok; 0xffff.xxxx if overflow */
	c &= 0x10000;
	jiffies32.j += c;
	return jiffies32.j;
}
