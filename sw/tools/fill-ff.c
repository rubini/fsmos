#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


int main(int argc, char **argv)
{
	char buf[16 * 1024];
	FILE *f = stdin;
	char *fname = "<stdin>";
	int size, done = 0, ret, mult = 1;
	char k, c;

	if (argc != 2) {
		fprintf(stderr, "%s: Use: \"%s <size>\" (trailing 'k' ok)\n",
			argv[0], argv[0]);
		exit(1);
	}
	switch(sscanf(argv[1], "%i%c%c", &size, &k, &c)) {
	case 1: break; /* number */
	case 2:
		if (k == 'k') {
			mult = 1024;
			break;
		}
	default:
		fprintf(stderr, "%s: \"%s\" not valid size\n",
			argv[0], argv[1]);
		exit(1);
	}
	size *= mult;

	while ( (ret = fread(buf, 1, sizeof(buf), f)) > 0) {
		fwrite(buf, 1, ret, stdout);
		done += ret;
	}
	if (ret < 0) {
		fprintf(stderr, "%s: read(%s): %s\n", argv[0], fname,
			strerror(errno));
		exit(1);
	}
	if (done > size) {
		fprintf(stderr, "%s: input bigger than %i\n", argv[0], size);
		exit(1);
	}
	memset(buf, 0xff, sizeof(buf));
	while (done < size) {
		int len = size - done;
		if (len > sizeof(buf))
			len = sizeof(buf);
		fwrite(buf, 1, len, stdout);
		done += len;
	}
	exit(0);
}
