#ifndef __TIMESTAMP_H__
#define __TIMESTAMP_H__
#include <io.h>
#include <time.h> /* we use a shared structure across architectures */

#if defined(CONFIG_JIFFIES_USES_CT32B1)
#define TIMER_BASEADDR (regs + REG_TMR32B1_BASE)
#elif defined (CONFIG_JIFFIES_USES_CT32B0)
#define TIMER_BASEADDR (regs + REG_TMR32B0_BASE)
#elif
#error "Unkown timer fortimestamping"
#endif

static inline void __timestamp_get(struct timestamp *t)
{
	uint32_t unused;

	asm("ldr   %[frac],     [%[addr], #16]\n\t"
	    "ldr   %[count],    [%[addr], #8]\n\t"
	    "ldr   %[tmp],       [%[addr], #16]\n\t"
	    "cmp   %[tmp], %[frac]\n\t"
	    "ldr   %[tmp],       [%[addr], #8]\n\t"
	    "bge   1f\n\t"
	    "sub   %[count], %[tmp], #1\n\t"
	    "1:"
	    : [count] "=&l" (t->counts),
	      [frac] "=&l" (t->fraction),
	      [tmp] "=&l" (unused)
	    : [addr] "l" (TIMER_BASEADDR)
	    : "cc");
}

/* duplicate code, but frac here is already known */
static inline void __timestamp_complete(struct timestamp *t, uint32_t frac)
{
	uint32_t unused;

	t->fraction = frac;
	asm("ldr   %[count],    [%[addr], #8]\n\t"
	    "ldr   %[tmp],       [%[addr], #16]\n\t"
	    "cmp   %[tmp], %[frac]\n\t"
	    "ldr   %[tmp],       [%[addr], #8]\n\t"
	    "bge   1f\n\t"
	    "sub   %[count], %[tmp], #1\n\t"
	    "1:"
	    : [count] "=&l" (t->counts),
	      [tmp] "=&l" (unused)
	    : [addr] "l" (TIMER_BASEADDR),
	      [frac] "l" (frac)
	    : "cc");
}

#endif /* __TIMESTAMP_H__ */

