#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv)
{
	int i, nsamples = 0, range = 0;

	if (argc == 3) {
		nsamples = atoi(argv[1]);
		range = atoi(argv[2]);
	}
	if (!nsamples || !range) {
		printf("%s: Use: \"%s <nsamples> <range>\"\n", argv[0],
		       argv[0]);
		exit(1);
	}
	for (i = 0; i < nsamples; i++)
		printf("%i\n", (int)(sin(M_PI / (2 * nsamples - 2) * i)
				     * range + 0.5));
	return 0;
}
