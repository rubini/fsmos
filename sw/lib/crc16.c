#include <stdint.h>
#include <crc.h>

uint16_t crc16(uint16_t poly, uint8_t *buf, int len)
{
	uint16_t crc = 0xffff;
	int i, j;

	for (i = 0; i < len; i++) {
		crc ^= buf[i];
		for (j = 0; j < 8; j++) {
			if (crc & 1) {
				crc >>= 1;
				crc ^= poly;
			} else {
				crc >>= 1;
			}
		}
	}
	return crc;
}
