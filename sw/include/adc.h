#ifndef __ADC_H__
#define __ADC_H__

#define ADC_MAX_CHANNELS 9 /* Unfortunately 112x blesses them 1..8 */

struct adc_dev {
	unsigned long driverdata;
	int nbits;
	int chmask; /* other pins won't be configured/acquired */
	int chvalue[ADC_MAX_CHANNELS];
	uint8_t chflags[ADC_MAX_CHANNELS];
};

/* The library sets the flags but doesn't clear them */
#define ADC_FLAG_DONE		0x01 /* chvalue is filled */
#define ADC_FLAG_OVERRUN	0x02 /* as signalled by hardware */

extern struct adc_dev *adc_create(struct adc_dev *);
extern void adc_destroy(struct adc_dev *);

/* get_channel is the "easy" one: it waits for acquisition */
extern int adc_get_channel(struct adc_dev *dev, int ch);
extern int adc_acquire(struct adc_dev *dev, int chmask, unsigned long flags);
#define ADC_ACQ_START		0x01 /* start, return -EAGAIN */
#define ADC_ACQ_POLL		0x02 /* don't start, may return -EAGAIN */

/* Versions with timeout (thus no start/poll) */
extern int adc_get_channel_to(struct adc_dev *dev, int ch, unsigned long to);
extern int adc_acquire_to(struct adc_dev *dev, int chmask, unsigned long to);

#endif /* __ADC_H__ */
