#include <io.h>
#include "revision.h"

void callme(int n1, int n2)
{
	char s[32] = "--";
	putc('<');
	if (n1) {
		callme(n1 - 1, n2 + 1);
	} else {
		putc(s[0]);
		printf("%i", n2);
		putc(s[1]);
	}
	putc('>');
}

static int stack_use(void)
{
	extern uint32_t _stack[], _fstack[];
	uint32_t *s;

	for (s = _stack; s < _fstack; s++)
		if (*s != 0xfac0ffee)
			break;
	return (_fstack - s) * sizeof(*s);
}

void main(void)
{
	int i;

	printf("Hello, here is %s, commit %s\n", __FILE__, __gitc__);
	while(1) {
		for (i = 0; ; i++) {
			callme(i, 0);
			printf(" %i \n", stack_use());
		}
	}
}
