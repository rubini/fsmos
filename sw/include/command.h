#ifndef __COMMAND_H__
#define __COMMAND_H__
/*
 * A generic command interface, with "key" support
 */
#define COMMAND_NARGS 10
extern char *command_args[COMMAND_NARGS];

void command_split_argv(char *s, int *argcp, char **argv, int argvsize);

/*
 * Please note that the command received a pointer to a string. It can
 * print into a preallocated buffer or change the pointer to somewhere else
 */
struct command {
        const char *cmd;
        int (*cmd_f)(char **reply, int argc, char **argv);
        int argc_min, argc_max;
};
#define COMMAND_RETURN_SILENT -8000 /* not an errno value */

/* Some generic commands, and their lines for the command table */
int command_commit(char **reply, int argc, char **argv);
int command_stack(char **reply, int argc, char **argv);
int command_r(char **reply, int argc, char **argv);
int command_w(char **reply, int argc, char **argv);
int command_gpio(char **reply, int argc, char **argv);
int command_help(char **reply, int argc, char **argv);
int command_config(char **reply, int argc, char **argv);
#define COMMAND_COMMIT  {"commit",  command_commit,    1, 1}
#define COMMAND_STACK   {"stack",   command_stack,     1, 1}
#define COMMAND_R       {"r",       command_r,         2, 2}
#define COMMAND_W       {"w",       command_w,         3, 3}
#define COMMAND_GPIO    {"gpio",    command_gpio,      2, 3}
#define COMMAND_HELP    {"help",    command_help,      1, 1}
#define COMMAND_CONFIG  {"config",  command_config,    1, 1}

/* The reply buffer (OK/KO and ID are automatically added) */
#ifndef CONFIG_COMMAND_REPLY_SIZE
#define CONFIG_COMMAND_REPLY_SIZE 64
#endif
extern char command_reply[CONFIG_COMMAND_REPLY_SIZE];

/* And the parsing function (but the user should puts the reply string) */
int command_parse(char *s, const struct command *cmds);

#endif
