#include <usb.h>
#include <net.h>

struct usbeth {
	struct usb_device *ud;
	unsigned long flags;
};

#define USBETH_USER_FLAGS    0xffff
#define USBETH_FLAG_INITED  0x10000

#ifndef CONFIG_USBETH_BSIZE
#define CONFIG_USBETH_BSIZE 512
#endif

extern struct usbeth *usbeth_init(struct usbeth *);
int usbeth_send(struct usbeth *ue, const void *buf, int len);
int usbeth_recv(struct usbeth *ue, void *buf, int len);

/* There is no need to call poll if you call recv() often enough */
extern void usbeth_poll(struct usbeth *ue);

extern int usbeth_frame_rx(struct ethhdr *, uint8_t *, uint32_t buf_size);
extern int usbeth_frame_tx(struct ethhdr_vlan *, uint8_t *, uint32_t len);

