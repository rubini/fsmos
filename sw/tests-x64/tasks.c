#include <io.h>
#include <time.h>
#include <sched-simple.h>

void *task_serial(void *arg)
{
	char *sarg = arg;
	puts(sarg);
	return arg;
}

struct task __task t_quarter = {
	.version = SCHED_VERSION,
	.name = "quarter",
	.job = task_serial,
	.arg = ".",
	.period = HZ / 4,
};
struct task __task t_second = {
	.version = SCHED_VERSION,
	.name = "second",
	.job = task_serial,
	.arg = "S",
	.period = HZ,
	.release = 1,
};
struct task __task t_10s = {
	.version = SCHED_VERSION,
	.name = "10s",
	.job = task_serial,
	.arg = "\n",
	.period = 10 * HZ,
	.release = 2,
};
struct task __task t_minute = {
	.version = SCHED_VERSION,
	.name = "minute",
	.job = task_serial,
	.arg = "minute!\n",
	.period = 60 * HZ,
	.release = 3,
};

void main(void)
{
	sched_simple();
}
