#include <assert.h>
#include <irq.h>
#include <io.h>
#include <command.h>

struct irq_handler {
	irq_handler_t handler;
	void *data;
	unsigned long nirq;
};

static struct irq_handler irq_handlers[__NR_IRQS];

/* We may want to add a stack dump before calling the low-level panic code */
static void irq_unexpected_with_diag(int irq, uint32_t fraction, uint32_t sp)
{
	static const char *names[] = {
		"R0 ", "R1 ", "R2 ", "R3 ", "R12", "LR ", "PC ", "PSR"
	};

	if (CONFIG_IRQ_IS_VERBOSE) {
		uint32_t *p = (void *)sp;
		extern uint32_t _fstack[];
		int i;
		printf("\nUnexpected irq\n");
		for (i = 0; i < ARRAY_SIZE(names); i++) {
			printf("%s = %08lx\n", names[i],
			       (unsigned long)p[i]);
		}
		printf("\nirq SP = %08lx\n\n", (unsigned long)sp);

		for (p = _fstack; p >= (uint32_t *)sp; p--) {
			printf("%p: %08x\n", p, (int)*p);
		}
	}
	irq_unexpected(irq, fraction, sp);
}

/* The default handler, that may spit diagnostic */
static irqreturn_t no_irq(void *data, uint32_t timestamp_fraction, uint32_t sp)
{
	int irq = (int)data;
	irq_unexpected_with_diag(irq, timestamp_fraction, sp);
	return IRQ_NONE;
}

static int __irq_clear_ptr(int irq)
{
	struct irq_handler *h = irq_handlers + irq;

	h->handler = no_irq;
	h->data = (void *)irq;
	return 0;
}

/* We can't make this an initcall, or it will always be linked */
static void irq_init(void)
{
	int i;

	if (irq_handlers[0].handler)
		return; /* already initialized */

	for (i = 0; i < ARRAY_SIZE(irq_handlers); i++)
		__irq_clear_ptr(i);
}

/*
 * Helpers for bits and 'ports'
 */
static inline uint32_t irq_bit(int irq)
{
	return 1 << (irq & 0x1f);
}

static inline uint32_t irq_word(int irq)
{
	if (__NR_IRQS <= 32)
		return 0;
	return irq >> 5;
}

/*
 * This is overriding the assembly function that panics, but it still
 * calls it with the "irq_unexpected" name.
 */
void irq_entry(int irq, uint32_t timestamp_fraction, uint32_t sp)
{
	struct irq_handler *h;
	irqreturn_t retval;

	irq -= __NVIC_INTERNAL_INTERRUPTS;
	if (irq < 0)
		irq_unexpected_with_diag(irq, timestamp_fraction, sp);

	h = irq_handlers + irq;
	retval = h->handler(h->data, timestamp_fraction, sp);
	h->nirq++;

	/*
	 * And then acknowledge in the VIC. This works for level interrupts,
	 * not for edge ones, but there is no general solution.
	 */
	if (retval == IRQ_HANDLED)
		regs[REG_NVIC_ICPR0 + irq_word(irq)] = irq_bit(irq);

}


/* Called by an application, that will, later, irq_enable() */
int irq_request(int irq, unsigned long flags, irq_handler_t func, void *data)
{
	struct irq_handler *h = irq_handlers + irq;

	/* flags are undefined yet: panic if "newer" code relies on flags */
	assert(flags == 0, 127, "Invalid IRQ flags 0x%lx\n", flags);

	irq_init();

	if (irq < 0 || irq >= ARRAY_SIZE(irq_handlers))
		return -EINVAL;
	if (h->handler != no_irq)
		return -EBUSY;

	h->data = data;
	h->handler = func;
	regs[REG_NVIC_ISER0 + irq_word(irq)] = irq_bit(irq);

	return 0;
}

int irq_free(int irq, void *data)
{
	struct irq_handler *h = irq_handlers + irq;

	if (irq < 0 || irq >= ARRAY_SIZE(irq_handlers))
		return -EINVAL;
	if (h->data != data)
		return -ENOENT;

	regs[REG_NVIC_ICPR0 + irq_word(irq)] = irq_bit(irq); /* ack */
	regs[REG_NVIC_ICER0 + irq_word(irq)] = irq_bit(irq); /* disable */
	return __irq_clear_ptr(irq);
}

/* a command to dump statistics */
int command_irqstat(char **reply, int argc, char **argv)
{
	struct irq_handler *h;
	int i;

	for (i = 0; i < ARRAY_SIZE(irq_handlers); i++) {
		h = irq_handlers + i;
		if (h->handler && h->handler != no_irq)
			printf("irq %3i: handler %p, count %5li\n",
			       i, h->handler, h->nirq);
	}
	return 0;
}
