/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2012 GSI (www.gsi.de)
 * Author: Wesley W. Terpstra <w.terpstra@gsi.de>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */
#include <string.h>
#include <time.h>
#include <io.h>
#include <net.h>
#include <ipv4.h>

enum ip_status ip_status = IP_TRAINING;
static uint8_t myIP[4];

/* bootp: bigger buffer, UDP based */
static uint8_t __bootp_queue[512];
static struct socket __static_bootp_socket = {
	.queue.buff = __bootp_queue,
	.queue.size = sizeof(__bootp_queue),
};
static struct socket *bootp_socket;

/* ICMP: smaller buffer */
static uint8_t __icmp_queue[128];
static struct socket __static_icmp_socket = {
	.queue.buff = __icmp_queue,
	.queue.size = sizeof(__icmp_queue),
};
static struct socket *icmp_socket;

unsigned int ipv4_checksum(unsigned short *buf, int shorts)
{
	int i;
	uint32_t sum;

	sum = 0;
	for (i = 0; i < shorts; ++i)
		sum += ntohs(buf[i]);

	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);

	return (~sum & 0xffff);
}

void ipv4_init(void)
{
	static struct sockaddr saddr;

	if (CONFIG_HAS_BOOTP) {
		/* Bootp: use UDP engine activated by function arguments  */
		bootp_socket = socket_create(&__static_bootp_socket, NULL,
					     SOCK_UDP, 68 /* bootpc */);
	}

	/* ICMP: specify raw (not UDP), with IPV4 ethtype */
	memset(&saddr, 0, sizeof(saddr));
	saddr.ethertype = htons(0x0800);
	icmp_socket = socket_create(&__static_icmp_socket, &saddr,
				    SOCK_RAW_ETHERNET, 0);
}

static int bootp_retry = 0;

/* receive bootp through the UDP mechanism */
static int bootp_poll(void)
{
	static struct sockaddr addr;
	static uint8_t buf[400];
	int len, ret = 0;
	static unsigned long j;

	len = socket_recvfrom(bootp_socket, &addr, buf, sizeof(buf));

	if (ip_status != IP_TRAINING)
		return 0;

	if (len > 0)
		ret = process_bootp(buf, len);

	if (!j)
		j = jiffies;
	if (time_before(jiffies, j))
		return ret;
	j += HZ;

	len = prepare_bootp(&addr, buf, ++bootp_retry);
	socket_sendto(bootp_socket, &addr, buf, len);
	return 1;
}

static int icmp_poll(void)
{
	struct sockaddr addr;
	static uint8_t buf[128];
	int len;

	len = socket_recvfrom(icmp_socket, &addr, buf, sizeof(buf));
	if (len <= 0)
		return 0;
	if (ip_status == IP_TRAINING)
		return 0;

	/* check the destination IP */
	if (check_dest_ip(buf))
		return 0;

	if ((len = process_icmp(buf, len)) > 0)
		socket_sendto(icmp_socket, &addr, buf, len);
	return 1;
}


int ipv4_poll(void)
{
	int ret = 0;

	/* FIXME: linkup event  --> ip_status = IP_TRAINING; */
	if (CONFIG_HAS_BOOTP)
		ret = bootp_poll();

	ret += icmp_poll();

	return ret != 0;
}

void getIP(unsigned char *IP)
{
	memcpy(IP, myIP, 4);
}

void setIP(unsigned char *ip)
{
	memcpy(myIP, ip, 4);

	if (ip == 0)
		ip_status = IP_TRAINING;
	bootp_retry = 0;
}

/* Check the destination IP of the incoming packet */
int check_dest_ip(unsigned char *buf)
{
	if (!buf)
		return -1;
	return memcmp(buf + IP_DEST, myIP, 4);
}
