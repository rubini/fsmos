#include <io.h>
#include <time.h>
#include <string.h>
#include <spi.h>
#include <spi-nor.h>
#include <errno.h>

/*
 * The "command" part is not something the caller is interested in. Define
 * commands and used them in the spinor_command() helper
 */
struct spinor_cmddef {
	int cmdlen;
	uint8_t cmd[];
};

/* Some commands are const: there is no variable data item */
static const struct spinor_cmddef  cmd_jedecid = {1, {0x9f}};
static const struct spinor_cmddef  cmd_writeenable = {1, {0x06}};
static const struct spinor_cmddef  cmd_readSR1 = {1, {0x05}};

/* Some commands are not const: they are modified by the caller */
static struct spinor_cmddef  cmd_read = {4, {0x03, 0, 0, 0}};
static struct spinor_cmddef  cmd_read32 = {5, {0x13, 0, 0, 0, 0}};
static struct spinor_cmddef  cmd_wpage = {4, {0x02, 0, 0, 0}};
static struct spinor_cmddef  cmd_wpage32 = {5, {0x12, 0, 0, 0, 0}};
static struct spinor_cmddef  cmd_erase = {4, {0xd8, 0, 0, 0}};
static struct spinor_cmddef  cmd_erase32 = {5, {0xdc, 0, 0, 0, 0}};
/* "sector erase". Data sheet call 4kB a "sector", linux says "block" (?) */
static struct spinor_cmddef  cmd_serase = {4, {0x20, 0, 0, 0}};
static struct spinor_cmddef  cmd_serase32 = {5, {0x21, 0, 0, 0, 0}};

static int __fits_24_bits(long pos)
{
	return ((pos >> 24) == 0);
}

/* Internal helper. We usually are not interested in the first reply byte */
static int spinor_command(struct spinor_dev *sn,
			  const struct spinor_cmddef *cmd,
			  void *idata, void *odata, int count)
{
	struct spi_dev *dev;
	struct spi_ibuf ibuf;
	struct spi_obuf obuf;
	int ret;

	if (!sn  || !sn->sdev)
		return -ENODEV;

	dev = sn->sdev;
	obuf.len = cmd->cmdlen;
	obuf.buf = (uint8_t *)cmd->cmd; /* avoid "const" warning */
	if (count == 0) {
		/* special case: command with no data phase */
		return spi_xfer(dev, 0, NULL, &obuf);
	}
	/* normal case: command doesn't realease CS and a data phase follows */
	ret = spi_xfer(dev, SPI_F_NOFINI, NULL, &obuf);
	if (ret != cmd->cmdlen) {
		/* raise CS */
		obuf.len = 0;
		spi_xfer(dev, SPI_F_NOINIT, NULL, &obuf);
		return ret;
	}
	ibuf.buf = idata;
	obuf.buf = odata;
	ibuf.len = obuf.len = count;
	return spi_xfer(dev, SPI_F_NOINIT, idata ? &ibuf : NULL,
			odata ? &obuf : NULL);
}

static const struct spinor_desc {
	uint8_t id[4]; /* actually 3 */
	uint32_t blocksize, nblocks;
} spinor_descs[] = {
	{{0x01, 0x02, 0x19}, 0x40000, 128},    /* spansion 256Mb */
	{{0x01, 0x20, 0x18}, 0x10000, 256},    /* spansion 128Mb */
	{{0x01, 0x40, 0x16}, 0x10000,  64},    /* cypress 32Mb */
	{{0x1f, 0x87, 0x01}, 0x10000,  64},    /* AT25SF321V, 32Mb */
	{{0x1f, 0x84, 0x01}, 0x10000,   8},    /* AT25SF041B, 4Mb */
	{{0xef, 0x40, 0x14}, 0x10000,  16},    /* spansion s25fl008k, 8Mb */
	{{0xef, 0x70, 0x15}, 0x10000,  32},    /* winbond 16Mb w25q16 */
	{{0xc2, 0x28, 0x16}, 0x10000,  64},    /* macronix 32Mb mx25r3235f */
	{{0xef, 0x40, 0x18}, 0x10000, 256},    /* winbond 128Mb w25q128 */
	{{0xef, 0x30, 0x13}, 0x10000,   8},    /* winbond 4Mb w25x40cl */
	{{0xc8, 0x40, 0x18}, 0x10000, 256},    /* gigadevice 128Mb gd25q128 */
	{{0x9d, 0x60, 0x18}, 0x10000, 256},    /* issi is25lp128 */
	{{0x20, 0xbb, 0x17}, 0x10000, 128},    /* micron 64Mb n25q064a11 */
	{},
};

struct spinor_dev *spinor_detect(struct spinor_dev *sn, struct spi_dev *s)
{
	uint8_t idbuffer[16];
	const struct spinor_desc *d;
	int ret;

	/* Identify device */
	sn->sdev = s;
	ret = spinor_command(sn, &cmd_jedecid, idbuffer, NULL, 3);
	sn->sdev = NULL;
	if (ret != 3) {
		if (CONFIG_SPINOR_IS_VERBOSE)
			printf("error %i in jedecid command\n", ret);
		return NULL;
	}
	if (CONFIG_SPINOR_IS_VERBOSE)
		printf("id: %02x %02x %02x\n",
		       idbuffer[0], idbuffer[1], idbuffer[2]);

	sn->mfid = idbuffer[0];
	sn->devid[0] = idbuffer[1];
	sn->devid[1] = idbuffer[2];
	for (d = spinor_descs; d->blocksize; d++)
		if (memcmp(d->id, idbuffer, 3) == 0)
			break;

	if (!d->blocksize) {
		if (CONFIG_SPINOR_IS_VERBOSE)
			printf("flash model not found in database\n");
		return NULL;
	}
	sn->size = d->blocksize * d->nblocks;
	sn->blocksize = d->blocksize;
	sn->nblocks = d->nblocks;
	sn->pagesize = 0x100; /* 0x200 not working on all devices */
	sn->sdev = s;
	return sn;
}


int spinor_read(struct spinor_dev *sn, void *buf, uint32_t off, int count)
{
	int ret;

	/* FIXME: split by page boundary (use pagesize info) */

	if (__fits_24_bits(off)) {
		cmd_read.cmd[1] = off >> 16;
		cmd_read.cmd[2] = off >> 8;
		cmd_read.cmd[3] = off;
		ret = spinor_command(sn, &cmd_read, buf, NULL, count);
	} else {
		cmd_read32.cmd[1] = off >> 24;
		cmd_read32.cmd[2] = off >> 16;
		cmd_read32.cmd[3] = off >> 8;
		cmd_read32.cmd[4] = off;
		ret = spinor_command(sn, &cmd_read32, buf, NULL, count);
	}
	return ret;
}

int spinor_write(struct spinor_dev *sn, void *buf, uint32_t pos, int count)
{
	int ret;

	if (count > sn->pagesize)
		return -EINVAL;

	ret = spinor_command(sn, &cmd_writeenable, NULL, NULL, 0);
	if (ret < 0)
		return ret;
	if (__fits_24_bits(pos)) {
		cmd_wpage.cmd[1] = pos >> 16;
		cmd_wpage.cmd[2] = pos >> 8;
		cmd_wpage.cmd[3] = pos;
		ret = spinor_command(sn, &cmd_wpage, NULL, buf, count);
	} else {
		cmd_wpage32.cmd[1] = pos >> 24;
		cmd_wpage32.cmd[2] = pos >> 16;
		cmd_wpage32.cmd[3] = pos >> 8;
		cmd_wpage32.cmd[4] = pos;
		ret = spinor_command(sn, &cmd_wpage32, NULL, buf, count);
	}
	return ret;
}

int spinor_erase(struct spinor_dev *sn, uint32_t pos)
{
	int ret;

	ret = spinor_command(sn, &cmd_writeenable, NULL, NULL, 0);
	if (ret < 0)
		return ret;

	if (__fits_24_bits(pos)) {
		cmd_erase.cmd[1] = pos >> 16;
		cmd_erase.cmd[2] = pos >> 8;
		cmd_erase.cmd[3] = pos;
		ret = spinor_command(sn, &cmd_erase, NULL, NULL, 0);
	} else {
		cmd_erase32.cmd[1] = pos >> 24;
		cmd_erase32.cmd[2] = pos >> 16;
		cmd_erase32.cmd[3] = pos >> 8;
		cmd_erase32.cmd[4] = pos;
		ret = spinor_command(sn, &cmd_erase32, NULL, NULL, 0);
	}

	return ret;
}

int spinor_erase_sector(struct spinor_dev *sn, uint32_t pos)
{
	int ret;

	ret = spinor_command(sn, &cmd_writeenable, NULL, NULL, 0);
	if (ret < 0)
		return ret;

	if (__fits_24_bits(pos)) {
		cmd_serase.cmd[1] = pos >> 16;
		cmd_serase.cmd[2] = pos >> 8;
		cmd_serase.cmd[3] = pos;
		ret = spinor_command(sn, &cmd_serase, NULL, NULL, 0);
	} else {
		cmd_serase32.cmd[1] = pos >> 24;
		cmd_serase32.cmd[2] = pos >> 16;
		cmd_serase32.cmd[3] = pos >> 8;
		cmd_serase32.cmd[4] = pos;
		ret = spinor_command(sn, &cmd_serase32, NULL, NULL, 0);
	}

	return ret;
}

int spinor_write_in_progress(struct spinor_dev *sn)
{
	char sr1;
	int ret;

	ret = spinor_command(sn, &cmd_readSR1, &sr1, NULL, 1);
	if (ret < 0)
		return ret;
	return sr1 & 0x01;
}

/* read and write with different prototypes, for parameter I/O */
int spinor_read_param(void *dev, int offset, void *buf, int size)
{
	return spinor_read(dev, buf, offset, size);
}

int spinor_write_param(void *dev, int offset, const void *buf, int size)
{
	int ret;
	unsigned long timeout;

	/* erase first, unfortunately */
	ret = spinor_erase(dev, offset);
	if (ret < 0)
		return ret;
	timeout = jiffies + HZ * 2;
	while(spinor_write_in_progress(dev))
		if (time_after(jiffies, timeout))
			return -ETIMEDOUT;

	/* we must discard const here */
	return spinor_write(dev, (void *)buf, offset, size);
}
