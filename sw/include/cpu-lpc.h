#ifndef __CPU_H__
#define __CPU_H__

#ifdef __ASSEMBLER__

#if CONFIG_CPU_IS_LPC17
#  define JIFFIES_ADDR		0x40094008
#else
#  if CONFIG_JIFFIES_USES_CT32B1
#    define JIFFIES_ADDR	0x40018008
#  elif CONFIG_JIFFIES_USES_CT32B0
#    define JIFFIES_ADDR	0x40014008
#  else
#    error "Invalid timer selection"
#  endif
#endif

#else /* not assembler */

#include <stdint.h>
#include <hw.h>

#if defined(CONFIG_LPC1) || defined(CONFIG_LPC1_OLD)
#  include <cpu-lpc1.h>
#elif defined(CONFIG_LPC17)
#  include <cpu-lpc17.h>
#else
#  error "Unknown LPC processor"
#endif

#define XTAL_FREQ  (12 * 1000 * 1000)
#define CPU_FREQ (XTAL_FREQ * CONFIG_PLL_CPU)

#endif /* __ASSEMBLER__ */
#endif /* __CPU_H__ */

