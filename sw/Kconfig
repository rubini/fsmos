
mainmenu "fsmOS configuration"

choice
	prompt "CPU family build for"

config ARCH_LPC
	bool "LPC (processors based on cortex-M)"
	select ARCH_32BITS
	select USE_LIBGCC
       
config ARCH_STM
	bool "STM32 (processors based on cortex-M)"
	select ARCH_32BITS
	select USE_LIBGCC
      
config ARCH_AVR
	bool "AVR (atmega and the like)"
	select ARCH_16BITS

config ARCH_X64
	bool "X86-64 w/ Linux kernel sandbox"
	select ARCH_64BITS

endchoice

config ARCH_IS_LPC
	int
	default 1 if ARCH_LPC
	default 0

config ARCH_IS_STM
	int
	default 1 if ARCH_STM
	default 0

config ARCH_IS_AVR
	int
	default 1 if ARCH_AVR
	default 0

config ARCH_IS_X64
	int
	default 1 if ARCH_X64
	default 0

config ARCH_64BITS
	bool

config ARCH_32BITS
	bool

config ARCH_16BITS
	bool

config ARCH_BITS
	int
	default 16 if ARCH_16BITS
	default 64 if ARCH_64BITS
	default 32

config ARCH
	string
	default "lpc" if ARCH_LPC
	default "stm" if ARCH_STM
	default "avr" if ARCH_AVR
	default "x64" if ARCH_X64
	default "undefined"

config USE_LIBGCC
	bool


if ARCH_LPC
source "Kconfig-lpc"
endif

if ARCH_STM
source "Kconfig-stm"
endif

if ARCH_AVR
source "Kconfig-avr"
endif

if ARCH_X64
source "Kconfig-x64"
endif

# The following file defule FLASHSIZE, CM0, such stuff
source "Kconfig-common"

choice
	prompt "Strerror implementation"

config STRERROR_SMALL
	bool "Only a few names, the most used; ~250 bytes"

config STRERROR_NONE
	bool "No names: strerror returns sprintf(-number); ~40 bytes"

config STRERROR_FULL
	bool "(Almost) all names: ~600 bytes"

endchoice


config PRINT_BUFSIZE
	int
	default 128

config NO_BOOTP
	bool "Avoid bootp network code (saves 550B ram and some flash)"

config HAS_BOOTP
       int
       default 0 if NO_BOOTP
       default 1

config AVOID_PRINTF
	bool "Avoid use of printf in system files"
	help
	  Printf is big on small systems (like Atmega48),
	  but is used in setup.c so it's always pulled in.
	  This option removes use of printf by system files,
	  to help making a smaller binary.

config USE_PRINTF
	int
	default 0 if AVOID_PRINTF
	default 1

config MAX_N_PARAM
	int "If you use parameters, maximum array size"
	default 8
	help
	  The parameter library must assemble all parameters to
	  a buffer, for writing/reading to/from i2c eeprom.
	  We need to size the buffer statically.

config PARAM_I2C
	bool "Support for parameters in i2c memory (may cost 1.5kB)"
	default n
	help
	  If you don't otherwise use i2c, this would pick in i2c code.
	  The binary is thus 1.5kB bigger. If don't use this feature,
	  you can leave this unset.

config PARAM_SPIFLASH
	bool "Support for parameters in SPI flash"
	default n
	help
	  If you don't otherwise use SPI, this would pick in SPI code.
	  The binary is thus bigger. If don't use this feature,
	  you can leave this unset.

config PARAM_LPC_EEPROM
	bool "Support for parameters in internal LPC eeprom"
	default y if ARCH_LPC
	default n

config PARAM_CUSTOM_CODE
	bool "Load/save parameters with application-defined callbacks"
	default n


source Kconfig-debug
