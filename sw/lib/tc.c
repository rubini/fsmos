/*
 * Thermocouple conversion
 *
 * Every number here, temperatures and millivolts, is 16.16 fixed point
 */
#include <stdint.h>
#include <tc.h>

#define F16(f)  ((int32_t)((f) * 65536 + 0.5))

/*
 * K table. I checked several sources, and they differ only at uV level.
 * Here I use data from http://www.pyromation.com/Downloads/Data/emfk_c.pdf
 */
static const int32_t __tc_k_values[] = { /* every 10 degrees */
	F16(-6.458), F16(-6.411), F16(-6.404), F16(-6.344), F16(-6.262),
	F16(-6.158), F16(-6.035), F16(-5.891), F16(-5.730), F16(-5.550),
	F16(-5.354), F16(-5.141), F16(-4.913), F16(-4.669), F16(-4.411),
	F16(-4.138), F16(-3.852), F16(-3.554), F16(-3.243), F16(-2.920),
	F16(-2.587), F16(-2.243), F16(-1.889), F16(-1.527), F16(-1.156),
	F16(-0.778), F16(-0.392), F16(0.000), F16(0.397), F16(0.798),
	F16(1.203), F16(1.612), F16(2.023), F16(2.436), F16(2.851),
	F16(3.267), F16(3.682), F16(4.096), F16(4.509), F16(4.920),
	F16(5.328), F16(5.735), F16(6.138), F16(6.540), F16(6.941),
	F16(7.340), F16(7.739), F16(8.138), F16(8.539), F16(8.940),
	F16(9.343), F16(9.747), F16(10.153), F16(10.561), F16(10.971),
	F16(11.382), F16(11.795), F16(12.209), F16(12.624), F16(13.040),
	F16(13.457), F16(13.874), F16(14.293), F16(14.713), F16(15.133),
	F16(15.554), F16(15.975), F16(16.397), F16(16.820), F16(17.243),
	F16(17.667), F16(18.091), F16(18.516), F16(18.941), F16(19.366),
	F16(19.792), F16(20.218), F16(20.644), F16(21.071), F16(21.497),
	F16(21.924), F16(22.350), F16(22.776), F16(23.203), F16(23.629),
	F16(24.055), F16(24.480), F16(24.905), F16(25.330), F16(25.755),
	F16(26.179), F16(26.602), F16(27.025), F16(27.447), F16(27.869),
	F16(28.289), F16(28.710), F16(29.129), F16(29.548), F16(29.965),
	F16(30.382), F16(30.798), F16(31.213), F16(31.628), F16(32.041),
	F16(32.453), F16(32.865), F16(33.275), F16(33.685), F16(34.093),
	F16(34.501), F16(34.908), F16(35.313), F16(35.718), F16(36.121),
	F16(36.524), F16(36.925), F16(37.326), F16(37.725), F16(38.124),
	F16(38.522), F16(38.918), F16(39.314), F16(39.708), F16(40.101),
	F16(40.494), F16(40.885), F16(41.276), F16(41.665), F16(42.053),
	F16(42.440), F16(42.826), F16(43.211), F16(43.595), F16(43.978),
	F16(44.359), F16(44.740), F16(45.119), F16(45.497), F16(45.873),
	F16(46.249), F16(46.623), F16(46.995), F16(47.367), F16(47.737),
	F16(48.105), F16(48.473), F16(48.838), F16(49.202), F16(49.565),
	F16(49.926), F16(50.286), F16(50.644), F16(51.000), F16(51.355),
	F16(51.708), F16(52.060), F16(52.410), F16(52.759), F16(53.106),
	F16(53.451), F16(53.795), F16(54.138), F16(54.479), F16(54.819)
};

static const int32_t *tc_k_values = __tc_k_values -TC_K_MIN / 10;
#define tc_k_min (TC_K_MIN / 10)
#define tc_k_max (TC_K_MAX / 10)

/* minmax array: degs, index, degs, index */
static const int32_t tc_k_minmax[4] = {
	F16(TC_K_MIN), tc_k_min,
	F16(TC_K_MAX), tc_k_max,
};

/*
 * T table. Again, I checked several sources. In the end I used
 * the nist text file: https://srdata.nist.gov/its90/download/type_t.tab
 */

static const int32_t __tc_t_values[] = { /* every 10 degrees */
	F16(-6.258), F16(-6.232), F16(-6.180), F16(-6.105), F16(-6.007),
	F16(-5.888), F16(-5.753), F16(-5.603), F16(-5.439), F16(-5.261),
	F16(-5.070), F16(-4.865), F16(-4.648), F16(-4.419), F16(-4.177),
	F16(-3.923), F16(-3.657), F16(-3.379), F16(-3.089), F16(-2.788),
	F16(-2.476), F16(-2.153), F16(-1.819), F16(-1.475), F16(-1.121),
	F16(-0.757), F16(-0.383), F16(0.000), F16(0.391), F16(0.790),
	F16(1.196), F16(1.612), F16(2.036), F16(2.468), F16(2.909),
	F16(3.358), F16(3.814), F16(4.279), F16(4.750), F16(5.228),
	F16(5.714), F16(6.206), F16(6.704), F16(7.209), F16(7.720),
	F16(8.237), F16(8.759), F16(9.288), F16(9.822), F16(10.362),
	F16(10.907), F16(11.458), F16(12.013), F16(12.574), F16(13.139),
	F16(13.709), F16(14.283), F16(14.862), F16(15.445), F16(16.032),
	F16(16.624), F16(17.219), F16(17.819), F16(18.422), F16(19.030),
	F16(19.641), F16(20.255), F16(20.872)
};

static const int32_t *tc_t_values = __tc_t_values -TC_T_MIN / 10;
#define tc_t_min (TC_T_MIN / 10)
#define tc_t_max (TC_T_MAX / 10)

/* minmax array: degs, index, degs, index */
static const int32_t tc_t_minmax[4] = {
	F16(TC_T_MIN), tc_t_min,
	F16(TC_T_MAX), tc_t_max,
};


/*
 * code follows
 */
static int32_t degs_to_mvs(int32_t degs, const int32_t minmax[],
			   const int32_t *values, const int32_t *__values)
{
	int q, r;
	int32_t result;

	if (degs <= minmax[0])
		return values[minmax[1]];
	if (degs >= minmax[2])
		return values[minmax[3]];

	/* Now use positives only, to avoid issues with % operator */
	degs += -minmax[0];
	q = degs / F16(10);
	r = degs % F16(10);

	/*
	 * linear interpolation -- but avoid overflow
	 * (rest is up to 0x9ffff but v[q+1] - v[q] ~= 0x6500)
	 */
	result = __values[q];
	result +=
		((__values[q + 1] - __values[q]))
		* (r >> 8)
		/ (F16(10) >> 8);
	return result;
}

static int32_t mvs_to_degs(int32_t mvs, const int32_t minmax[],
			   const int32_t *values, const int32_t *__values)
{
	int i;
	int32_t result;

	if (mvs <= values[minmax[1]])
		return minmax[0];
	if (mvs >= values[minmax[3]])
		return minmax[2];

	/* look up */
	for (i = minmax[1]; i < minmax[3]; i++)
		if (values[i + 1] > mvs)
			break;

	/*
	 * Linear interpolation, again avoid overflow.
	 * Divide and shift up later, which is sharper
	 */
	result = F16(i * 10);
	result += ((F16(10) >> 8) * (mvs - values[i]) /
		   (values[i+1] - values[i])) << 8;
	return result;
}

int32_t tc_k_convert(int32_t cold_degs, int32_t millivolts)
{
	int32_t cold_mvs;

	cold_mvs = degs_to_mvs(cold_degs, tc_k_minmax,
			       tc_k_values, __tc_k_values);
	return mvs_to_degs(cold_mvs + millivolts, tc_k_minmax,
			       tc_k_values, __tc_k_values);
}

int32_t tc_t_convert(int32_t cold_degs, int32_t millivolts)
{
	int32_t cold_mvs;

	cold_mvs = degs_to_mvs(cold_degs, tc_t_minmax,
			       tc_t_values, __tc_t_values);
	return mvs_to_degs(cold_mvs + millivolts, tc_t_minmax,
			       tc_t_values, __tc_t_values);
}

#if __STDC_HOSTED__  /* we build on the host: we run the tests */
#include <stdio.h>
#include <stdlib.h>

static char *pf16(int32_t f16)
{
	char c = " -"[f16 < 0];
	uint32_t uf16 = f16 * (1 - 2*(f16 < 0));
	char *result;

	result = malloc(128); /* leak: who cares */
	sprintf(result, "%c%04i.%04i", c, uf16 >> 16,
		((uf16 & 0xffff) * 10000) >> 16);
	return result;
}

/* Note: to build for the host turn <tc.h> to "../include/tc.h" */
int main(int argc, char **argv)
{
	int32_t a, b;

	/* we duplicate code. No arguments: K type. One argument: T type */
	if (argc == 1) {
	printf("pointers: %p (%x %s) %p (%x %s)\n",
	       __tc_k_values, __tc_k_values[0], pf16(__tc_k_values[0]),
	       tc_k_values, tc_k_values[0], pf16(tc_k_values[0]));

	printf("index min %i (%x %s), max %i (%x %s)\n",
	       tc_k_min, tc_k_values[tc_k_min], pf16(tc_k_values[tc_k_min]),
	       tc_k_max, tc_k_values[tc_k_max], pf16(tc_k_values[tc_k_max]));


	for (a = F16(TC_K_MIN - 10); a < F16(TC_K_MAX + 10); a+= 0xedc) {
		b = degs_to_mvs(a, tc_k_minmax, tc_k_values, __tc_k_values);
		printf("degs %s     mvs %s\n", pf16(a), pf16(b));
	}

	for (a = tc_k_values[tc_k_min] - 0x1000;
	     a < tc_k_values[tc_k_max] + 0x1000; a += 0x55) {
		b = mvs_to_degs(a, tc_k_minmax, tc_k_values, __tc_k_values);
		printf("mvs %s     degs %s\n", pf16(a), pf16(b));
	}
	} else {
	printf("pointers: %p (%x %s) %p (%x %s)\n",
	       __tc_t_values, __tc_t_values[0], pf16(__tc_t_values[0]),
	       tc_t_values, tc_t_values[0], pf16(tc_t_values[0]));

	printf("index min %i (%x %s), max %i (%x %s)\n",
	       tc_t_min, tc_t_values[tc_t_min], pf16(tc_t_values[tc_t_min]),
	       tc_t_max, tc_t_values[tc_t_max], pf16(tc_t_values[tc_t_max]));


	for (a = F16(TC_T_MIN - 10); a < F16(TC_T_MAX + 10); a+= 0xedc) {
		b = degs_to_mvs(a, tc_t_minmax, tc_t_values, __tc_t_values);
		printf("degs %s     mvs %s\n", pf16(a), pf16(b));
	}

	for (a = tc_t_values[tc_t_min] - 0x1000;
	     a < tc_t_values[tc_t_max] + 0x1000; a += 0x55) {
		b = mvs_to_degs(a, tc_t_minmax, tc_t_values, __tc_t_values);
		printf("mvs %s     degs %s\n", pf16(a), pf16(b));
	}
	}
	return 0;
}

/*

 #test K type w/ gnuplot
 ./tc > /tmp/t; grep '^degs' /tmp/t > /tmp/d;  grep '^mv' /tmp/t > /tmp/m
 plot [-400:1400] [-10:60] '/tmp/d' using 2:4, 'emfk_c-selected.txt' using 1:2
 plot [-400:1400] [-10:60] '/tmp/m' using 4:2, 'emfk_c-selected.txt' using 1:2

 #test T type w/ gnuplot
 ./tc t > /tmp/t; grep '^degs' /tmp/t > /tmp/d;  grep '^mv' /tmp/t > /tmp/m
 plot [-400:450] [-10:25] '/tmp/d' using 2:4, 'emft_c-selected.txt' using 1:2
 plot [-400:450] [-10:25] '/tmp/m' using 4:2, 'emft_c-selected.txt' using 1:2

*/

#endif /* hosted */
