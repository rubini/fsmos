#include <stdint.h>
#include <i2c.h>

#define PCA9535_INPUT0		0x00
#define PCA9535_INPUT1		0x01
#define PCA9535_OUTPUT0		0x02
#define PCA9535_OUTPUT1		0x03
#define PCA9535_INVERT0		0x04
#define PCA9535_INVERT1		0x05
#define PCA9535_DIRIN0		0x06
#define PCA9535_DIRIN1		0x07

struct pca9535 {
	struct i2c_dev *dev;
	int addr7;
	uint16_t inmask;
	uint16_t values;
};

/* depends in i2c device and addr7 to be set */
extern struct pca9535 *pca9535_create(struct pca9535 *pca);
extern int pca9535_set_outmask(struct pca9535 *pca, int outmask, int values);
extern int pca9535_dir(struct pca9535 *pca, int gpio, int output, int value);

/* I/O of cached data */
extern int pca_9535_rd_values(struct pca9535 *pca);
extern int pca_9535_wr_values(struct pca9535 *pca);

/* get and set, based on above ones, w/ error check on arguments */
extern int pca9535_get(struct pca9535 *pca, int gpio);
extern int pca9535_set(struct pca9535 *pca, int gpio, int value);

/* The following calls make no error check nor data transfers */
static inline int pca9535_get_cached(struct pca9535 *pca, int gpio)
{
	return (pca->values >> gpio) & 1;
}

static inline void pca9535_set_cached(struct pca9535 *pca, int gpio, int val)
{
	pca->values &= ~(1 << gpio);
	pca->values |= val << gpio;
}

/* And three aliases fro mgpio use */
int pca9535_mgpio_dir(void *dev, int gpio, int output, int value);
int pca9535_mgpio_get(void *dev, int gpio);
int pca9535_mgpio_set(void *dev, int gpio, int value);
