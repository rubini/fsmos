#include <io.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <command.h>
#include <usb.h>
#include <usb-serial.h>
#include <modbus.h>

/* the host board I use for testing */
#define GPIO_RX_ENAL	GPIO_NR(1,22)
#define GPIO_TX_ENAH	GPIO_NR(0,15)
#define GPIO_LEDY	GPIO_NR(1,31)
#define GPIO_LEDG	GPIO_NR(1,21)

struct usbser usbser = {
	.flags = 0,
}, *us;

/* Override puts -- and never use the uart for diagnostics */
int puts(const char *s)
{
	if (!CONFIG_HAS_USB)
		return console_puts(s);
	if (!memcmp("USB: ", s, 5))
		return 0; /* usb diagnostic message */
	if (us->ud->state != USB_CONFIGURED)
		return 0;
	return usbser_puts(us, s);
}

/*
 * Modbus infrastructure
 */
uint8_t mbm_send_buf[256];
uint8_t mbm_recv_buf[256];

struct modbus_req3 *req3 = (void *)mbm_send_buf;
struct modbus_req6 *req6 = (void *)mbm_send_buf;
struct modbus_req10 *req10 = (void *)mbm_send_buf;
struct modbus_resp34 *resp3 = (void *)mbm_recv_buf;
struct modbus_resp6 *resp6 = (void *)mbm_recv_buf;
struct modbus_resp10 *resp10 = (void *)mbm_recv_buf;

/* This is a synchronous send, and we read back what we write */
static int mbm_send(struct modbus_device *dev, void *msg, int msglen,
		unsigned long when)
{
	uint8_t *s = msg;
	int i;

	while (time_before(jiffies, when))
		;

	gpio_set(GPIO_LEDY, 1);
	gpio_set(GPIO_TX_ENAH, 1);
	for (i = 0; i < msglen; i++) {
		putc(s[i]);
		getc();
	}
	dev->last_byte = jiffies;
	gpio_set(GPIO_TX_ENAH, 0);
	gpio_set(GPIO_LEDY, 0);
	return 0;
}

/* And a synchronous receive */
static int mbm_recv(struct modbus_device *dev, void *msg, int msglen,
		    unsigned long timeout)
{
	uint8_t *s = msg;
	unsigned long last_byte;
	int i;

	int c;

	while ((c = pollc()) < 0) {
		/* first byte used provided timeout */
		if (time_after_eq(jiffies, timeout))
			return -ETIMEDOUT;
	}
	gpio_set(GPIO_LEDY, 1);
	last_byte = jiffies;
	s[0] = c;
	i = 1;
	while (i < msglen) {
		/* read everything up to inter-frame delay */
		timeout = last_byte + dev->inter_frame;
		while ((c = pollc()) < 0) {
			if (time_after_eq(jiffies, timeout)) {
				gpio_set(GPIO_LEDY, 0);
				goto crc;
			}
		}
		last_byte = jiffies;
		s[i++] = c;
	}
	gpio_set(GPIO_LEDY, 0);
	/* end of buffer but still ok (?) */

crc:
	return modbus_crc_check(msg, i, i);
};


struct modbus_device mbm_dev = {
	.send = mbm_send,
	.resp_delay = ms_to_jiffies_const(100),
	.inter_frame = ms_to_jiffies_const(5), /* 9600 baud */
	.recv = mbm_recv,
};

/*
 * Finally, modbus-specific commands, to read and write remote registers
 */

/* modbus write: <addr> <reg> <val> -- decimal values */
static int command_mbw(char **reply, int argc, char **argv)
{
	int addr, reg, val, crc, ret;
	unsigned long timeout;
	char c;

	if (sscanf(argv[1], "%i%c", &addr, &c) != 1)
		return -EINVAL;
	if (addr < 1 || addr > 247)
		return -ERANGE;
	if (sscanf(argv[2], "%i%c", &reg, &c) != 1)
		return -EINVAL;
	if (reg & ~0xffff)
		return -ENXIO;
	if (sscanf(argv[3], "%i%c", &val, &c) != 1)
		return -EINVAL;
	if (val & ~0xffff)
		return -EINVAL;

	req6->h.address = addr;
	req6->h.func = 6;
	req6->reg_name = htons(reg);
	req6->reg_value = htons(val);
	crc = modbus_crc(req6, sizeof(*req6) - sizeof(req6->crc));
	req6->crc = crc; /* little endian */
	mbm_dev.send(&mbm_dev, req6, sizeof(*req6), jiffies);
	timeout = mbm_dev.last_byte + mbm_dev.resp_delay;
	ret = mbm_dev.recv(&mbm_dev, resp6, sizeof(mbm_recv_buf), timeout);
	if (ret < 0)
		sprintf(*reply, "modbus-rx: %s\n", strerror(-ret));
	return ret;
}

/* modbus multiple write: <addr> <reg> <val> [<val> ... ] -- decimal values */
static int command_mbmw(char **reply, int argc, char **argv)
{
	int addr, i, reg, nregs, val, crc, ret;
	unsigned long timeout;
	char c;

	if (sscanf(argv[1], "%i%c", &addr, &c) != 1)
		return -EINVAL;
	if (addr < 1 || addr > 247)
		return -ERANGE;
	if (sscanf(argv[2], "%i%c", &reg, &c) != 1)
		return -EINVAL;
	if (reg & ~0xffff)
		return -ENXIO;
	nregs = argc - 3;


	req10->h.address = addr;
	req10->h.func = 0x10;
	req10->reg_name = htons(reg);
	req10->reg_count = htons(nregs);
	req10->nbyte = 2 * nregs;

	for (i = 3; i < argc; i++) {
		if (sscanf(argv[i], "%i%c", &val, &c) != 1)
			return -EINVAL;
		if (val & ~0xffff)
			return -EINVAL;
		req10->data[i-3] = htons(val);
	}
	crc = modbus_crc(req10, sizeof(*req10) + 2 * nregs);
	req10->data[nregs] = crc; /* little endian */
	mbm_dev.send(&mbm_dev, req10, sizeof(*req10) + 2 * nregs + 2, jiffies);
	timeout = mbm_dev.last_byte + mbm_dev.resp_delay;
	ret = mbm_dev.recv(&mbm_dev, resp10, sizeof(mbm_recv_buf), timeout);
	if (ret < 0)
		sprintf(*reply, "modbus-rx: %s\n", strerror(-ret));
	return ret;
}

/* modbus read: <addr> <reg> [<nregs>] -- decimal values */
static int command_mbr(char **reply, int argc, char **argv)
{
	int addr, reg, nregs = 1, crc, ret, i;
	unsigned long timeout;
	char c;

	if (sscanf(argv[1], "%i%c", &addr, &c) != 1)
		return -EINVAL;
	if (addr < 1 || addr > 247)
		return -ERANGE;
	if (sscanf(argv[2], "%i%c", &reg, &c) != 1)
		return -EINVAL;
	if (argc > 3) {
		if (sscanf(argv[3], "%i%c", &nregs, &c) != 1)
			return -EINVAL;
		if (nregs < 0 || nregs > 100)
			return -EINVAL;
	}
	if (reg & ~0xffff)
		return -ENXIO;
	req3->h.address = addr;
	req3->h.func = 3;
	req3->reg_name = htons(reg);
	req3->nregs = htons(nregs);
	crc = modbus_crc(req3, sizeof(*req3) - sizeof(req3->crc));
	req3->crc = crc; /* little-endian */
	mbm_dev.send(&mbm_dev, req3, sizeof(*req3), jiffies);
	timeout = mbm_dev.last_byte + mbm_dev.resp_delay;
	ret = mbm_dev.recv(&mbm_dev, resp3, sizeof(mbm_recv_buf), timeout);
	if (ret < 0)
		return ret;
	/* print possibly multiple registers */
	printf("%i@%i =", reg, addr);
	for (i = 0; i < resp3->byte_count / 2; i++)
		printf(" %i (%04x)",
		       ntohs(resp3->data[i]), ntohs(resp3->data[i]));
	printf("\n");
	return ret;
}

static struct command rwgo_commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	COMMAND_STACK,
	{"mbr",    command_mbr,      3, 4},
	{"mbw",    command_mbw,      4, 4},
	{"mbmw",   command_mbmw,     4, 8},
	{}
};


void main(void)
{
	static char str[80];

	if (!CONFIG_HAS_USB) {
		panic(1, "No usb, this test can't run\n");
	}
	us = usbser_init(&usbser);
	if (!us)
		panic(15, "Can't init usb serial");

	gpio_dir_af(GPIO_LEDY, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(GPIO_LEDG, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	/* rx enable is on, tx enable is off */
	gpio_dir_af(GPIO_RX_ENAL, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(GPIO_TX_ENAH, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);
	while (1) {
		gpio_set(GPIO_LEDG, (jiffies / (HZ / 2)) & 1);
		if (usbser_gets(us, str, sizeof(str)-1) > 0) {
			command_parse(str, rwgo_commands);
			puts(command_reply);
			str[0] = '\0';
		}
	}
}
