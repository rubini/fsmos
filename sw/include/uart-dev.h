/*
 * This defines the driver data for async uart support (i.e. irq
 * based).  The application must instantiate the data itself, or pick
 * it from platform-specific code, if it provides the data.  Code is
 * platform-specific.
 */
#ifndef __UART_DEV_H__
#define __UART_DEV_H__

struct uart_driver_data {
	uint32_t baseaddr;
	short irqnr;
	short gpio_irqtime;   /* if any, used to track irq time on the scope */
	short gpio_irqtoggle; /* if any, toggle on and off each time */
	short gpio_toggleval; /* keep track, as each irq changes it */
};

#endif /* __UART_DEV_H__ */
